package org.n_is_1._attack_vector;

import org.joda.time.DateTimeZone;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration;

@SpringBootApplication(exclude={
        WebSocketAutoConfiguration.class,
        JmxAutoConfiguration.class})
public class Application {

    public static void main(String[] args) {
        DateTimeZone.setDefault(DateTimeZone.forID("Europe/Amsterdam"));
        SpringApplication.run(Application.class, args);
    }
}
