package org.n_is_1._attack_vector.test;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class MessageListener extends ListenerAdapter
{
    public static final String verikId = "-";
    public static final String jinkeId = "-";

    private JDA jda;

    MessageListener(JDA jda) {
        this.jda = jda;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) {
            System.out.printf("Ignoring bot message\n");
            return;
        }
        if (event.getAuthor().isSystem()) {
            System.out.printf("Ignoring System message\n");
            return;
        }

        if (!event.isFromType(ChannelType.PRIVATE)) {
            System.out.println("event type: " + event.getChannelType());
            return;
        }

        System.out.println("Message type: " + event.getMessage().getType());

        String userId = event.getAuthor().getId();
        if (!verikId.equals(userId) && !jinkeId.equals(userId)) {
            System.out.printf("[Ignored PM] %s <%s>: %s\n", event.getAuthor().getName(), event.getAuthor().getId(),
                    event.getMessage().getContentDisplay());
            return;
        }

        System.out.printf("[Accepted PM] %s <%s>: %s\n", event.getAuthor().getName(), event.getAuthor().getId(),
                event.getMessage().getContentDisplay());

        System.out.printf("ChannelId: %s",
                event.getChannel().getId());





    }

}