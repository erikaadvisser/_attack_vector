package org.n_is_1._attack_vector.model.admin;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Holder class for user roles (Spring security Authority format)
 */
public class UserRole {

    public static final String GM = "gm";
    public static final String HACKER = "hacker";
    public static final String ADMIN = "admin";

    public static final SimpleGrantedAuthority AUTHORITY_ROLE_HACKER = createRole(HACKER);
    public static final SimpleGrantedAuthority AUTHORITY_ROLE_GM = createRole(GM);
    public static final SimpleGrantedAuthority AUTHORITY_ROLE_ADMIN = createRole(ADMIN);


    private static SimpleGrantedAuthority createRole(String name) {
        return new SimpleGrantedAuthority("ROLE_" + name);
    }

}
