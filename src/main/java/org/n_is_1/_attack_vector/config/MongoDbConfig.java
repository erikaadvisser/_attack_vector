package org.n_is_1._attack_vector.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.n_is_1._attack_vector.Application;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Configuration class for Mongo Db
 */
@Configuration
@EnableMongoRepositories(basePackageClasses = Application.class)
public class MongoDbConfig extends AbstractMongoConfiguration {

    private MongoClientURI clientUri;

    public MongoDbConfig() {
        String url = System.getenv("MONGODB_URI");
        if (url == null || url.trim().isEmpty()) {
            url = "mongodb://av2:av2@localhost/admin?authMechanism=SCRAM-SHA-1";
        }
        clientUri = new MongoClientURI(url);
    }



    @Override
    protected String getDatabaseName() {
        String dbName = System.getenv("MONGODB_NAME");
        if (dbName == null || dbName.trim().isEmpty()) {
            dbName = clientUri.getDatabase();
        }
        System.out.println("Using database: " + dbName);
        return dbName;
    }

    @Override
    public Mongo mongo() throws Exception {
//        MongoClientURI uri = new MongoClientURI("mongodb://av2:***@192.168.99.100/av?authMechanism=SCRAM-SHA-1");
        return new MongoClient(clientUri);
    }

    @Override
    protected String getMappingBasePackage() {
        return "org.n_is_1._attack_vector";
    }
}