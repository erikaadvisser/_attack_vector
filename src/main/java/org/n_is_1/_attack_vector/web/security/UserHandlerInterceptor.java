package org.n_is_1._attack_vector.web.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This interceptor adds the currently logged in user to the model & view
 */
public class UserHandlerInterceptor extends HandlerInterceptorAdapter {


    @Override
    public void postHandle(final HttpServletRequest request,
                           final HttpServletResponse response, final Object handler,
                           final ModelAndView modelAndView) throws Exception {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return;
        }

        Object principal = auth.getPrincipal();
        if (principal != null && principal.getClass().getSimpleName().equals("User") && modelAndView != null) {
            modelAndView.getModelMap().addAttribute("myUser", principal);
        }
        else {
            return;
        }
    }
}