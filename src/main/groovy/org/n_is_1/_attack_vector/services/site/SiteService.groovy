package org.n_is_1._attack_vector.services.site

import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteGeneration
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.model.site.SiteType
import org.n_is_1._attack_vector.repo.SiteGenerationRepo
import org.n_is_1._attack_vector.repo.SiteRepo
import org.n_is_1._attack_vector.repo.SiteScanRepo
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.util.IdUtil
import org.n_is_1._attack_vector.services.util.RandomService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

import static org.n_is_1._attack_vector.model.site.SiteType.MISSION

/**
 * Service for storing and retrieving sites
 */
@Service
class SiteService {

    Logger log = LoggerFactory.getLogger(SiteService)

    @Autowired RandomService random
    @Autowired SiteRepo siteRepo
    @Autowired SiteScanRepo siteScanRepo
    @Autowired SiteGenerationRepo siteGenerationRepo
    @Autowired HackingRunService hackingRunService

    @Autowired IceService iceService

    private Map<String, Site> sitesById = [:]
    private Map<String, SiteGeneration> generationsById = [:]
    private Map<String, SiteScan> scansById = [:]

    @PostConstruct
    void init() throws Exception {
        siteRepo.findAll().each{ site ->
            sitesById[site.id] = site
        }
        log.info "Loaded: ${sitesById.size()} sites."

        siteGenerationRepo.findAll().each { generation ->
            generationsById[generation.id] = generation
        }
        log.info "Loaded: ${generationsById.size()} generations."

        siteScanRepo.findAll().each { scan ->
            scansById[scan.id] = scan
        }
        log.info "Loaded: ${scansById.size()} scans."
    }

    Site getSiteById(String siteId) {
        return sitesById[siteId]
    }

    SiteGeneration getGenerationById(String generationId) {
        return generationsById[generationId]
    }

    SiteScan getScanById(String scanId) {
        return scansById[scanId]
    }

    boolean save(Site site) {
        saveWithoutUpdatingGeneration(site)
        site.initServicesById()
        return updateGeneration(site.id)
    }

    void saveWithoutUpdatingGeneration(Site site) {
        sitesById[site.id] = site
        siteRepo.save(site)
    }


    def getActiveList() {
        def list = sitesById.values().findAll { it.active }
        list.sort { a, b -> a.id <=> b.id }
        return list
    }

    def getInActiveList() {
        def list = sitesById.values().findAll { !it.active }
        list.sort { a, b -> a.id <=> b.id }
        return list
    }

    int getInActiveCount() {
        def count = sitesById.values().count { !it.active }
        return count
    }

    void purge() {
        sitesById = [:]
        scansById = [:]
        generationsById = [:]

        siteScanRepo.deleteAll()
        siteGenerationRepo.deleteAll()
        siteRepo.deleteAll()
    }

    SiteGeneration getOrCreateGeneration(String siteId) {
        def existingGeneration = getSiteGenerationBySiteId(siteId)

        if (existingGeneration) {
            return existingGeneration
        }
        updateGeneration(siteId)
        return getSiteGenerationBySiteId(siteId)
    }

    private SiteGeneration getSiteGenerationBySiteId(String siteId) {
        return generationsById.values().find { it.siteId == siteId }
    }

    private boolean updateGeneration(String siteId) {
        def previousGeneration = this.generationsById.values().find { it.siteId == siteId }

        int versionNumber = (previousGeneration) ? previousGeneration.versionNumber + 1 : 1


        def id = IdUtil.createId("gen")
        def site = sitesById[siteId]
        def generation = new SiteGeneration(id: id, siteId: siteId, versionNumber: versionNumber)

        site.nodes.each { node ->
            node.services.each { service ->
                if (service.type.isIce()) {
                    def iceGeneration = iceService.create(service, id)
                    generation.iceGenerationsById[service.id] = iceGeneration
                }
            }
        }

        boolean obsoletedStuff = removeOldGenerations(siteId)

        generationsById[generation.id] = generation
        siteGenerationRepo.save(generation)

        return obsoletedStuff
    }

    boolean removeOldGenerations(String siteId) {
        def generationsToDelete = this.generationsById.values().findAll { it.siteId == siteId }

        boolean obsoletedStuff = false

        generationsToDelete.each {
            if (hackingRunService.deleteRunsForGeneration(it)) {
                obsoletedStuff = true
            }
            if (deleteScansForGeneration(it.id)) {
                obsoletedStuff = true
            }
            this.generationsById.remove(it.id)
            siteGenerationRepo.delete(it)
            log.debug("Removed generation ${it.id} for site ${siteId}")
        }
        return obsoletedStuff
    }

    boolean deleteScansForGeneration(String generationId) {
        def scansToDelete = this.scansById.values().findAll { it.generationId == generationId }
        scansToDelete.each {
            this.scansById.remove(it.id)
            siteScanRepo.delete(it)
            log.debug("Deleted scan ${it.id} for generation ${generationId}.")
        }

        return !scansToDelete.isEmpty()
    }

    SiteScan createScan(SiteGeneration generation) {
        def scan = createScanInternal(generation)
        scansById[scan.id] = scan
        siteScanRepo.save(scan)
        return scan
    }

    private SiteScan createScanInternal(SiteGeneration generation) {
        def id = IdUtil.createId("scan")
        def site = sitesById[generation.siteId]

        Map<String, NodeStatus> nodeStatusById = [:]
        site.nodes.each { node ->
            nodeStatusById[node.id] = NodeStatus.UNDISCOVERED
        }

        def startNode = site.findNodeByNetworkId(site.startNodeNetworkId)
        nodeStatusById[startNode.id] = NodeStatus.DISCOVERED

        def scan = new SiteScan(id: id,
                siteId: site.id,
                generationId: generation.id,
                nodeStatusById: nodeStatusById)


        return scan
    }

    SiteScan createScanForGm(Site site) {
        def generation = getOrCreateGeneration(site.id)

        def scan = createScanInternal(generation)

        site.nodes.each { node ->
            def status = (node.hasIce()) ? NodeStatus.PROTECTED : NodeStatus.FREE
            scan.nodeStatusById[node.id] = status
            scan.connectionScannedNodeIds.add(node.id)
        }

        return scan
    }

    String deactivate(String siteId) {
        def site = sitesById[siteId]
        site.active = false
        site.hackable = false
        save(site)
        return "${site.id} deactivated and unhackable."
    }

    String activate(String siteId) {
        def site = sitesById[siteId]
        site.active = true
        save(site)
        return "${site.id} activated, but it is not hackable yet."
    }

    void delete(String scanId) {
        SiteScan scan = getScanById(scanId)
        scansById.remove(scan.id)
        siteScanRepo.delete(scan)
    }

    List<Site> findAllWithType(SiteType type) {
        return sitesById.values().findAll { it.type == type }
    }

    Site createFromTemplate(Site template, String siteId) {
        Site site = template.deepClone()
        site.id = siteId
        site.name = siteId
        site.type = MISSION
        site.initServicesById()

        return site
    }

    void purgeScans() {
        scansById = [:]
        siteScanRepo.deleteAll()
    }

    void purgeMissionSites() {
        def missionSites = new ArrayList<Site>()
        missionSites.addAll(
                sitesById.values().findAll { it.type == MISSION }
        )

        missionSites.forEach {
            deleteSite(it)
        }
    }

    void deleteSite(Site site) {
        sitesById.remove(site.id)
        siteRepo.delete(site)
    }
}
