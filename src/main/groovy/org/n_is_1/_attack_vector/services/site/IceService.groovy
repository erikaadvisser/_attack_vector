package org.n_is_1._attack_vector.services.site

import org.n_is_1._attack_vector.model.site.IceGeneration
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.services.ice.magiceye.MagicEyeService
import org.n_is_1._attack_vector.services.ice.manual.ManualIceService
import org.n_is_1._attack_vector.services.ice.netwalk.NetwalkIceService
import org.n_is_1._attack_vector.services.ice.passwords.PasswordIceService
import org.n_is_1._attack_vector.services.ice.wordsearch.WordSearchService
import org.n_is_1._attack_vector.services.util.RandomService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * Service for retrieving and rendering ice
 */
@Service
class IceService {

    static final def log = LoggerFactory.getLogger(IceService)

    @Autowired ManualIceService manualIceService
    @Autowired WordSearchService wordSearchService
    @Autowired MagicEyeService magicEyeService
    @Autowired PasswordIceService passwordIceService
    @Autowired NetwalkIceService netwalkIceService
    @Autowired RandomService random
    @Autowired SiteService siteService

    List<String> simpleWords = []
    List<String> exoticWords = []


    @PostConstruct
    void afterPropertiesSet() throws Exception {
        loadList("passcode_simple.txt", simpleWords)
        loadList("passcode_exotic.txt", exoticWords)
    }

    void loadList(String fileName, List<String> container) {
        String location = "ice/${fileName}"
        getClass().getClassLoader().getResourceAsStream(location).eachLine { rawLine ->
            def line = rawLine.trim().toLowerCase()
            if (line) {
                container.add(line)
            }
        }
    }

    def retrieveData(String generationId, String serviceId) {
        def generation = siteService.getGenerationById(generationId)
        return generation.iceGenerationsById[serviceId].iceData
    }

    IceGeneration create(NodeService service, String generationId) {
        def data = createIceData(service)
        String passcode = generatePassCode()

        def generation = new IceGeneration(
                serviceId: service.id,
                generationId: generationId,
                type: service.type,
                passcode: passcode,
                iceData: data)

        return generation
    }

    String generatePassCode() {
        def words = []
        words.add(generateWord(simpleWords))
        words.add(generateWord(simpleWords))
        words.add(generateWord(simpleWords))
        words.add(generateWord(exoticWords))
        Collections.shuffle(words, random.random)
        String passcode = words.join(" ")
        return passcode
    }

    String generateWord(List<String> list) {
        int index = random.nextInt(list.size())
        return list[index]
    }

    def createIceData(NodeService service) {
        switch(service.type) {
            case ServiceType.ICE_WORD_SEARCH: return wordSearchService.createPuzzle(service.data)
            case ServiceType.ICE_MAGIC_EYE: return magicEyeService.create(service.data)
            case ServiceType.ICE_MANUAL: return manualIceService.create(service.data)
            case ServiceType.ICE_PASSWORD: return passwordIceService.createPuzzle(service.data)
            case ServiceType.ICE_NETWALK: return netwalkIceService.generate(service.data)
            case ServiceType.ICE_UNHACKABLE: return [:]
            default: throw new RuntimeException("Unsupported ICE service type ${service.type}")
        }
    }
}
