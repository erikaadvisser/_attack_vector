package org.n_is_1._attack_vector.services.user

import org.n_is_1._attack_vector.model.admin.HackerSpecialization
import org.n_is_1._attack_vector.model.admin.UserType
import org.n_is_1._attack_vector.web.model.UserModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * Providing Spring Security with our custom authentication mechanism
 */
@Service
class DefaultUserDataService {

    @Autowired UserService userService

    @PostConstruct
    void init() {
        def existingUserGm = userService.usersByLoginName["gm"]
        if (!existingUserGm) {
            def gm = new UserModel(
                    loginName: "gm",
                    type: UserType.GM,
                    enabled: true,
                    id: "-1",
                    ocName: "Milan Otten",
                    specialization: HackerSpecialization.NONE,
                    passcode: "22gg88"
            )
            userService.createUser(gm)
        }

        def existingUserAdmin = userService.usersByLoginName["admin"]
        if (!existingUserAdmin) {
            def admin = new UserModel(
                    loginName: "admin",
                    type: UserType.ADMIN,
                    enabled: true,
                    id: "-1",
                    ocName: "Mirthe van Wegberg",
                    passcode: "77aa44"
            )
            userService.createUser(admin)
        }

        def existingUserHacker

//        existingUserHacker = userService.usersByLoginName["invictus"]
//        if (!existingUserHacker) {
//            def hacker = new UserModel(
//                    loginName: "invictus",
//                    type: UserType.HACKER,
//                    enabled: true,
//                    ocName: "Berend van Dam",
//                    icName: "Victus",
//                    skill_it_main: 10,
//                    specialization: HackerSpecialization.ARCHITECT,
//                    passcode: "jj44kk"
//            )
//            userService.createUser(hacker)
//        }
//
//        existingUserHacker = userService.usersByLoginName["turien"]
//        if (!existingUserHacker) {
//            def hacker = new UserModel(
//                    loginName: "turien",
//                    type: UserType.HACKER,
//                    enabled: true,
//                    ocName: "Janne Kooistra",
//                    icName: "Bituin",
//                    skill_it_main: 7,
//                    specialization: HackerSpecialization.ELITE,
//                    passcode: "ll88dd"
//            )
//            userService.createUser(hacker)
//        }
//
//        existingUserHacker = userService.usersByLoginName["danama"]
//        if (!existingUserHacker) {
//            def hacker = new UserModel(
//                    loginName: "danama",
//                    type: UserType.HACKER,
//                    enabled: true,
//                    ocName: "Gijs van Olde B.",
//                    icName: "Maati",
//                    skill_it_main: 8,
//                    specialization: HackerSpecialization.ARCHITECT,
//                    passcode: "qq77zz"
//            )
//            userService.createUser(hacker)
//        }
//
//        existingUserHacker = userService.usersByLoginName["black/3"]
//        if (!existingUserHacker) {
//            def hacker = new UserModel(
//                    loginName: "black/3",
//                    type: UserType.HACKER,
//                    enabled: true,
//                    ocName: "Lucy Sanders",
//                    icName: "Kodama",
//                    skill_it_main: 3,
//                    specialization: HackerSpecialization.NONE,
//                    passcode: "99tt44"
//            )
//            userService.createUser(hacker)
//        }
//
//        existingUserHacker = userService.usersByLoginName["boom"]
//        if (!existingUserHacker) {
//            def hacker = new UserModel(
//                    loginName: "boom",
//                    type: UserType.HACKER,
//                    enabled: true,
//                    ocName: "Verik",
//                    icName: "Hryar",
//                    skill_it_main: 1,
//                    specialization: HackerSpecialization.NONE,
//                    passcode: "33gg55"
//            )
//            userService.createUser(hacker)
//        }
    }
}
