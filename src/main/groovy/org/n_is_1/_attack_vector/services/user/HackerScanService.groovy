package org.n_is_1._attack_vector.services.user

import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.repo.HackerScanLinkRepo
import org.n_is_1._attack_vector.model.hacker.HackerScanLink
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.util.IdUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * Manages hackers
 */
@Service
class HackerScanService {

    Logger log = LoggerFactory.getLogger(HackerScanService)

    @Autowired HackerScanLinkRepo repo
    @Autowired UserService userService
    @Autowired SiteService siteService

    List<HackerScanLink> hackerScanLinks = []

    @PostConstruct
    void init() {
        repo.findAll().each { link ->
            hackerScanLinks.add(link)
        }
        log.info "Loaded: ${hackerScanLinks.size()} scans links."
    }
//
//    List<User> list() {
//        List<User> list = []
//        list.addAll(userService.usersById.values())
//
//        list.sort { a, b -> a.loginName.toLowerCase() <=> b.loginName.toLowerCase() }
//        return list
//    }

    /** Add scan link if the user did not yet have this scan */
    void addScan(User user, SiteScan scan) {
        def existingLink =  findLink(user, scan.id)
        if (!existingLink) {
            String id = IdUtil.createId("link")
            def link = new HackerScanLink(id: id , userId: user.id, scanId: scan.id, siteId: scan.siteId)
            hackerScanLinks.add(link)
            repo.save(link)
        }
    }

    boolean removeScan(User user, String scanId) {
        def link = findLink(user, scanId)
        if (!link) {
            return false
        }

        hackerScanLinks.remove(link)
        repo.delete(link)
        if (hackerScanLinks.count { it.scanId == scanId } == 0) {
            if (siteService.getScanById(scanId)) {
                siteService.delete(scanId)
            }
        }
        return true
    }

    List<SiteScan> scansForHacker(User user) {
        def links = hackerScanLinks.findAll { it.userId == user.id }
        def scans = links.collect {
            def scan = siteService.getScanById(it.scanId)
            if (scan) {
                return scan
            }
            if (it.siteId) {
                return new SiteScan(id: it.scanId, siteId: it.siteId )
            }
            return new SiteScan(id: it.scanId, siteId: "<unknown>" )
        }
        return scans
    }

    HackerScanLink findLink(User user, String scanId) {
        return  hackerScanLinks.find{ it.userId == user.id && it.scanId == scanId }
    }

    void purge() {
        hackerScanLinks = []
        repo.deleteAll()
    }

    void purgeScansAndLinks() {
        purge()
        siteService.purgeScans()
    }
}


