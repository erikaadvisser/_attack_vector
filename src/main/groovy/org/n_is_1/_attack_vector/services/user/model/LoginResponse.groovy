package org.n_is_1._attack_vector.services.user.model
/**
 * Holder class to contain result of login
 */
class LoginResponse {
    boolean success
    String message

    LoginResponse(String message) {
        this.success = false
        this.message = message
    }

    LoginResponse(boolean success, String message) {
        this.success = success
        this.message = message
    }
}
