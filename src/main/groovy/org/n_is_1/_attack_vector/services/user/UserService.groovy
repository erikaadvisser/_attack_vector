package org.n_is_1._attack_vector.services.user

import org.joda.time.DateTime
import org.joda.time.Minutes
import org.joda.time.Seconds
import org.n_is_1._attack_vector.model.admin.HackerSpecialization
import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.admin.UserRole
import org.n_is_1._attack_vector.model.admin.UserType
import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.repo.UserRepo
import org.n_is_1._attack_vector.services.DebugService
import org.n_is_1._attack_vector.services.user.model.LoginResponse
import org.n_is_1._attack_vector.services.util.IdUtil
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.UserUtil
import org.n_is_1._attack_vector.web.model.UserModel
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError

import javax.annotation.PostConstruct
import java.nio.charset.StandardCharsets
import java.security.MessageDigest

/**
 * Service for managing users, primarily creating and retrieving them.
 */
@Service
class UserService {


    static final def log = LoggerFactory.getLogger(UserService)

    static MessageDigest sha256 = MessageDigest.getInstance("SHA-256");

    @Autowired UserRepo userRepo
    @Autowired LogService logService
    @Autowired DebugService debugService

    Map<String, User> usersByLoginName = [:]
    Map<String, User> usersById = [:]

    @PostConstruct
    void init() throws Exception {
        userRepo.findAll().each { user ->
            usersByLoginName[user.loginName.toLowerCase()] = user
            usersById[user.id] = user
        }
        log.info "Loaded: ${usersById.size()} users."
    }

    LoginResponse login(String loginName, String passcode) {
        if (!passcode) {
            return new LoginResponse("Please enter a passcode")
        }

        def user = usersByLoginName[loginName.toLowerCase()]
        if (!user) {
            return new LoginResponse("Login or passcode invalid")
        }
        if (user.blocked) {
            if (new DateTime(user.blockedUntil).isBefore(DateTime.now())) {
                user.blocked = false
                userRepo.save(user)
            }
            else {
                def blockDuration = getRemainingBlockedDuration(user)
                return new LoginResponse("Login blocked for another ${blockDuration}")
            }
        }


        String passcodeHash = createHash(passcode, user.salt)
        if (user.passcodeHash == passcodeHash) {
            return loginSuccess(user)
        }

        user.failedLoginCount += 1
        userRepo.save(user)

        if ( user.failedLoginCount > 3) {
            int surplus = user.failedLoginCount - 3
            int blockTimes = Math.pow(2, (surplus-1))
            int blockSeconds = 15 * blockTimes
            def blockedUntil = DateTime.now().plusSeconds(blockSeconds)
            user.blocked = true
            user.blockedUntil = blockedUntil.getMillis()
            userRepo.save(user)


            def blockDuration = getRemainingBlockedDuration(user)
            logService.event(LogAction.LOGIN, "Login for ${user.loginName} blocked (too many failed logins) for ${blockDuration}")
            return new LoginResponse("Login blocked (too many failed logins) for ${blockDuration}")
        }
        return new LoginResponse("Login or passcode invalid")
    }

    LoginResponse loginSuccess(User user) {
        String message = ""

        if (user.failedLoginCount > 0) {
            def fails = user.failedLoginCount
            user.failedLoginCount = 0
            userRepo.save(user)
            message =  "First successful login after ${fails} failed logins."
        }

        def authorities = grantAuthorities(user)
        def authentication = new UsernamePasswordAuthenticationToken(user, null, authorities)
        SecurityContextHolder.getContext().setAuthentication(authentication);

        logService.action(LogAction.LOGIN, "${user.ocName} logged in as ${user.loginName}")

        return new LoginResponse(true, message)
    }

    static def getRemainingBlockedDuration(User user) {
        def now = DateTime.now()
        def blocked = new DateTime(user.blockedUntil).plusMillis(998)

        def seconds = Seconds.secondsBetween(now, blocked).getSeconds()
        def minutes = Minutes.minutesBetween(now, blocked).getMinutes()
        seconds -= minutes * 60
        return (minutes > 0) ? "${minutes} minutes and ${seconds} seconds" : "${seconds} seconds"
    }


    String createHash(String passcode, String salt) {
        String total = passcode + salt
        def bytesToHash = total.getBytes(StandardCharsets.UTF_8)
        def hashBytes = sha256.digest(bytesToHash)

        def sb = new StringBuilder()
        for (byte b : hashBytes) {
            sb.append(String.format("%02X", b))
        }
        return sb.toString()

    }

    User createUser(UserModel model) {
        String id = IdUtil.createId("user")

        def user = new User(id, model)
        updatePasscode(user, model.passcode)
        userRepo.save(user)

        usersByLoginName[user.loginName.toLowerCase()] = user
        usersById[user.id] = user
    }

    void updateUser(UserModel model) {
        def user = usersById[model.id]
        usersByLoginName.remove(user.loginName.toLowerCase())
        user.update(model)
        if (model.passcode) {
            updatePasscode(user, model.passcode)
        }
        usersByLoginName[user.loginName.toLowerCase()] = user

        userRepo.save(user)
    }
    def changePasscode(User user, String passcode) {
        updatePasscode(user, passcode)
        userRepo.save(user)
    }

    private void updatePasscode(User user, String passcode) {
        String salt = IdUtil.createId("salt")
        String passcodeHash = createHash(passcode, salt)
        user.salt = salt
        user.passcodeHash = passcodeHash
    }

    void purge() {
        usersById = [:]
        usersByLoginName = [:]
        userRepo.deleteAll()
    }

    List<User> list() {
        List<User> list = []
        list.addAll(usersById.values())

        list.sort { a, b -> a.loginName.toLowerCase() <=> b.loginName.toLowerCase() }.sort{ a,b -> a.type.toString() <=> b.type.toString() }
        return list
    }

    String validate(UserModel user, BindingResult bindingResult, boolean passcodeMandatory) {
        if (bindingResult.hasErrors()) {
            def error = bindingResult.getAllErrors()[0]
            if (error instanceof FieldError) {
                def fieldError = (FieldError)error
                def uiFieldName = translateFieldName(fieldError.field)
                return "'${uiFieldName}' ${error.defaultMessage}"
            }
            else {
                return error.defaultMessage
            }
        }

        if (passcodeMandatory) {
            if (!user.passcode) {
                return "'Passcode' mandatory (8+ characters)"
            }
            if (user.passcode.length() < 8) {
                return "'Passcode' size must be 8+ characters"
            }
        }

        if (user.type == UserType.HACKER) {
            if (!user.icName) {
                return "'Character name' mandatory for Hackers"
            }
            if (!user.skill_it_main) {
                return "'Skill IT' mandatory for Hackers"
            }
        }
        if (user.skill_it_main >= 6 && (user.specialization == null || user.specialization == HackerSpecialization.NONE)) {
            return "Choose a specialization"
        }
        if (user.skill_it_main <= 5 && (user.specialization != HackerSpecialization.NONE)) {
            return "'Skill IT' too low for specialization."
        }

        def existingUser = usersByLoginName[user.loginName.toLowerCase()]
        if (existingUser && existingUser.id != user.id ) {
            return "A user already exists with this name"
        }
        return null
    }

    Object translateFieldName(String input) {
        switch(input) {
            case "loginName": return "User name"
            case "ocName": return "OC name"
            case "passcode": return "Passcode"
            case "icName": return "Character name"
            case "skill_it_main": return "Skill IT"
        }
    }

    List<GrantedAuthority> grantAuthorities(User user) {
        def authorities = []

        if (user.type == UserType.HACKER) {
            authorities.add(UserRole.AUTHORITY_ROLE_HACKER)
        }
        if ( user.type == UserType.GM) {
            authorities.add(UserRole.AUTHORITY_ROLE_GM)
        }
        if (user.type == UserType.ADMIN) {
            authorities.add(UserRole.AUTHORITY_ROLE_HACKER)
            authorities.add(UserRole.AUTHORITY_ROLE_GM)
            authorities.add(UserRole.AUTHORITY_ROLE_ADMIN)
        }

        return authorities
    }

    String unblock(String userId) {
        def user = usersById[userId]
        user.blocked = false
        user.failedLoginCount = 0
        user.blockedUntil = 0
        userRepo.save(user)
        return "${user.loginName} unblocked, failed login count reset"
    }

    User findUserById(String userId) {
        return usersById[userId]
    }

    /** Development function, allows login without password. Only works if environment is dev. */
    LoginResponse directLogin(String loginName) {
        def currentUser = UserUtil.getUser()
        def loginAs = (currentUser?.type == UserType.GM || currentUser?.type == UserType.ADMIN)

        if (!debugService.debug() && !loginAs) {
            log.error("Cannot use direct login.")
            return new LoginResponse(false, "Log in as GM or Admin first before attempting direct login.")
        }

        def user = usersByLoginName[loginName.toLowerCase()]
        if (!user) {
            log.error("User not found for direct login: ${loginName}")
            return new LoginResponse(false, "User not found for direct login: ${loginName}.")
        }

        if (loginAs) {
            logService.gm(LogAction.GM_LOGIN_AS, "${currentUser.ocName} logged in as someone else: ${user.ocName} | ${user.loginName}")
        }
        else {
            logService.gm(LogAction.DEV_LOGIN, "Development action: direct login as: ${user.ocName} | ${user.loginName}")
        }

        def loginResult = loginSuccess(user)

        loginResult.message = "Direct login as ${loginName}"
        return loginResult
    }

    String validateForSignUp(UserModel model, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            def error = bindingResult.getAllErrors()[0]
            if (error instanceof FieldError) {
                def fieldError = (FieldError)error
                def uiFieldName = translateFieldName(fieldError.field)
                return "'${uiFieldName}' ${error.defaultMessage}"
            }
            else {
                return error.defaultMessage
            }
        }

        if (usersByLoginName[model.loginName.toLowerCase()]) {
            return "A user already exists with this name"
        }

        if (model.passcode.length() < 8) {
            return "'Passcode' size must be 8+ characters"
        }
        return null
    }

    def signUpUser(UserModel model) {
        def user = createUser(model)
        user.loginName = user.loginName.toLowerCase()
        user.skill_it_main = 6
        user.specialization = HackerSpecialization.ELITE
        user.type = UserType.HACKER
        userRepo.save(user)
        loginSuccess(user)
    }

    void loginBeacon() {
        def user = usersByLoginName["gm"]

        loginSuccess(user)
    }
}
