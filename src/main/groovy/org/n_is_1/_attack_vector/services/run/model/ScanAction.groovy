package org.n_is_1._attack_vector.services.run.model

/**
 * The actions a scan probe can do
 */
enum ScanAction {
    MOVE,              // no action, just passing to a target
    SCAN_CONNECTIONS,  // scan the node to find the network connections it has
    SCAN_NODE,         // scan the node to find out more about it
    ANALYZE,           // script: analyze ice layer to reveal puzzle
    SNOOP              // script: analyze syscon, passcode vault or local core to reveal spoils
}