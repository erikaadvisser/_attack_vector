package org.n_is_1._attack_vector.services.run

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.repo.SiteScanRepo
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.run.model.ScanProbe
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.terminal.CommandRunService
import org.n_is_1._attack_vector.services.terminal.CommandViewService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.UserUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service for handling scanning of sites
 */
@Service
class ScanService {

    Logger log = LoggerFactory.getLogger(ScanService)

    @Autowired SiteScanRepo scanRepo
    @Autowired SiteService siteService
    @Autowired HackingRunService attackService
    @Autowired CommandViewService commandViewService
    @Autowired LogService logService
    @Autowired CommandRunService commandRunService
    @Autowired MissionService missionService


    TerminalResponse handleCommand(String scanId, String command) {
        try {
            def scan = siteService.getScanById(scanId)
            if (!scan) {
                return new TerminalResponse(message: "Connection unstable, re-establish scan. (Possible security update in progress).")
            }

            def site = siteService.getSiteById(scan.siteId)

            if (!site.hackable) {
                logService.highlight(LogAction.HACKING_RUN_DENIED, "Denied hacking of  ${site.id} as hacking is not currently allowed", site.id, scanId,)
                return new TerminalResponse(message: "The site can currently not be hacked. Ask an SL to enable this site for hacking.")
            }

            if (site.missionId != null) {
                if (missionService.hasMissionEnded(site.missionId)) {
                    logService.highlight(LogAction.HACKING_RUN_DENIED, "Denied hacking of  ${site.id} as the mission has expired", site.id, scanId,)
                    return new TerminalResponse(message: "Connection refused, active protection detected. Involvement of architect suspected.")
                }
            }


            command = command.toLowerCase()

            if (command.startsWith("help")) {
                return handleHelp()
            }
            if (command.startsWith("attack")) {
                return handleAttack(site, scanId)
            }
            if (command.startsWith("scan")) {
                return handleScan(site, scan)
            }
            if (command.startsWith("run")) {
                return commandRunService.run(command, site, scan)
            }

            return new TerminalResponse(message: "Command not supported. Try 'help'.")
        }
        catch (Exception exception) {
            log.error("Command execution failed due to", exception)
            logService.error("Command execution failed due to  ${exception.stackTrace}")
            return new TerminalResponse(message: "∻23 ⇥ Resolve failed, ((OC problem))\n\n(details: ${exception.getMessage()}))")
        }

    }

    TerminalResponse handleHelp() {
        return new TerminalResponse(message: "Commands:\nscan\nattack\n");
    }

    TerminalResponse handleAttack(Site site, String scanId) {
        if (!site) {
            return new TerminalResponse(message: "Cannot start run, target invalid.")
        }

        if (!site.hackable) {
            logService.highlight(LogAction.HACKING_RUN_DENIED, "Denied hacking of  ${site.id} as hacking is not currently allowed", site.id, scanId,)
            return new TerminalResponse(message: "The site can currently not be hacked. Ask an SL to enable this hack.")
        }

        HackingRun run = attackService.startRun(site, scanId)
        return new TerminalResponse(message: "start", type: "ATTACK", payload: run.runId)
    }

    TerminalResponse handleScan(Site site, SiteScan scan) {
        def user = UserUtil.getUser()
        if (user.skill_it_main < 3) {
            return new TerminalResponse(message: "Scan probe rejected by site (IT skill too low).")
        }

        def probe = new ScanProbe(scan, site)
        boolean scanComplete = probe.scanComplete()

        if (scanComplete) {
            scan.scanComplete = true
            scanRepo.save(scan)
            logService.event(LogAction.SCAN_COMPLETED, "Scan completed for ${site.id}.", site.id, scan.id, scan.generationId)
            return new TerminalResponse(message: "\nScan progress 100%\nScan complete." )
        }
        else {
            probe.moveAndScan()
            logService.action(LogAction.SCAN_COMMAND, "scanning probe launched to ${probe.targetNode.services[0].data['networkId']}.", site.id, scan.id, scan.generationId)
            return new TerminalResponse(message: " ", type: "SCAN", payload: probe.moves )
        }
    }

    NodeStatus nextProbeStatus(SiteNode node, NodeStatus status) {
        if (status == NodeStatus.DISCOVERED) {
            return NodeStatus.PROBED;
        }
        if (status == NodeStatus.PROBED) {
            return (node.hasIce()) ? NodeStatus.PROTECTED : NodeStatus.FREE;
        }
        throw new RuntimeException("Trying to probe a node which status is already known: ${node.id}")
    }

    def handleScanConnections(String scanId, String nodeId) {
        def scan = siteService.getScanById(scanId)
        def site = siteService.getSiteById(scan.siteId)
        def probeNode = site.findNodeById(nodeId)
        List<String> nodesDiscovered = []

        List<String> connectedNodes = site.findConnectedNodeIds(probeNode)
        connectedNodes.each { connectedNodeId ->
            def node = site.findNodeById(connectedNodeId)
            if (scan.nodeStatusById[node.id] == NodeStatus.UNDISCOVERED) {
                nodesDiscovered.add(connectedNodeId)
                scan.nodeStatusById[node.id] = NodeStatus.DISCOVERED
            }
        }
        scan.connectionScannedNodeIds.add(nodeId)

        List<String> connectionsDiscovered = determineAdditionalConnections(nodesDiscovered, site)

        scanRepo.save(scan)

        if (nodesDiscovered.isEmpty()) {
            return [ type: "NO_NODES_DISCOVERED" ]
        }
        else {
            return [ type: "NODES_DISCOVERED",
                     payload: [ nodes: nodesDiscovered, connections: connectionsDiscovered] ]
        }

    }

    List<Object> determineAdditionalConnections(List<String> nodeIds, Site site) {
        List<Object> additionalConnections = []

        nodeIds.each { nodeId ->
            SiteNode node = site.findNodeById(nodeId)
            node.connections.each { targetNodeId ->
                if (nodeIds.contains(targetNodeId)) {
                    def connection = [from: nodeId, to: targetNodeId]
                    additionalConnections.add(connection)
                }
            }
        }
        return additionalConnections
    }

    def handleScanNode(String scanId, String nodeId) {
        def scan = siteService.getScanById(scanId)
        def site = siteService.getSiteById(scan.siteId)
        def node = site.findNodeById(nodeId)

        def newNodeStatus = nextProbeStatus(node, scan.nodeStatusById[node.id])

        String message = determineNodeUpdateMessage(node, newNodeStatus)

        scan.nodeStatusById[node.id] = newNodeStatus;
        scanRepo.save(scan)

        return [ type: "NODE_UPDATE",
                 status: newNodeStatus,
                 message: message]
    }

    String determineNodeUpdateMessage(SiteNode node, NodeStatus status) {
        if (status == NodeStatus.PROBED) {
            return "Node probed, services: ${node.services.size()}"
        }

        def message = new StringBuilder()
        def viewMessage = commandViewService.viewMessage(node, [], true)

        message.append("Node Analysis:\n")
        message.append(viewMessage)

        return message.toString()
    }

    void handleAnalyze(String scanId, String serviceId) {
        def scan = siteService.getScanById(scanId)
        scan.analyzedServiceIds.add(serviceId)
        scanRepo.save(scan)

        logService.event(LogAction.SCAN_ANALYZE, "Script/analysis run on service ${serviceId} of '${scan.siteId}'", scan.siteId, scan.id, serviceId)
    }

    String handleSnoop(String scanId, String serviceId) {
        def scan = siteService.getScanById(scanId)
        scan.analyzedServiceIds.add(serviceId)
        scanRepo.save(scan)

        logService.event(LogAction.SCAN_ANALYZE, "Script/snoop run on service ${serviceId} of '${scan.siteId}'", scan.siteId, scan.id, serviceId)

        def site = siteService.getSiteById(scan.siteId)
        def service = site.findServiceById(serviceId)

        String info = service.snoopInfo(site)
        return "${service.scanName()} snooped. ${info}"
    }
}
