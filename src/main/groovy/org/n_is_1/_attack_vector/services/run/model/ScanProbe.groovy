package org.n_is_1._attack_vector.services.run.model

import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteScan

/**
 * A scan probe is the server representation of the scan probe. It is used to virtually travel the connections between
 * the nodes to find the path the 'real' probe will traverse.
 */
class ScanProbe {

    SiteScan scan
    Site site

    SiteNode startNode
    SiteNode targetNode
    List<SiteNode> currentKnownNodes

    List<ScanMove> moves = []
    boolean deepScan
    int minScanLevel

    /** this is the set of connections we have not yet traveled, used to find a unique path that does not loop */
    Map<SiteNode, List<SiteNode>> connections = [:]

    ScanProbe(SiteScan scan, Site site) {
        this.scan = scan
        this.site = site

        site.nodes.each { node ->
            if (scan.nodeStatusById[node.id].level >= NodeStatus.DISCOVERED.level ) {
                def connectedNodeIds = site.findConnectedNodeIds(node)
                def knownConnectedNodeIds = connectedNodeIds.findAll { scan.nodeStatusById[it].level >= NodeStatus.DISCOVERED.level }
                def connectedNodes = knownConnectedNodeIds.collect { site.findNodeById(it) }
                connections[node] = connectedNodes
            }
        }

        minScanLevel = (scan.nodeStatusById.values().collect{ it.level}).min()
        deepScan =  (minScanLevel >= NodeStatus.PROBED.level ) && (scan.connectionScannedNodeIds.size() == site.nodes.size())
    }

    boolean scanComplete() {
        return  (minScanLevel > NodeStatus.PROBED.level)
    }

    void moveAndScan() {
        currentKnownNodes = findCurrentKnownNodes()
        targetNode = pickTargetNode()

        moveToTarget()
        moves.last().action = determinAction()
        this.moves = this.moves.reverse()
    }


    void moveToTarget() {
        startNode = site.findNodeById(site.startNodeId)

        def initialPath = new ProbePath(startNode, [], connections)
        List<ProbePath> paths = [initialPath]
        while(true) {
            List<ProbePath> nextPaths = []
            for (def path: paths) {
                if (path.currentNode == targetNode) {
                    programProbeMoves(path)
                    return
                }
                def nextPathsForThisPath = path.proceed()
                nextPaths.addAll( nextPathsForThisPath )
            }

            paths = nextPaths
        }
    }

    void programProbeMoves(ProbePath path) {
        path.visitedNodes.each { node ->
            def move = new ScanMove(
                    nodeId: node.id,
                    waitDeciSeconds: 0,
                    action: ScanAction.MOVE
            )
            moves.add(move)
        }
    }

    List<SiteNode> findCurrentKnownNodes() {
        def nodeIds = scan.nodeStatusById.findAll {
            it.value.level >= NodeStatus.DISCOVERED.level
        }
        return nodeIds.keySet().collect { site.findNodeById(it) }
    }

    /** Find node with lowest scan status level */
    SiteNode pickTargetNode() {
        int lowestLevel = NodeStatus.HACKED.level + 1
        SiteNode currentTarget = null

        currentKnownNodes.each { node ->
            int level = scan.nodeStatusById[node.id].level

            if (scan.connectionScannedNodeIds.contains(node.id)) {
                level = level + 1
            }

            if ( level < lowestLevel) {
                lowestLevel = level
                currentTarget = node
            }
        }

        return currentTarget
    }

    ScanAction determinAction() {
        def status = scan.nodeStatusById[targetNode.id]

        if (status == NodeStatus.DISCOVERED) {
            return ScanAction.SCAN_NODE
        }
        if (status == NodeStatus.PROBED) {
            if (deepScan) {
                return ScanAction.SCAN_NODE
            }
            if (!scan.connectionScannedNodeIds.contains(targetNode.id)) {
                return ScanAction.SCAN_CONNECTIONS
            }
        }

        throw new RuntimeException("No scan action available at target node")
    }

    def programForScript(SiteNode targetNode, NodeService targetService, ScanAction scanAction) {
        this.targetNode = targetNode
        moveToTarget()

        moves.last().action = scanAction
        moves.last().serviceId = targetService.id
        this.moves = this.moves.reverse()
    }
}
