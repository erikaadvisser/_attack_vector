package org.n_is_1._attack_vector.services.run.model

import org.n_is_1._attack_vector.model.site.SiteNode

/**
 * This class represents a virtual probe path, a possible path the probe can travel. Used to find the shortest
 * destination to the target node.
 */
class ProbePath {
    List<SiteNode> visitedNodes

    SiteNode currentNode

    Map<SiteNode, List<SiteNode>> connections

    ProbePath(SiteNode currentNode, List<SiteNode> visitedNodes, Map<SiteNode, List<SiteNode>> connections) {
        this.currentNode = currentNode
        this.connections = connections
        this.visitedNodes = visitedNodes.clone()
        this.visitedNodes.add(currentNode)
    }

    /** Move from the current node to any nodes unvisited. Each move yields a new path */
    List<ProbePath> proceed() {
        List<ProbePath> next = []
        def possibleTargets = connections[currentNode]

        possibleTargets.each { targetNode ->
            if (!visitedNodes.contains(targetNode)) {
                def newPath = new ProbePath(targetNode, visitedNodes, connections)
                next.add(newPath)
            }
        }
        return next
    }
}
