package org.n_is_1._attack_vector.services.run

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingMission
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteGeneration
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteType
import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.repo.MissionRepo
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.util.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service for starting a run.
 */
@Service
class HackingRunService {

    final Logger log = LoggerFactory.getLogger(HackingRunService)

    @Autowired RandomService random
    @Autowired SiteService siteService
    @Autowired LogService logService
    @Autowired MissionService missionService


    def timeParseUtil = new TimeParseUtil()

    Map<String, HackingRun> runsById = [:]

    Map<String, HackingRun> runsByHackerId = [:]

    HackingRun startRun(Site site, String scanId) {
        // check early!
        timeParseUtil.parseToMillis(site.hackTime, "Trace time invalid, ")

        def mission = missionService.getById(site.missionId)
        def user = UserUtil.getUser()
        def scan = siteService.getScanById(scanId)
        def generation = siteService.getGenerationById(scan.generationId)
        def startNode = site.findNodeByNetworkId(site.startNodeNetworkId)

        String runId = generateRunId()
        def run = new HackingRun(
                runId: runId,
                site: site,
                currentNode: startNode,
                hackerId: user.id,
                generation: generation,
                scan: scan,
                mission: mission,
                nodeStatusById: scan.nodeStatusById.clone())
        runsById[runId] = run

        if (mission != null) {
            missionService.updateMissionForRun(mission, site, user)
        }

        logService.highlightForRun(LogAction.HACKING_RUN_START, "New run started on ${site.id}", run)
        log.info("Run started against ${site.name} by ${user.loginName} [${runId}].")

        def existingRun = runsByHackerId[user.id]
        if (existingRun != null) {
            existingRun.active = false
        }

        runsByHackerId[user.id] = run

        return run
    }

    HackingRun startClock(String runId) {
        def run = runsById[runId]

        long runStart = System.currentTimeMillis()
        long hackTime = timeParseUtil.parseToMillis(run.site.hackTime, "Trace time invalid, ")
        long bonusSeconds = 1000 * random.nextInt(12)

        long traceEnd = runStart + hackTime + bonusSeconds
        if (run.mission != null) {
            traceEnd = Math.min(traceEnd, run.mission.endTimeStamp)
        }

        run.runStart = runStart
        run.traceEnd = traceEnd

        if (traceEnd < runStart) {
            throw new UserInputException("Secure shutdown detected")
        }

        return run
    }

    HackingRun getRun(String runId) {
        return runsById[runId]
    }

    String generateRunId() {
        return IdUtil.createId("run")
    }

    String hack(String runId, String serviceId) {
        def run = getRun(runId)
        def service = run.site.findServiceById(serviceId)
        if (!serviceId) {
            throw new RuntimeException("Service ${serviceId} not found in run ${runId}")
        }
        run.hackedServices.add(service)
        log.debug "${serviceId} is hacked"

        SiteNode node = run.site.findNodeByServiceId(serviceId)
        boolean allIceHacked = isAllIceHacked(node, run)
        if (allIceHacked) {
            run.nodeStatusById[node.id] = NodeStatus.HACKED
        }
        return (allIceHacked) ? node.id : null
    }

    private boolean isAllIceHacked(SiteNode node, HackingRun run) {
        return node.services.every { (!it.type.isIce()) || run.hackedServices.contains(it) }
    }

    NodeStatus updateNodeStatus(String runId, String nodeId) {
        def newStatus = determineNodeStatus(runId, nodeId);
        def run = getRun(runId)
        run.nodeStatusById[nodeId] = newStatus
        return newStatus
    }

    NodeStatus determineNodeStatus(String runId, String nodeId) {
        def run = getRun(runId)
        def node = run.site.findNodeById(nodeId)
        return determineNodeStatus(run, node)
    }

    NodeStatus determineNodeStatus(HackingRun run, SiteNode node) {
        if (!node.hasIce()) {
            return NodeStatus.FREE;
        }
        def allIceHacked = isAllIceHacked(node, run)
        return (allIceHacked) ? NodeStatus.HACKED : NodeStatus.PROTECTED
    }

    void detectedTimeOut(String runId) {
        def run = getRun(runId)
        logService.highlightForRun(LogAction.HACKING_RUN_DETECTED_TIMEOUT, "Hacker detected, time ran out in '${run.site.id}'", run)
    }

    boolean deleteRunsForGeneration(SiteGeneration generation) {
        def runIdsToDelete = this.runsById.values()
                .findAll { it.generation.id == generation.id }

        runIdsToDelete.each {
            this.runsById.remove(it.runId)
            log.debug("Deleted hacking run ${it.runId} for generation ${generation.id}")
        }
        return !runIdsToDelete.isEmpty()
    }

    List<HackingRun> activeRuns() {
        long now = DateTime.now().millis
        return runsById.values().findAll{ it.traceEnd > now }
    }

    List<HackingRun> activeRunsForMission(HackingMission mission) {
        long now = DateTime.now().millis
        return runsById.values().findAll{ it.traceEnd > now  && it.mission == mission }
    }

    void purge() {
        runsById = [:]
    }

    void disconnect(HackingRun run) {
        run.traceEnd == DateTime.now().millis - 1;
    }
}
