package org.n_is_1._attack_vector.services.run.model

/**
 * A single move of a scanning probe
 */
class ScanMove {

    String nodeId
    int waitDeciSeconds
    boolean scan
    ScanAction action
    String serviceId
}
