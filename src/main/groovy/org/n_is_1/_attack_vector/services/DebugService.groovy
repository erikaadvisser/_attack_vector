package org.n_is_1._attack_vector.services

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class DebugService {

    @Value("#{systemEnvironment['ENVIRONMENT']}") String environment

    boolean debug() {
        return (environment != null && environment.startsWith("dev-"))
    }
}
