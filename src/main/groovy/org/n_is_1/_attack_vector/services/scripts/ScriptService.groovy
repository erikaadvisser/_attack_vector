package org.n_is_1._attack_vector.services.scripts

import org.joda.time.DateTime
import org.joda.time.DateTimeFieldType
import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.repo.HackingScriptRepo
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.scripts.model.ScriptLevel
import org.n_is_1._attack_vector.services.scripts.model.ScriptType
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.UserUtil
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This service manages the creation and usage of hacking scripts.
 */
@Service
class ScriptService {
    static final def log = LoggerFactory.getLogger(ScriptService)

    @Autowired HackingScriptRepo scriptRepo
    @Autowired UserService userService
    @Autowired LogService logService

    Map<String, HackingScript> scriptsById = [:]


    @PostConstruct
    void init() throws Exception {
        scriptRepo.findAll().each { script ->
            scriptsById[script.id] = script
        }
        log.info "Read: ${scriptsById.size()} scripts"
    }

    List<HackingScript> getScriptsForHacker(String userId) {
        def scripts = scriptsById.values().findAll { it.ownerId == userId }
        scripts.sort { a, b -> a.createdTimestamp <=> b.createdTimestamp}
        scripts.each { it.originalOwnerName = userService.usersById[it.ownerId].loginName }
        return scripts
    }

    HackingScript getScript(String scriptId) {
        return scriptsById[scriptId]
    }

    int setUsed(String userId, String scriptId) {
        def script = scriptsById[scriptId]
        script.used = true
        script.usedTimestamp = DateTime.now().millis
        scriptRepo.save(script)

        def gm = UserUtil.getUser()
        def hacker = userService.findUserById(userId)
        logService.gm(LogAction.GM_USE_SCRIPT, "${gm.loginName} sets ${script.type} (${script.id}) as used for ${hacker.loginName}")

        return 0
    }

    void setExpired(HackingScript script) {
        script.expired = true
        script.deleted = true
        scriptRepo.save(script)
    }

    void useScript(HackingScript script, HackingRun run) {
        script.used = true;
        script.usedTimestamp = DateTime.now().millis
        script.runId = run?.runId
        scriptRepo.save(script)
    }

    int setUnused(String userId, String scriptId) {
        def script = scriptsById[scriptId]
        script.used = false
        script.usedTimestamp = 0
        scriptRepo.save(script)

        def gm = UserUtil.getUser()
        def hacker = userService.findUserById(userId)
        logService.gm(LogAction.GM_UNUSE_SCRIPT, "${gm.loginName} sets ${script.type} (${script.id}) as not used for ${hacker.loginName}")
        return 0
    }

    HackingScript delete(String scriptId) {
        def script = scriptsById[scriptId]
        script.deleted = true
        scriptRepo.save(script)
        return script
    }


    HackingScript undelete(String scriptId) {
        def script = scriptsById[scriptId]
        script.deleted = false
        scriptRepo.save(script)
        return script
    }

    HackingScript deleteOrUndelete(String scriptId) {
        def script = scriptsById[scriptId]
        if (script.deleted) {
            return undelete(scriptId)
        }
        else {
            return delete(scriptId)
        }
    }


    void purge() {
        scriptsById = [:]
        scriptRepo.deleteAll()
    }

    // ------------------------------ ------------------------------ ------------------------------

    def create(String userId, ScriptType type) {
        String id =  UUID.randomUUID().toString().substring(9,23)
        def script = new HackingScript(
                id: id,
                ownerId: userId,
                type: type,
                createdTimestamp: DateTime.now().millis
        )

        scriptsById[id] = script
        scriptRepo.save(script)
        def gm = UserUtil.getUser()
        def hacker = userService.findUserById(userId)
        logService.gm(LogAction.GM_ADD_SCRIPT, "${gm.loginName} give ${script.type} (${script.id}) to ${hacker.loginName}")
        return 0
    }

    Collection<HackingScript> list() {
        return scriptsById.values()
    }

    String pruneCreate() {
        def user = UserUtil.getUser()
        def scripts = getScriptsForHacker(user.id)
        def pruneMessage = prune(scripts)
        def addMessage = addMissingScripts(user, scripts)

        return "${pruneMessage} ${addMessage}"
    }

    String prune(List<HackingScript> scripts) {
        def expireTimeStamp = deterimineExpiry()
        int scriptsExpired = 0

        scripts.each { script ->
            if (!script.expired && new DateTime(script.createdTimestamp).isBefore(expireTimeStamp)) {
                setExpired(script)
                scriptsExpired ++
            }
        }
        return "${scriptsExpired} scripts expired."
    }

    DateTime deterimineExpiry() {
        def expireTimeStamp = DateTime.now().withField(DateTimeFieldType.millisOfDay(), 0). withHourOfDay(6)
        if (DateTime.now().isBefore(expireTimeStamp)) {
            expireTimeStamp = expireTimeStamp.minusDays(1)
        }
        return expireTimeStamp
    }

    String addMissingScripts(User user, List<HackingScript> scripts) {
        int scriptsAdded = 0
        ScriptLevel.scriptsByLevel.each { byLevel ->
            if (user.skill_it_main >= byLevel.level) {
                if (byLevel.level <= 5 || (byLevel.level >= 6 && byLevel.specialization == user.specialization)) {
                    scriptsAdded += addScriptIfMissing(user, byLevel.type, scripts)
                }
            }
        }
        return "${scriptsAdded} scripts added."
    }

    int addScriptIfMissing(User user, ScriptType type, List<HackingScript> scripts) {
        def existingScript = scripts.find { it.type == type && !it.expired }

        if ( existingScript == null) {
            create(user.id, type)
            return 1
        }
        return 0
    }

}
