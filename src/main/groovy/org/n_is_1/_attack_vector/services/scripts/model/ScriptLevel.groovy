package org.n_is_1._attack_vector.services.scripts.model

import org.n_is_1._attack_vector.model.admin.HackerSpecialization

/**
 * This class represents a ICT-skill-level based reward. I.e. scripts you can get per day.
 */
class ScriptLevel {

    int level
    ScriptType type
    HackerSpecialization specialization

    ScriptLevel(int level, ScriptType type, HackerSpecialization specialization) {
        this.level = level
        this.type = type
        this.specialization = specialization
    }
    static List<ScriptLevel> scriptsByLevel = []

    static {
        scriptsByLevel.add( new ScriptLevel(4, ScriptType.ANALYZE, null))
        scriptsByLevel.add( new ScriptLevel(4, ScriptType.SNOOP, null))
        scriptsByLevel.add( new ScriptLevel(4, ScriptType.MASK, null))

        scriptsByLevel.add( new ScriptLevel(5, ScriptType.ANALYZE, null))
        scriptsByLevel.add( new ScriptLevel(5, ScriptType.ANALYZE, null))
        scriptsByLevel.add( new ScriptLevel(5, ScriptType.ANALYZE, null))
        scriptsByLevel.add( new ScriptLevel(5, ScriptType.SNOOP, null))
        scriptsByLevel.add( new ScriptLevel(5, ScriptType.SNOOP, null))

        scriptsByLevel.add( new ScriptLevel(6, ScriptType.ELEVATE, HackerSpecialization.ELITE))
        scriptsByLevel.add( new ScriptLevel(6, ScriptType.MASK, HackerSpecialization.ELITE))
        scriptsByLevel.add( new ScriptLevel(6, ScriptType.MASK, HackerSpecialization.ELITE))
        scriptsByLevel.add( new ScriptLevel(6, ScriptType.MASK, HackerSpecialization.ELITE))

        scriptsByLevel.add( new ScriptLevel(7, ScriptType.ANALYZE, HackerSpecialization.ELITE))
        scriptsByLevel.add( new ScriptLevel(7, ScriptType.ANALYZE, HackerSpecialization.ELITE))
        scriptsByLevel.add( new ScriptLevel(7, ScriptType.ANALYZE, HackerSpecialization.ELITE))

        scriptsByLevel.add( new ScriptLevel(7, ScriptType.DEFLECT, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(7, ScriptType.DEFLECT, HackerSpecialization.ARCHITECT))

        scriptsByLevel.add( new ScriptLevel(8, ScriptType.STORM, HackerSpecialization.ELITE))
        scriptsByLevel.add( new ScriptLevel(8, ScriptType.STORM, HackerSpecialization.ELITE))

        scriptsByLevel.add( new ScriptLevel(8, ScriptType.SOFTDOS, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(8, ScriptType.SOFTDOS, HackerSpecialization.ARCHITECT))


        scriptsByLevel.add( new ScriptLevel(9, ScriptType.BYPASS, HackerSpecialization.ELITE))
        scriptsByLevel.add( new ScriptLevel(9, ScriptType.ELEVATE, HackerSpecialization.ELITE))

        scriptsByLevel.add( new ScriptLevel(10, ScriptType.BACKDOOR, HackerSpecialization.ELITE))

        scriptsByLevel.add( new ScriptLevel(10, ScriptType.MASK, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.MASK, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.MASK, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.MASK, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.MASK, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.ANALYZE, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.ANALYZE, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.ANALYZE, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.ELEVATE, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.SOFTDOS, HackerSpecialization.ARCHITECT))
        scriptsByLevel.add( new ScriptLevel(10, ScriptType.SOFTDOS, HackerSpecialization.ARCHITECT))

    }
}
