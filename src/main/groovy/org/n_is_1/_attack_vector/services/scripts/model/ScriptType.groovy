package org.n_is_1._attack_vector.services.scripts.model
/**
 * Defines the different types of scripts in existence
 */
enum ScriptType {

    MASK (   "mask", "&nbsp;&nbsp;&nbsp;"),
    ELEVATE( "elevate", ""),
    BYPASS(  "bypass", "&nbsp;"),
    ANALYZE( "analyze", ""),
    SNOOP(   "snoop", "&nbsp;&nbsp;"),
    BACKDOOR("backdoor", ""),
    STORM(   "storm", "&nbsp;&nbsp;"),
    DEFLECT( "deflect", ""),
    SOFTDOS( "softdos", "")

    String command
    String padding

    ScriptType(String command, String padding) {
        this.command = command
        this.padding = padding
    }
}