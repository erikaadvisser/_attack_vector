package org.n_is_1._attack_vector.services.scripts.model

import org.joda.time.DateTime

/**
 * This is a hacking script.
 */
class HackingScript {

    /** Also the version id used by hacker to activate it */
    String id

    /** Creation of script */
    long createdTimestamp

    ScriptType type

    /** Hacker id of the person who received the script. */
    String ownerId

    /** For display purposes: the original owner of the script */
    transient String originalOwnerName


    /** a script can only be used once */
    boolean used

    /** null if not used yet */
    long usedTimestamp

    /** The run this script was used on, null if not used yet. In case the script is used to trace the hacker */
    String runId

    /** Deleted scrips are never really gone, you just won't see them */
    boolean deleted = false

    /** A script can only be used the day it was created. After that it is expired. This flag is for
     * convenience to keep track of which scripts a hacker has that are still useful.
     */
    boolean expired = false


    boolean usable() {
        return !(expired || used)
    }

    String displayCreate() {
        return new DateTime(createdTimestamp).toString("YYYY-MM-dd HH:mm")
    }

    String displayUsed() {
        if (usedTimestamp > 0) {
            return new DateTime(usedTimestamp).toString("YYYY-MM-dd HH:mm")
        }
        else {
            return ""
        }
    }

    String command() {
        return "run ${type.command} ${id}"
    }


}
