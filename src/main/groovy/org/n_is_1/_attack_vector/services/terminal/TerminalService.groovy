package org.n_is_1._attack_vector.services.terminal

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.services.discord.ChatService
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service for processing Terminal commands.
 */
@Service
class TerminalService {

    Logger log = LoggerFactory.getLogger(TerminalService)

    @Autowired CommandRunService runService
    @Autowired CommandMoveService moveService
    @Autowired CommandViewService viewService
    @Autowired CommandHackService hackService
    @Autowired CommandPasscodeService passcodeService
    @Autowired HackingRunService hackingRunService
    @Autowired CommandMessageService messageService

    public TerminalResponse processCommand(HackingRun run, String rawInput) {
        def input = rawInput.trim().toLowerCase()


        try {
            if (input.startsWith("dc")) {
                hackingRunService.disconnect(run)
                return new TerminalResponse(message: "⊿00 ↠ Disconnecting", type: "DISCONNECT")
            }
            if (input.startsWith("scan")) {
                return new TerminalResponse(message: "⊿00 ↠ scan complete", type: "SCAN")
            }
            if (input.startsWith("help")) {
                return new TerminalResponse(message: "∲01 ↠ Command overview\n\nhelp\nmove\nview\nhack\npasscode\ndc\nmessage\n", type: "HELP")
            }
            if (input.startsWith("move")) {
                return moveService.process(run, input)
            }
            if (input.startsWith("hack")) {
                return processLayerCommand(run, input)
            }
            if (input.startsWith("passcode")) {
                return processLayerCommand(run, input)
            }
            if (input.startsWith("view")) {
                return viewService.process(run)
            }
            else if (input.startsWith("run")) {
                return runService.run(run, input)
            }
            if (input.startsWith("message ")) {
                return processMessageCommand(run, input)
            }
            else {
                return new TerminalResponse(message: "∦14 ⇥ Invalid syntax or command, try 'help'.")
            }
        }
        catch (Exception exception) {
            log.error("Command execution failed due to .", exception)
            return new TerminalResponse(message: "∻23 ⇥ Resolve failed, ((OC problem))\n\n(details: ${exception.getMessage()}))")
        }
    }

    public TerminalResponse processMessageCommand(HackingRun run, String input) {
        SiteNode currentNode = run.currentNode

        def targetService = currentNode.services.find {it.type == ServiceType.CHAT}
        if (targetService == null) {
            return new TerminalResponse(message: "∦38 ⇥ no layer with message service found in this node.")
        }

        NodeService iceBlockingLayer = checkIceBlocksAccess(currentNode, targetService, run)
        if (iceBlockingLayer) {
            return new TerminalResponse(message: "∦12 ⇥ Access blocked by ${iceBlockingLayer.type.text} at layer ${iceBlockingLayer.layer}")
        }

        def message = input.substring("message ".size())
        messageService.sendMessage(run, message)
        return new TerminalResponse(message: "⊿00 ↠ Message posted")
    }

    public TerminalResponse processLayerCommand(HackingRun run, String input) {
        String[] parts = input.split(" ")
        if (parts.length == 1) {
            return createHelpLayerResponse(run, input)
        }

        String layerInput = parts[1]
        int targetLayer
        try {
            targetLayer = Integer.parseInt(layerInput)
        }
        catch (NumberFormatException ignore) {
            return new TerminalResponse(message: "∦14 ⇥ layer '${layerInput}' not understood, please use a number.")
        }

        SiteNode currentNode = run.currentNode
        if (targetLayer < 0 || targetLayer >= currentNode.services.size()) {
            return new TerminalResponse(message: "∦13 ⇥ layer '${targetLayer}' not found.")
        }

        def targetService = currentNode.services[targetLayer]
        NodeService iceBlockingLayer = checkIceBlocksAccess(currentNode, targetService, run)
        if (iceBlockingLayer) {
            return new TerminalResponse(message: "∦12 ⇥ Access blocked by ${iceBlockingLayer.type.text} at layer ${iceBlockingLayer.layer}")
        }

        parts[0] = ""
        parts[1] = ""
        def argument = parts.join(" ").trim()

        if (input.startsWith("hack")) {
            return hackService.process(run, targetService, argument)
        }
        return passcodeService.process(run, targetService, targetLayer, argument, currentNode)
    }

    TerminalResponse createHelpLayerResponse(HackingRun run, String input) {
        if (input.startsWith("hack")) {
            return new TerminalResponse(message: "∲01 ↠ Please specify the layer to be hacked. For example: hack ${run.currentNode.services.size() - 1}", type: "HELP")
        }
        else {
            return new TerminalResponse(message: "∲01 ↠ Please specify the layer and passcode. For example: passcode ${run.currentNode.services.size() - 1} correct horse battery staple", type: "HELP")
        }
    }


    NodeService checkIceBlocksAccess(SiteNode node, NodeService targetService, HackingRun run) {
        for (def service : node.services.reverse()) {
            if (service == targetService) {
                return null
            }
            if (service.type.isIce() && !run.hackedServices.contains(service)) {
                return service;
            }
        }
    }
}
