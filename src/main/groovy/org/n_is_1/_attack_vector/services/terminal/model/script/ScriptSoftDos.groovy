package org.n_is_1._attack_vector.services.terminal.model.script

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.RandomService
import org.n_is_1._attack_vector.services.util.TimeParseUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This class implements the Softdos script
 */
@Service
class ScriptSoftDos {

    @Autowired RandomService random
    @Autowired ScriptService scriptService
    @Autowired LogService logService
    @Autowired SiteService siteService

    TimeParseUtil timeParseUtil = new TimeParseUtil()

    TerminalResponse run(HackingScript script, Site site, HackingRun run) {
        if (run != null) {
            return new TerminalResponse(message: "∦16 ⇥ Script must be used from the scan window.")
        }
        if (site.softdos) {
            return new TerminalResponse(message: "∦44 ⇥ Softdos already active against this site.")
        }

        int defaultPercentiles = 30
        int bonusPercentiles = random.nextInt(21)

        int totalPercentiles = defaultPercentiles + bonusPercentiles

        String oldTraceTime = site.hackTime

        long normalMillis = timeParseUtil.parseToMillis(oldTraceTime, "input invalid")
        long bonusMillis = (totalPercentiles * normalMillis) / 100
        long newMillis = normalMillis + bonusMillis

        String newTraceTime = timeParseUtil.parseToTime(newMillis)

        // doublecheck new trace time is valid
        timeParseUtil.parseToMillis(newTraceTime, "Softdos script internal error")

        site.hackTime = newTraceTime
        site.softdos = true

        siteService.saveWithoutUpdatingGeneration(site)
        scriptService.useScript(script, run)

        logService.event(LogAction.HACKING_RUN_SCRIPT, "Softdos script used, from: ${oldTraceTime} to ${newTraceTime} (${totalPercentiles}%) (${script.id})", site.id)
        return new TerminalResponse(message: "⊿00 ↠ Script reports success, softdos active, achieving ${totalPercentiles}% effectiveness. ", scriptId: script.id)
    }
}
