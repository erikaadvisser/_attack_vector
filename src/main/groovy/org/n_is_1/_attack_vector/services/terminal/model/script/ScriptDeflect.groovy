package org.n_is_1._attack_vector.services.terminal.model.script

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.RandomService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This class implements the deflect script
 */
@Service
class ScriptDeflect {

    @Autowired ScriptService scriptService
    @Autowired LogService logService
    @Autowired MissionService missionService

    TerminalResponse run(HackingScript script, HackingRun run) {
        if (run == null) {
            return new TerminalResponse(message: "∦17 ⇥ Script must be used during a hacking run.")
        }

        run.deflectCount ++

        scriptService.useScript(script, run)

        logService.eventForRun(LogAction.HACKING_RUN_SCRIPT, "Deflect script run, deflect protection active. (${script.id})", run)
        return new TerminalResponse(message: "⊿00 ↠ Script reports success. Total deflects active: ${run.deflectCount}", scriptId: script.id)
    }
}
