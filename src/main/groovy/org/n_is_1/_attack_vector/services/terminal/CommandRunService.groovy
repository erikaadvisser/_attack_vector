package org.n_is_1._attack_vector.services.terminal

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.scripts.model.ScriptType
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.terminal.model.script.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Handles 'run' commands
 */
@Service
class CommandRunService {

    @Autowired HackingRunService attackService
    @Autowired ScriptService scriptService

    @Autowired ScriptElevate elevate
    @Autowired ScriptMask mask
    @Autowired ScriptBypass gmNodeBypass
    @Autowired ScriptAnalyze analyze
    @Autowired ScriptSnoop snoop
    @Autowired ScriptBackdoor backdoor
    @Autowired ScriptStorm storm
    @Autowired ScriptDeflect deflect
    @Autowired ScriptSoftDos softDos

    TerminalResponse run(HackingRun run, String command) {
        runInternal(run, command, null, null)
    }


    TerminalResponse run(String command, Site site, SiteScan scan) {
        runInternal(null, command, site, scan)
    }

    private TerminalResponse runInternal(HackingRun run, String command, Site site, SiteScan scan) {
        if (command.length() == 3) {
            command = command + " "
        }
        def arguments = command.substring("run ".length())

        HackingScript script = null
        ScriptType type = null

        ScriptType.values().each { it ->
            if (arguments.startsWith(it.command)) {
                type = it
                def args = arguments.substring(it.command.length()).trim()
                if (args.length() >= 14) {
                    script = scriptService.getScript(args.substring(0,14))
                }
            }
        }

        if (!type) {
            return new TerminalResponse(message: "∦31 ⇥ Script type unknown.")
        }

        if (!script || script.type != type) {
            return new TerminalResponse(message: "∦32 ⇥ Script version unknown.")
        }

        def expireTimeStamp = scriptService.deterimineExpiry()
        if (script.used || script.expired) {
            return new TerminalResponse(message: "∦75 ⇥ Script reports vulnerability patched. System unaffected.")
        }
        if (new DateTime(script.createdTimestamp).isBefore(expireTimeStamp)) {
            if (!script.expired) {
                scriptService.setExpired(script)
            }
            return new TerminalResponse(message: "∦75 ⇥ Script reports vulnerability patched. System unaffected.")
        }

        switch(script.type) {
            case ScriptType.MASK: return mask.run(script, run)
            case ScriptType.ELEVATE: return elevate.run(script, run, arguments)
            case ScriptType.BYPASS: return gmNodeBypass.run(script, run)
            case ScriptType.ANALYZE: return analyze.run(script, arguments, site, scan)
            case ScriptType.SNOOP: return snoop.run(script, arguments, site, scan)
            case ScriptType.BACKDOOR: return backdoor.run(script, run, arguments)
            case ScriptType.STORM: return storm.run(script, run)
            case ScriptType.DEFLECT: return deflect.run(script, run)
            case ScriptType.SOFTDOS: return softDos.run(script, site, run)
            default: return new TerminalResponse(message: "∻03 ⇥ Script unknown: ${script.type}")
        }
    }
}
