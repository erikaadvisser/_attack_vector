package org.n_is_1._attack_vector.services.terminal

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.services.discord.ChatService
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.run.ScanService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.ConfigUtil
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.TimeParseUtil
import org.n_is_1._attack_vector.services.util.UserUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import static org.n_is_1._attack_vector.model.site.ServiceType.ICE_MANUAL
import static org.n_is_1._attack_vector.model.site.ServiceType.ICE_NETWALK
import static org.n_is_1._attack_vector.model.site.ServiceType.ICE_PASSWORD

/**
 * Handles 'hack' commands
 */
@Service
class CommandHackService {

    @Autowired
    LogService logService
    @Autowired
    HackingRunService hackingRunService
    @Autowired
    ScanService scanService
    @Autowired
    MissionService missionService
    @Autowired
    ChatService chatService

    def timeParseUtil = new TimeParseUtil()
    def configUtil = new ConfigUtil()

    TerminalResponse process(HackingRun run, NodeService targetService, String argument) {
        if (run.hackedServices.contains(targetService)) {
            return new TerminalResponse(message: "∦01 ⇥ Service already subverted.")
        }

        if (argument.length() > 0) {
            return new TerminalResponse(message: "∦03 ⇥ Additional parameters detected: '${argument}'. Did you mean 'passcode'?")
        }

        if (targetService.type.isIce()) {
            return hackIce(targetService, run)
        }

        switch (targetService.type) {
            case ServiceType.CODE: return hackCode(targetService, run)
            case ServiceType.FILE: return hackFile(targetService, run)
            case ServiceType.TEXT: return hackText(targetService, run)
            case ServiceType.CHAT: return hackChat(targetService, run)
            case ServiceType.OS: return hackOs(run)
            case ServiceType.RESOURCE: return hackResource(targetService, run)
            case ServiceType.TIME: return hackTime(targetService, run)
            case ServiceType.SYSCON: return hackSysCon(targetService, run)
            case ServiceType.SCAN_BLOCKER: return hackScanBlocker(targetService, run)
            case ServiceType.LOG_DB: return hackLogDb(targetService, run)
            case ServiceType.REMOTE_TRACER: return hackRemoteTracer(targetService, run)
            case ServiceType.TRACE_LOGS: return hackTraceLogs(targetService, run)
            default: return new TerminalResponse(message: "∦05 ⇥ Cannot hack service type: ${targetService.type}", type: "HELP")
        }
    }

    TerminalResponse hackTraceLogs(NodeService service, HackingRun run) {
        if (run.mission == null) {
            return new TerminalResponse(message: "∻85 service inactive due to faulty configuration.")
        }
        String message = missionService.removeTraceLogs(run)
        return new TerminalResponse(message: message)
    }

    TerminalResponse hackRemoteTracer(NodeService service, HackingRun run) {
        if (run.mission == null) {
            return new TerminalResponse(message: "∻85 service inactive due to faulty configuration.")
        }
        def traceSiteId = service.data["siteId"]
        return new TerminalResponse(message: "⊿00 ↠ Remote trace site uncovered: ${traceSiteId}")
    }

    TerminalResponse hackIce(NodeService service, HackingRun run) {
        if (service.type != ICE_PASSWORD && service.type != ICE_MANUAL && service.type != ICE_NETWALK) {
            def user = UserUtil.getUser()
            if (user.skill_it_main < 2) {
                return new TerminalResponse(message: "Ice has no known vulnerabilities (IT skill too low).")
            }
        }

        logService.eventForRun(LogAction.HACKING_RUN_ICE_HACK_START, "Starting hack of ice ${service.type}", run)

        String url = "${service.type.iceUrl}/${run.runId}/${service.id}"
        return new TerminalResponse(message: "⊿00 ↠ hacking ICE.", type: "ATTACK", payload: url)
    }

    TerminalResponse hackCode(NodeService service, HackingRun run) {
        String targetId = service.data['targetId']
        def iceGeneration = run.generation.iceGenerationsById[targetId]
        def passcode = iceGeneration.passcode

        def text = service.data['text'] ? service.data['text'] : ""

        def message = "⊿00 ↠ Hacking ${service.hackingRunName()}...\nSuccess.\n${text}\nPasscode extracted: ${passcode}\n"
        logService.eventForRun(LogAction.HACKING_RUN_RECOVERED_PASSCODE, "Hacked ${service.hackingRunName()} and got passcode ${passcode}", run)
        return new TerminalResponse(message: message, type: "VIEW")
    }

    TerminalResponse hackFile(NodeService service, HackingRun run) {
        def url = service.data['url']
        def message = "⊿00 ↠ Hacking ${service.hackingRunName()}...\nSuccess.\nData extracted: ${url}\n"
        logService.highlightForRun(LogAction.HACKING_RUN_RECOVERED_FILE, "Hacked ${service.hackingRunName()} for: ${url}", run)
        return new TerminalResponse(message: message, type: "VIEW")
    }

    TerminalResponse hackText(NodeService service, HackingRun run) {
        def text = service.data['text']
        if (run.mission != null) {
            text = missionService.hackText(service, run)
        }
        def message = "⊿00 ↠ Hacking ${service.hackingRunName()}...\nSuccess.\n${text}\n"
        logService.highlightForRun(LogAction.HACKING_RUN_RECOVERED_FILE, "Hacked ${service.hackingRunName()} for:  ${text}", run)
        return new TerminalResponse(message: message, type: "VIEW")
    }

    TerminalResponse hackChat(NodeService service, HackingRun run) {
        def welcome = service.data['welcome']
        def remoteAuthorName = service.data['authorName']
        def messages = chatService.getMessages()
        messages.findAll({ it.authorName == null })
                .forEach({ it.authorName = remoteAuthorName })
        def messageStrings = messages.collect{ "[[;#31708f;]${it.timeString}] [[;#3c763d;]${it.authorName}] ${it.message}"}.join("\n")
        def text = "${welcome}\n${messageStrings}\nTo reply to this chat, use the \"message\" command."

        def message = "⊿01 ↠ Accessing ${service.hackingRunName()}...Success.\n${text}"
        logService.highlightForRun(LogAction.HACKING_RUN_RECOVERED_FILE, "Hacked chat: ${service.hackingRunName()} ", run)
        return new TerminalResponse(message: message, type: "VIEW")
    }


    TerminalResponse hackOs(HackingRun run) {
        def nodesDiscovered = []
        List<String> connectedNodes = run.site.findConnectedNodeIds(run.currentNode)
        connectedNodes.each { connectedNodeId ->
            def node = run.site.findNodeById(connectedNodeId)

            if (run.nodeStatusById[node.id] == NodeStatus.UNDISCOVERED) {
                nodesDiscovered.add(connectedNodeId)
                run.nodeStatusById[node.id] = NodeStatus.DISCOVERED
            }
        }

        def connectionsDiscovered = scanService.determineAdditionalConnections(nodesDiscovered, run.site)

        def message = (nodesDiscovered.isEmpty()) ? "⊿00 ⇥ OS hacked, no new nodes discovered." : "⊿00 ↠ OS hacked, new nodes discovered."
        return new TerminalResponse(message: message, type: "NODES_DISCOVERED", payload: [nodes: nodesDiscovered, connections: connectionsDiscovered])
    }

    TerminalResponse hackResource(NodeService service, HackingRun run) {
        if (run.hackedServices.contains(service)) {
            return new TerminalResponse(message: "∦12 ↠ Service already subverted.")
        }
        if (run.mission) {
            boolean firstToHack = missionService.hackResource(service, run)
            if (!firstToHack) {
                return new TerminalResponse(message: "⊿00 ↠ Hacking ${service.hackingRunName()}...\nSuccess.\nFound nothing of value.\n")
            }
        }

        def description = service.data['description']
        def value = service.data['value']
        def message = "⊿00 ↠ Hacking ${service.hackingRunName()}...\nSuccess.\n${description}\nApprox recovered resources: ${value} \n\n"
        logService.highlightForRun(LogAction.HACKING_RUN_RECOVERED_RESOURCE, "Recovered resources: ${value}", run)
        run.hackedServices.add(service)

        return new TerminalResponse(message: message, type: "VIEW")
    }

    TerminalResponse hackTime(NodeService service, HackingRun run) {
        if (run.hackedServices.contains(service)) {
            return new TerminalResponse(message: "∦12 ↠ Service already subverted.")
        }

        String timeInput = service.data["time"]
        long millis = timeParseUtil.parseToMillis(timeInput, "Time invalid, ")
        run.traceEnd += millis

        logService.eventForRun(LogAction.HACKING_RUN_RECOVERED_TIME, "Extra hacking time gained: ${timeInput}", run)
        run.hackedServices.add(service)
        return new TerminalResponse(message: "⊿00 ↠ Hacked local core security, slowed down detection by: ${timeInput}", type: "TIME", payload: run.traceEnd)
    }

    TerminalResponse hackScanBlocker(NodeService service, HackingRun run) {
        return new TerminalResponse(message: "∦18 ↠ service resisted subversion.")
    }

    TerminalResponse hackLogDb(NodeService nodeService, HackingRun hackingRun) {
        return new TerminalResponse(message: "⊿00 ↠ logs corrupted")
    }

    TerminalResponse hackSysCon(NodeService service, HackingRun run) {
        boolean disableIce = configUtil.parseBoolean(service.data['ice'], null)
        boolean disableTimer = configUtil.parseBoolean(service.data['timer'], null)

        String message = ""
        def nodes = new HashSet<SiteNode>()

        def payload = [
                disableIce     : disableIce,
                disableTimer   : disableTimer,
                nodeStatus     : [],
                discoveredNodes: []]

        if (disableTimer) {
            run.traceEnd = -42
            message += "- detection disabled\n"
        }

        if (disableIce) {
            run.site.nodes.forEach { node ->
                node.services.findAll { it.type.isIce() }.forEach { iceService ->
                    if (!run.hackedServices.contains(iceService)) {
                        run.hackedServices.add(iceService)
                    }
                }
            }
            message += "- all Ice disabled\n"
        }

        run.site.nodes.forEach { node ->
            def oldStatus = run.nodeStatusById[node.id]
            def status = hackingRunService.determineNodeStatus(run, node)
            run.nodeStatusById[node.id] = status

            if (oldStatus == NodeStatus.UNDISCOVERED) {
                def discovery = [
                        nodeId     : node.id,
                        status     : status,
                        connections: run.site.findConnectedNodeIds(node)]
                payload.discoveredNodes.add(discovery)
            } else if (status != oldStatus) {
                nodes.add(node)
            }
        }

        nodes.each { node ->
            payload.nodeStatus.add([id: node.id, status: run.nodeStatusById[node.id]])
        }

        boolean mapRevealed = (payload.nodeStatus.size() > 0) || (payload.discoveredNodes.size() > 0)

        if (mapRevealed) {
            message = "- site map revealed\n${message}"
        }

        if (disableIce || disableTimer || mapRevealed) {
            message = "⊿00 ⇥ Syscon hacked\n${message}"
        } else {
            message = "∦04 ⇥ Syscon hardened, no intel gained."
        }

        logService.eventForRun(LogAction.HACKING_RUN_RECOVERED_SYSCON, "Hacked syscon (disable all ice: ${disableIce} stop timer: ${disableTimer}).", run)
        return new TerminalResponse(message: message, payload: payload, type: "SYSCON")
    }

}
