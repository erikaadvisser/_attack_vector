package org.n_is_1._attack_vector.services.terminal.model.script

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Implements the GM only script for bypassing all Ice in a single node.
 */
@Service
class ScriptBypass {

    @Autowired ScriptService scriptService
    @Autowired LogService logService

    TerminalResponse run(HackingScript script, HackingRun run) {
        if (run == null) {
            return new TerminalResponse(message: "∦17 ⇥ Script must be used during a hacking run.")
        }

        def servicesHacked = []
        run.currentNode.services.each { service ->
            if (service.type.isIce()) {
                if (!run.hackedServices.contains(service)) {
                    servicesHacked.add(service)
                }
            }
        }


        if (servicesHacked.isEmpty()) {
            return new TerminalResponse(message: "∦01 ⇥ All ice layers already subverted.")
        }
        run.hackedServices.addAll(servicesHacked)
        run.nodeStatusById[run.currentNode.id] = NodeStatus.HACKED
        scriptService.useScript(script, run)

        logService.eventForRun(LogAction.HACKING_RUN_SCRIPT, "GM Bypass script run, all services hacked. (${script.id})", run)
        return new TerminalResponse(message: "⊿00 ↠ Script reports success. Ice layers subverted: ${servicesHacked.size()}", type:"FW_BYPASS", scriptId: script.id)
    }
}