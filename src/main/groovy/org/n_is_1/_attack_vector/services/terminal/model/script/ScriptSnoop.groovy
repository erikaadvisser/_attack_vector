package org.n_is_1._attack_vector.services.terminal.model.script

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.services.run.model.ScanAction
import org.n_is_1._attack_vector.services.run.model.ScanProbe
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.scripts.model.ScriptType
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This class implements the SNOOP script
 */
@Service
class ScriptSnoop {

    @Autowired ScriptService scriptService
    @Autowired LogService logService

    TerminalResponse run(HackingScript script, String arguments, Site site, SiteScan scan) {
        def util = new ServiceTargettingScriptUtil(ScriptType.SNOOP)

        def errorResponse = util.run(script, arguments, site, scan)
        if (errorResponse) {
            return errorResponse
        }
        def targetService = util.service
        def node = util.node


        if (scan.analyzedServiceIds.contains(targetService.id)) {
            return new TerminalResponse(message: "∦35 ⇥ Target service already snooped.")
        }
        def type = targetService.type
        if (type != ServiceType.SYSCON && type != ServiceType.TIME) {
            return new TerminalResponse(message: "∦34 ⇥ Target service is of the wrong type, target: Local core or Central core.")
        }


        scriptService.useScript(script, null)
        logService.event(LogAction.HACKING_RUN_SCRIPT, "Snoop script started on ${arguments}", site.id, scan.id)
        def probe = new ScanProbe(scan, site)
        probe.programForScript(node, targetService, ScanAction.SNOOP)

        return new TerminalResponse(message: "⊿00 ↠ Snoop probe launched.", type: "SCAN", payload: probe.moves, scriptId: script.id)
    }

}
