package org.n_is_1._attack_vector.services.terminal

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Handles 'passcode' commands
 */
@Service
class CommandPasscodeService {

    @Autowired HackingRunService hackingRunService
    @Autowired LogService logService

    TerminalResponse process(HackingRun run, NodeService targetService, int layer, String argument, SiteNode node) {
        if (run.hackedServices.contains(targetService)) {
            return new TerminalResponse(message: "∦01 ⇥ ICE already neutralized.", type: "HELP")
        }

        if (!targetService.type.isIce()) {
            return new TerminalResponse(message: "∦02 ⇥ passcode is only used on ICE layers.", type: "HELP")
        }

        if (argument == "") {
            return new TerminalResponse(message: "∦3 ⇥ please specify passcode. For example: passcode ${layer} correct horse battery staple", type: "HELP")
        }
        if (argument == "correct horse battery staple") {
            return new TerminalResponse(message: "∦42 ⇥ That was the sample password, smeg head!", type: "HELP")
        }

        def generation = run.generation.iceGenerationsById[targetService.id]

        if (generation.passcode.toLowerCase() != argument.toLowerCase()) {
            return new TerminalResponse(message: "∦12 ⇥ Authentication refused.", type: "HELP")
        }

        run.hackedServices.add(targetService)

        def status = hackingRunService.determineNodeStatus(run.runId, node.id)
        run.nodeStatusById[node.id] = status
        def responseType = (status == NodeStatus.HACKED) ? "NODE_HACKED" : "VIEW"

        logService.eventForRun(LogAction.HACKING_RUN_USE_PASSCODE, "Used passcode to bypass Ice ${targetService.type}", run)

        return new TerminalResponse(message: "⊿00 ⇥ Authentication accepted, Ice allows access.", type: responseType, payload: targetService.data['id'])
    }
}
