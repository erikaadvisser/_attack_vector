package org.n_is_1._attack_vector.services.terminal

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.springframework.stereotype.Service

/**
 * Service that processes 'layers' commands
 */
@Service
class CommandViewService {

    TerminalResponse process(HackingRun run) {
        def content = viewMessage(run.currentNode, run.hackedServices, false)
        return new TerminalResponse(message: "⊿00 ↠ Node service layers\n${content}", type: "VIEW")
    }

    String viewMessage(SiteNode currentNode, List<NodeService> hackedServices, boolean scan) {
        boolean blocked = false
        def lines = []
        int totalLayerIndex = currentNode.services.size() -1

        currentNode.services.reverse().eachWithIndex { NodeService service, int index ->
            def blockString = (blocked) ? "*" : ""
            def serviceTextPrefix = (service.type.isIce()) ? "Ice:" : ""

            String hackedSuffix = ""
            if (service.type.isIce()) {
                if (hackedServices.contains(service)) {
                    hackedSuffix = " (hacked)"
                }
                else {
                    blocked = true
                }
            }

            def serviceName = (scan) ? service.scanName() : service.hackingRunName()

            def line = "${blockString}layer ${totalLayerIndex - index} - ${serviceTextPrefix}${serviceName} ${hackedSuffix}"
            lines.add(line)
        }
        lines = lines.reverse()
        def content = lines.join("\n")
        return content
    }
}
