package org.n_is_1._attack_vector.services.terminal.model.script

import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.scripts.model.ScriptType
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse

/**
 * Class that helps scripts that target a specific service. This class is not thread safe, use a new class for each call.
 */
class ServiceTargettingScriptUtil {

    ScriptType scriptType

    NodeService service = null
    SiteNode node = null

    ServiceTargettingScriptUtil(ScriptType scriptType) {
        this.scriptType = scriptType
    }

    TerminalResponse run(HackingScript script, String arguments, Site site, SiteScan scan) {
        if (!site) {
            return new TerminalResponse(message: "∦17 ⇥ Script must be used during scanning.")
        }

        def scriptArgs = arguments.substring(scriptType.command.length()).trim()

        def target = scriptArgs.substring(14).trim()
        def parts = target.split(" ")

        if (parts.length != 2) {
            return new TerminalResponse(message: "∦32 ⇥ Specify script target: netword-id layer.\n For instance to target layer 2 of node 00, try:\n run ${scriptType.command} ${script.id} 00 2")
        }


        String targetNodeNetworkId = parts[0]

        node = site.findNodeByNetworkId(targetNodeNetworkId)
        def nodeStatus = scan.nodeStatusById[node?.id]
        if (!node || nodeStatus == NodeStatus.UNDISCOVERED) {
            return new TerminalResponse(message: "∦35 ⇥ Target node not found: '${targetNodeNetworkId}")
        }

        if (nodeStatus.level < NodeStatus.FREE.level) {
            return new TerminalResponse(message: "∦36 ⇥ Services on target node unknown, scan it more thoroughly first.")
        }

        int targetLayer
        try {
            targetLayer = Integer.parseInt(parts[1])

        }
        catch (NumberFormatException ignored) {
            return new TerminalResponse(message: "∦33 ⇥ Target layer unknown: '${target}")
        }

        NodeService targetService = node.services[targetLayer]

        if (!targetService) {
            return new TerminalResponse(message: "∦38 ⇥ Target service not found.")
        }

        this.service = targetService

        return null
    }
}