package org.n_is_1._attack_vector.services.terminal.model.script

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.scripts.model.ScriptType
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This class implements the Elevate script
 */
@Service
class ScriptElevate {

    @Autowired ScriptService scriptService
    @Autowired LogService logService

    TerminalResponse run(HackingScript script, HackingRun run, String arguments) {
        if (run == null) {
            return new TerminalResponse(message: "∦17 ⇥ Script must be used during a hacking run.")
        }

        def scriptArgs = arguments.substring(ScriptType.ELEVATE.command.length()).trim()

        def target = scriptArgs.substring(14).trim()
        if (target.length() < 1) {
            return new TerminalResponse(message: "∦33 ⇥ Specify script target. For instance to target layer 3:\nrun ${ScriptType.ELEVATE.command} xxxx-xxxx-xxxx 3")
        }

        int targetLayer
        try {
            targetLayer = Integer.parseInt(target)

        }
        catch (NumberFormatException ignored) {
            return new TerminalResponse(message: "∦33 ⇥ Target layer unknown: '${target}")
        }

        NodeService targetService = run.currentNode.services[targetLayer]

        if (!targetService) {
            return new TerminalResponse(message: "∦38 ⇥ Target service not found.")
        }

        if (!targetService.type.isIce()) {
            return new TerminalResponse(message: "∦34 ⇥ Target service is not an Ice layer.")
        }
        if (run.hackedServices.contains(targetService)) {
            return new TerminalResponse(message: "∦35 ⇥ Target service already hacked.")
        }

        int unhackedIceInNode = run.currentNode.services.count { service ->
            service.type.isIce() && !run.hackedServices.contains(service)
        }

        if (unhackedIceInNode == 1) {
            return new TerminalResponse(message: "∦36 ⇥ Last Ice layer is reinforced, cannot elevate privileges.")
        }

        scriptService.useScript(script, run)
        run.hackedServices.add(targetService)
        logService.eventForRun(LogAction.HACKING_RUN_SCRIPT, "Elevate script run, ${targetService.type} bypassed. (${script.id})", run)
        return new TerminalResponse(message: "⊿00 ↠ Script reports success. Privileges elevated. Ice at layer ${targetLayer} bypassed.", scriptId: script.id)
    }

}
