package org.n_is_1._attack_vector.services.terminal.model

/**
 * A response to a terminal command
 */
class TerminalResponse {
    String message
    String type = "ERROR"
    Object payload
    String scriptId
}
