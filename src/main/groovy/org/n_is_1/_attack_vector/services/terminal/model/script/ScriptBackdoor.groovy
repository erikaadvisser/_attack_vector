package org.n_is_1._attack_vector.services.terminal.model.script

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.scripts.model.ScriptType
import org.n_is_1._attack_vector.services.terminal.CommandMoveService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This class implements the Backdoor script
 */
@Service
class ScriptBackdoor {

    @Autowired ScriptService scriptService
    @Autowired LogService logService
    @Autowired CommandMoveService commandMoveService

    TerminalResponse run(HackingScript script, HackingRun run, String arguments) {
        if (run == null) {
            return new TerminalResponse(message: "∦17 ⇥ Script must be used during a hacking run.")
        }


        def util = new ServiceTargettingScriptUtil(ScriptType.BACKDOOR)
        def errorResponse = util.run(script, arguments, run.site, run.scan)
        if (errorResponse) {
            return errorResponse
        }
        def targetService = util.service
        def node = util.node

        if (targetService.type != ServiceType.SYSCON) {
            return new TerminalResponse(message: "∦24 ⇥ Target service is not a Central Core.")
        }

        scriptService.useScript(script, null)
        logService.event(LogAction.HACKING_RUN_SCRIPT, "Backdoor used ${arguments} - moved to ${node.deriveNetworkId()}", run.runId, run.site.id)

        run.currentNode = node

        def traceAddendum = commandMoveService.resolveTracer(run, node)
        def message = "⊿00 ↠ Backdoor activated." + traceAddendum

        return new TerminalResponse(message: message, type: "BACKDOOR", payload: [nodeId: node.id], scriptId: script.id)
    }
}