package org.n_is_1._attack_vector.services.terminal

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service for managing the movement of the persona.
 */
@Service
class CommandMoveService {

    @Autowired HackingRunService runService
    @Autowired LogService logService
    @Autowired MissionService missionService

    public TerminalResponse process(HackingRun run, String command) {
        if ( command.startsWith("move ")) {
            String target = command.substring(5);
            return move(run, target)
        }
        return new TerminalResponse(message: "∲01 ↠ Please specify move target. For example: move 01", type: "HELP")
    }


    private TerminalResponse move(HackingRun run, String networkId) {
        def currentNode = run.currentNode
        def targetNode = run.site.findNodeByNetworkId(networkId)

        if (currentNode == targetNode) {
            return new TerminalResponse(message: "∦31 ⇥ already at ▣ ${networkId}");
        }

        if (!targetNode || run.nodeStatusById[targetNode.id] == NodeStatus.UNDISCOVERED) {
            return new TerminalResponse(message: "∦32 ⇥ not found ▣ ${networkId}")
        }


        if (!currentNode.connections.contains(targetNode.id) &&
                !targetNode.connections.contains(currentNode.id)) {
            return new TerminalResponse(message: "∦33 ⇥ no path to ▣ ${networkId}")
        }

        def iceServices = currentNode.services.findAll { it.type.isIce() }
        boolean iceBypassed = false
        if (!iceServices.isEmpty() && run.previousNode != targetNode) {
            if (run.hackedServices.containsAll(iceServices)) {
                iceBypassed = true
            }
            else {
                return new TerminalResponse(message: "∦12 ⇥ Access denied. Ice demands access token.")
            }
        }

        run.previousNode = currentNode
        run.currentNode = targetNode
        def nameInput = targetNode.services[0].data['name']
        def name = (nameInput) ? nameInput : ""

        def intro = (iceBypassed) ? "Bypassing ICE, establishing" : "Establishing"

        def newNodeStatus = null
        def nodeStatus = run.nodeStatusById[targetNode.id]
        if (nodeStatus.level <= NodeStatus.PROBED.level) {
            nodeStatus = runService.determineNodeStatus(run, targetNode)
            run.nodeStatusById[targetNode.id] = nodeStatus
            newNodeStatus = nodeStatus
        }


        logService.actionForRun(LogAction.HACKING_RUN_MOVE, "Moved to ${networkId}", run)

        String traceAddendum = resolveTracer(run, targetNode)

        def payload = [
                nodeId: targetNode.id,
                newNodeStatus: newNodeStatus
        ]

        String message = "⊿00 ↠ ${intro} persona in ▣ ${networkId} ${name}" + traceAddendum
        return new TerminalResponse(message: message, type: "MOVE", payload: payload)
    }

    String resolveTracer(HackingRun run, SiteNode node) {
        List<NodeService> traceServices = node.services.findAll{ it.type == ServiceType.REMOTE_TRACER }
        if (traceServices.isEmpty()) {
            return ""
        }

        if (!run.mission) {
            return "\n\n∻85 Remote trace service detected with faulty configuration. Trace aborted."
        }

        String addendum = ""

        traceServices.forEach{traceService ->
            if (run.deflectCount > 0) {
                run.deflectCount --
                addendum += "\n # Remote trace deflected. Total remaining deflects: ${run.deflectCount}."
            }
            else {
                addendum += "\n > Remote trace started."
                missionService.addTrace(run, traceService)
            }
        }

        return addendum
    }

}
