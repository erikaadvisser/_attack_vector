package org.n_is_1._attack_vector.services.terminal.model.script

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.RandomService
import org.n_is_1._attack_vector.services.util.TimeParseUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * This class implements the MASK script
 */
@Service
class ScriptMask {

    @Autowired RandomService random
    @Autowired ScriptService scriptService
    @Autowired LogService logService

    TimeParseUtil timeParseUtil = new TimeParseUtil()

    TerminalResponse run(HackingScript script, HackingRun run) {
        if (run == null) {
            return new TerminalResponse(message: "∦17 ⇥ Script must be used during a hacking run.")
        }

        int bonusSeconds = random.nextInt(8)
        String timeInput = "1:0${bonusSeconds}"
        long millis = timeParseUtil.parseToMillis(timeInput, "input invalid")
        println " old trace end " + new DateTime(run.traceEnd)
        run.traceEnd += millis
        println " new trace end " + new DateTime(run.traceEnd)
        scriptService.useScript(script, run)

        logService.eventForRun(LogAction.HACKING_RUN_SCRIPT, "Mask script run, time gained: ${timeInput} (${script.id})", run)
        return new TerminalResponse(message: "⊿00 ↠ Script reports success. Detection delayed by: ${timeInput}", type:"TIME", payload: run.traceEnd, scriptId: script.id)
    }
}
