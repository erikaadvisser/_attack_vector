package org.n_is_1._attack_vector.services.terminal

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.repo.UserRepo
import org.n_is_1._attack_vector.services.discord.ChatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CommandMessageService {

    @Autowired ChatService chatService
    @Autowired UserRepo userRepo

    void sendMessage(HackingRun run, String message) {
        def hacker = userRepo.findOne(run.hackerId)
        chatService.addPlayerMessage(message, hacker, run.site)
    }
}
