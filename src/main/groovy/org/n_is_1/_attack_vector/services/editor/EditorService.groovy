package org.n_is_1._attack_vector.services.editor

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteType
import org.n_is_1._attack_vector.model.site.services.OsService
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.IceService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.util.IdUtil
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.TimeParseUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service for the system editor
 */
@Service
class EditorService {

    @Autowired HackingRunService currentAttackService
    @Autowired IceService iceService
    @Autowired SiteService siteService
    @Autowired LogService logService
    TimeParseUtil timeParseUtil = new TimeParseUtil()

    public boolean save(Site site) {
        validate(site)
        fillInUid(site)

        site.startNodeId = site.findNodeByNetworkId(site.startNodeNetworkId).id
        validateScanBlocking(site)
        site.nodes.sort { left, right -> left.id <=> right.id }
        site.nodes.each { node ->
            validate(node, site)
        }
        boolean stuffRemoved = siteService.save(site)

        logService.gm(LogAction.GM_OBSOLETE_SCAN_OR_RUN, "Site ${site.id} saved from editor, new site generation created, existing scans or runs deleted.", site.id)
        return stuffRemoved
    }

    void fillInUid(Site site) {
        if (site.uid == null) {
            site.uid = IdUtil.createId("site")
        }
    }

    void validate(Site site) {
        if (!site.name) {
            throw new SiteValidationException("Site name is mandatory");
        }
        if (!site.gmName) {
            throw new SiteValidationException("SL name is mandatory");
        }
        if (!site.startNodeNetworkId) {
            throw new SiteValidationException("Start node ID is mandatory");
        }
        def startNodeFound = site.nodes.find { node ->
            node.services[0].data['networkId'] == site.startNodeNetworkId
        }
        if (!startNodeFound) {
            throw new SiteValidationException("Start node invalid. There is no node with network id '${site.startNodeNetworkId}'.");
        }

        if (!site.hackTime) {
            throw new SiteValidationException("Trace time is mandatory.")
        }

        if (site.type.isTemplate() && site.type != SiteType.TEMPLATE_TRACER) {

            def textServices = findSerivcesOfType(site, ServiceType.TEXT)
            def missionDbs = textServices.findAll{ it.data["text"] == "MISSION" }
            if ( missionDbs.size() != 1 ) {
                throw new SiteValidationException("A mission template site must have exactly one Text service (Data vault) with Hacked text: MISSION .")
            }
        }

        if (site.type == SiteType.TEMPLATE_TRACER) {
            def traceLogServices = findSerivcesOfType(site, ServiceType.TRACE_LOGS)
            if (traceLogServices.size() != 1) {
                throw new SiteValidationException("A tracer template site must have exactly one Trace log (t-logs) service .")
            }
        }

        checkNetworkIdsUnique(site)

        timeParseUtil.parseToMillis(site.hackTime, "Trace time invalid, ")
    }

    void checkNetworkIdsUnique(Site site) {
        Set idsUsed = new HashSet<String>()

        findSerivcesOfType(site, ServiceType.OS).forEach{ service ->
            String networkId = service.data[OsService.DATA_NETWORK_ID]
            if (idsUsed.contains(networkId)) {
                throw new SiteValidationException("Network ID ${networkId} is present multiple times");
            }
            idsUsed.add(networkId)
        }
    }

    List<NodeService> findSerivcesOfType(Site site, ServiceType type) {
        def services = new LinkedList<NodeService>()
        site.nodes.forEach{ node ->
            node.services.forEach{ service ->
                if (service.type == type) {
                    services.add(service)
                }
            }
        }
        return services
    }

    void validateScanBlocking(Site site) {
        site.scannable = true
        site.nodes.each { node ->
            node.services.each { service ->
                if (service.type == ServiceType.SCAN_BLOCKER) {
                    def networkId = node.services[0].data['networkId']
                    if ( networkId == site.startNodeNetworkId) {
                        site.scannable = false;
                    }
                    else {
                        throw new RuntimeException("A scan blocker can only reside in the start node, found on in node: ${networkId}")
                    }
                }
            }
        }
    }


    def validate(SiteNode node, Site site) {
        node.services.each { service ->
            service.type.validate(service, node, site)
        }
    }

}