package org.n_is_1._attack_vector.services.editor

/**
 * Marker class so that these exceptions can be exempted from logging.
 */
class SiteValidationException extends RuntimeException {

    SiteValidationException(String message) {
        super(message)
    }
}