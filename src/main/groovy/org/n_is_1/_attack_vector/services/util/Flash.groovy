package org.n_is_1._attack_vector.services.util

import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Utility class for setting a flash message using flash attributes via Spring RedirectAttributes
 */
class Flash {

    static void success(String message, RedirectAttributes redirectAttributes) {
        Flash.message(message, "success", redirectAttributes)
    }

    static void warn(String message, RedirectAttributes redirectAttributes) {
        Flash.message(message, "warn", redirectAttributes)
    }

    static void error(String message, RedirectAttributes redirectAttributes) {
        Flash.message(message, "error", redirectAttributes)
    }

    static void message(String message, String type, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("flashMessage", message)
        redirectAttributes.addFlashAttribute("flashType", type)
    }

}
