package org.n_is_1._attack_vector.services.util

import org.n_is_1._attack_vector.model.admin.User
import org.springframework.security.core.context.SecurityContextHolder

/**
 * Utility class for retrieving the session user and managing it
 */
class UserUtil {

    public static User getUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication()?.getPrincipal();
        if (principal && principal instanceof User) {
            return principal
        }
        else {
            return null
        }
    }
}
