package org.n_is_1._attack_vector.services.util

import org.n_is_1._attack_vector.model.site.UserInputException

/**
 * Class for helping other classes parse config data
 */
class ConfigUtil {

    List<String> optionalFindCommaSeparatedSet(Map<String, String> input, String key) {
        String stringValue = getStringValueOptional(input, key)
        if (!stringValue) {
            return []
        }

        def rawParts = stringValue.split(",")
        List<String> parts = []
        rawParts.each { raw ->
            String clean = raw.trim()
            if (clean) {
                parts.add(clean)
            }
        }

        return parts
    }

    int findInt(Map<String, String> input, String key, int min, int max) {
        String stringValue = getStringValue(input, key)
        try {
            int value = Integer.parseInt(stringValue)
            if (value < min) {
                throw new UserInputException("Value '${value}' for '${key}', is too low, min: ${min}.")
            }
            if (value > max) {
                throw new UserInputException("Value '${value}' for '${key}', is too high, max: ${max}.")
            }
            return value
        }
        catch( NumberFormatException e) {
            throw new UserInputException("'${key}' must be a whole number (rejected: '${stringValue}')")
        }

    }

    String getStringValueOptional(Map<String, String> input, String key) {
        String stringValue = input.get(key)
        if (!stringValue || !stringValue.trim()) {
            return null
        }
        stringValue.trim()
    }

    private String getStringValue(Map<String, String> input, String key) {
        String stringValue = input.get(key)
        if (!stringValue || !stringValue.trim()) {
            throw new UserInputException("Cannot find value for '${key}'.")
        }
        stringValue.trim()
    }

    String findStringWithDefault(Map<String, String> input, String key, String defaultValue) {
        String value = getStringValueOptional(input, key)
        return (value) ? value: defaultValue
    }

    boolean parseBoolean(String input, String field) {
        def normalized = input.trim().toLowerCase()
        if ("true".equals(normalized)) {
            return true
        }
        if ("false".equals(normalized)) {
            return false
        }
        throw new UserInputException("Value '${input}' should be either 'true' or 'false' for parameter ${field}")
    }
}
