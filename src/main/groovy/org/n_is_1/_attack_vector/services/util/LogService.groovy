package org.n_is_1._attack_vector.services.util

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.admin.log.LogLevel
import org.n_is_1._attack_vector.model.admin.log.LogRecord
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.repo.LogRecordRepo
import org.n_is_1._attack_vector.services.user.model.LoginResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import static org.n_is_1._attack_vector.model.admin.log.LogLevel.*

/**
 * Specific logger that stores logs in the database
 */
@Service
class LogService {

    @Autowired LogRecordRepo repo

    void init() {
        // this service does not maintain in internal cache of the log records, so there is nothing to init.
    }

    void debug(LogAction action, String message, String siteId = null, String... additionalIds) {
        createLogRecord(DEBUG, action, message, siteId, additionalIds)
    }


    void actionForRun(LogAction action, String message, HackingRun run) {
        this.action(action, message, run.site.id, run.runId, run.generation.id, run.scan.id)
    }

    void action(LogAction action, String message, String siteId = null, String... additionalIds) {
        createLogRecord(ACTION, action, message, siteId, additionalIds)
    }


    void eventForRun(LogAction action, String message, HackingRun run) {
        event(action, message, run.site.id, run.runId, run.generation.id, run.scan.id)
    }

    void event(LogAction action, String message, String siteId = null, String... additionalIds) {
        createLogRecord(EVENT, action, message, siteId, additionalIds)
    }

    void highlightForRun(LogAction action, String message, HackingRun run) {
        highlight(action, message, run.site.id, run.runId, run.generation.id, run.scan.id)
    }

    void highlight(LogAction action, String message, String siteId = null, String... additionalIds) {
        createLogRecord(HIGHLIGHT, action, message, siteId, additionalIds)
    }


    void error(String message, String siteId = null, String... additionalIds) {
        createLogRecord(ERROR, LogAction.ERROR, message, siteId, additionalIds)
    }

    void gm(LogAction action, String message, String siteId = null, String... additionalIds) {
        createLogRecord(GM, action, message, siteId, additionalIds)
    }

    // -------------------------

    def getLogs(LogLevel level) {
        List<LogRecord> records = getLogsInternal(level)

        records.each { it.displayTime = new DateTime(it.timestamp).toString('yyyy-MM-dd HH:mm:ss')}


    }
    def getLogsInternal(LogLevel level) {
        switch(level) {
            case DEBUG: return repo.findByLevelIn([DEBUG, ERROR])
            case ACTION: return repo.findByLevelIn([ACTION, EVENT, HIGHLIGHT])
            case EVENT: return repo.findByLevelIn([EVENT, HIGHLIGHT])
            case HIGHLIGHT: return repo.findByLevelIn([ HIGHLIGHT])
            case ERROR: return repo.findByLevelIn([ERROR])
            case GM: return repo.findByLevelIn([GM])
        }
    }

    // -------------------------

    LogRecord createLogRecord(LogLevel level, LogAction action, String message, String siteId, String[] additionalIds) {

        def user = UserUtil.getUser()
        def now = DateTime.now().millis

        String generationId = parseAdditionIds("gen", additionalIds)
        String scanId = parseAdditionIds("scan", additionalIds)
        String runId  = parseAdditionIds("run", additionalIds)
        String serviceId = findServiceId(additionalIds)

        def record = new LogRecord(
                userId: user?.id,
                timestamp: now,
                level: level,
                action: action,
                message: message,
                siteId: siteId,
                generationId: generationId,
                scanId: scanId,
                runId: runId,
                serviceId: serviceId
        )
        repo.save(record)
    }

    String parseAdditionIds(String indicator, String[] input) {
        input.find { it.startsWith(indicator)}
    }

    String findServiceId(String[] input) {
        input.find { it.isNumber()}
    }

    void purge() {
        repo.deleteAll()
    }

}
