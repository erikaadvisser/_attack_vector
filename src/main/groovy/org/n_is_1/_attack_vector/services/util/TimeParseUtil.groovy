package org.n_is_1._attack_vector.services.util

import org.n_is_1._attack_vector.model.site.UserInputException

/**
 * Class to help time of the format: "12:44" to indicate 12 hours and 44 minutes.
 */
class TimeParseUtil {

    long parseToMillis(String timeInput, String errorText) {
        try {
            int colonIndex = timeInput.indexOf(":")
            String minutesInput = timeInput.substring(0, colonIndex)
            String secondsInput = timeInput.substring(colonIndex+1)

            int minutes = Integer.parseInt(minutesInput)
            int seconds = Integer.parseInt(secondsInput)

            int totalSeconds = 60 * minutes + seconds
            return (totalSeconds * 1000)
        }
        catch (UserInputException rethrow) {
            throw rethrow;
        }
        catch (Exception ignore) {
            throw new UserInputException("${errorText} failed to parse time input '${timeInput}', use 'minutes:seconds', for instance: '15:0'")
        }
    }

    String parseToTime(long millis) {
        int seconds = millis / 1000
        int timeMinutes = seconds / 60
        int timeSeconds = seconds % 60

        return "${timeMinutes}:${timeSeconds}"
    }
}
