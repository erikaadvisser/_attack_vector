package org.n_is_1._attack_vector.services.util

import org.springframework.stereotype.Service

/**
 * Central place for managing randomness. Useful for testing.
 *
 * For convenience, mimics the most used methods of java.util.Random.
 */
@Service
class RandomService {

    Random random = new Random()


    int nextInt(int upperBound) {
        return random.nextInt(upperBound)
    }

    boolean nextBoolean() {
        return random.nextBoolean()
    }

    void reset() {
//        random = new Random(4)
    }
}
