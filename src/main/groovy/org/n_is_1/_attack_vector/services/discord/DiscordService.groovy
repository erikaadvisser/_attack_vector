package org.n_is_1._attack_vector.services.discord


import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.PrivateChannel
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.utils.cache.CacheFlag
import org.n_is_1._attack_vector.model.admin.BotConfiguration
import org.n_is_1._attack_vector.repo.DiscordRepo
import org.n_is_1._attack_vector.services.util.IdUtil
import org.n_is_1._attack_vector.test.MessageListener
import org.n_is_1._attack_vector.web.model.DiscordConfiguration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import javax.security.auth.login.LoginException

@Service
class DiscordService {

    Logger log = LoggerFactory.getLogger(DiscordService)

    @Autowired DiscordRepo repo
    @Autowired ChatService chatService

    TextChannel reportChannel
    BotConfiguration currentConfiguration = null
    JDA jda = null


    @PostConstruct
    void setup() throws LoginException, InterruptedException {

        BotConfiguration activeConfiguration = repo.findAll().find { it.active }

        if (activeConfiguration?.channelId == "-") {
            return // previous generation of configuration used different property names.
        }

        if (activeConfiguration) {
            configureBot(activeConfiguration)
        }
    }

    public String activate(String id) {
        BotConfiguration config = repo.findOne(id)
        return configureBot(config)
    }

    public Iterable<BotConfiguration> getAll() {
        return repo.findAll()
    }

    private String configureBot(BotConfiguration newConfiguration) {
        if (jda != null) {
            jda.shutdownNow()
        }

        jda = JDABuilder
                .createDefault(newConfiguration.botToken, GatewayIntent.DIRECT_MESSAGES)
                .disableCache(CacheFlag.VOICE_STATE, CacheFlag.EMOTE)
                .build()
        jda.awaitReady()

        jda.addEventListener(new DiscordMessageListener(newConfiguration.pmUserId, chatService));


        reportChannel = jda.getTextChannelById(newConfiguration.channelId)
        if (reportChannel == null) {
            throw new RuntimeException("Discord configuration problem: no channel found for channel-id: ${newConfiguration.channelId} ( ${newConfiguration.channelName} )")
        }

        if (currentConfiguration != null) {
            currentConfiguration.active = false
            repo.save(currentConfiguration)
        }
        newConfiguration.active = true
        repo.save(newConfiguration)
        currentConfiguration = newConfiguration

        return "Configuration activated for: ${newConfiguration.channelName} / ${newConfiguration.pmUserName}"

    }


    String postMessageToChannel(String message) {
        if (currentConfiguration && reportChannel) {
            try {
                reportChannel.sendMessage(message).queue()
                log.info("Posted on Discord:\n" + message)
                return null
            }
            catch (Exception problem) {
                log.error("Failed to send message to discord: ${message}", problem)
                return problem.getMessage()
            }
        } else {
            log.error("Failed to send message to discord: ${message}")
            return "No active configuration (or it is invalid)"
        }
    }

    void sendPrivateMessage(String message) {
        if (currentConfiguration && currentConfiguration.pmUserId) {
            User user = jda.getUserById(currentConfiguration.pmUserId)
            if (user == null) {
                user = jda.retrieveUserById(currentConfiguration.pmUserId).complete()
            }
            user.openPrivateChannel().queue({ channel ->
                channel.sendMessage(message).queue()
            })
        }
    }

    def addConfiguration(DiscordConfiguration config) {
        String id = IdUtil.createId("bot")

        BotConfiguration botConfig = new BotConfiguration(
                id: id,
                botToken: config.botToken,
                channelId: config.botChannel,
                channelName: config.description,
                active: false,
                pmUserId: config.userId,
                pmUserName: config.userName
        )

        repo.save(botConfig)
    }

    public String remove(String id) {
        BotConfiguration botConfig = repo.findOne(id)
        repo.delete(botConfig)
        if (currentConfiguration?.id == id) {
            currentConfiguration = null
            configureBot()
            return "Removed active configuration, Discord disabled (removed: ${botConfig.channelName} / ${botConfig.pmUserName})"
        }
        return "Removed configuration: ${botConfig.channelName} / ${botConfig.pmUserName}"

    }
}
