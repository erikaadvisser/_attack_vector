package org.n_is_1._attack_vector.services.discord

import net.dv8tion.jda.api.entities.ChannelType
import net.dv8tion.jda.api.entities.MessageType
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter


class DiscordMessageListener extends ListenerAdapter {

    private String pmUserId
    private ChatService chatService

    DiscordMessageListener(userId, ChatService chatService) {
        this.pmUserId = userId
        this.chatService = chatService

    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event)
    {
        if (event.author.bot || event.author.system || !event.isFromType(ChannelType.PRIVATE) ) {
            // Don't respond to yourself or system messages or non private messages
            return
        }

        String userId = event.getAuthor().getId();
        if (pmUserId != userId) {
            // Ignoring message from wrong user
            return
        }

        if (event.message.type != MessageType.DEFAULT) {
            // ignoring non message events, such as pinning messages to top
            return
        }

        def message = event.message.contentDisplay
        def user = event.author
        chatService.processGmMessage(message, user)

    }


}
