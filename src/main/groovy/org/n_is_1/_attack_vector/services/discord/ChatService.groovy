package org.n_is_1._attack_vector.services.discord

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.chat.ChatMessage
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.repo.ChatRepo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class ChatService {

    private Logger log = LoggerFactory.getLogger(DiscordService)

    private DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern("HH:mm:ss")

    @Autowired private ChatRepo repo
    @Autowired private DiscordService discordService

    List<ChatMessage> getMessages() {
        return repo.findAll().sort {it.timestamp }
    }

    void storeMessage(String message, String authorName) {
        def chatMessage = new ChatMessage(
                message: message,
                authorName: authorName,
                timeString: LocalDateTime.now().format(timestampFormatter),
                timestamp: DateTime.now().millis
        )
        repo.save(chatMessage)
    }

    void addPlayerMessage(String message, User hacker, Site site) {
        storeMessage(message, hacker.loginName)
        def messageForGM = "```yaml\n${hacker.loginName}: ${message} # ${site.id}\n```"
        discordService.sendPrivateMessage(messageForGM)
    }

    void purge() {
        repo.deleteAll()
    }


    def void processGmMessage(String message, net.dv8tion.jda.api.entities.User user) {
        if (!message.startsWith(".")) {
            storeMessage(message, null)
            return
        }
        if (message.startsWith(".clear")) {
            repo.deleteAll();
            discordService.sendPrivateMessage("Bot: Chat history cleared.");
            return
        }
        if (message.startsWith(".history")) {
            discordService.sendPrivateMessage(chatLog())
            return
        }
        if (message.startsWith(".undo")) {
            def messages = getMessages()
            if (messages.isEmpty()) {
                discordService.sendPrivateMessage("Bot: No messages to undo.")
                return
            }
            def lastMessage = messages.last()
            if (lastMessage.authorName != null) {
                discordService.sendPrivateMessage("Bot: Last message not from you but from player.")
                return
            }
            repo.delete(lastMessage)
            discordService.sendPrivateMessage("Bot: Removed last message. New chat history: \n\n")
            discordService.sendPrivateMessage(chatLog())
            return
        }
        discordService.sendPrivateMessage("Bot: any message starting with a . is a command to me.\n" +
                "I know the following commands:\n" +
                ".clear  <- clear all messages\n" +
                ".history <- repeat chat history\n" +
                ".undo <- remove last message _you_ sent")

    }

    def String chatLog() {
        def messages = getMessages()
        if (messages.isEmpty()) {
            return "(No messages, chat history empty)"
        }
        messages.findAll({ it.authorName == null })
                .forEach({ it.authorName = "you" })
        def messageStrings = messages.collect {"${it.timeString} ${it.authorName}: ${it.message}"}
        return "```yaml\n" + messageStrings.join("\n") + "\n```"

    }
}
