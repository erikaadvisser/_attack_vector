package org.n_is_1._attack_vector.services.admin.model

import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.admin.log.LogRecord
import org.n_is_1._attack_vector.model.run.HackingMission
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteGeneration
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.model.hacker.HackerScanLink
import org.n_is_1._attack_vector.services.scripts.model.HackingScript

/**
 * Represents a backup of the entire game world as far as it is present in the database
 */
class Backup {

    /** Useful for when restoring backups of an older format */
    String version = "1.00"

    /** From which environment was this backup made */
    String environment

    /** Local time at the server */
    String backupTime


    List<User> users = []
    List<Site> sites = []
    List<HackingScript> scripts = []
    List<SiteGeneration> generations = []
    List<SiteScan> scans = []
    List<HackerScanLink> links = []
    List<LogRecord> logRecords = []
    List<HackingMission> missions = []
}
