package org.n_is_1._attack_vector.services.admin

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.admin.log.LogRecord
import org.n_is_1._attack_vector.model.run.HackingMission
import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteGeneration
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.repo.ChatRepo
import org.n_is_1._attack_vector.repo.HackerScanLinkRepo
import org.n_is_1._attack_vector.repo.HackingScriptRepo
import org.n_is_1._attack_vector.repo.LogRecordRepo
import org.n_is_1._attack_vector.repo.MissionRepo
import org.n_is_1._attack_vector.repo.SiteGenerationRepo
import org.n_is_1._attack_vector.repo.SiteRepo
import org.n_is_1._attack_vector.repo.SiteScanRepo
import org.n_is_1._attack_vector.repo.UserRepo
import org.n_is_1._attack_vector.services.admin.model.Backup
import org.n_is_1._attack_vector.model.hacker.HackerScanLink
import org.n_is_1._attack_vector.services.discord.ChatService
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.user.HackerScanService
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.util.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

/**
 * Service for performing backups and restores
 */
@Service
class BackupService {

    @Autowired SiteService siteService
    @Autowired HackerScanService hackerScanService
    @Autowired ScriptService scriptService
    @Autowired UserService userService
    @Autowired LogService logService
    @Autowired MissionService missionService
    @Autowired ChatService chatService

    @Autowired HackerScanLinkRepo hackerScanLinkRepo
    @Autowired HackingScriptRepo scriptRepo
    @Autowired LogRecordRepo logRecordRepo
    @Autowired SiteGenerationRepo siteGenerationRepo
    @Autowired SiteRepo siteRepo
    @Autowired SiteScanRepo siteScanRepo
    @Autowired UserRepo userRepo
    @Autowired MissionRepo missionRepo

    @Value("#{systemEnvironment['ENVIRONMENT']}") String environment


    byte[] generateBackup() {
        def backup = new Backup()

        backup.environment = environment
        backup.backupTime = DateTime.now().toString()

        backup.users.addAll(  userRepo.findAll() )
        backup.scripts.addAll(  scriptRepo.findAll() )
        backup.sites.addAll( siteRepo.findAll() )
        backup.generations.addAll( siteGenerationRepo.findAll() )
        backup.scans.addAll( siteScanRepo.findAll() )
        backup.links.addAll( hackerScanLinkRepo.findAll() )
        backup.logRecords.addAll( logRecordRepo.findAll() )
        backup.missions.addAll( missionRepo.findAll() )

        String backupAsString = JsonOutput.prettyPrint(JsonOutput.toJson(backup))

        def byte_out = new ByteArrayOutputStream()
        def zip_out = new ZipOutputStream(byte_out)
        zip_out.putNextEntry(new ZipEntry("all.json"))
        def writer = new OutputStreamWriter(zip_out, "UTF-8")

        writer.write(backupAsString)
        writer.close()

        return byte_out.toByteArray()
    }

    def restore(MultipartFile file) {
        String json = extractFileToJson(file)
        inflateAndStore(json)
    }

    void purge() {
        hackerScanService.purge()
        scriptService.purge()
        siteService.purge()
        userService.purge()
        logService.purge()
        missionService.purge()
        chatService.purge();
    }


    // --------------------------- --------------------------- --------------------------- ------------------------ //


    private String extractFileToJson(MultipartFile file) {
        def zip_in = new ZipInputStream(file.getInputStream())
        zip_in.nextEntry
        def reader = new InputStreamReader(zip_in, "UTF-8")

        String json = reader.text
        reader.close()
        return json
    }

    void inflateAndStore(String json) {
        def slurper = new JsonSlurper()
        def backupAsMap = slurper.parseText(json)

        purge()

        inflateUsers(backupAsMap.users)
        userService.init()

        inflateScripts(backupAsMap.scripts)
        scriptService.init()

        inflateLogRecords(backupAsMap.logRecords)
        logService.init()

        inflateSites(backupAsMap.sites)
        inflateGenerations(backupAsMap.generations)
        inflateScans(backupAsMap.scans)
        siteService.init()

        inflateLinks(backupAsMap.links)
        hackerScanService.init()

        if (backupAsMap.hasProperty("missions")) {
            inflateMissions(backupAsMap.missions)
            missionService.init()
        }
    }

    void inflateMissions(def jsonMissions) {
        jsonMissions.each {
            def mission = new HackingMission(it)
            missionRepo.save(mission)
        }

    }
// --------------------------- //

    void inflateUsers(def jsonUsers) {
        jsonUsers.each {
            def user = new User(it)
            userRepo.save(user)
        }
        println "Restored ${jsonUsers.size()} users."
    }

    // --------------------------- //

    void inflateScripts(def jsonScripts) {
        jsonScripts.each { jsonScript ->
            def script = new HackingScript(jsonScript)
            scriptRepo.save(script)
        }
        println "Restored ${jsonScripts.size()} scripts"
    }

    // --------------------------- //

    void inflateLogRecords(def jsonLogRecords) {
        jsonLogRecords.each { jsonRecord ->
            def record = new LogRecord(jsonRecord)
            logRecordRepo.save(record)
        }
    }

    // --------------------------- //

    void inflateSites(def jsonSites) {
        jsonSites.each { jsonSite ->
            Site site = inflateSite(jsonSite)
            siteRepo.save(site)
        }
        println "Restored ${jsonSites.size()} sites"
    }

    /**
     * The Json object looks like the real deal, but is filled with strings and has no methods.
     * Need to transfer the properties into a real object.
     */
    Site inflateSite(def jsonSite) {
        def site = new Site(jsonSite);
        site.nodes = []

        jsonSite.nodes.each { jsonNode ->
            def node = inflateNode(jsonNode)
            site.nodes.add(node)
        }
        return site
    }

    SiteNode inflateNode(def jsonNode) {
        def node = new SiteNode(jsonNode)
        node.services = []

        jsonNode.services.each { jsonService ->
            def service = new NodeService(jsonService)
            node.services.add(service)
        }

        return node
    }

    // --------------------------- //

    void inflateGenerations(def jsonGenerations) {
        jsonGenerations.each { Map jsonGeneration ->
            if (jsonGeneration.containsKey("generationId")) {
                jsonGeneration.remove("generationId")
            }
            def generation = new SiteGeneration(jsonGeneration)
            siteGenerationRepo.save(generation)
        }
    }

    // --------------------------- //

    void inflateScans(Object jsonScans) {
        jsonScans.each { jsonScan ->
            def scan = new SiteScan(jsonScan)
            siteScanRepo.save(scan)
        }
    }

    // --------------------------- //

    void inflateLinks(def jsonLinks) {
        jsonLinks.each { jsonLink ->
            def link = new HackerScanLink(jsonLink)
            hackerScanLinkRepo.save(link)
        }
    }
}
