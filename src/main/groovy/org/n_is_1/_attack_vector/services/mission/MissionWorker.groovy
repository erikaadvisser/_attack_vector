package org.n_is_1._attack_vector.services.mission

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.run.HackingMission
import org.n_is_1._attack_vector.services.run.HackingRunService

class MissionWorker implements Runnable {

    private def SAFETY_PERIOD = 1000 * 5l

    HackingMission mission
    HackingRunService hackingRunService
    MissionService missionService

    boolean deleted = false
    boolean manuallyEnded = false
    boolean ended = false

    @Override
    void run() {
        while (active() ) {
            Thread.sleep(1000)
        }
        processMissionEnd()
    }


    void processMissionEnd() {
        ended = true
        if (deleted) {
            return
        }
        missionService.processMissionEnd(mission, this)
    }
    boolean active() {
        if (deleted || manuallyEnded) {
            return false
        }
        if (DateTime.now().millis < mission.endTimeStamp + SAFETY_PERIOD) {
            return true
        }
        return hackingRunService.activeRunsForMission(mission).size() > 0
    }
}
