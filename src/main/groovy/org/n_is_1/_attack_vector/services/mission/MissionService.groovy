package org.n_is_1._attack_vector.services.mission

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.run.HackingMission
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.*
import org.n_is_1._attack_vector.repo.MissionRepo
import org.n_is_1._attack_vector.services.discord.DiscordService
import org.n_is_1._attack_vector.services.ice.passwords.model.Faction
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.util.IdUtil
import org.n_is_1._attack_vector.services.util.RandomService
import org.n_is_1._attack_vector.web.model.MissionParameters
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

import static org.n_is_1._attack_vector.model.site.ServiceType.REMOTE_TRACER
import static org.n_is_1._attack_vector.model.site.ServiceType.RESOURCE
import static org.n_is_1._attack_vector.model.site.SiteType.TEMPLATE_TRACER

@Service
class MissionService {

    Logger log = LoggerFactory.getLogger(MissionService)


    @Autowired SiteService siteService
    @Autowired RandomService random
    @Autowired MissionRepo missionRepo
    @Autowired UserService userService
    @Autowired DiscordService discordService
    @Autowired HackingRunService hackingRunService


    private def rewardDescriptions = new LinkedList<RewardDescription>()
    private def zeroRewardDescription = new RewardDescription(serviceName: "Data vault", description: "nothing of value.")

    private Map<String, HackingMission> missionsById = [:]
    private List<MissionWorker> workers = new LinkedList<>()


    HackingMission getById(String id) {
        return missionsById[id]
    }

    @PostConstruct
    void afterPropertiesSet() throws Exception {
        getClass().getClassLoader().getResourceAsStream("mission/resourceRewardDescriptions.txt").eachLine { line ->
            String[] parts = line.split("=")
            def rewardDescription = new RewardDescription()
            rewardDescription.serviceName = parts[0]
            rewardDescription.description = parts[1]
            rewardDescriptions.add(rewardDescription)
        }
        log.info "Loaded: ${rewardDescriptions.size()} reward descriptions."
    }

    @PostConstruct
    void init() {
        def now = DateTime.now().millis
        missionRepo.findAll().each { mission ->
            missionsById[mission.id] = mission

            if (mission.endTimeStamp > now) {
                startMissionWorker(mission)
            }

        }
        log.info "Loaded: ${missionsById.size()} missions."
    }

    private void startMissionWorker(HackingMission mission) {
        def missionWorker = new MissionWorker()
        missionWorker.mission = mission
        missionWorker.missionService = this
        missionWorker.hackingRunService = hackingRunService

        new Thread(missionWorker).start()
        workers.add(missionWorker)
    }

    void updateMissionForRun(HackingMission mission, Site site, User user) {
        if (site.type == TEMPLATE_TRACER) {
            mission.hackersTraceSites.add(user.id)
        }
        else {
            mission.hackersTargetSite.add(user.id)
        }

        save(mission)
    }

    boolean hasMissionEnded(String missionId) {
        def mission = getById(missionId)
        def missionEnd = new DateTime(mission.endTimeStamp)
        // Don't allow new runs if the hacker could never do anything before the mission end
        def missionSoftEnd = missionEnd.minusSeconds(30)
        return DateTime.now().isAfter(missionSoftEnd)
    }

    def addTrace(HackingRun run, NodeService traceService) {
        def mission = run.mission
        def traceSiteId = traceService.data["siteId"]
        def hackerId = run.hackerId
        addTrace(traceSiteId, mission, hackerId)
    }

    void addTrace(String traceSiteId, HackingMission mission, String hackerId) {
        def traceSite = siteService.getSiteById(traceSiteId)
        if (mission.traces[traceSite.uid] == null) {
            mission.traces[traceSite.uid] = new HashSet<String>()
        }
        mission.traces[traceSite.uid].add(hackerId)
        save(mission)
    }

    String removeTraceLogs(HackingRun run) {
        def traceSiteId = run.site.uid
        def mission = run.mission
        Set<String> tracedHackerIds = mission.traces[traceSiteId]
        if (traceSiteId == null || tracedHackerIds.isEmpty()) {
            return "∦01 ↠ No recent traces found"
        }
        mission.traces[traceSiteId] = new HashSet<String>()

        def traces = ""

        tracedHackerIds.forEach{ hackerId ->
            def hacker = userService.findUserById(hackerId)
            if (traces != "") {
                traces += ", "
            }
            traces += hacker.loginName
        }

        save(mission)

        return "⊿00 ↠ Deleted traces of: " + traces
    }

    String hackText(NodeService service, HackingRun run) {
        String text = service.data['text']
        if ( text == "MISSION") {
            completeMission(run)
            return run.mission.info
        }
        return text
    }

    private void completeMission(HackingRun run) {
        def mission = run.mission
        mission.goalReached = true
        mission.hackersWithMissionInfo.add(run.hackerId)

        save(mission)
    }


    void createMission(MissionParameters parameters) {
        parameters.siteId = parameters.siteId.trim()
        parameters.info = parameters.info.trim()

        String missionId = IdUtil.createId("mission")
        Site template = selectTemplate(parameters.difficulty)

        def site = siteService.createFromTemplate(template, parameters.siteId)
        def traceSites = initTraceSites(site, parameters.siteId)

        updatePasswordFactions(site, parameters.faction)
        distributeRewards(site, traceSites, parameters.reward)

        site.hackable = true
        site.missionId = missionId
        siteService.save(site)

        traceSites.forEach{
            it.hackable = true
            it.missionId = missionId
            siteService.save(it)
        }

        def mission = new HackingMission()
        mission.endTimeStamp = calculateEnd(parameters.time)
        mission.traceSiteIds = traceSites.collect { it.id }
        mission.targetSiteId = site.id
        mission.id = missionId
        mission.template = template.id
        mission.difficulty = parameters.difficulty
        mission.faction = parameters.faction
        mission.info = parameters.info
        mission.reward = parameters.reward

        save(mission)
        startMissionWorker(mission)
        notifyMissionStart(mission, parameters)
    }

    void save(HackingMission mission) {
        this.missionsById[mission.id] = mission
        missionRepo.save(mission)
    }

    private void distributeRewards(Site site, List<Site> traceSites, int totalReward) {
        def resourceServices = new LinkedList<NodeService>()
        resourceServices.addAll(site.findAllServicesByType(RESOURCE))
        traceSites.forEach {
            def traceResourceServices = it.findAllServicesByType(RESOURCE)
            resourceServices.addAll(traceResourceServices)

        }
        if (!resourceServices.isEmpty() && totalReward > 0) {
            int serviceCount = resourceServices.size()
            int averageReward = (int) totalReward / serviceCount
            resourceServices.forEach { assignReward(it, averageReward) }
        }
    }

    def assignReward(NodeService service, int averageReward) {
        int reward = calcSingleReward(averageReward)
        service.data["value"] = "" + reward

        def rewardDescription = generateRewardDescription(reward)

        service.data["description"] = "Found: " + rewardDescription.description
        service.data["scanName"] = rewardDescription.serviceName
        service.data["runName"] = rewardDescription.serviceName
    }

    private int calcSingleReward(int averageReward) {
        if (averageReward == 0) {
            return 0
        }
        int deviation = (int)(averageReward / 2)
        int baseReward = averageReward - deviation
        int bonus = random.nextInt(2 * deviation)
        return baseReward + bonus

    }

    private RewardDescription generateRewardDescription(int reward) {
        if (reward == 0) {
            return zeroRewardDescription
        }

        int count = rewardDescriptions.size()
        int index = random.nextInt(count)
        return rewardDescriptions.get(index)
    }

    void updatePasswordFactions(Site site, Faction faction) {
        def passwordIces = site.findAllServicesByType(ServiceType.ICE_PASSWORD)

        passwordIces.forEach{ updateFaction(it, faction)}
    }

    void updateFaction(NodeService service, Faction faction) {
        if (faction == Faction.OTHER) {
            def newFaction = randomFaction()
            updateFaction(service, newFaction)
        }
        else {
            service.data["faction1"] = faction.name()
            service.data["faction2"] = randomFaction().name()
            service.data["faction3"] = randomFaction().name()
        }
    }

    Faction randomFaction() {
        def index = random.nextInt(Faction.values().size() - 1)
        return Faction.values()[index]
    }

    long calculateEnd(int minutes) {
        def end = DateTime.now().plusMinutes(minutes)
        return end.getMillis()
    }


    List<Site> initTraceSites(Site site, String siteId) {
        def traceServices = site.findAllServicesByType(REMOTE_TRACER)

        def traceSites = new LinkedList<Site>()

        traceServices.forEach{ service ->
            def traceTemplate = selectTemplate(TEMPLATE_TRACER)
            def traceSiteId = createTraceSiteId(siteId)
            def traceSite = siteService.createFromTemplate(traceTemplate, traceSiteId)

            traceSites.add(traceSite)
            service.data["siteId"] = traceSiteId
        }

        return traceSites
    }

    private String createTraceSiteId(String siteId) {
        return IdUtil.createId("trace") + "." + siteId
    }

    private Site selectTemplate(SiteType type) {
        def matchingTemplates = siteService.findAllWithType(type)

        if (matchingTemplates.isEmpty()) {
            if (type == TEMPLATE_TRACER) {
                throw new UserInputException("No trace sites found. Mission cannot be generated.")
            }
            else {
                throw new UserInputException("No template found for this difficulty level. Please choose another level.")
            }
        }

        def lowestUses = matchingTemplates.min { it.templateUses }.templateUses
        def candidateTemplates = matchingTemplates.findAll { it.templateUses == lowestUses }

        candidateTemplates.sort{ it.id }
        Site template = candidateTemplates.first()
        template.templateUses += 1
        siteService.save(template)
        return template
    }

    void purge() {
        missionsById = [:]
        missionRepo.deleteAll()
    }

    boolean hackResource(NodeService service, HackingRun run) {
        def mission = run.mission
        if (mission.resourcesHacked.containsKey(service.id)) {
            return false
        }
        mission.resourcesHacked[service.id] = run.hackerId
        save(mission)
        return true
    }

    def getAll() {
        return missionsById.values()
    }

    void purgeMissionsAndSites() {
        purge()
        siteService.purgeMissionSites()
    }

    String hackerDescription(String hackerId) {
        def hacker = userService.findUserById(hackerId)
        return "<${hacker.ocName.replaceAll(" ", "_")} ${hacker.icName.replaceAll(" ", "_")}>"
    }

    void notifyMissionStart(HackingMission mission, MissionParameters parameters) {

        DateTime end = new DateTime(mission.endTimeStamp)
        def endTime = end.toString("HH:mm:ss")

        String report =
                "```xml\n" +
                        "Mission started\n" +
                        "Site id: <${mission.targetSiteId}>\n" +
                        "Ends: <${endTime}> (${parameters.time} minutes)\n" +
                        "Goal: < . ${mission.info} . >\n" +
                        "Difficulty: <${mission.difficulty.difficulty}>\n" +
                        "```"

        discordService.postMessageToChannel(report)
    }

    void processMissionEnd(HackingMission mission, MissionWorker worker) {
        String successText = (mission.goalReached) ? "< = SUCCESS = >" : "<! FAILURE>"
        String textGrabbedBy = "<! No one>"
        if (mission.goalReached) {
            textGrabbedBy = ""
            mission.hackersWithMissionInfo.forEach{ hackerId ->
                textGrabbedBy += hackerDescription(hackerId) + "\n"
            }
        }
        String hackersInvolved = createHackersInvolved(mission)
        String additionalSpoils = createAdditionalSpoils(mission)

        String report =
                "```xml\n" +
                        "Mission report: <${mission.targetSiteId}>\n" +
                        "Mission goal: < . ${mission.info} . >\n" +
                        "\n" +
                        "Mission result: ${successText}\n" +
                        "Target text was grabbed by:\n" +
                        "${textGrabbedBy}\n" +
                        "\n" +
                        "Hackers involved:\n" +
                        "${hackersInvolved}" +
                        "\n" +
                        "Additional spoils: \n" +
                        "${additionalSpoils}" +
                        "```"

        discordService.postMessageToChannel(report)
        workers.remove(worker)
    }

    String createAdditionalSpoils(HackingMission mission) {
        String spoils = ""

        mission.resourcesHacked.keySet().forEach{ serviceId ->
            String hackerId = mission.resourcesHacked[serviceId]
            Site site = siteService.getSiteById(mission.targetSiteId)
            NodeService service = site.findServiceById(serviceId)
            String description = service.data['description']
            String value = service.data['value']

            spoils += hackerDescription(hackerId) + "\t\t<${value} sonure> ${description}\n"

        }
        if (spoils.is()) {
            spoils="<! None>"
        }
        return spoils
    }

    String createHackersInvolved(HackingMission mission) {
        List<String> hackers = new LinkedList<>()
        hackers.addAll(mission.hackersTargetSite)
        hackers.addAll(mission.hackersTraceSites)

        Map<String, Integer> traceCount = new HashMap<>()
        hackers.forEach{ hackerId ->
            traceCount[hackerId] = 0
        }

        mission.traces.values().forEach{ tracedHackerSet ->
            tracedHackerSet.forEach{ tracedHackerId ->
                traceCount[tracedHackerId] ++
            }
        }

        int totalTraceSites = mission.traceSiteIds.size()

        String involved = ""

        hackers.forEach{ hackerId ->
            String traceText
            if (traceCount[hackerId] == 0) {
                traceText = "<not-traced>\t"
            }
            else {
                traceText = "<! traced: ${traceCount[hackerId]}/${totalTraceSites}>\t"
            }
            involved += traceText + hackerDescription(hackerId) + "\n"
        }

        return involved
    }

    Set<String> getHackersInvolved(HackingMission mission) {
        def set = new HashSet<String>()

        def traceSiteHackers = findHackers(mission.hackersTraceSites)
        def targetSiteHackers = findHackers(mission.hackersTargetSite)

        set.addAll(traceSiteHackers)
        set.addAll(targetSiteHackers)

        return set
    }

    Set<String> findHackers(Set<String> hackerIds) {
        def hackers = new HashSet<String>()

        hackerIds.forEach{ hackerId ->
            def hacker = userService.findUserById(hackerId)
            String name = hacker.ocName + " (" + hacker.icName + ")"
            hackers.add(name)
        }

        return hackers
    }

    int removeTracesViaStorm(HackingRun run) {
        def mission = run.mission

        def hackerId = run.hackerId

        int removeCount = 0
        mission.traces.keySet().forEach{ traceSiteId ->
            Set<String> tracedHackers = mission.traces[traceSiteId]
            boolean removed = tracedHackers.remove(hackerId)
            if (removed) {
                removeCount ++
            }
        }

        save(mission)
        return removeCount
    }

    Set<String> determineTracedHackers(HackingMission mission) {
        Set<String> detected = new HashSet<>()

        mission.traces.values().forEach{ Set<String> detectedAtSite ->
            def allAtThisSite = findHackers( detectedAtSite )
            detected.addAll(allAtThisSite)
        }
        return detected
    }

    void detectTimeOut(String runId) {
        HackingRun run = hackingRunService.getRun(runId)
        if (run.mission == null) {
            return
        }
        def mission = run.mission
        def hackerId = run.hackerId

        mission.traceSiteIds.forEach{ traceSiteId ->
            addTrace(traceSiteId, mission, hackerId)
        }
    }
}
