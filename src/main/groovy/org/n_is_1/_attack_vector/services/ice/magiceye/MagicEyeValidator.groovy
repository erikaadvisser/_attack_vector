package org.n_is_1._attack_vector.services.ice.magiceye

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.services.ServiceValidator

/**
 * Validates the config of a Magic Eye puzzle
 */
class MagicEyeValidator extends ServiceValidator {

    def validate_internal(NodeService service, SiteNode node, Site site) {
        MagicEyeService.INSTANCE.create(service.data)
        return null
    }
}
