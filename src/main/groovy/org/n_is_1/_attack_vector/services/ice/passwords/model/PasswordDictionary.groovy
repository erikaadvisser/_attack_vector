package org.n_is_1._attack_vector.services.ice.passwords.model

/**
 *
 */
enum PasswordDictionary {

    L33T(            0, "(l33t)"),
    RANDOM(          1, "4 Human random"),
    CULTURE_AQUILA(  2, "Aquila culture"),
    NAME_AQUILA(     3, "Aquila name"),
    ALL(             4, "All words"),
    COMMON(          5, "Common words"),
    CULTURE_DUGO(    6, "Dugo culture"),
    NAME_DUGO(       7, "Dugo name"),
    CULTURE_EKANESH( 8, "Ekanesh culture"),
    NAME_EKANESH(    9, "Ekanesh name"),
    NUMBER(         10, "Number/Year"),
    CULTURE_PENDZAL(11, "Pendzal culture"),
    NAME_PENDZAL(   12, "Pendzal name"),
    CULTURE_SONA(   13, "Sona culture"),
    NAME_SONA(      14, "Sona name")

    static Map<String, PasswordDictionary> byName

    String name
    int prio

    PasswordDictionary(int prio, String name) {
        this.name = name
        this.prio = prio
        if (byName == null) {
            byName = [:]
        }
        byName[name] = this
    }

    static def fromName(String name) {
        return byName[name]
    }
}
