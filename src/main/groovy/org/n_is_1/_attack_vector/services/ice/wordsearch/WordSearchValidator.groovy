package org.n_is_1._attack_vector.services.ice.wordsearch

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.services.ServiceValidator

/**
 * Validator for Word Search Ice puzzles
 */
class WordSearchValidator extends ServiceValidator {

    def validate_internal(NodeService service, SiteNode node, Site site) {
        WordSearchService.INSTANCE.createPuzzle(service.data)
        return null
    }
}
