package org.n_is_1._attack_vector.services.ice.wordsearch.model

/**
 * Defines the entire map of letters
 */
class LetterMap {

    int sizeX
    int sizeY
    List<LetterLocation> locations = []
    Map<String, LetterLocation> lettersByPosition = new HashMap<>()

    LetterMap(int x, int y) {
        sizeX = x
        sizeY = y
    }

    LetterMap(List<String> rows) {
        sizeX = rows[0].length()
        sizeY = rows.size()
        for (int y = 0; y < sizeY; y += 1) {
            for (int x = 0; x < sizeX; x += 1 ) {
                char c = rows[y].charAt(x)
                add(x,y)
                getLocationAt(x, y).letter = c;
            }
        }
    }

    def add(int x, int y) {
        LetterLocation location = new LetterLocation(x: x, y: y)
        locations.push( location )
        lettersByPosition["${x}:${y}"] = location
    }

    def getLocationAt(int x, int y) {
        return lettersByPosition["${x}:${y}"]
    }

}
