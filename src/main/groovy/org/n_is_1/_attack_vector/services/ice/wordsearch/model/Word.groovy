package org.n_is_1._attack_vector.services.ice.wordsearch.model

/**
 * This class represents a single word in the wordsearch / word search ice
 */
class Word {

    /** number of the word in the sequence of words */
    String order

    String text
    List<LetterLocation> locations = []
    String modifiedText
}
