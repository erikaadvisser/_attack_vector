package org.n_is_1._attack_vector.services.ice.passwords.model

/**
 * These are the level of access that exist in the password hacking ice game.
 */
enum PasswordAccessLevel {

    ENCRYPTED,
    LATENCY,
    VOLUME,
    PRIORITY;

    String name

    PasswordAccessLevel() {
        this.name = this.toString().toLowerCase()
    }

}
