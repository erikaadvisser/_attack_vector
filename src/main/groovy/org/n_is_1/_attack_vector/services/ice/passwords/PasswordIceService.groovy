package org.n_is_1._attack_vector.services.ice.passwords

import org.n_is_1._attack_vector.services.ice.passwords.model.PasswordIceConfig
import org.n_is_1._attack_vector.services.ice.passwords.model.PasswordIcePuzzle
import org.springframework.stereotype.Service

/**
 * Service for creating and managing Password Ice.
 */
@Service
class PasswordIceService {

    static PasswordIceService INSTANCE

    PasswordIceService() {
        INSTANCE = this
    }


    PasswordIcePuzzle createPuzzle(Map<String, String> input) {
        def config = new PasswordIceConfig(input)
        def generator = new PasswordIceGenerator(config)
        def puzzle = generator.generate()
        return puzzle
    }

}
