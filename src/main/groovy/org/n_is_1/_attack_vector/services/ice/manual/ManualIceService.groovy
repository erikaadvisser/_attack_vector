package org.n_is_1._attack_vector.services.ice.manual

import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.ice.manual.model.ManualIceData
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * Manages manual puzzle ice walls
 */
@Service
class ManualIceService {

    static ManualIceService INSTANCE

    @PostConstruct
    def init() {
        INSTANCE = this;
    }

    ManualIceData create(Map<String, String> data) {
        if (!data.solution) {
            throw new UserInputException("Solution is mandatory.")
        }

        if (!data.strength) {
            data.strength = "Unknown"
        }
        return new ManualIceData(
                solution: data.solution,
                hint: data.hint,
                strength: data.strength)
    }

}
