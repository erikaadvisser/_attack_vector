package org.n_is_1._attack_vector.services.ice.manual

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.services.ServiceValidator

/**
 * Validator for Manual ICE
 */
class ManualIceValidator extends ServiceValidator {

    def validate_internal(NodeService service, SiteNode node, Site site) {
        ManualIceService.INSTANCE.create(service.data)
        return null
    }
}
