package org.n_is_1._attack_vector.services.ice.magiceye

import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.ice.magiceye.model.MagicEyeConfig
import org.n_is_1._attack_vector.services.ice.magiceye.model.MagicEyeImage
import org.n_is_1._attack_vector.services.ice.magiceye.model.MagicEyePuzzle
import org.n_is_1._attack_vector.services.util.RandomService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.servlet.ServletContext
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener

/**
 * Service for defining magic eye images for a ice
 */
@Service
class MagicEyeService implements ServletContextListener {

    private static final String IMAGE_ROOT = "/resources/images/ice/magiceye"

    static List<MagicEyeImage> regulars = []
    static List<MagicEyeImage> blanks = []

    static MagicEyeService INSTANCE

    @Autowired RandomService random

    MagicEyeService() {
        INSTANCE = this;
    }

    MagicEyePuzzle create(Map<String, String> data) {
        def config = new MagicEyeConfig(data)

        def puzzle = new MagicEyePuzzle(strength: config.strength)
        int count = 0
        for (; count < config.blankCount; count++) {
            addImageFromSet(puzzle, blanks, ["<blank>"])
        }

//        List<String> tags = config.tags.split(" ")

        List<MagicEyeImage> imageSet = regulars.findAll { image ->
            return (image.tags.containsAll(config.strength))
        }

        if (imageSet.size() < config.imageCount) {
            throw new UserInputException("Not enough images found for strength: ${config.strength}, have ${imageSet.size()}, and need ${config.imageCount - config.blankCount}")
        }

        for (; count < config.imageCount; count++) {
            addImageFromSet(puzzle, imageSet, config.strength)
        }

        puzzle.images.each {
            println "${it.solution} - ${it.location}"
        }

        return puzzle
    }

    MagicEyeImage addImageFromSet(MagicEyePuzzle puzzle, List<MagicEyeImage> imageSet, strength) {
        int attempts = 0;

        while( attempts < 1000) {
            int imageIndex = random.nextInt(imageSet.size())
            def image = imageSet[imageIndex]

            if (!puzzle.images.contains(imageIndex)) {
                puzzle.images.add(image)
                return
            }
        }

        throw new RuntimeException("Tried to find an unused image for 1000 times, but failed. strength: '${strength}'. Image set size: ${imageSet.size()}, images already present in puzzle: ${puzzle.images.size()}")
    }

    @Override
    void contextInitialized(ServletContextEvent contextEvent) {
        try {
            def context = contextEvent.getServletContext()
            loadImages(IMAGE_ROOT, context)
        }
        catch ( Exception e) {
            e.printStackTrace()
        }
    }

    void loadImages(String rootPath, ServletContext context) {
        def paths = context.getResourcePaths(rootPath)
        if (paths) {
            paths.each { path ->
                loadImages(path, context)
            }
        }
        else {
            loadImage(rootPath, context)
        }
    }

    void loadImage(String path, ServletContext context) {
        String location = path.substring(IMAGE_ROOT.length())
        int nameStart = location.lastIndexOf("/")
        String nameWithPng = location.substring(nameStart + 1)
        if (!nameWithPng.endsWith(".png")) {
            println "Found file without .png at end: '${path}'";
            return;
        }
        String solution = nameWithPng.substring(0, nameWithPng.length() - 4 );
        if (solution.startsWith("blank_")) {
            solution = ""
        }

        String locationWithoutName = location.substring(1, location.length() - nameWithPng.length() - 1)
        String[] tags = locationWithoutName.split("/")

        MagicEyeImage puzzle = new MagicEyeImage(
            solution: solution,
            location: path,
            tags: tags
        )
        if (solution) {
            regulars.add(puzzle)
        }
        else {
            blanks.add(puzzle)
        }

    }

    @Override
    void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
