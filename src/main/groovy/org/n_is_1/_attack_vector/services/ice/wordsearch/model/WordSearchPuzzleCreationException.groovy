package org.n_is_1._attack_vector.services.ice.wordsearch.model

/**
 * This exception indicates that the creation of a word search puzzle failed
 */
class WordSearchPuzzleCreationException extends RuntimeException {

    WordSearchPuzzleCreationException(String message) {
        super(message)
    }
}
