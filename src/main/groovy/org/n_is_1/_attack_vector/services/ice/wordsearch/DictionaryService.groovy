package org.n_is_1._attack_vector.services.ice.wordsearch

import org.n_is_1._attack_vector.services.ice.wordsearch.model.WordBin
import org.n_is_1._attack_vector.services.util.RandomService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Stores the words that wordsearch uses, and allows finding appropriate words
 */
@Service
class DictionaryService implements InitializingBean {

    final Logger log = LoggerFactory.getLogger(DictionaryService)

    @Autowired RandomService random

    def wordBins = new HashMap<Integer, WordBin>();

    void afterPropertiesSet() throws Exception {
        getClass().getClassLoader().getResourceAsStream("ice/wordsearch/wordlist-1.txt").eachLine { line ->
            if (line.length() == 0 || line.isEmpty()) {
                return
            }

            def length = line.length();

            if (wordBins[length] == null) {
                wordBins[length] = new WordBin()
            }
            wordBins[length].words.push(line.toUpperCase())
        }

        wordBins.entrySet().forEach { entry ->
            log.debug "${entry.key} - ${entry.value.words.size()} words"
        }
    }

    String findRandomWord(int length) {
        if (wordBins[length] == null) {
            throw new IllegalArgumentException("No words available for length ${length}")
        }

        int size = wordBins[length].words.size()
        int wordIndex = random.nextInt(size)

        return wordBins[length].words[wordIndex]
    }
}
