package org.n_is_1._attack_vector.services.ice.passwords.model

/**
 * A single password Ice puzzle. Contains both requirements and solutions
 */
class PasswordIcePuzzle {

    Map<String, PasswordAccessLevel> available = [:]
    Map<String, Integer> needed = [:]

    List<Faction> factions = []

    String strength

}
