package org.n_is_1._attack_vector.services.ice.magiceye

/**
 *
 */
class RandomHexNumberGenerator {

    static Random random = new Random()
//    static def main(def args) {
//        (1..10).each {
//            random()
//        }
//    }

    static def random() {
        def s = ""
        (1..6).each {
            s = s + addVals()
        }
        println ""
        println "${s.substring(0,5)} ${s.substring(6,11)}"
    }

    static addVals() {
        int i = random.nextInt(256)
        String r = Integer.toHexString(i)
        if (i >= 16) {
            return r
        }
        return "0"+r
    }
}
