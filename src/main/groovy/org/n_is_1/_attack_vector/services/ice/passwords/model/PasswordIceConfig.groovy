package org.n_is_1._attack_vector.services.ice.passwords.model

import groovy.transform.AutoClone
import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.model.site.ice.IceStrength
import org.n_is_1._attack_vector.services.util.ConfigUtil

import static org.n_is_1._attack_vector.services.ice.passwords.model.Faction.OTHER

/**
 * A configuration of an Ice
 */
@AutoClone
class PasswordIceConfig {
    IceStrength strength

    List<Faction> factions = []

    PasswordIceConfig(Map<String, String> input) {
        def util = new ConfigUtil()

        strength = parseStrength(input["strength"].trim())

        Faction faction1 = parseFaction(input['faction1'].trim(), 'faction1')
        factions.add(faction1)

        String faction2Input = util.getStringValueOptional(input, "faction2")
        if (faction2Input) {
            Faction faction2 = parseFaction(input['faction2'].trim(), 'faction2')
            factions.add(faction2)
        }

        String faction3Input = util.getStringValueOptional(input, "faction3")
        if (faction3Input) {
            Faction faction3 = parseFaction(input['faction3'].trim(), 'faction3')
            factions.add(faction3)
        }


    }

    Faction parseFaction(String input, String name) {
        try {
            def faction = Faction.valueOf(input.toUpperCase())
            if ( faction == OTHER ) {
                throw IllegalArgumentException()
            }
            return faction
        }
        catch (Exception e) {
            throw new UserInputException("Input for '${name}' is not understood, value is '${input}', \nshould be one of ${Faction.values()}\n")
        }
    }

    IceStrength parseStrength(String input) {
        try {
            return IceStrength.valueOf(input.toUpperCase())
        }
        catch (Exception e) {
            throw new UserInputException("Input for 'strength' is not understood, value is '${input}', \nshould be one of ${IceStrength.values()}\n")
        }
    }
}
