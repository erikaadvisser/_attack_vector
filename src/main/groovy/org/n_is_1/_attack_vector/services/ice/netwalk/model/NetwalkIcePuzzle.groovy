package org.n_is_1._attack_vector.services.ice.netwalk.model

import org.n_is_1._attack_vector.model.site.ice.IceStrength

class NetwalkIcePuzzle {
    IceStrength strength
    int x
    int y
    boolean wrapping
    String net

    NetwalkIcePuzzle(IceStrength strength, int x, int y, boolean wrapping, String net) {
        this.strength = strength
        this.x = x
        this.y = y
        this.wrapping = wrapping
        this.net = net
    }

    String toJson() {
        return "{\"x\": ${x}, \"y\": ${y}, \"wrapping\": ${wrapping}, \"net\": \"${net}\"}"
    }
}
