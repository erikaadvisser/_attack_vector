package org.n_is_1._attack_vector.services.ice.wordsearch.model

/**
 * This class represents the location of a letter on the word search map.
 */
class LetterLocation {

    int x
    int y

    char letter

    // a letter location can be locked, meaning that it cannot be altered any more
    boolean locked = false
}
