package org.n_is_1._attack_vector.services.ice.wordsearch

/**
 * Utililty that translates a letter into a css style that displays the letter. This way the letter cannot be found with CTRL-F.
 */
class WsUtil {
    private static String specials = '*_{}@() ,\\\"/192';

    static String convert(char c) {
        if (c >= ('A' as char) && c <= ('Z' as char) ||
                c >= ('a' as char) && c <= ('z' as char)) {
            return "ws" + c
        }

        int index = specials.indexOf(c as int)
        if (index == -1) {
            System.err.println("Cannot convert character '" + c + "'")
            throw new RuntimeException("Cannot convert character '" + c + "'")
        }
        return "ws" + index
    }
}
