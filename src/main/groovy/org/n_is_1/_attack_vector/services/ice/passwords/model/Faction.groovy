package org.n_is_1._attack_vector.services.ice.passwords.model

/**
 * Factions
 */
enum Faction {
    AQUILA,
    DUGO,
    EKANESH,
    PENDZAL,
    SONA,
    OTHER

    String capitalized() {
        return this.toString().toLowerCase().capitalize()
    }

}
