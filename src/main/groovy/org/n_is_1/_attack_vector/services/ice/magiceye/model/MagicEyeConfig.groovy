package org.n_is_1._attack_vector.services.ice.magiceye.model

import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.util.ConfigUtil

/**
 * Stores values of magic eye data
 */
class MagicEyeConfig {

    String strength
    int imageCount
    int blankCount

    MagicEyeConfig(Map<String, String> input) {
        def util = new ConfigUtil()

        strength = util.findStringWithDefault(input, "strength", "Unknown").toLowerCase()
        if (strength != "weak" && strength != "average" && strength != "strong") {
            throw new UserInputException("unknown value for strength: '${strength}', use one of: 'weak', 'average', 'strong'.")
        }

        imageCount = util.findInt(input, "images", 1, 30)
        blankCount = 0
    }


}
