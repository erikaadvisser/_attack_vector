package org.n_is_1._attack_vector.services.ice.manual.model

/**
 * Data for manual ice
 */
class ManualIceData {

    /** The solution 'code' found by solving the puzzle, or a random password invented
        by the GM and handed to them when they have finished the puzzle. */
    String solution

    /** Hint for players */
    String hint

    /** Indication for player */
    String strength


}
