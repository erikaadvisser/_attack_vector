package org.n_is_1._attack_vector.services.ice.wordsearch.model

/**
 * This class represents a word search puzzle, a matrix of letters and associated words.
 */
class WordSearchPuzzle {

    List<Word> words = []
    LetterMap map
    String strength


    List<List<Character>> rows = []

    def initForRendering() {
        for (int y = 0; y < map.sizeY; y++) {
            List<Character> row = []
            for (int x = 0; x < map.sizeX; x++) {
                LetterLocation location = map.getLocationAt(x, y)
                if (location != null) {
                    row.push(location.letter)
                }
            }
            rows.push(row)
        }

        map = null
    }


}
