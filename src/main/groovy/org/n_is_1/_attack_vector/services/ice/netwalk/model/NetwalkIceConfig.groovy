package org.n_is_1._attack_vector.services.ice.netwalk.model

import groovy.transform.AutoClone
import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.model.site.ice.IceStrength
import org.n_is_1._attack_vector.services.ice.passwords.model.Faction
import org.n_is_1._attack_vector.services.util.ConfigUtil

import static org.n_is_1._attack_vector.services.ice.passwords.model.Faction.OTHER

@AutoClone
class NetwalkIceConfig {
    public IceStrength strength


    NetwalkIceConfig(Map<String, String> input) {
        strength = parseStrength(input["strength"].trim())
    }

    IceStrength parseStrength(String input) {
        try {
            return IceStrength.valueOf(input.toUpperCase())
        }
        catch (Exception e) {
            throw new UserInputException("Input for 'strength' is not understood, value is '${input}', \nshould be one of ${IceStrength.values()}\n")
        }
    }
}
