package org.n_is_1._attack_vector.services.ice.passwords

import org.n_is_1._attack_vector.model.site.ice.IceStrength
import org.n_is_1._attack_vector.services.ice.passwords.model.*
import org.n_is_1._attack_vector.services.util.RandomService

import static org.n_is_1._attack_vector.services.ice.passwords.model.PasswordDictionary.*

/**
 * This is a one use object that creates a generation of ICE
 */
class PasswordIceGenerator {

    def random = new RandomService()
    def generation = new PasswordIcePuzzle()
    Map<Faction, Integer> passwordsByFaction = [:]

    int totalRequired = 0
    int common = 0
    int obscure  = 0
    int crossFaction  = 0

    // private copy of config, we can modify it.
    PasswordIceConfig config

    // list of access levels that need to be filled with passwords
    List<PasswordAccessLevel> levelsNotFilled = []

    PasswordIceGenerator(PasswordIceConfig config) {
        this.config = config.clone()
    }

    // -------------------------------

    PasswordIcePuzzle generate() {
        processConfig()

        createPasswordsNeeded()
        create_levelsNotFilled()

        generateCommon()
        generateObscure()
        generateCrossFaction()
        generateFaction()


//        generation.needed.each { key, val ->
//            println "${key} = ${val}";
//        }
//        generation.available.each { key, val ->
//            println "${key} = ${val}";
//        }
//        println "\nTotal number of passwords: ${generation.available.size()}"
        return generation
    }

    // -------------------------------

    /** Generate working parameters from the supplied config */
    def processConfig() {
        generation.factions = config.factions;
        generation.strength = config.strength.name
        switch(config.strength) {
            case IceStrength.VERY_WEAK: return configForVeryWeak()
            case IceStrength.WEAK: return configForWeak()
            case IceStrength.AVERAGE: return configForAverage()
            case IceStrength.STRONG: return configForStrong()
            case IceStrength.VERY_STRONG: return configForVeryStrong()
            case IceStrength.IMPENETRABLE: return configForImpenerable()
        }

    }

    /**
     lvl 0: very weak    0-1 min (4/17+ or 425% availability)
     - 4 passwords needed
     - 1 faction (150s) with 8 passwords
     - common (80s) with 4 passwords
     - obscure (95s) with 5 passwords
     - additional factions just add 8 passwords
     - no cross-faction passwords
     */
    def configForVeryWeak() {
        totalRequired = 4
        common = 4
        obscure = 5
        crossFaction = 0

        config.factions.each { faction ->
            passwordsByFaction[faction] = 8
        }
    }

    /** lvl 1: weak         1-2 min  (5/13+ or 260% availability)
     - 5 passwords needed
     - 1 faction (150s) with 5 passwords
     - common (80s) with 4 passwords
     - obscure (95s) with 4 passwords
     - additional factions just add 5 passwords
     - no cross-faction passwords
     */
    def configForWeak() {
        totalRequired = 5
        common = 4
        obscure = 4
        crossFaction = 0

        config.factions.each { faction ->
            passwordsByFaction[faction] = 5
        }
    }

    /** lvl 2: average      2-3 min (5/10 or 200% availability)
     - 5 passwords needed
     - 1 faction (150s) with 4 passwords
     - common (80s) with 3 passwords
     - obscure (95s) with 3 passwords
     - no additional factions
     - no cross-faction passwords
     */
    def configForAverage() {
        if (config.factions.size() == 0) {
            throw new RuntimeException("A password ice with strength Average must have at least one factions, currently has 0.")
        }

        totalRequired = 5
        common = 3
        obscure = 3
        crossFaction = 0

        config.factions.eachWithIndex { faction, index ->
            if (index < 1) {
                passwordsByFaction[faction] = 4
            }
        }
    }

    /**lvl 3: strong       3-5 min (6/9 or 150% availability)
     - 6 passwords needed
     - faction 1 (150s) with 2 passwords
     - faction 2 (150s) with 2 passwords
     - cross faction password: 2 (taken from faction 1 and faction 2)
     - common (80s) with 1 passwords
     - obscure (95s) with 2 passwords
     - additional factions not allowed
     */
    def configForStrong() {
        if (config.factions.size() < 2) {
            throw new RuntimeException("A password ice with strength Strong must have at least two factions, currently has ${config.factions.size()}.")
        }

        totalRequired = 6
        common = 1
        obscure = 2
        crossFaction = 2

        config.factions.eachWithIndex { faction, index ->
            if (index < 2) {
                passwordsByFaction[faction] = 2
            }
        }
    }

    /** lvl 4: very strong  5+ min (8/11 or 138% availability)
     - 8 passwords needed
     - faction 1 (150s) with 2 passwords
     - faction 2 (150s) with 2 passwords
     - cross faction password: 3 (taken from faction 1 and faction 2)
     - common (80s) with 1 passwords
     - obscure (95s) with 3 passwords
     - additional factions not allowed
     */
    def configForVeryStrong() {
        if (config.factions.size() != 2) {
            throw new RuntimeException("A password ice with strength Very Strong must have at least three factions, currently has ${config.factions.size()}.")
        }

        totalRequired = 8
        common = 1
        obscure = 3
        crossFaction = 3

        config.factions.eachWithIndex { faction, index ->
            if (index < 3) {
                passwordsByFaction[faction] = 2
            }
        }
    }

    /**
     lvl 5: impenetrable 10+ min (11/13 or 118% availability)
     - 11 passwords needed
     - faction 1 (150s) with 2 passwords
     - faction 2 (150s) with 2 passwords
     - faction 3 (150s) with 2 passwords
     - cross faction password: 3 (taken from faction 1, 2 and 3)
     - common (80s) with 1 passwords
     - obscure (95s) with 3 passwords
     - additional factions not allowed
     */
    def configForImpenerable() {
        if (config.factions.size() < 3) {
            throw new RuntimeException("A password ice with strength Imprenetable must have at least three factions, currently has ${config.factions.size()}.")
        }

        totalRequired = 11
        common = 1
        obscure = 3
        crossFaction = 3

        config.factions.eachWithIndex { faction, index ->
            if (index < 3) {
                passwordsByFaction[faction] = 2
            }
        }
    }
// -------------------------------

    def create_levelsNotFilled() {
        generation.needed.each { key, val ->
            for (int i =0; i < val; i++) {
                levelsNotFilled.add(key)
            }
        }
        Collections.shuffle(levelsNotFilled)
    }

    void createPasswordsNeeded() {
        PasswordAccessLevel.values().each { level ->
            generation.needed[level.toString()] = 0;
        }

        // You always need to have encrypted communication allowed
        generation.needed[PasswordAccessLevel.ENCRYPTED.toString()] = 1;

        for (int i = 1; i < totalRequired; i++) {
            def r = random.nextInt(4)
            def level = PasswordAccessLevel.values()[r]
            generation.needed[level.toString()] = generation.needed[level.toString()] + 1;
        }
    }

    // -------------------------------
    enum Common {

        N1([NUMBER]),
        N2([NUMBER, NUMBER]),
        N3([COMMON]),
        N4([COMMON, NUMBER]),
        N5([L33T, COMMON]),
        N6([COMMON, RANDOM]),
        N7([ALL]),
        N8([RANDOM]),
        N9([RANDOM, RANDOM]);

        String totalName = ""
        Common(List<PasswordDictionary> dictionaries) {
                dictionaries.sort { a,b -> a.prio <=> b.prio }
            dictionaries.each { totalName = totalName + it.name }
        }
    }

    void generateCommon() {
        for (int i = 0; i < common; i++) {
            addPassword("common", Common.values())
        }
    }

    // -------------------------------
    enum Obscure {

        O1([NUMBER, NUMBER, NUMBER]),
        O2([NUMBER, NUMBER, NUMBER, NUMBER]),
        O3([COMMON, NUMBER, NUMBER]),
        O4([RANDOM, NUMBER]),
        O5([RANDOM, RANDOM, NUMBER]),
        O6([L33T, RANDOM]),
        O7([L33T, RANDOM, NUMBER]);

        String totalName = ""

        Obscure(List<PasswordDictionary> dictionaries) {
            dictionaries.sort { a,b -> a.prio <=> b.prio }
            dictionaries.each { totalName = totalName + it.name }
        }
    }

    void generateObscure() {
        for (int i = 0; i < obscure; i++) {
            addPassword("obscure", Obscure.values())
        }
    }

    // -------------------------------

    void generateCrossFaction() {
        for (int i = 0; i < crossFaction; i++) {
            generateOneCrossFaction()
        }
    }

    void generateOneCrossFaction() {
        for (int i = 0; i < 100; i++) {

            Faction faction1 = findFaction1()
            Faction faction2 = findFaction2(faction1)

            def suffix1 = random.nextBoolean() ? "name" : "culture"
            def suffix2 = random.nextBoolean() ? "name" : "culture"

            def name1 = faction1.capitalized() + " " + suffix1
            def name2 = faction2.capitalized() + " " + suffix2

            def dictionary1 = PasswordDictionary.fromName(name1)
            def dictionary2 = PasswordDictionary.fromName(name2)

            List<PasswordDictionary> dictionaries = [dictionary1, dictionary2]
            dictionaries.sort { a, b -> a.prio <=> b.prio }

            String totalName = dictionaries[0].name + dictionaries[1].name

            if (!generation.available.containsKey(totalName)) {
                generation.available[totalName] = nextAccessLevel()
                return
            }
        }

        throw new RuntimeException("Failed to find free faction combinations for cross-faction password")

    }

    Faction findFaction1() {
        def r = random.nextInt(config.factions.size())
        return config.factions[r]
    }

    Faction findFaction2(Faction exclude) {
        for (int i = 0; i < 100; i++) {
            def r = random.nextInt(config.factions.size())
            def faction = config.factions[r]
            if (faction != exclude) {
                return faction
            }
        }
        throw new RuntimeException("Failed to find available unused faction for cross-faction password")
    }


// -------------------------------

    void generateFaction() {
        Faction.values().each { faction ->
            generateForFaction(faction)
        }
    }

    /**
     People name combinations
     3s (name)
     6s (name) x (number)
     9s (name) x (name)
     9s (name) x (hrandom)
     15s (name) x (common word)
     9s (l33t) x (name)
     18s (l33t) x (name) x (number)

     People culture combinations
     3s (culture)
     6s (culture) x (number)
     9s (culture) x (culture)
     9s (culture) x (hrandom)
     15s (culture) x (common word)
     9s (l33t) x (culture)
     18s (l33t) x (culture) x (number)

     Combi name culter
     9s (people name) x (people culture)     */
    void generateForFaction(Faction faction) {
        if (!passwordsByFaction[faction]) {
            return
        }

        PasswordDictionary name = PasswordDictionary.fromName(faction.capitalized() + " name")
        PasswordDictionary culture = PasswordDictionary.fromName(faction.capitalized() + " culture")

        int toGenerate = passwordsByFaction[faction]
        for (int i = 0; i < toGenerate; i++) {
            generateSingleForFaction(name, culture)
        }
    }

    void generateSingleForFaction(PasswordDictionary name, PasswordDictionary culture) {
        for (int i = 0; i < 100; i++) {
            List<PasswordDictionary> list = randomFactionPassword(name, culture)
            list.sort { a, b -> a.prio <=> b.prio }

            String totalName = list.inject ("") { result, it -> return result + it.name }

            if (!generation.available.containsKey(totalName)) {
                generation.available[totalName] = nextAccessLevel()
                return
            }
        }
        throw new RuntimeException("Failed to construct unqiue password for '${name}' + '${culture}'")
    }

    List<PasswordDictionary> randomFactionPassword(PasswordDictionary name, PasswordDictionary culture) {
        int r = random.nextInt(15)

        switch (r){
            case 1: return [name]
            case 2: return [name, NUMBER]
            case 3: return [name, name]
            case 4: return [name, RANDOM]
            case 5: return [name, COMMON]
            case 6: return [L33T, name]
            case 7: return [L33T, name, NUMBER]
            case 8: return [culture]
            case 9: return [culture, NUMBER]
            case 10: return [culture, culture]
            case 11: return [culture, RANDOM]
            case 12: return [culture, COMMON]
            case 13: return [L33T, culture]
            case 14: return [L33T, culture, NUMBER]

            default: return [name, culture]
        }
    }

// -------------------------------

    void addPassword(String type, def options) {
        for (int i = 0; i < 100; i++) {
            def r = random.nextInt(options.size())
            String key = options[r].totalName

            if (!generation.available.containsKey(key)) {
                generation.available[key] = nextAccessLevel()
                return
            }

        }
        throw new RuntimeException("Failed to find unique password of type ${type}, maybe too many were requested.")
    }

    PasswordAccessLevel nextAccessLevel() {
        if ( levelsNotFilled.isEmpty()) {
            create_levelsNotFilled()
        }
        return levelsNotFilled.pop()
    }
}
