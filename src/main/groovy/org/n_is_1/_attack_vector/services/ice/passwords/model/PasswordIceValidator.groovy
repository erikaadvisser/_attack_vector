package org.n_is_1._attack_vector.services.ice.passwords.model

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.services.ServiceValidator
import org.n_is_1._attack_vector.services.ice.passwords.PasswordIceService

/**
 * Validates the GM config of a password ice service.
 */
class PasswordIceValidator extends ServiceValidator {

    def validate_internal(NodeService service, SiteNode node, Site site) {
        PasswordIceService.INSTANCE.createPuzzle(service.data)
        return null
    }
}
