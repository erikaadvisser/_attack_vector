package org.n_is_1._attack_vector.services.ice.magiceye.model

/**
 * Represents a single magic eye image
 */
public class MagicEyeImage {
    String solution
    String location
    List<String> tags = []
}
