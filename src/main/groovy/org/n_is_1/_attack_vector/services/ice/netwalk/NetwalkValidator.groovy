package org.n_is_1._attack_vector.services.ice.netwalk

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.services.ServiceValidator
import org.n_is_1._attack_vector.services.ice.netwalk.model.NetwalkIceConfig

/**
 * Validates the config of a Magic Eye puzzle
 */
class NetwalkValidator extends ServiceValidator {


    def validate_internal(NodeService service, SiteNode node, Site site) {
        new NetwalkIceConfig(service.data)
        return null
    }
}
