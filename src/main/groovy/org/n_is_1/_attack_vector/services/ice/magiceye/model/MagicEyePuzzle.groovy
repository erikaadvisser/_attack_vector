package org.n_is_1._attack_vector.services.ice.magiceye.model

/**
 * Represents the puzzle a Magic Eye ice is.
 */
class MagicEyePuzzle {

    List<MagicEyeImage> images = []
    String strength

    public static MagicEyePuzzle inflateFromJson(def jsonInput) {
        return new MagicEyePuzzle(jsonInput)
    }
}
