package org.n_is_1._attack_vector.services.ice.wordsearch

import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.ice.wordsearch.model.*
import org.n_is_1._attack_vector.services.util.RandomService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * This class is the service for generating and managing Word Search ice content.
 */
@Service
class WordSearchService {

    private static final Logger log = LoggerFactory.getLogger(WordSearchService)

    private static final char[] LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ@(){}".toCharArray()
    private static final int LETTER_COUNT = LETTERS.size()
    private static final char[] INITIAL_STRING = "******************____inject{@authorize(92), @c_override(\"header\", \"trusted/1\"), @remote_core_dump_analyze()}___".toCharArray();
    private static final int INITIAL_STRING_COUNT = INITIAL_STRING.size()

    @Autowired DictionaryService dictionaryService
    @Autowired RandomService random

    static WordSearchService INSTANCE

    @PostConstruct
    def init() {
        INSTANCE = this;
    }

    WordSearchPuzzle createPuzzle(Map<String, String> input) {
        def config = new WordSearchConfig(input)

        int maxAttempts = 10;
        Exception lastException = null
        for (int i = 0; i < maxAttempts; i++) {
            try {
                return createPuzzle(config)
            }
            catch (Exception exception) {
                log.info("Rejected puzzle due to:" + exception.getMessage())
                lastException = exception
            }
        }
        throw new RuntimeException("Failed to create word search puzzle after ${maxAttempts} attempts. Last exception: ${lastException.getMessage()}", lastException)
    }

    WordSearchPuzzle createPuzzle(WordSearchConfig config, boolean initForRendering = true) {
        def wordLiteralList = createWordList(config)

        def letterMap = createLetterMap(config.x, config.y)
        List<Word> words = []

        wordLiteralList.forEach { wordLiteral ->
            def word = placeWord(wordLiteral, letterMap)
            words.push(word)
        }
        verifyNoDoubleSolution(words, letterMap)

        def puzzle = new WordSearchPuzzle(words: words, map: letterMap, strength: config.strength)

        if (initForRendering) {
            puzzle.initForRendering()
        }

        return puzzle
    }

    List<String> createWordList(WordSearchConfig config) {
        List<String> availableWords = new LinkedList<>()
        for (int i = config.shortest; i <= config.longest; i++) {
            availableWords.addAll(dictionaryService.wordBins[i].words)
        }
        Collections.shuffle(availableWords, random.random)
        availableWords.addAll(0, config.use_words)

        if (config.word_count > availableWords.size()) {
            throw new RuntimeException("Not enough available words(${availableWords.size()}) to fill the needed ${config.word_count}. " +
                    "\nChange the shortest and longest length to increase available words.")
        }

        return availableWords.subList(0, config.word_count)
    }

    Word placeWord(String originalText, LetterMap map) {
        Word word = null
        String textToPlace = random.nextBoolean() ? originalText : originalText.reverse()
        int attempts = 0
        boolean vertical
        while (word == null && attempts < 100) {
            if (random.nextBoolean()) {
                word = attemptPlaceHorizontal(textToPlace, map, originalText)
                vertical = false
            }
            else {
                word = attemptPlaceVertical(textToPlace, map, originalText)
                vertical = true
            }
            attempts += 1
        }
        if (word == null) {
            throw new UserInputException("Failed to place word in word search puzzle, the map must be too small or we were unlucky")
        }

        word.modifiedText = textToPlace

//        if (vertical) {
//            println "${word.text} - ${word.modifiedText} - vertical"
//        }
//        else {
//            println "${word.text} - ${word.modifiedText} - horizontal"
//        }
        return word
    }

    Word attemptPlaceVertical(String letters, LetterMap map, String originalText) {
        def attemptXY = createAttemptXy(map)

        if (attemptXY.y + letters.length() >= map.sizeY) {
            return null
        }

        List<LetterLocation> locations = []

        for (int i = 0; i < letters.length(); i++) {
            int y = attemptXY.y + i
            def location = map.getLocationAt(attemptXY.x, y)
            if (location == null || location.locked) {
                return null
            }
            locations.push(location)
        }

        locations.eachWithIndex { location, index ->
            location.locked = true
            location.letter = letters.charAt(index)
        }

        return new Word(text: originalText, locations: locations)
    }

    Word attemptPlaceHorizontal(String letters, LetterMap map, String originalText) {
        def attemptXY = createAttemptXy(map)

        if (attemptXY.x + letters.length() >= map.sizeX) {
            return null
        }

        List<LetterLocation> locations = []

        for (int i = 0; i < letters.length(); i++) {
            int x = attemptXY.x + i
            def location = map.getLocationAt(x, attemptXY.y)
            if (location == null || location.locked) {
                return null
            }
            locations.push(location)
        }

        locations.eachWithIndex { location, index ->
            location.locked = true
            location.letter = letters.charAt(index)
        }

        return new Word(text: originalText, locations: locations)
    }

    def createAttemptXy(LetterMap letterMap) {
        int x = random.nextInt(letterMap.sizeX)
        int y = random.nextInt(letterMap.sizeY)

        return [x: x, y: y]
    }

    LetterMap createLetterMap(int sizeX, int sizeY) {
        def map = new LetterMap(sizeX, sizeY)

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                map.add(x, y)
            }
        }

        map.locations.eachWithIndex { location, index ->
            setLetter(location, index)
        }

        return map
    }

    void setLetter(LetterLocation location, int count) {
        if (count < INITIAL_STRING_COUNT) {
            char c = INITIAL_STRING[count]
            if (c != '*') {
                location.letter = c
                location.locked = true
                return
            }
        }
        int i = random.nextInt(LETTER_COUNT)
        location.letter = LETTERS[i]
    }

    void verifyNoDoubleSolution(ArrayList<Word> words, LetterMap map) {
        words.each { word ->
            int matches = countMatches(word.text, map)
            if (matches != 1) {
                throw new RuntimeException("More than one solution generated for: ${word} - ${matches} solutions.")
            }
        }
    }

    int countMatches(String word, LetterMap map) {
        int total = 0

        total += countMatchesHorizontal(word, map)
        total += countMatchesHorizontal(word.reverse(), map)
        total += countMatchesVertical(word, map)
        total += countMatchesVertical(word.reverse(), map)

        return total
    }

    int countMatchesHorizontal(String word, LetterMap map) {
        int matches = 0

        for (int y = 0; y != map.sizeY; y += 1) {
            int matchLength = 0
            char nextCharacterToMatch = word.charAt(0)
            for (int x = 0; x < map.sizeX; x += 1) {
                if (map.getLocationAt(x, y).letter == nextCharacterToMatch) {
                    matchLength += 1
                    if (matchLength == word.size()) {
                        matches += 1
                        println "Match at y:${y}, x:${x} (horizontal: ${word})"
                        matchLength = 0
                        nextCharacterToMatch = word.charAt(0)
                    }
                    else {
                        nextCharacterToMatch = word.charAt(matchLength)
                    }
                }
                else if (matchLength > 0) {
                    matchLength = 0
                    nextCharacterToMatch = word.charAt(0)
                }
            }
        }

        return matches
    }

    int countMatchesVertical(String word, LetterMap map) {
        int matches = 0

        for (int x = 0; x < map.sizeX; x += 1) {
            int matchLength = 0
            char nextCharacterToMatch = word.charAt(0)
            for (int y = 0; y != map.sizeY; y += 1) {
                if (map.getLocationAt(x, y).letter == nextCharacterToMatch) {
                    matchLength += 1
                    if (matchLength == word.size()) {
                        matches += 1
                        println "Match at y:${y}, x:${x} (vertical: ${word})"
                        matchLength = 0
                        nextCharacterToMatch = word.charAt(0)
                    }
                    else {
                        nextCharacterToMatch = word.charAt(matchLength)
                    }
                }
                else if (matchLength > 0) {
                    matchLength = 0
                    nextCharacterToMatch = word.charAt(0)
                }
            }
        }

        return matches
    }

}
