package org.n_is_1._attack_vector.services.ice.netwalk

import org.n_is_1._attack_vector.model.site.ice.IceStrength
import org.n_is_1._attack_vector.services.ice.netwalk.model.NetwalkIceConfig
import org.n_is_1._attack_vector.services.ice.netwalk.model.NetwalkIcePuzzle
import org.n_is_1._attack_vector.services.util.RandomService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import static org.n_is_1._attack_vector.model.site.ice.IceStrength.*

@Service
class NetwalkIceService {

    @Autowired RandomService random

    NetwalkIcePuzzle generate(Map<String, String> input) {
        def config = new NetwalkIceConfig(input)

        int x = determineX(config.strength)
        int y = determineX(config.strength)
        boolean wrap = determineWrap(config.strength)


        def generator = new NetwalkIceGenerator(x, y, wrap, random, config.strength)
        return generator.create()
    }

    boolean determineWrap(IceStrength iceStrength) {
        return iceStrength == IMPENETRABLE
    }

    int determineX(IceStrength iceStrength) {
        switch (iceStrength) {
            case VERY_WEAK: return 5
            case WEAK: return 7
            case AVERAGE: return 9
            case STRONG: return 11
            case VERY_STRONG: return 13
            case IMPENETRABLE: return 8
        }
    }
}
