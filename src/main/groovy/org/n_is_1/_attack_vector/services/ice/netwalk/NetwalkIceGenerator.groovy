package org.n_is_1._attack_vector.services.ice.netwalk

import org.n_is_1._attack_vector.model.site.ice.IceStrength
import org.n_is_1._attack_vector.services.ice.netwalk.model.NetwalkIcePuzzle
import org.n_is_1._attack_vector.services.util.RandomService

/**
 * https://en.wikipedia.org/wiki/Maze_generation_algorithm
 *
 * Randomized Prim's algorithm
 * File:MAZE 30x20 Prim.ogv
 * An animation of generating a 30 by 20 maze using Prim's algorithm.
 * This algorithm is a randomized version of Prim's algorithm.
 *
 * Start with a grid full of walls.
 * Pick a cell, mark it as part of the maze. Add the walls of the cell to the wall list.
 * While there are walls in the list:
 * Pick a random wall from the list. If only one of the two cells that the wall divides is visited, then:
 * Make the wall a passage and mark the unvisited cell as part of the maze.
 * Add the neighboring walls of the cell to the wall list.
 * Remove the wall from the list.
 * It will usually be relatively easy to find the way to the starting cell, but hard to find the way anywhere else.
 *
 * Note that simply running classical Prim's on a graph with random edge weights would create mazes stylistically identical to Kruskal's, because they are both
 * minimal spanning tree algorithms. Instead, this algorithm introduces stylistic variation because the edges closer to the starting point have a lower
 * effective weight.
 *
 * Modified version
 * Although the classical Prim's algorithm keeps a list of edges, for maze generation we could instead maintain a list of adjacent cells. If the randomly chosen
 * cell has multiple edges that connect it to the existing maze, select one of these edges at random. This will tend to branch slightly more than the edge-based
 * version above.
 *
 *
 *
 * ---
 *
 * This implementation is classic Prim's
 */
class NetwalkIceGenerator {

    private int xSize
    private int ySize
    private def wrap
    private RandomService random
    private IceStrength iceStrength

    NetwalkIceGenerator(int xSize, int ySize, boolean wrap, RandomService random, IceStrength iceStrength) {
        this.xSize = xSize
        this.ySize = ySize
        this.wrap = wrap
        this.random = random
        this.iceStrength = iceStrength
    }

    class Point {
        int x
        int y

        Point(int x, int y) {
            this.x = x
            this.y = y
        }

        boolean equals(o) {
            if (this.is(o)) return true
            if (getClass() != o.class) return false

            Point point = (Point) o

            if (x != point.x) return false
            if (y != point.y) return false

            return true
        }

        int hashCode() {
            int result
            result = x
            result = 31 * result + y
            return result
        }

        String toString() {
            return "(${x}, ${y})"
        }
    }

    class Wall {
        Point from
        Point to

        Wall(Point from, Point to) {
            this.from = from
            this.to = to
        }

        boolean equals(o) {
            if (this.is(o)) return true
            if (getClass() != o.class) return false

            Wall wall = (Wall) o

            if (from == wall.from && to == wall.to) return true
            if (from == wall.to && to == wall.from) return true

            return false
        }

        int hashCode() {
            int hash1 = hashCodeCalc(from, to)
            int hash2 = hashCodeCalc(to, from)

            return Math.min(hash1, hash2)
        }


        int hashCodeCalc(from, to) {
            int result
            result = (from != null ? from.hashCode() : 0)
            result = 31 * result + (to != null ? to.hashCode() : 0)
            return result
        }

        String toString() {
            return "(${from.x}, ${from.y}) - (${to.x}, ${to.y})"
        }

    }


    private void printMaze(String maze) {
        for (int i = 0; i < maze.size(); i++) {
            print(maze.charAt(i))
            print(" ")
            if ((i + 1) % xSize == 0) {
                System.out.print("\n")
            }
        }
    }

    /**
     * Start with
     * - a maze grid that only contains the starting point.
     * - a list of walls that contains 4 walls around the starting point.
     *
     * while there are walls in the list:
     * - pick a random wall
     * - if the other side is already part of the maze grid:
     *   this wall is staying, do not connect. Add wall to final wall list
     *   but remove it from the (temporary) wall list
     * - else:
     *   remove this wall
     *   add the other side to the maze grid
     * @return
     */
    NetwalkIcePuzzle create() {
        def grid = new LinkedList<Point>()
        def walls = new HashSet<Wall>()
        def finalWalls = new HashSet<Wall>()

        def start = new Point(((xSize-1)/2).intValue(), ((ySize-1)/2).intValue())
//        def start = new Point(1,1)

        grid.add(start)
        def startWalls = findWalls(start)
        walls.addAll(startWalls)

        while (!walls.isEmpty()) {
            printGrid(grid)
            def i = random.nextInt(walls.size())
            def wall = getWall(walls, i)
//            println("Adding point: ${wall.to}")
//            println()
            if (grid.contains(wall.to)) {
                // The other side of the wall is already part of the maze.
                // This wall will stay in the maze
                finalWalls.add(wall)
                walls.remove(wall)
            }
            else {
                // connect to the other side.
                // remove wall but add position to grid
                grid.add(wall.to)
                def newWalls = findWalls(wall.to)
                newWalls.removeAll(finalWalls)
                walls.addAll(newWalls)
                walls.remove(wall)
            }
        }

        if (!wrap) {
            for (int x = 0; x < xSize; x++) {
                def topWall = new Wall(new Point(x, 0), new Point(x, -1))
                def bottomWall = new Wall(new Point(x, ySize - 1), new Point(x, ySize))
                finalWalls.add(topWall)
                finalWalls.add(bottomWall)
            }
            for (int y = 0; y < ySize; y++) {
                def leftWall = new Wall(new Point(0, y), new Point(-1, y))
                def rightWall = new Wall(new Point(xSize - 1, y), new Point(xSize, y))
                finalWalls.add(leftWall)
                finalWalls.add(rightWall)
            }
        }


        def builder = new StringBuilder()
        for (int y = 0; y < ySize; y++) {
            for (int x = 0; x < xSize; x++) {
                def point = new Point(x, y)
                builder.append(mapPoint(point, finalWalls))
            }
        }

        def net = builder.toString()
        def puzzle = new NetwalkIcePuzzle(iceStrength, xSize, ySize, wrap, net)

//        printMaze(net)
        return puzzle
    }

    void printGrid(List<Point> grid) {
        for (int y = 0; y < ySize; y++) {
            for (int x = 0; x < xSize; x++) {
                def point = new Point(x, y)
                String symbol = grid.contains(point) ? "O" : "∘"
//                SystemSystem.out.print(symbol)
            }
//            System.out.print("\n")
        }
    }

    String mapPoint(Point point, HashSet<Wall> walls) {
        String wallString = createWallString(point, walls)

        switch (wallString) {
            case "1000": return "┳"
            case "0100": return "┫"
            case "0010": return "┻"
            case "0001": return "┣"
            case "1010": return "━"
            case "0101": return "┃"
            case "0111": return "╵"
            case "1011": return "╶"
            case "1101": return "╷"
            case "1110": return "╴"
            case "1100": return "┓"
            case "0110": return "┛"
            case "0011": return "┗"
            case "1001": return "┏"
            case "1111": return "┄"
            case "0000": return "╋"
            default: throw new IllegalStateException("Invalid grid wall state: ${wallString}")
        }
    }

    String createWallString(Point point, HashSet<Wall> walls) {
        Point n = new Point(point.x, point.y - 1)
        Point s = new Point(point.x, point.y + 1)
        Point w = new Point(point.x - 1, point.y)
        Point e = new Point(point.x + 1, point.y)

        Wall nWall = new Wall(point, n)
        Wall eWall = new Wall(point, e)
        Wall sWall = new Wall(point, s)
        Wall wWall = new Wall(point, w)

        def builder = new StringBuilder()
        builder.append(wallDigit(walls, nWall))
        builder.append(wallDigit(walls, eWall))
        builder.append(wallDigit(walls, sWall))
        builder.append(wallDigit(walls, wWall))

        return builder.toString()
    }

    String wallDigit(HashSet<Wall> walls, Wall toFind) {
        if (walls.contains(toFind)) {
            return "1"
        }
        else {
            return "0"
        }

    }

    Wall getWall(HashSet<Wall> walls, int index) {
        int i = 0
        for (Wall wall : walls) {
            if (i == index) {
                return wall
            }
            i++
        }
        throw new IndexOutOfBoundsException("Index ${index} is too large for set size: ${walls.size()}")
    }


    List<Wall> findWalls(Point from) {

        def walls = new LinkedList<Wall>()
        if (from.x -1 >= 0) {
            walls.add(new Wall(from, new Point(from.x -1, from.y)))
        }
        else {
            if (wrap) {
                walls.add(new Wall(from, new Point(from.xSize - 1, from.y)))
            }
        }
        if (from.x +1 < xSize ) {
            walls.add(new Wall(from, new Point(from.x + 1, from.y)))
        }
        else {
            if (wrap) {
                walls.add(new Wall(from, new Point(0, from.y)))
            }
        }
        if (from.y - 1 >= 0) {
            walls.add(new Wall(from, new Point(from.x, from.y - 1)))
        }
        else {
            if (wrap) {
                walls.add(new Wall(from, new Point(from.x, from.ySize - 1)))
            }
        }
        if (from.y +1 < ySize ) {
            walls.add(new Wall(from, new Point(from.x, from.y + 1)))
        }
        else {
            if (wrap) {
                walls.add(new Wall(from, new Point(from.x, 0)))
            }
        }
        return walls
    }
}
