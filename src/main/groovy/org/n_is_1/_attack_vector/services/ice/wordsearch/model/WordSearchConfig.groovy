package org.n_is_1._attack_vector.services.ice.wordsearch.model

import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.util.ConfigUtil

/**
 * Config class for word search ice puzzles
 */
class WordSearchConfig {

    String strength
    int x
    int y
    int word_count
    List<String> use_words = []
    int shortest
    int longest

    public WordSearchConfig() {
        // for test
    }

    public WordSearchConfig(Map<String, String> input) {
        def util = new ConfigUtil()

        strength = util.findStringWithDefault(input, "strength", "Unknown")
        this.x = util.findInt(input, "x", 15, 30)
        this.y = util.findInt(input, "y", 15, 30)
        this.word_count = util.findInt(input, "wordcount", 1, 20)
        this.shortest = util.findInt(input, "shortest", 3, 11)
        this.longest = util.findInt(input, "longest", 3, 13)
        if (shortest > longest) {
            throw new UserInputException("Shortest (${shortest})must be shorter than Longest (${longest}).")
        }
        this.use_words = util.optionalFindCommaSeparatedSet(input, "use_words").collect { it.toUpperCase() }
        this.use_words.reverse()
    }
}
