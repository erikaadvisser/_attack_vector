package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.chat.ChatMessage
import org.springframework.data.repository.PagingAndSortingRepository


interface ChatRepo extends PagingAndSortingRepository<ChatMessage, String> {
}
