package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.admin.log.LogLevel
import org.n_is_1._attack_vector.model.admin.log.LogRecord
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * Repository interface (Spring data) for LogRecord class.
 */
public interface LogRecordRepo extends PagingAndSortingRepository<LogRecord, String> {

    List<LogRecord> findByLevelIn(List<LogLevel> levels);
}
