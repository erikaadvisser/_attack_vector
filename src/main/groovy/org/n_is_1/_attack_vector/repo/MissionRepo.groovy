package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.run.HackingMission
import org.springframework.data.repository.PagingAndSortingRepository

interface MissionRepo extends PagingAndSortingRepository<HackingMission, String> {

}