package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.site.Site
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * Repository for Site class.
 */
interface SiteRepo extends PagingAndSortingRepository<Site, String> {
}
