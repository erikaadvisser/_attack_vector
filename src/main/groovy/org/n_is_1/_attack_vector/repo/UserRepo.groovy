package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.admin.User
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * Repository interface (Spring data) for User class.
 */
interface UserRepo extends PagingAndSortingRepository<User, String> {
}
