package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.hacker.HackerScanLink
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * Repo for HackerScanLink
 */
interface HackerScanLinkRepo extends PagingAndSortingRepository<HackerScanLink, String> {
}