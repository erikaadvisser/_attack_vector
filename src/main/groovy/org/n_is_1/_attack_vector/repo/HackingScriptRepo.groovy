package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.services.scripts.model.HackingScript
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * Repo for HackingScript
 */
interface HackingScriptRepo extends PagingAndSortingRepository<HackingScript, String> {
}