package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.site.SiteScan
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * Repository for SiteScan class.
 */
interface SiteScanRepo extends PagingAndSortingRepository<SiteScan, String> {
}
