package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.admin.BotConfiguration
import org.springframework.data.repository.PagingAndSortingRepository

interface DiscordRepo extends PagingAndSortingRepository<BotConfiguration, String> {
}
