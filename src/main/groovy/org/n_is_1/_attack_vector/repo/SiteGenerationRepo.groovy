package org.n_is_1._attack_vector.repo

import org.n_is_1._attack_vector.model.site.SiteGeneration
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * Repository for SiteGeneration class.
 */
interface SiteGenerationRepo extends PagingAndSortingRepository<SiteGeneration, String> {
}