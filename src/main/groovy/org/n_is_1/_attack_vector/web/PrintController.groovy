package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.site.*
import org.n_is_1._attack_vector.services.ice.netwalk.model.NetwalkIcePuzzle
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.IceService
import org.n_is_1._attack_vector.services.site.SiteService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Controller for the print site pages
 */
@Controller()
class PrintController extends ExceptionPrintingController {

    def log = LoggerFactory.getLogger(EditorController);

    @Autowired SiteService siteService
    @Autowired HackingRunService attackService
    @Autowired IceService iceService

    @RequestMapping(value = "/print/{scanId}", method = RequestMethod.GET)
    public String printSitePlayer(@PathVariable(value = "scanId") String scanId,
                                  Model model) {
        SiteScan scan = siteService.getScanById(scanId)
        if (scan == null) {
            model.addAttribute("page", "scan")
            return "hacker/scan-obsolete.jsp"
        }
        Site site = siteService.getSiteById(scan.siteId)
        SiteGeneration generation = siteService.getGenerationById(scan.generationId)
        model.addAttribute("site", site)
        model.addAttribute("scan", scan)
        model.addAttribute("generation", generation)
        model.addAttribute("gm", false)
        model.addAttribute("printSolution", false)
        model.addAttribute("analyzed", scan.analyzedServiceIds)
        return "site/print.jsp"
    }

    @RequestMapping(value = "/print/load/{siteId}/")
    @ResponseBody
    public Site load(@PathVariable(value = "siteId") String siteId) {
        Site site = siteService.getSiteById(siteId)
        return site
    }

    @RequestMapping(value = "/gm/print/{siteId}/", method = RequestMethod.GET)
    public String printSite(@PathVariable(value = "siteId") String siteId,
                            Model model) {
        Site site = siteService.getSiteById(siteId)
        SiteScan scan = siteService.createScanForGm(site)
        SiteGeneration generation = siteService.getGenerationById(scan.generationId)
        model.addAttribute("site", site)
        model.addAttribute("scan", scan)
        model.addAttribute("generation", generation)
        model.addAttribute("gm", true)
        model.addAttribute("printSolution", false)
        return "site/print.jsp"
    }

    @RequestMapping(value = "/gm/print-solution/{siteId}/", method = RequestMethod.GET)
    public String printSiteSolution(@PathVariable(value = "siteId") String siteId,
                                    Model model) {
        printSite(siteId, model)
        model.addAttribute("printSolution", true)
        return "site/print.jsp"
    }

    @RequestMapping(value = "/print/ice/{generationId}/{serviceId}", method = RequestMethod.GET)
    public String printIce(@PathVariable(value = "generationId") String generationId,
                                @PathVariable(value = "serviceId") String serviceId,
                                Model model) {
        SiteGeneration generation = siteService.getGenerationById(generationId)
        Site site = siteService.getSiteById(generation.siteId)
        NodeService service = site.findServiceById(serviceId)
        model.addAttribute("serviceId", serviceId)
        switch( service.type ) {
            case ServiceType.ICE_WORD_SEARCH : return printWordSearch(generationId, serviceId, model)
            case ServiceType.ICE_MAGIC_EYE   : return printMagicEye(generationId, serviceId, model)
            case ServiceType.ICE_MANUAL      : return printManual(generationId, serviceId, model)
            case ServiceType.ICE_UNHACKABLE  : return printUnhackable(generationId, serviceId, model)
            case ServiceType.ICE_PASSWORD    : return printPassword(generationId, serviceId, model)
            case ServiceType.ICE_NETWALK     : return printNetwalk(generationId, serviceId, model)
            default: throw new IllegalArgumentException("Unsupported ice type: ${service.type}")
        }
    }

    String printNetwalk(String generationId, String serviceId, Model model) {
        NetwalkIcePuzzle puzzle = iceService.retrieveData(generationId, serviceId)
        model.addAttribute("puzzleDef", puzzle.toJson())
        model.addAttribute("x", puzzle.x)
        model.addAttribute("y", puzzle.y)
        model.addAttribute("serviceId", serviceId)
        model.addAttribute("strength", puzzle.strength)

        return "ice/netwalk-print.jsp"
    }

    String printPassword(String generationId, String serviceId, Model model) {
        def puzzle = iceService.retrieveData(generationId, serviceId)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("generationId", generationId)
        model.addAttribute("serviceId", serviceId)
        return "ice/password-print.jsp"
    }

    String printWordSearch(String generationId, String serviceId, Model model) {
        def puzzle = iceService.retrieveData(generationId, serviceId)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("generationId", generationId)
        model.addAttribute("serviceId", serviceId)
        return "ice/wordsearch-print.jsp"
    }

    String printManual(String generationId, String serviceId, Model model) {
        def puzzle = iceService.retrieveData(generationId, serviceId)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("generationId", generationId)
        model.addAttribute("serviceId", serviceId)
        return "ice/manual-print.jsp"
    }

    def printMagicEye(String generationId, String serviceId, Model model) {
        def puzzle = iceService.retrieveData(generationId, serviceId)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("generationId", generationId)
        model.addAttribute("serviceId", serviceId)
        return "ice/magiceye-print.jsp"
    }

    def printUnhackable(String generationId, String serviceId, Model model) {
        model.addAttribute("generationId", generationId)
        model.addAttribute("serviceId", serviceId)
        return "ice/unhackable-print.jsp"
    }

}
