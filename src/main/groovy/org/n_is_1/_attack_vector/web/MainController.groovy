package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.admin.UserType
import org.n_is_1._attack_vector.repo.UserRepo
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.user.model.LoginResponse
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.services.util.UserUtil
import org.n_is_1._attack_vector.web.model.UserModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import javax.validation.Valid

/**
 * The Spring MVC Controller for the hack itself.
 */
@Controller()
class MainController extends ExceptionPrintingController {

    @Autowired SiteService siteService
    @Autowired UserRepo hackerRepo
    @Autowired UserService userService

    @RequestMapping("/favicon.ico")
    String favicon() {
        return "forward:/resources/images/Vector-icon-64.png"
    }

    @RequestMapping("/")
    String welcome() {
        return directToHome()
    }

    String directToHome() {
        def user = UserUtil.getUser()
        switch (user?.type) {
            case null: return "redirect:/login"
            case UserType.HACKER: return "redirect:/home"
            case UserType.GM: return "redirect:/gm/sites"
            case UserType.ADMIN: return "redirect:/admin/"
            default: throw new RuntimeException("Unsupported user type: ${user.type}")
        }
    }

    @RequestMapping(value="/login", method=RequestMethod.GET)
    String login(Model model) {
        model.addAttribute("page", "login")
        return "login.jsp"
    }

    @RequestMapping(value="/loginSubmit", method=RequestMethod.POST)
    String loginSubmit(@RequestParam("loginName") String loginName,
                       @RequestParam("passcode") String passcode,
                       RedirectAttributes redirectAttributes) {
        LoginResponse login = userService.login(loginName, passcode)
        redirectAttributes.addFlashAttribute("flashMessage", login.message)
        redirectAttributes.addFlashAttribute("flashType", "error")

        if ( login.success ) {
            return directToHome()
        }

        return "redirect:/login"
    }

    @RequestMapping(value="/login/{loginName}", method=RequestMethod.GET)
    String directLogin(@PathVariable("loginName") String loginName,
                       RedirectAttributes redirectAttributes) {
        def login = userService.directLogin(loginName)
        if (login.success) {
            Flash.success(login.message, redirectAttributes)
            return directToHome()
        }
        else {
            Flash.error(login.message, redirectAttributes)
            return "redirect:/login"
        }

    }


    @RequestMapping(value = "/signUp", method = RequestMethod.GET)
    public def signUp(Model model) {
        def userModel = new UserModel()
        model.addAttribute("user", userModel)
        model.addAttribute("page", "signUp")
        return "signUp.jsp"
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public def signUp(@Valid UserModel userModel, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
//        model.addAttribute("flashMessage", "No signup during an event!")
//        model.addAttribute("user", userModel)
//        model.addAttribute("page", "signUp")
//        return "signUp.jsp"

        def errorMessage = userService.validateForSignUp(userModel, bindingResult)
        if (errorMessage) {
            model.addAttribute("flashMessage", errorMessage)
            model.addAttribute("user", userModel)
            model.addAttribute("page", "signUp")
            return "signUp.jsp"
        }
        userService.signUpUser(userModel)
        redirectAttributes.addFlashAttribute("flashMessage", "User '${userModel.loginName}' updated.")
        redirectAttributes.addFlashAttribute("flashType", "success")
        return "redirect:/"
    }



    @RequestMapping("/about")
    String about(Model model) {
        model.addAttribute("page", "about")
        return "about.jsp";
    }

    @RequestMapping("/notChrome")
    String notChrome() {
        return "not_chrome.jsp";
    }

    /**
     * This call is used to keep the session alive of the user, so that the hacker does not have to log in again
     * after scanning a large site.
     */
    @RequestMapping("/keepAlive")
    @ResponseBody
    String keepAlive() {
//        log.trace("keepAlive")
        return "ok";
    }

}
