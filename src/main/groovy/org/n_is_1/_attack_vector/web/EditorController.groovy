package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.services.editor.EditorService
import org.n_is_1._attack_vector.services.editor.SiteValidationException
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.web.model.JqueryNotifyResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Controller for the editor.
 */
@Controller
@RequestMapping("/gm/editor")
public class EditorController extends ExceptionPrintingController {

    def log = LoggerFactory.getLogger(EditorController);

    @Autowired EditorService editorService
    @Autowired SiteService siteService


    @RequestMapping(value = "/{siteId}/")
    public def editor(Model model,
                      @PathVariable(value = "siteId") String siteId) {

        Site site = siteService.getSiteById(siteId)

        model.addAttribute("siteId", siteId)
        model.addAttribute("loadOnStartup", (site != null))

        return "site/editor.jsp"
    }

    // -- -- -- -- REST CALLS -- -- -- -- //

    @RequestMapping(value = "/save")
    @ResponseBody
    public JqueryNotifyResponse save(@RequestBody Site site) {
        try {
            boolean stuffObsoleted = editorService.save(site)
            if (stuffObsoleted) {
                return new JqueryNotifyResponse("Site saved. Warning: existing scans and/or runs deleted.", "warn", true)
            }
            else {
                return new JqueryNotifyResponse("Site saved.")
            }
        }
        catch (SiteValidationException e) {
            log.debug("Save failed for ${site.id} due to: ${e.getMessage()}")
            return new JqueryNotifyResponse("Save failed: ${e.getMessage()}", "error", false);
        }
        catch (Exception e) {
            log.error("Save failed", e)
            return new JqueryNotifyResponse("Save failed: ${e.getMessage()}", "error", false);
        }
    }

    @RequestMapping(value = "/load/{siteId}/")
    @ResponseBody
    public Site load(@PathVariable(value = "siteId") String siteId) {
        Site site = siteService.getSiteById(siteId)
        return site
    }
}