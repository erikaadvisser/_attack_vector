package org.n_is_1._attack_vector.web


import org.n_is_1._attack_vector.services.discord.DiscordService
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.web.model.DiscordConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Controller for functions like backup & restore, clean
 */
@Controller
@RequestMapping("/discord")
class DiscordController extends ExceptionPrintingController {

    @Autowired DiscordService service

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String emptyIndex(Model model) {
        return "redirect:/discord/"
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        def configurations = service.getAll()
        model.addAttribute("configurations", configurations)

        return "admin/discord.jsp"
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@ModelAttribute DiscordConfiguration config, RedirectAttributes redirectAttributes) {
        String validationError = config.validate()
        if (validationError != null) {
            Flash.error(validationError, redirectAttributes)
        }
        else {
            service.addConfiguration(config)
        }
        return "redirect:/discord/"
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
    public String removeConfiguration(@PathVariable(value = "id") String id, RedirectAttributes redirectAttributes) {
        String message = service.remove(id)
        Flash.error(message, redirectAttributes)
        return "redirect:/discord/"
    }

    @RequestMapping(value = "/activate/{id}", method = RequestMethod.GET)
    public String activate(@PathVariable(value = "id") String id, RedirectAttributes redirectAttributes) {
        String message = service.activate(id)
        Flash.message(message, "info", redirectAttributes)
        return "redirect:/discord/"
    }



    @RequestMapping(value = "/message")
    public String test(@ModelAttribute DiscordConfiguration messageHolder, RedirectAttributes redirectAttributes) {
        def message = "[test message] " + messageHolder.value
        def error = service.postMessageToChannel(message)
        if (error) {
            Flash.error("Failed to send message due to: ${error}", redirectAttributes)
        }
        else {
            Flash.success("Message sent, check Discord.", redirectAttributes)
        }
        return "redirect:/discord/"
    }



}
