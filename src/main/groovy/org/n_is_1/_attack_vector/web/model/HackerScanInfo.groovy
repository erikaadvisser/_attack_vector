package org.n_is_1._attack_vector.web.model

/**
 * A view item: combination of sitescan + site for display to hackers
 */
class HackerScanInfo {

    String siteId
    String siteName
    String scanId
    String age = "minutes ago"

}
