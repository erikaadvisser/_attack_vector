package org.n_is_1._attack_vector.web.model

import org.n_is_1._attack_vector.model.site.SiteType
import org.n_is_1._attack_vector.services.ice.passwords.model.Faction

class MissionParameters {

    Faction faction
    String siteId
    SiteType difficulty
    String info
    int reward
    int time

}
