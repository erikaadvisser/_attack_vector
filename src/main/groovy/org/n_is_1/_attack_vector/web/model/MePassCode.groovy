package org.n_is_1._attack_vector.web.model

/**
 * Data Transfer Object for passcode form of /me/
 */
class MePassCode {

    String passcode1
    String passcode2

}
