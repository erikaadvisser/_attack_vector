package org.n_is_1._attack_vector.web.model

/**
 * This is a response that can be rendered using jquery notify.
 */
class JqueryNotifyResponse {

    public String text
    public boolean success = true

    /**
     * Type of notify to use. Options are: success, info, warn, error.
     */
    public String type = "success"

    JqueryNotifyResponse(String text) {
        this.text = text
    }

    JqueryNotifyResponse(String text, String type, boolean success) {
        this.text = text
        this.type = type
        this.success = success
    }
}
