package org.n_is_1._attack_vector.web.model

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.HackerSpecialization
import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.model.admin.UserType

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * This is an i/o model of the User class. Very similar to the User, but it has a password instead of a hash and a salt.
 */
class UserModel {

    String id

    Boolean enabled
    Boolean blocked

    @NotNull
    @Size(min=2, max=30)
    String loginName

    @NotNull
    @Size(min=2, max=30)
    String ocName

    String passcode

    UserType type = UserType.HACKER

    @Size(max=30)
    String icName

    @Min(0l)
    @Max(10l)
    Integer skill_it_main

    DateTime blockedUntil

    int failedLoginCount

    HackerSpecialization specialization = HackerSpecialization.NONE

    /** Constructor for Spring MVC */
    UserModel() {
    }

    UserModel(User user) {
        this.id = user.id
        this.enabled = user.enabled
        this.loginName = user.loginName
        this.ocName = user.ocName
        this.passcode = null
        this.type = user.type
        this.icName = user.icName
        this.skill_it_main = user.skill_it_main
        this.specialization = user.specialization

        if (user.blocked) {
            if (DateTime.now().isAfter(user.blockedUntil)) {
                this.blocked = false
                return
            }

            this.blocked = user.blocked
            this.failedLoginCount = user.failedLoginCount
            this.blockedUntil = new DateTime(user.blockedUntil)
        }
    }
}
