package org.n_is_1._attack_vector.web.model

class DiscordConfiguration {

    String value

    String validate() {
        if (value.split(";").length != 5) {
            return "Format of discord configuration is invalid. Format is: <channel>;<bot-token>;<description>;<user-id>;<user-name>"
        }
        return null
    }

    String getBotChannel() {
        return value.split(";")[0]
    }

    String getBotToken() {
        return value.split(";")[1]
    }

    String getDescription() {
        return value.split(";")[2]
    }

    String getUserId() {
        return value.split(";")[3]
    }

    String getUserName() {
        return value.split(";")[4]
    }
}
