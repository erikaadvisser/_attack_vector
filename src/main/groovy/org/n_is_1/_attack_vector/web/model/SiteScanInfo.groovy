package org.n_is_1._attack_vector.web.model

import org.n_is_1._attack_vector.model.site.SiteScan

/**
 * DTO for displaying info on a single site scan (on the hacker home page)
 */
class SiteScanInfo {

    String siteId
    String siteName
    SiteScan scan

    SiteScanInfo(String siteId, String siteName, SiteScan scan) {
        this.siteId = siteId
        this.siteName = siteName
        this.scan = scan
    }
}
