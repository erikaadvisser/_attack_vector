package org.n_is_1._attack_vector.web

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.web.ErrorAttributes
import org.springframework.boot.autoconfigure.web.ErrorController
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 *
 */
@Controller
class SystemErrorController implements ErrorController {

    def log = LoggerFactory.getLogger(SystemErrorController)

    @Autowired ErrorAttributes errorAttributes
    @Autowired MainController mainController

    @Override
    public String getErrorPath() {
        return "/error"
    }

    @RequestMapping(value = "/gm/die")
    def die() {
        throw new RuntimeException("die")
    }

    @RequestMapping(value = "/error")
    def error(HttpServletRequest request, HttpServletResponse response, Model model, RedirectAttributes redirectAttributes) {
        def requestAttributes = new ServletRequestAttributes(request)
        def errorMap = errorAttributes.getErrorAttributes(requestAttributes, true)
        def cause = errorAttributes.getError(requestAttributes)

        model.addAttribute("timestamp", errorMap["timestamp"])    // 1484162659884
        model.addAttribute("status", errorMap["status"])          // 404 | 500
        model.addAttribute("error", errorMap["error"])            // Not Found | Internal Server error
        model.addAttribute("message", errorMap["message"])        // Not Found | org.springframework.web.util.NestedServletException: Request processing failed; nested exception is java.lang.RuntimeException: die
        model.addAttribute("path", errorMap["path"])              // /gm/sitesa
        model.addAttribute("exception", errorMap["exception"])    // "java.lang.RuntimeException"
        model.addAttribute("fullTrace", cause)                        // the actual exception with stack trace

        if (errorMap["status"] == 403) {
            redirectAttributes.addFlashAttribute("flashMessage", "403: Access denied to: ${errorMap["path"]}")
            redirectAttributes.addFlashAttribute("flashType", "warning")
            return mainController.welcome()
        }

        if (errorMap["status"] == 404) {
            redirectAttributes.addFlashAttribute("flashMessage", "404: URL error: ${errorMap["path"]}")
            redirectAttributes.addFlashAttribute("flashType", "warning")
            return mainController.welcome()
        }
        def trace = errorMap["trace"]

        def traceLines = trace.split("\r\n")
        def usefulLines = traceLines.findAll { String line ->
            !line.startsWith('\tat org.springframework') &&
                    !line.startsWith('\tat org.eclipse.jetty') &&
                    !line.startsWith('\tat org.codehaus.groovy.runtime.')
        }


        trace = usefulLines.inject { result, value -> result + "<br>" + value}

        model.addAttribute("trace", trace)            // ""trace" -> "java.lang.RuntimeException: die at \ sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method) \ at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)

        if (cause) {
            log.error("Error page returned due to exception", cause)
        }

        return "error.jsp"
    }

}
