package org.n_is_1._attack_vector.web

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * This is the controller for all the pages of the manual
 */
@Controller
class ManualController {

    @RequestMapping(value="/manual", method=RequestMethod.GET)
    String index() {
        return "redirect:/manual/index"
    }

    @RequestMapping(value="/manual/{page}", method=RequestMethod.GET)
    String page(@PathVariable("page") String page) {
        return "manual/${page}.jsp"
    }

}
