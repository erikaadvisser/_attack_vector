package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.user.HackerScanService
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.services.util.UserUtil
import org.n_is_1._attack_vector.web.model.SiteScanInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Controller for hacker main screen
 */
@Controller
@RequestMapping("/")
class HackerController {

    @Autowired HackerScanService hackerScanService
    @Autowired SiteService siteService
    @Autowired ScriptService scriptService

    @RequestMapping(value = "/home")
    public def home(ModelMap map) {
        def scans = hackerScanService.scansForHacker(UserUtil.getUser())

        def scanInfos = scans.collect {
            def site = siteService.getSiteById(it.siteId)
            if (site) {
                return new SiteScanInfo(site.id, site.name, it)
            }
            else {
                def scan = new SiteScan(id: it.id, siteId: it.siteId)
                return new SiteScanInfo(it.id, "Scan obsolete", scan)
            }
        }

        scanInfos.sort { a, b -> a.siteId <=> b.siteId }

        map.addAttribute("scans", scanInfos)
        map.addAttribute("page", "home")
        return "hacker/hackerHome.jsp"
    }

    @RequestMapping(value = "/scripts")
    public def scriptsActive(ModelMap map) {
        return scripts(map, false)
    }

    @RequestMapping(value = "/scripts/deleted")
    public def scriptsDeleted(ModelMap map) {
        return scripts(map, true)
    }

    private def scripts(ModelMap map, boolean deleted) {
        def user = UserUtil.getUser()
        def scripts = scriptService.getScriptsForHacker(user.id).findAll { it.deleted == deleted }
        map.addAttribute("scripts", scripts)
        map.addAttribute("deleted", deleted)
        return "hacker/scripts.jsp";
    }

    @RequestMapping(value = "/scripts/delete/{scriptId}")
    public def deleteScript(@PathVariable(value = "scriptId") String scriptId,
                            RedirectAttributes redirectAttributes) {
        def script = scriptService.delete(scriptId)
        Flash.success("Deleted script ${script.id}", redirectAttributes)
        return "redirect:/scripts"
    }

    @RequestMapping(value = "/scripts/undelete/{scriptId}")
    public def undeleteScript(@PathVariable(value = "scriptId") String scriptId,
                            RedirectAttributes redirectAttributes) {
        def script = scriptService.undelete(scriptId)
        Flash.success("Deleted script ${script.id}", redirectAttributes)
        return "redirect:/scripts"
    }


    @RequestMapping(value = "/scripts/prune_create")
    public def pruneCreate(RedirectAttributes redirectAttributes) {
        String message = scriptService.pruneCreate()
        Flash.success(message, redirectAttributes)
        return "redirect:/scripts"
    }


}
