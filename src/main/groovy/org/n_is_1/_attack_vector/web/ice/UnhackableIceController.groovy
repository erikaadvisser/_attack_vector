package org.n_is_1._attack_vector.web.ice

import org.n_is_1._attack_vector.model.site.ServiceType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Controller for unhackable ice
 */
@Controller
class UnhackableIceController {

    @RequestMapping(path = "ice/unhackable/{runId}/{nodeIid}", method = RequestMethod.GET)
    public String ice(Model model,
                           @PathVariable(value = "runId") String runId,
                           @PathVariable(value = "nodeIid") String nodeIid) {

        model.addAttribute("iceType", ServiceType.ICE_UNHACKABLE)
        return "ice/unhackable.jsp"
    }
}
