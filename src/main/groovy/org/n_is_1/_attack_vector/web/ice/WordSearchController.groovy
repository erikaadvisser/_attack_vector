package org.n_is_1._attack_vector.web.ice

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.services.editor.EditorService
import org.n_is_1._attack_vector.services.ice.wordsearch.WordSearchService
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.IceService
import org.n_is_1._attack_vector.services.util.RandomService
import org.n_is_1._attack_vector.web.ExceptionPrintingController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * The Spring MVC Controller for Word Search Puzzles. This is the controller for the HTML, not the Ajax/REST stuff.
 */
@Controller
class WordSearchController extends ExceptionPrintingController {

    @Autowired RandomService random
    @Autowired WordSearchService service
    @Autowired EditorService editorService
    @Autowired IceService iceService
    @Autowired HackingRunService hackingRunService


    @RequestMapping(path="ice/wordsearch-parent", method = RequestMethod.GET)
    public String wordSearchParent(Model model) {
        def puzzle = service.createPuzzle(15, 20)
        model.addAttribute("puzzle", puzzle)
        return "wordsearch/wordsearch-parent.jsp"
    }

    @RequestMapping(path="ice/wordsearch/{runId}/{serviceId}", method = RequestMethod.GET)
    public String wordSearch(Model model,
                             @PathVariable(value = "runId") String runId,
                             @PathVariable(value = "serviceId") String serviceId) {

        HackingRun run = hackingRunService.getRun(runId)

        def iceData = iceService.retrieveData(run.generation.id, serviceId)
        model.addAttribute("puzzle", iceData)
        model.addAttribute("iceType", ServiceType.ICE_WORD_SEARCH)
        model.addAttribute("serviceId", serviceId)

        return "wordsearch/wordsearch.jsp"
    }


    @RequestMapping(path="wordsearch-print", method = RequestMethod.GET)
    public String wordSearchPrint(Model model) {
        wordSearch(model);
        return "wordsearch/wordsearch-print.jsp"
    }

}