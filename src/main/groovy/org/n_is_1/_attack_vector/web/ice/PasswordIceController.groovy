package org.n_is_1._attack_vector.web.ice

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.services.ice.passwords.PasswordIceService
import org.n_is_1._attack_vector.services.ice.passwords.model.Faction
import org.n_is_1._attack_vector.services.ice.passwords.model.PasswordIcePuzzle
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.IceService
import org.n_is_1._attack_vector.services.util.RandomService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Controller for password type firewalls
 */
@Controller
class PasswordIceController {

    @Autowired PasswordIceService passwordIceService
    @Autowired IceService iceService

    @Autowired HackingRunService hackingRunService


    @RequestMapping(path = "ice/password/{runId}/{serviceId}", method = RequestMethod.GET)
    public String ice(Model model,
                      @PathVariable(value = "runId") String runId,
                      @PathVariable(value = "serviceId") String serviceId) {

        HackingRun run = hackingRunService.getRun(runId)

        PasswordIcePuzzle puzzle = iceService.retrieveData(run.generation.id, serviceId)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("serviceId", serviceId)
        model.addAttribute("iceType", ServiceType.ICE_PASSWORD)

        return "ice/password.jsp"
    }


    @RequestMapping(path="ice/password-parent", method = RequestMethod.GET)
    public String wordSearchParent(Model model) {
        def r = new RandomService().nextInt(5)
        def f= Faction.values()[r]
        def input = [
                strength: "strong",
                faction1: "dugo",
                faction2: f.toString().toLowerCase()
        ]

        def puzzle = passwordIceService.createPuzzle(input)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("serviceId", "123")
        model.addAttribute("iceType", ServiceType.ICE_PASSWORD)

        return "ice/password-parent.jsp"
    }

}

