package org.n_is_1._attack_vector.web.ice

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.services.ice.manual.model.ManualIceData
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.IceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Controller for manual ice
 */
@Controller
class ManualIceController {

    @Autowired IceService iceService
    @Autowired HackingRunService hackingRunService

    @RequestMapping(path = "ice/manual/{runId}/{serviceId}", method = RequestMethod.GET)
    public String ice(Model model,
                           @PathVariable(value = "runId") String runId,
                           @PathVariable(value = "serviceId") String serviceId) {

        HackingRun run = hackingRunService.getRun(runId)


        ManualIceData puzzle = iceService.retrieveData(run.generation.id, serviceId)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("serviceId", serviceId)
        model.addAttribute("iceType", ServiceType.ICE_MANUAL)

        return "ice/manual.jsp"
    }
}
