package org.n_is_1._attack_vector.web.ice

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.model.site.ice.IceStrength
import org.n_is_1._attack_vector.services.ice.netwalk.NetwalkIceGenerator
import org.n_is_1._attack_vector.services.ice.netwalk.NetwalkIceService
import org.n_is_1._attack_vector.services.ice.netwalk.model.NetwalkIcePuzzle
import org.n_is_1._attack_vector.services.ice.passwords.model.PasswordIcePuzzle
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.IceService
import org.n_is_1._attack_vector.services.util.RandomService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * Controller for password type firewalls
 */
@Controller
class NetwalkIceController {

    @Autowired NetwalkIceService netwalkIceService
    @Autowired IceService iceService

    @Autowired HackingRunService hackingRunService


    @RequestMapping(path = "ice/netwalk/{runId}/{serviceId}", method = RequestMethod.GET)
    String ice(Model model,
                      @PathVariable(value = "runId") String runId,
                      @PathVariable(value = "serviceId") String serviceId) {

        HackingRun run = hackingRunService.getRun(runId)
        NetwalkIcePuzzle puzzle = iceService.retrieveData(run.generation.id, serviceId)
        addAttributes(model, puzzle, serviceId)
        return "ice/netwalk.jsp"
    }

    private void addAttributes(Model model, NetwalkIcePuzzle puzzle, String serviceId) {
        model.addAttribute("puzzleDef", puzzle.toJson())
        model.addAttribute("x", puzzle.x)
        model.addAttribute("y", puzzle.y)
        model.addAttribute("serviceId", serviceId)
    }

    @RequestMapping(path="ice/netwalk-parent", method = RequestMethod.GET)
    String wordSearchParent(Model model) {
        def data = [ "strength" : IceStrength.IMPENETRABLE.toString() ]
        def puzzle = netwalkIceService.generate(data)
        addAttributes(model, puzzle, "12")
        return "ice/netwalk-parent.jsp"
    }



}

