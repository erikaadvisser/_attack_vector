package org.n_is_1._attack_vector.web.ice

import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.site.ServiceType
import org.n_is_1._attack_vector.services.editor.EditorService
import org.n_is_1._attack_vector.services.ice.magiceye.model.MagicEyePuzzle
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.site.IceService
import org.n_is_1._attack_vector.web.ExceptionPrintingController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * The Spring MVC Controller for Word Search Puzzles. This is the controller for the HTML, not the Ajax/REST stuff.
 */
@Controller()
class MagicEyeController extends ExceptionPrintingController {

    @Autowired EditorService editorService
    @Autowired IceService iceService
    @Autowired HackingRunService hackingRunService

    @RequestMapping(path = "ice/magiceye-generator")
    public String magicEyeGenerator() {
        return "ice/magiceye-generator.jsp"
    }
    @RequestMapping(path = "ice/magiceye/{runId}/{serviceId}", method = RequestMethod.GET)
    public String magicEye(Model model,
                           @PathVariable(value = "runId") String runId,
                           @PathVariable(value = "serviceId") String serviceId) {

        HackingRun run = hackingRunService.getRun(runId)

        MagicEyePuzzle puzzle = iceService.retrieveData(run.generation.id, serviceId)
        model.addAttribute("puzzle", puzzle)
        model.addAttribute("iceType", ServiceType.ICE_MAGIC_EYE)

        return "ice/magiceye.jsp"
    }
}