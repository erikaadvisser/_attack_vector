package org.n_is_1._attack_vector.web

import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import org.n_is_1._attack_vector.services.admin.BackupService
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.user.HackerScanService
import org.n_is_1._attack_vector.services.util.Flash
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

/**
 * Controller for functions like backup & restore, clean
 */
@Controller
@RequestMapping("/admin")
class AdminController extends ExceptionPrintingController {

    Logger log = LoggerFactory.getLogger(AdminController)

    Properties gitProperties = new Properties()

    @PostConstruct
    public void init() {
        try {
            InputStream is = this.class.getClassLoader().getResourceAsStream("git.properties")
            if (is) {
                gitProperties.load(is)
                is.close()
            }
            else {
                log.error("No git.properties found. Maybe this was not locally build from git.")
            }
        }
        catch (Exception exception) {
            log.error("Failed to load git version information", exception)
            gitProperties.put("git.commit.id.abbrev", "?")
            gitProperties.put("git.commit.message.full", "?")
        }
    }


    private def YYYY_MM_DD = DateTimeFormat.forPattern("YYYY-MM-dd")

    @Autowired BackupService backupService
    @Autowired HackerScanService hackerScanService
    @Autowired HackingRunService hackingRunService
    @Autowired MissionService missionService

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String emptyIndex(Model model) {
        return "redirect:/admin/"
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        Runtime runtime = Runtime.getRuntime()

        long mb = 1024 * 1024
        long maxMemory = (long) (runtime.maxMemory() / mb)
        long allocatedMemory = (long) (runtime.totalMemory() / mb)
        long freeMemory = (long) (runtime.freeMemory() / mb)

        model.addAttribute("maxMemory", maxMemory)
        model.addAttribute("allocatedMemory", allocatedMemory)
        model.addAttribute("freeMemory", freeMemory)

        model.addAttribute("commitIdAbbrev", gitProperties.getProperty("git.commit.id.abbrev"))
        model.addAttribute("commitMessageFull", gitProperties.getProperty("git.commit.message.full"))

        return "admin/admin.jsp"
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String fileUpload(@RequestParam("file") MultipartFile file) {
        backupService.restore(file)

        return "redirect:/gm/sites"
    }


    @RequestMapping(value = "/backup", produces="application/zip")
    @ResponseBody
    public byte[] generateBackup(HttpServletResponse response) {
        String date = LocalDate.now().toString(YYYY_MM_DD)
        response.addHeader("Content-Disposition", "attachment; filename=av-backup-${date}.zip")
        return backupService.generateBackup()
    }

    @RequestMapping(value = "/purge")
    public String purge(RedirectAttributes redirectAttributes) {
        backupService.purge()
        Flash.success("Purged all data", redirectAttributes)
        return "redirect:/admin/"
    }

    @RequestMapping(value = "/removeScans")
    public String removeScans(RedirectAttributes redirectAttributes) {
        if (! hackingRunService.activeRuns().isEmpty()) {
            Flash.error("There are active runs, cannot modify scans", redirectAttributes)
        }
        else {
            hackingRunService.purge()
            hackerScanService.purgeScansAndLinks()
            Flash.success("Removed all scans", redirectAttributes)
        }

        return "redirect:/admin/"
    }

    @RequestMapping(value = "/deleteScansAndMissions")
    public String deleteScansAndMissions(RedirectAttributes redirectAttributes) {
        hackingRunService.purge()
        missionService.purgeMissionsAndSites()
        hackerScanService.purgeScansAndLinks()
        Flash.success("Removed all missions and mission sites", redirectAttributes)

        return "redirect:/admin/"
    }

    @RequestMapping(value = "/deleteLogs")
    public String deleteLogs(RedirectAttributes redirectAttributes) {
        logService.purge()
        Flash.success("Removed all logs", redirectAttributes)

        return "redirect:/admin/"

    }



}
