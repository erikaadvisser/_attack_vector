package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.admin.User
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.util.UserUtil
import org.n_is_1._attack_vector.web.model.MePassCode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Controller for the profile page of the user
 */
@Controller()
@RequestMapping("/me")
class MeController {

    @Autowired UserService userService

    @RequestMapping("/")
    public def home(Model model) {
        model.addAttribute("user", UserUtil.getUser())
        return "me.jsp"
    }

    @RequestMapping("/passcode")
    public def passcode(MePassCode mePassCode, Model model) {
        def user = UserUtil.getUser()
        model.addAttribute("user", user)

        String errorMessage = validatePassCodes(mePassCode, user)
        if (errorMessage) {
            model.addAttribute("flashMessage", errorMessage)
            model.addAttribute("flashType", "error")
            return "me.jsp"
        }
        userService.changePasscode(user, mePassCode.passcode1)
        model.addAttribute("flashMessage", "Passcode updated.")
        model.addAttribute("flashType", "success")
        return "me.jsp"
    }

    String validatePassCodes(MePassCode input, User user) {
        if (input.passcode1 != input.passcode2) {
            return "Passcodes do not match."
        }
        def passcode = input.passcode1

        if (passcode.length() < 8) {
            return "Passcode too short, minimum 8 characters"
        }

        def login = user.loginName

        if (passcode.startsWith(login)) {
            return "Please don't repeat login in passcode"
        }

        return null;
    }
}
