package org.n_is_1._attack_vector.web

import org.codehaus.groovy.runtime.ExceptionUtils
import org.n_is_1._attack_vector.services.util.LogService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.ExceptionHandler

/**
 * Superclass for all controllers to make them print their exceptions. (somehow the @ControllerAdvice solution does
 * not work).
 */
class ExceptionPrintingController {

    def log = LoggerFactory.getLogger(getClass())

    @Autowired LogService logService

    @ExceptionHandler(value = Exception.class)
    public void defaultErrorHandler(Exception e) throws Exception {
        log.error("Caught exception thrown by controller", e)

//        StringWriter errors = new StringWriter()
//        def writer = new PrintWriter(errors)
//        e.stackTrace.each{
//            if (
//                !it.className.startsWith("org.codehaus.groovy") &&
//                !it.className.startsWith("org.eclipse.jetty") &&
//                !it.className.startsWith("org.springframework")) {
//                writer.println(it.toString())
//                println "##${it.className}"
//            }
//        }
//        def stackTrace = errors.toString();


        logService.error("${e.getMessage()}\n${e.stackTrace}")

        throw e;
    }
}
