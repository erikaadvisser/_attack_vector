package org.n_is_1._attack_vector.web

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.log.LogLevel
import org.n_is_1._attack_vector.model.admin.log.LogRecord
import org.n_is_1._attack_vector.model.run.HackingMission
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.services.util.LogService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Controller for the editor.
 */
@Controller
@RequestMapping("/gm")
public class GmController extends ExceptionPrintingController {

    def log = LoggerFactory.getLogger(GmController)

    @Autowired SiteService siteService
    @Autowired LogService logService
    @Autowired UserService userService
    @Autowired MissionService missionService


    @RequestMapping(value = "/sites")
    public def gmSites(Model model) {
        def sites = siteService.getActiveList()
        def inactiveCount = siteService.getInActiveCount()
        model.addAttribute("sites", sites)
        model.addAttribute("active", true)
        model.addAttribute("inactiveCount", inactiveCount)
        return "gm/sites.jsp"
    }

    @RequestMapping(value = "/missions")
    def missions(Model model) {
        def missions = missionService.getAll()
        model.addAttribute("missions", missions)
        return "gm/missions.jsp"
    }

    @RequestMapping(value = "/mission/{missionId}")
    def mission(Model model,
                       @PathVariable(value = "missionId") String missionId) {
        def mission = missionService.getById(missionId)
        def hackersInvolved = missionService.getHackersInvolved(mission)

        DateTime end = new DateTime(mission.endTimeStamp)
        def endTime = end.toString("HH:mm:ss")

        def hackersTraced = missionService.determineTracedHackers(mission)

        model.addAttribute("mission", mission)
        model.addAttribute("hackersInvolved", hackersInvolved)
        model.addAttribute("hackersTraced", hackersTraced)
        model.addAttribute("endTime", endTime)
        return "gm/mission.jsp"
    }


    @RequestMapping(value = "/logs/")
    def gmLogs() {
        return "redirect:/gm/logs/HIGHLIGHT"
    }

    @RequestMapping(value = "/logs/{level}")
    public def gmLogsLevel(Model model,
                           @PathVariable(value = "level") String level) {
        def logLevel = LogLevel.valueOf(level)
        def logs = logService.getLogs(logLevel)
        addUsersToLogs(logs)
        model.addAttribute("logs", logs)
        model.addAttribute("level", level)
        return "gm/logs.jsp"
    }


    @RequestMapping(value = "/sites/inactive")
    public def inactive(Model model) {
        def sites = siteService.getInActiveList()
        model.addAttribute("sites", sites)
        model.addAttribute("active", false)
        model.addAttribute("inactiveCount", 0)
        return "gm/sites.jsp"
    }

    @RequestMapping(value = "/sites/deactivate/{siteId}/")
    public def deactivate(@PathVariable(value = "siteId") String siteId,
                          RedirectAttributes redirectAttributes) {
        String response = siteService.deactivate(siteId)
        Flash.success(response, redirectAttributes)
        return "redirect:/gm/sites"
    }

    @RequestMapping(value = "/sites/activate/{siteId}/")
    public def activate(@PathVariable(value = "siteId") String siteId,
                          RedirectAttributes redirectAttributes) {
        String response = siteService.activate(siteId)
        Flash.success(response, redirectAttributes)
        return "redirect:/gm/sites"
    }


    // -----------------------------

    void addUsersToLogs(Iterable<LogRecord> records) {
        records.each { record ->
            if (record.userId) {
                def userForAction = userService.usersById[record.userId]

                record.userName = (userForAction) ? userForAction.loginName : record.userId
            }
            else {
                record.userName = "-"
            }
        }
    }
}