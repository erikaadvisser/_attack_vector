package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.site.SiteType
import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.ice.passwords.model.Faction
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.web.model.MissionParameters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import static org.springframework.web.bind.annotation.RequestMethod.POST

@Controller
@RequestMapping("/beacon")
class BeaconController {

    @Autowired MissionService missionService
    @Autowired SiteService siteService
    @Autowired UserService userService

    @RequestMapping(value = "", method = RequestMethod.GET)
    String empty() {
        return "redirect:/beacon/"
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    String missionMenuGet(Model model) {
        def parameters = new MissionParameters()
        parameters.time = 59999999
        parameters.difficulty = SiteType.TEMPLATE_MEDIUM
        parameters.reward = 0
        parameters.faction = Faction.OTHER

        model.addAttribute("parameters", parameters)

        userService.loginBeacon()

        return "beacon/createMission.jsp"
    }

    @RequestMapping(value = "/", method = POST)
    String missionMenuPost(MissionParameters parameters, Model model, RedirectAttributes redirectAttributes) {
        def errorMessage = validate(parameters)
        if (errorMessage) {
            model.addAttribute("flashMessage", errorMessage)
            model.addAttribute("parameters", parameters)
            return "beacon/createMission.jsp"
        }

        try {
            missionService.createMission(parameters)
            Flash.success("Created mission: ${parameters.siteId}", redirectAttributes)
            return "redirect:/gm/missions/"
        }
        catch (UserInputException exception) {
            model.addAttribute("flashMessage", exception.getMessage())
            model.addAttribute("parameters", parameters)
            return "beacon/createMission.jsp"
        }
    }

    String validate(MissionParameters parameters) {
        if (parameters.info == null || parameters.info.trim().isEmpty()) {
            return "Please enter the info to find."
        }
        if (parameters.siteId == null || parameters.siteId.trim().isEmpty()) {
            return "Please enter a site id."
        }
        if (siteService.getSiteById(parameters.siteId)) {
            return "There is already a mission with this site id, choose another."
        }
        return null
    }
}
