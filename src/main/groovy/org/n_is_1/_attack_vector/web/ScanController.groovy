package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteScan
import org.n_is_1._attack_vector.services.DebugService
import org.n_is_1._attack_vector.services.run.ScanService
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.user.HackerScanService
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.UserUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Controller that facilitates a run
 */
@Controller
@RequestMapping("/scan")
class ScanController extends ExceptionPrintingController {

    @Autowired SiteService siteService
    @Autowired ScanService scanService
    @Autowired HackerScanService hackerScanService
    @Autowired LogService logService
    @Autowired ScriptService scriptService
    @Autowired DebugService debugService

    @RequestMapping(value = "/lookup/{thingId}/")
    public def lookupSite(@PathVariable(value = "thingId") String thingId,
                          RedirectAttributes redirectAttributes) {

        if (thingId.startsWith("scan-")) {
            def scan = siteService.getScanById(thingId)
            if (scan) {
                return "redirect:/scan/${scan.id}"
            }
            else {
                Flash.error("Scan not found or obsolete: ${thingId}", redirectAttributes)
                return "redirect:/home"
            }
        }

        def site = siteService.getSiteById(thingId)

        if (!site) {
            Flash.error("Site not found: ${thingId}", redirectAttributes)
            return "redirect:/home"
        }

        def generation = siteService.getOrCreateGeneration(thingId)
        def scan = siteService.createScan(generation)
        def user = UserUtil.getUser()
        hackerScanService.addScan(user, scan)

        logService.event(LogAction.SCAN_INIT, "New scan started on ${site.id}", site.id, scan.id, generation.id)

        return "redirect:/scan/${scan.id}"
    }
    @RequestMapping(value = "/{scanId}", method = RequestMethod.GET)
    public def scan(@PathVariable(value = "scanId") String scanId,
                    ModelMap map) {

        def scan = siteService.getScanById(scanId)
        if (scan == null) {
            return handleScanNotFound(map)
        }
        Site site = siteService.getSiteById(scan.siteId)

        def user = UserUtil.getUser()
        hackerScanService.addScan(user, scan)

        def scripts = scriptService.getScriptsForHacker(user.id).findAll { !it.expired && !it.deleted }

        map.addAttribute("scan", scan)
        map.addAttribute("site", site)
        map.addAttribute("scripts", scripts)
        map.addAttribute("page", "scan")
        map.addAttribute("debug", debugService.debug())

        return "hacker/scan.jsp"
    }

    def handleScanNotFound(ModelMap map) {
        def user = UserUtil.getUser()
        def scripts = scriptService.getScriptsForHacker(user.id).findAll { !it.expired && !it.deleted }

        map.addAttribute("scripts", scripts)
        map.addAttribute("page", "scan")

        return "hacker/scan-obsolete.jsp"
    }

    @RequestMapping(value = "/delete/{scanId}")
    public def deleteScan(@PathVariable(value = "scanId") String scanId,
                          RedirectAttributes redirectAttributes) {

        boolean success = hackerScanService.removeScan(UserUtil.getUser(), scanId)

        if (!success) {
            Flash.error("Not found: ${scanId}", redirectAttributes)
            return "redirect:/home"
        }
        Flash.success("Removed: ${scanId}", redirectAttributes)
        return "redirect:/home"
    }


    // ---------------------- REST CONTROLLER -------------------------

    @RequestMapping(value = "/load/{scanId}")
    @ResponseBody
    public def load(@PathVariable(value = "scanId") String scanId) {
        def scan = siteService.getScanById(scanId)
        def site = addNodeStateToSite(scan)
        return site
    }

    Site addNodeStateToSite(SiteScan scan) {
        def site = siteService.getSiteById(scan.siteId).deepClone()
        scan.nodeStatusById.each { nodeId, status ->
            def node = site.nodes.find { it.id == nodeId }
            node.statusForClient = status
        }

        return site
    }

    @RequestMapping(value = "/command/{scanId}", method = RequestMethod.POST)
    @ResponseBody
    public TerminalResponse command(@RequestParam("command") String command,
                                  @PathVariable(value = "scanId") String scanId) {
        return scanService.handleCommand(scanId, command)
    }

    @RequestMapping(value = "/probe/SCAN_CONNECTIONS/{scanId}", method = RequestMethod.POST)
    @ResponseBody
    public def scan_connections(@RequestParam("nodeId") String nodeId,
                                  @PathVariable(value = "scanId") String scanId) {
        return scanService.handleScanConnections(scanId, nodeId)
    }

    @RequestMapping(value = "/probe/SCAN_NODE/{scanId}", method = RequestMethod.POST)
    @ResponseBody
    public def scan_node(@RequestParam("nodeId") String nodeId,
                         @PathVariable(value = "scanId") String scanId) {
        return scanService.handleScanNode(scanId, nodeId)
    }

    @RequestMapping(value = "/probe/ANALYZE/{scanId}", method = RequestMethod.POST)
    @ResponseBody
    public def analyze(@RequestParam("serviceId") String serviceId,
                       @PathVariable(value = "scanId") String scanId) {
        scanService.handleAnalyze(scanId, serviceId)
        return [message: "Ice analyzed, see site report for details."]
    }

    @RequestMapping(value = "/probe/SNOOP/{scanId}", method = RequestMethod.POST)
    @ResponseBody
    public def snoop(@RequestParam("serviceId") String serviceId,
                       @PathVariable(value = "scanId") String scanId) {
        def message = scanService.handleSnoop(scanId, serviceId)
        return [message: message]
    }

}