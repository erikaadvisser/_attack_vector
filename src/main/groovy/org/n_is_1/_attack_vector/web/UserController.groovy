package org.n_is_1._attack_vector.web

import org.n_is_1._attack_vector.model.admin.UserType
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.scripts.model.ScriptType
import org.n_is_1._attack_vector.services.user.UserService
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.web.model.JqueryNotifyResponse
import org.n_is_1._attack_vector.web.model.UserModel
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import javax.validation.Valid

/**
 * Controller for the editor.
 */
@Controller
@RequestMapping("/gm/user")
public class UserController extends ExceptionPrintingController {

    def log = LoggerFactory.getLogger(UserController);

    @Autowired UserService userService
    @Autowired ScriptService scriptService

    @RequestMapping(value = "")
    public def empty() {
        return "redirect:/gm/user/"
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public def list(Model model) {
        def users = userService.list()
        def hackers = users.findAll{ it.type == UserType.HACKER}
        def gmsAdmins = users.findAll{ it.type == UserType.GM || it.type == UserType.ADMIN }
        model.addAttribute("hackers", hackers)
        model.addAttribute("gmsAdmins", gmsAdmins)
        return "gm/user/user_list.jsp";
    }


    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public def newUser(Model model) {
        def userModel = new UserModel()
        model.addAttribute("user", userModel)
        return "gm/user/user_edit.jsp";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public def newUserSubmit(@Valid UserModel userModel, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        def errorMessage = userService.validate(userModel, bindingResult, true)
        if (errorMessage) {
            model.addAttribute("flashMessage", errorMessage)
            model.addAttribute("user", userModel)
            return "gm/user/user_edit.jsp";
        }
        userService.createUser(userModel)
        redirectAttributes.addFlashAttribute("flashMessage", "User '${userModel.loginName}' updated.")
        redirectAttributes.addFlashAttribute("flashType", "success")
        return "redirect:/gm/user/";
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public def user(Model model, @PathVariable(value = "userId") String userId ){
        def user = userService.usersById[userId]
        def userModel = new UserModel(user)
        model.addAttribute("user", userModel)
        def scripts = scriptService.getScriptsForHacker(user.id).findAll { !it.expired }
        model.addAttribute("scripts", scripts)

        return "gm/user/user_edit.jsp";
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    public def editUserSubmit(@Valid UserModel userModel, BindingResult bindingResult, Model model,
                             @PathVariable(value = "userId") String userId, RedirectAttributes redirectAttributes ) {
        boolean passcodeSupplied = userModel.passcode != null && (!userModel.passcode.trim().isEmpty())

        def errorMessage = userService.validate(userModel, bindingResult, passcodeSupplied)
        if (errorMessage) {
            model.addAttribute("flashMessage", errorMessage)
            model.addAttribute("user", userModel)
            return "gm/user/user_edit.jsp";
        }
        userService.updateUser(userModel)
        def passcodeSuffix = (passcodeSupplied) ? ", passcode updated." : "."
        Flash.success("User '${userModel.loginName}' updated${passcodeSuffix}", redirectAttributes)
        return "redirect:/gm/user/";
    }


    @RequestMapping(value = "/unblock/{userId}")
    public def unblockUser(@PathVariable(value = "userId") String userId, RedirectAttributes redirectAttributes ) {
        String message = userService.unblock(userId)
        Flash.success(message, redirectAttributes)
        return "redirect:/gm/user/";
    }



    // -- -- -- -- REST CALLS -- -- -- -- //


    @RequestMapping(value = "/script")
    @ResponseBody
    public JqueryNotifyResponse script(@RequestBody def command) {
        try {
            String scriptId = command.script_id
            String userId = command.hacker_id
            String action = command.action

            performAction(action, userId, scriptId)
            return new JqueryNotifyResponse("ok", "success", true)
        }
        catch (Exception e) {
            log.error("Save failed", e)
            return new JqueryNotifyResponse("Save failed: ${e.getMessage()}", "error", false);
        }
    }

    private def performAction(String action, String userId, String scriptId) {
        switch(action) {
            case "add_mask": return scriptService.create(userId, ScriptType.MASK)
            case "add_elevate": return scriptService.create(userId, ScriptType.ELEVATE)
            case "add_snoop": return scriptService.create(userId, ScriptType.SNOOP)
            case "add_analyze": return scriptService.create(userId, ScriptType.ANALYZE)
            case "add_bypass": return scriptService.create(userId, ScriptType.BYPASS)
            case "add_deflect": return scriptService.create(userId, ScriptType.DEFLECT)
            case "add_storm": return scriptService.create(userId, ScriptType.STORM)
            case "add_softdos": return scriptService.create(userId, ScriptType.SOFTDOS)
            case "add_backdoor": return scriptService.create(userId, ScriptType.BACKDOOR)
            case "set_used": return scriptService.setUsed(userId, scriptId)
            case "set_unused": return scriptService.setUnused(userId, scriptId)
            case "delete": return scriptService.deleteOrUndelete(scriptId)
            default: throw new RuntimeException("Unknown action: ${action}")
        }
    }
}