package org.n_is_1._attack_vector.web

import org.joda.time.DateTime
import org.n_is_1._attack_vector.model.admin.log.LogAction
import org.n_is_1._attack_vector.model.run.HackingRun
import org.n_is_1._attack_vector.model.run.NodeStatus
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.services.DebugService
import org.n_is_1._attack_vector.services.mission.MissionService
import org.n_is_1._attack_vector.services.run.HackingRunService
import org.n_is_1._attack_vector.services.scripts.ScriptService
import org.n_is_1._attack_vector.services.site.SiteService
import org.n_is_1._attack_vector.services.terminal.TerminalService
import org.n_is_1._attack_vector.services.terminal.model.TerminalResponse
import org.n_is_1._attack_vector.services.util.Flash
import org.n_is_1._attack_vector.services.util.LogService
import org.n_is_1._attack_vector.services.util.UserUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Controller that facilitates a run
 */
@Controller
@RequestMapping("/run")
class HackingRunController extends ExceptionPrintingController {

    @Autowired SiteService siteService
    @Autowired HackingRunService hackingRunService
    @Autowired TerminalService terminalService
    @Autowired LogService logService
    @Autowired ScriptService scriptService
    @Autowired MissionService missionService
    @Autowired DebugService debugService

    @RequestMapping(value = "/{runId}")
    public def run(Model model,
                   @PathVariable(value = "runId") String runId,
                   RedirectAttributes redirectAttributes) {
        HackingRun run = hackingRunService.getRun(runId)

        if (!run) {
            Flash.error("Hack could not be resumed, possible security update.", redirectAttributes)
            return "redirect:/home/"
        }

        def user = UserUtil.getUser()

        if (run.hackerId != user.id) {
            Flash.error("You cannot  share an attack URL with other hackers, you need to start your own attack", redirectAttributes)
            return "redirect:/scan/${run.scan.id}"
        }

        def scripts = scriptService.getScriptsForHacker(user.id).findAll { !it.expired  && !it.deleted }

        model.addAttribute("run", run)
        model.addAttribute("currentMillis", System.currentTimeMillis())
        model.addAttribute("scripts", scripts)
        model.addAttribute("page", "run")
        model.addAttribute("debug", debugService.debug())

        return "run/hackingRun.jsp"
    }

    @RequestMapping(value = "/terminal/send/{runId}", method = RequestMethod.POST)
    @ResponseBody
    public TerminalResponse index(@RequestParam("command") String command,
                                  @PathVariable(value = "runId") String runId) {
        HackingRun run = hackingRunService.getRun(runId)
        if (!run) {
            return new TerminalResponse(message: "Persona stability decreasing. Security update in progress.", type: "RESET")
        }
        if (!run.active) {
            return new TerminalResponse(message: "Persona stability decreasing due to multiplexing. Aborting this persona.", type: "RESET")
        }

        logService.actionForRun(LogAction.TERM_COMMAND, command, run)
        def response = terminalService.processCommand(run, command)
        logService.actionForRun(LogAction.TERM_RESPONSE, response.message, run)

        return response;
    }

    @RequestMapping(value = "/load/{runId}")
    @ResponseBody
    public def load(@PathVariable(value = "runId") String runId) {
        HackingRun run = hackingRunService.getRun(runId)
        def site = addNodeStateToSite(run)
        return site
    }

    Site addNodeStateToSite(HackingRun run) {
        Site site = run.site.deepClone()
        run.nodeStatusById.each { nodeId, status ->
            def node = site.nodes.find { it.id == nodeId }
            node.statusForClient = status
        }

        return site
    }

    @RequestMapping(value = "/startClock/{runId}")
    @ResponseBody
    public long startClock(@PathVariable(value = "runId") String runId) {
        def run = hackingRunService.startClock(runId)
        def detectTime = new DateTime(run.traceEnd).toString("HH:mm")
        logService.actionForRun(LogAction.HACKING_RUN_CLOCK_START, "Clock started for run, detect time: ${detectTime}", run)
        return run.traceEnd
    }


    @RequestMapping(value = "/hacked/{runId}/{serviceId}")
    @ResponseBody
    public Object hacked(@PathVariable(value = "runId") String runId,
                         @PathVariable(value = "serviceId") String serviceId,
                         @RequestParam(value = "seconds") int seconds) {
        String nodeId = hackingRunService.hack(runId, serviceId)

        boolean nodeHacked = (nodeId != null)

        def minutes = (int)(seconds / 60);
        def secondsLeft = (int)(seconds % 60);

        def hackingRun = hackingRunService.getRun(runId)
        def service = hackingRun.site.findServiceById(serviceId)
        def difficulty = service.data['strength']

        def msg = "Hacked ICE ${service.hackingRunName()} - ${difficulty} in ${minutes}:${secondsLeft}"

        logService.event(LogAction.HACKING_RUN_ICE_HACKED, msg, null, runId)

        return [
                allHacked: nodeHacked,
                nodeId: nodeId
        ]
    }

    @RequestMapping(value = "/nodeStatus/{runId}/{nodeId}")
    @ResponseBody
    public Object nodeStatus(@PathVariable(value = "runId") String runId,
                                @PathVariable(value = "nodeId") String nodeId) {
        NodeStatus status = hackingRunService.updateNodeStatus(runId, nodeId)

        return [ status: status ]
    }

    @RequestMapping(value = "/detected_timeout/{runId}")
    @ResponseBody
    Object detected(@PathVariable(value = "runId") String runId) {
        hackingRunService.detectedTimeOut(runId)
        missionService.detectTimeOut(runId)
        return "ok"
    }

}