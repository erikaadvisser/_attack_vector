package org.n_is_1._attack_vector.model.site.services

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteType

class RemoteTracerValidator extends ServiceValidator {

    String validate_internal(NodeService service, SiteNode ignore, Site site) {
        if (site.type == SiteType.TEMPLATE_TRACER ) {
            return "Tracer services are part of the target site, not the trace site. (Cannot nest tracers in trace sites.)."
        }

        if (!site.type.isTemplate()) {
            return "Tracer services can only be uses in mission templates."
        }

        return null
    }
}
