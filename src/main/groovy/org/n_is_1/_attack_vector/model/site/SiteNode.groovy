package org.n_is_1._attack_vector.model.site

import groovy.transform.AutoClone
import org.n_is_1._attack_vector.model.run.NodeStatus

/**
 * A site node.
 *
 * Note: For ice nodes it does NOT contain the actual generated ice puzzle data.
 * This data is stored separately by the IceService.
 */
@AutoClone
class SiteNode {

    /** out of game id, unique and unchangeable */
    String id

    NodeType nodeType

    /** This one is used by the client, when site is loaded during scan or run */
    transient NodeStatus statusForClient

    int x
    int y
    int size

    /** NodeIds of connected nodes */
    List<String> connections = []

    /** The services that run on this node. The first service will always be the OS service. */
    List<NodeService> services

    boolean hasIce() {
        def iceService = this.services.find({ (it.type.isIce()) })
        return ( iceService != null)
    }

    String toString() {
        try {
            return services[0].data['networkId']
        }
        catch (NullPointerException  ignore) {
            return this.id
        }
    }

    String deriveNetworkId() {
        return services[0].data['networkId']
    }
}
