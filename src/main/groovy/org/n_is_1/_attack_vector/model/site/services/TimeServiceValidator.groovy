package org.n_is_1._attack_vector.model.site.services

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.services.util.TimeParseUtil

/**
 * Validator for the ServiceType.CODE
 */
class TimeServiceValidator extends ServiceValidator {

    def timeParseUtil = new TimeParseUtil()

    String validate_internal(NodeService service, SiteNode ignore, Site site) {
        String timeString  = service.data['time']
        if (!timeString) {
            return "Time input is mandatory, see Time"
        }
        timeParseUtil.parseToMillis(timeString, "")
        return null
    }
}
