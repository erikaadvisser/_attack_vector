package org.n_is_1._attack_vector.model.site

import groovy.transform.AutoClone
import org.n_is_1._attack_vector.model.site.services.OsService

import static org.n_is_1._attack_vector.model.site.SiteType.REGULAR

/**
 * A representation of a site.
 */
@AutoClone
public class Site {

    transient String _OC_WARNING_ = "▣ ▣ LET OP: je bent nu OC aan het hacken, dit is tegen de regels en geest van Frontier. ▣ ▣"

    /** site ID , also used as the URL in game */
    String id

    /** synthetic id, used when site ID cannot be used, for instance when using the
     * site ID as a key in a map stored by MongoDB.
     */
    String uid

    String name
    String description
    String faction
    String gmName
    String startNodeNetworkId
    String startNodeId
    String hackTime
    SiteType type = REGULAR
    int templateUses = 0
    boolean hackable = false
    boolean active = true
    boolean scannable = true
    String missionId = null

    transient Map<String, NodeService> servicesById = [:]

    List<SiteNode> nodes = []

    /** Only one softdos can be active at one time */
    boolean softdos = false



    Site deepClone() {
        Site clone = this.clone()
        clone.servicesById = this.servicesById.collectEntries { k, v -> [k, v.clone()] }
        return clone
    }

    NodeService findServiceById(String id) {
        if (!servicesById) {
            initServicesById()
        }
        return servicesById[id]
    }

    public void initServicesById() {
        nodes.each { node ->
            node.services.each { service ->
                String id = service.data['id']
                servicesById[id] = service
            }
        }
    }

    SiteNode findNodeById(String nodeId) {
        return nodes.find { it.id == nodeId }
    }

    SiteNode findNodeByNetworkId(String networkId) {
        return nodes.find { it.services[0].data[OsService.DATA_NETWORK_ID] == networkId }
    }

    SiteNode findNodeByServiceId(String serviceId) {
        return nodes.find { node ->
            def service = node.services.find{ service ->
                return service.id == serviceId
            }
            return service != null
        }
    }

    List<String> findConnectedNodeIds(SiteNode from) {
        def currentNodeId = from.id
        List<String> connectedNodeIds = (List<String>)from.connections.clone()

        this.nodes.each { node ->
            node.connections.each { targetNodeId ->
                if (targetNodeId == currentNodeId ) {
                    connectedNodeIds.add(node.id)
                }
            }
        }

        return connectedNodeIds
    }

    List<NodeService> findAllServicesByType(ServiceType type) {
        return servicesById.values().findAll{ it.type == type }
    }
}
