package org.n_is_1._attack_vector.model.site

import groovy.transform.AutoClone
import org.n_is_1._attack_vector.services.util.ConfigUtil

/**
 * This class represents a service running on a node
 */
@AutoClone
class NodeService {

    ServiceType type

    /** ID of this service, also value of data['id]' */
    String id

    /** 0 = OS, higher = service layers. Also value of data['layer']. */
    int layer

    /**
     * Key value pairs, as defined in "node_definitions.js".
     * These must are the stringly equivalents of the ice configs,such as:
     * - MagicEyeConfig
     */
    Map<String, String> data


    String snoopInfo(Site site) {
        if (type == ServiceType.TIME) {
            def timeGain = data['time']
            return "Detection delay ${timeGain}"
        }
        if (type == ServiceType.CODE) {
            def targetId = data['targetId']
            def targetNode = site.findNodeByServiceId(targetId)
            String info = "Contains passcode of Ice located in node ${targetNode}, layer ${layer}"
            return info
        }
        if (type == ServiceType.SYSCON) {
            def configUtil = new ConfigUtil()

            boolean disableIce = configUtil.parseBoolean(data['ice'], null)
            boolean disableTimer = configUtil.parseBoolean(data['timer'], null)
            String info = "Reveals site map"

            if (disableTimer) {
                info += ", disables detection"
            }
            if (disableIce) {
                info += ", disables all Ice"
            }
            return info
        }
        return "Invalid service targeted, no information gained."
    }

    boolean hasScanInfo() {
        return (type == ServiceType.REMOTE_TRACER)
    }

    String scanInfo() {
        if (type == ServiceType.REMOTE_TRACER) {
            if (data["scannable"].trim().toLowerCase() == "true" ) {
                def traceLogSiteId = data['siteId']
                return "Trace log site: ${traceLogSiteId}"
            }
            else {
                return "Trace log site: <masked>"
            }

        }
        return "-"
    }

    String hackingRunName() {
        return data["runName"] ? data["runName"] : type.text
    }

    String scanName() {
        return data["scanName"] ? data["scanName"] : type.text
    }

}
