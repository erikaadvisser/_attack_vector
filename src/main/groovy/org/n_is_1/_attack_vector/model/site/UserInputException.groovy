package org.n_is_1._attack_vector.model.site

/**
 * These exceptions do not need to be logged, they are expected user input errors.
 */
class UserInputException extends RuntimeException {

    UserInputException(String message) {
        super(message)
    }
}
