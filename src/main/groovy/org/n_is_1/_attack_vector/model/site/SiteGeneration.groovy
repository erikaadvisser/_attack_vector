package org.n_is_1._attack_vector.model.site

/**
 * This class defines a generation of a site: a version with all the random element having specific values:
 * each ICE (puzzle) has a specific solution.
 *
 * Sites can have multiple generations, where each generation the ICE puzzles have different solutions.
 */
class SiteGeneration {

    String id

    String siteId

    int versionNumber = 1

    /** The objects in this map are ICE puzzle data, for example: WordSearchPuzzle */
    Map<String, IceGeneration> iceGenerationsById = [:]
}
