package org.n_is_1._attack_vector.model.site.services

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.editor.SiteValidationException
import org.slf4j.LoggerFactory

/**
 * Generic interface for services that can validate their (editor supplied) data.
 */
abstract class ServiceValidator {

    def log = LoggerFactory.getLogger(this.getClass())

    def validate(NodeService service, SiteNode node, Site site) {
        String error
        try {
            error = validate_internal(service, node, site)
        }
        catch (UserInputException exception) {
            error = exception.getMessage()
        }
        catch (Exception unexpectedException) {
            log.warn("Unexpected exception", unexpectedException)
            error = unexpectedException.getMessage()
        }

        if (!error) {
            return
        }

        int index = node.services.indexOf(service)

        def msg = "${error} node: '${node.services[0].data['networkId']}' layer #${index}"
        throw new SiteValidationException(msg)

    }

    abstract validate_internal(NodeService service, SiteNode node, Site site);
}
