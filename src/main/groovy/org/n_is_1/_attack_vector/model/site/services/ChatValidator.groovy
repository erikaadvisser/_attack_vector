package org.n_is_1._attack_vector.model.site.services

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.UserInputException
import org.n_is_1._attack_vector.services.util.ConfigUtil

/**
 * Validator for Syscon services
 */
class ChatValidator extends ServiceValidator {


    String validate_internal(NodeService service, SiteNode ignore, Site site) {
        def welcome = service.data['welcome']
        def authorName = service.data['authorName']

        if (!welcome || welcome.trim().isEmpty()) {
            throw new UserInputException("Welcome message is mandatory. It's the only thing players will see when hacking this service. Invite them to start typing...")
        }

        if (!authorName || authorName.trim().isEmpty()) {
            throw new UserInputException("Author name message is mandatory. This is the name attached to the GM's messages.")
        }

        return null
    }
}
