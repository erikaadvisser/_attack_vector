package org.n_is_1._attack_vector.model.site

import org.n_is_1._attack_vector.model.run.NodeStatus

/**
 * This class defines the state of a site as it is known to one or more hackers as part of a scan
 */
class SiteScan {

    /** Unique ID of a scan, also used for hackers to share this scan and contribute to it */
    String id

    String siteId

    String generationId

    boolean scanComplete = false

    /** The scan status of each node */
    Map<String, NodeStatus> nodeStatusById = [:]

    /** list of ids of nodes whose connections are known. */
    List<String> connectionScannedNodeIds = []

    /** list of serviceIds of services that were analyzed (or snooped) , i.e. their puzzle is known to the hackers or the spoils are revealed. */
    List<String> analyzedServiceIds = []
}