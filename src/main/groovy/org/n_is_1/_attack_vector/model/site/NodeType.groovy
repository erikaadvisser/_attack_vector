package org.n_is_1._attack_vector.model.site
/**
 * Enum representing the various node types
 */
enum NodeType {

    SYSCON("Syscon",           false, "symbols-shapes/cube.png"),
    INFO("Information vault",  false, "symbols-shapes/tile4-sc36.png"),
    PASSWORD("Password vault", false, "symbols-shapes/puzzle3-ps.png"),
    RESOURCE("Resource vault", false, "symbols-shapes/tile2.png"),
    NODE_SQUARE("Transit",     false, "symbols-shapes/shape-square-frame.png"),
    NODE_CIRCLE("Transit",     false, "symbols-shapes/shapes-circle-frame.png"),
    NODE_DIAMOND("Transit",    false, "symbols-shapes/shapes-diamond-frame.png"),
    NODE_HEXAGON("Transit",    false, "symbols-shapes/shapes-hexagon-frame.png"),
    FW_WORD_SEARCH("Pumer",     true, "symbols-shapes/spinner2-sc30.png"),
    FW_MAGIC_EYE("Pryder",      true, "symbols-shapes/spinner3-sc36.png"),
    FW_PASSWORD("Kama",        false, "symbols-shapes/spinner8-sc36.png"),
    FW_UNHACKABLE("Jagannatha", true, "natural-wonders/snowflake4-sc37.png"),
    FW_MANUAL_1(" Mitra",       true, "natural-wonders/flower3.png"),
    FW_MANUAL_2("Andraste",     true, "natural-wonders/flower13-sc36.png"),
    FW_MANUAL_3("Vayu",         true, "natural-wonders/flower-cauliflower.png");

    String text
    boolean ice
    String imageName

    NodeType(String text, boolean ice = false, String imageName) {
        this.text = text
        this.ice = ice
        this.imageName = imageName
    }

}