package org.n_is_1._attack_vector.model.site.ice

/**
 * Strength of ICE
 */
enum IceStrength {

    VERY_WEAK("Very weak"),
    WEAK("Weak"),
    AVERAGE("Average"),
    STRONG("Strong"),
    VERY_STRONG("Very strong"),
    IMPENETRABLE("Impenetrable");

    String name

    IceStrength(String name) {
        this.name = name
    }
}
