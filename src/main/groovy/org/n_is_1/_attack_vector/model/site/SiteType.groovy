package org.n_is_1._attack_vector.model.site

enum SiteType {
    MISSION ("-", false),  // as in: this site is part of a mission.
    REGULAR ("non-mission", false),
    TEMPLATE_TRACER ("-", false),
    TEMPLATE_SINGLE ("single", true),
    TEMPLATE_EASY ("easy", true),
    TEMPLATE_MEDIUM ("medium", true),
    TEMPLATE_HARD("hard", true),
    TEMPLATE_UNHACKABLE("unhackable", true)

    String difficulty
    boolean template

    SiteType(String difficulty, boolean template) {
        this.difficulty = difficulty
        this.template = template
    }

}