package org.n_is_1._attack_vector.model.site.services

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.model.site.SiteType

class TraceLogsValidator extends ServiceValidator {

    String validate_internal(NodeService service, SiteNode ignore, Site site) {
        if (site.type != SiteType.TEMPLATE_TRACER) {
            return "Trace logs services can only exist in site type: (Template) Tracer."
        }

        return null
    }
}
