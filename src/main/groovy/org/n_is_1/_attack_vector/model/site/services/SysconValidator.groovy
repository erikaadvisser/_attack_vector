package org.n_is_1._attack_vector.model.site.services

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode
import org.n_is_1._attack_vector.services.util.ConfigUtil

/**
 * Validator for Syscon services
 */
class SysconValidator extends ServiceValidator {

    def configUtil = new ConfigUtil()

    String validate_internal(NodeService service, SiteNode ignore, Site site) {
        def disableIceInput = service.data['ice']
        def disableTimerInput = service.data['timer']

        configUtil.parseBoolean(disableIceInput, "Disable Ice, ")
        configUtil.parseBoolean(disableTimerInput, "Stop timer, ")

        return null
    }
}
