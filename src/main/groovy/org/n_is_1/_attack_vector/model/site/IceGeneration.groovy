package org.n_is_1._attack_vector.model.site

/**
 * This class defines a specific generation of version of ICE data. This means fixed values for the ICE puzzle.
 * If you encounter the same IceGeneration twice, the same solution will work, and it will have the same passcode.
 */
class IceGeneration {

    /** The id ICE service that this is a generation for */
    String serviceId

    /** Generation 1, generation 2... */
    String generationId

    ServiceType type

    /** Using this code, you can bypass the ICE */
    String passcode

    /** Unstructured data of the ICE. The data depends on the ICE type */
    def iceData


}
