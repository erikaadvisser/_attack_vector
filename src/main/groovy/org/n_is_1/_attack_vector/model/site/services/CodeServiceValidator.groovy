package org.n_is_1._attack_vector.model.site.services

import org.n_is_1._attack_vector.model.site.NodeService
import org.n_is_1._attack_vector.model.site.Site
import org.n_is_1._attack_vector.model.site.SiteNode

/**
 * Validator for the ServiceType.CODE
 */
class CodeServiceValidator extends ServiceValidator {

    String validate_internal(NodeService service, SiteNode ignore, Site site) {
        String targetId = service.data['targetId']
        if (!targetId) {
            return "Target id is mandatory, see Code"
        }

        boolean success = false
        String error = null

        site.nodes.each{ node ->
            node.services.each { otherService ->
                if (otherService.id == targetId) {
                    if (otherService.type.isIce()) {
                        success = true;
                        return 0
                    }
                    error = "Target service (${targetId}) is not ICE, see Code"
                    return 0
                }
            }
        }

        if (success) {
            return null
        }
        return (error) ? error: "Target service id '${targetId}' not found, see Code"
    }
}
