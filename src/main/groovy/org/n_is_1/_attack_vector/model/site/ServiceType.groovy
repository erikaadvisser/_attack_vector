package org.n_is_1._attack_vector.model.site

import org.n_is_1._attack_vector.model.site.services.ChatValidator
import org.n_is_1._attack_vector.model.site.services.CodeServiceValidator
import org.n_is_1._attack_vector.model.site.services.RemoteTracerValidator
import org.n_is_1._attack_vector.model.site.services.ServiceValidator
import org.n_is_1._attack_vector.model.site.services.SysconValidator
import org.n_is_1._attack_vector.model.site.services.TimeServiceValidator
import org.n_is_1._attack_vector.model.site.services.TraceLogsValidator
import org.n_is_1._attack_vector.services.ice.magiceye.MagicEyeValidator
import org.n_is_1._attack_vector.services.ice.manual.ManualIceValidator
import org.n_is_1._attack_vector.services.ice.netwalk.NetwalkValidator
import org.n_is_1._attack_vector.services.ice.passwords.model.PasswordIceValidator
import org.n_is_1._attack_vector.services.ice.wordsearch.WordSearchValidator

/**
 * Definitions of the services a node can have.
 */
enum ServiceType {

    OS("Node OS", "775/2", null, null, []),
    SYSCON("Central core", "875/4", null, new SysconValidator(), ["ice", "timer"]),
    TEXT("Data vault", "873/1", null, null, ["text"]),
    CHAT("Chat client", "863/9", null, new ChatValidator(), ["welcome", "authorName"]),
    FILE("Data vault", "874/3", null, null, ["url"]),
    TIME("Local core", "877/4", null, new TimeServiceValidator(), ["time"]),
    CODE("Passcode vault", "876/3", null, new CodeServiceValidator(), ["targetId"]),
    RESOURCE("Data bank", "801/3", null, null, ["description", "value"]),
    LOG_DB( "Log vault", "901/3", null, null, []),
    SCAN_BLOCKER("Scan blocker", "893/2", null, null, []),
    REMOTE_TRACER( "Remote tracer", "922/1", null, new RemoteTracerValidator(), ['siteId']),
    TRACE_LOGS("Trace logs", "922/1", null, new TraceLogsValidator(), []),
    ICE_WORD_SEARCH("Pumer", "883/3", "/ice/wordsearch", new WordSearchValidator(), []),
    ICE_MAGIC_EYE("Pryder", "881/7", "/ice/magiceye", new MagicEyeValidator(), []),
    ICE_PASSWORD("Kama", "880/4", "/ice/password", new PasswordIceValidator(), []),
    ICE_MANUAL("Mitra", "882/4", "/ice/manual", new ManualIceValidator(), []),
    ICE_UNHACKABLE("Jagannatha", "902/3", "/ice/unhackable", null, []),
    ICE_NETWALK("Dahana", "881/2", "/ice/netwalk", new NetwalkValidator(), [] )

    String text
    String version
    String iceUrl
    ServiceValidator validator
    List<String> gmPrintData

    ServiceType(String text, String version, String iceUrl, ServiceValidator validator, List<String> gmPrintData) {
        this.text = text
        this.version = version
        this.iceUrl = iceUrl
        this.validator = validator
        this.gmPrintData = gmPrintData
    }

    def validate(NodeService service, SiteNode node, Site site) {
        if (validator) {
            validator.validate(service, node, site)
        }
    }

    boolean isIce() {
        return iceUrl != null
    }
}
