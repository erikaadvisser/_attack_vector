package org.n_is_1._attack_vector.model.chat

class ChatMessage {

    String id
    String authorName
    String message
    String timeString // format: "18:20:20"
    long timestamp


}
