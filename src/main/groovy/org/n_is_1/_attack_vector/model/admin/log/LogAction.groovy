package org.n_is_1._attack_vector.model.admin.log

/**
 * Actions that are logged
 */
enum LogAction {

    ERROR,                          // generic action that triggers error. To prevent ease reporting of errors

    LOGIN,                          // any login action success and failure
    TERM_COMMAND,                   // any terminal command entered during a hacking run or scan
    TERM_RESPONSE,                  // system response to terminal command
    SCAN_INIT,                      // hacker starts scan on site
    SCAN_COMMAND,                   // run the 'scan' command
    SCAN_COMPLETED,                 // scan completed
    SCAN_ANALYZE,                   // script/analysis of service performed
    HACKING_RUN_START,              // hacker starts run
    HACKING_RUN_DENIED,             // hacker wanted to hack a site that was not open to hacking
    HACKING_RUN_CLOCK_START,        // clock started on hacking run
    HACKING_RUN_ICE_HACKED,         // hacked an ice layer
    HACKING_RUN_ICE_HACK_START,     // start hacking ice
    HACKING_RUN_RECOVERED_PASSCODE, // hacked and recovered passcode
    HACKING_RUN_RECOVERED_FILE,     // found file (text or url)
    HACKING_RUN_RECOVERED_RESOURCE, // found resource
    HACKING_RUN_RECOVERED_TIME,     // found extra time
    HACKING_RUN_RECOVERED_SYSCON,   // hacked syscon
    HACKING_RUN_MOVE,               // move to new node
    HACKING_RUN_USE_PASSCODE,       // passcode used to hack ice
    HACKING_RUN_SCRIPT,             // script used
    HACKING_RUN_DETECTED_TIMEOUT,   // Hacker detected due to timeout on run

    GM_ADD_SCRIPT,                  // GM gives a script to a hacker
    GM_USE_SCRIPT,                  // GM sets a script as used
    GM_UNUSE_SCRIPT,                // GM allows a script to be re-used
    GM_OBSOLETE_SCAN_OR_RUN,        // GM saved the site, an existing scan or run existed for it.

    DEV_LOGIN,                      // login as someone (developer without entering password)
    GM_LOGIN_AS,                    // login as gm as someone else

}
