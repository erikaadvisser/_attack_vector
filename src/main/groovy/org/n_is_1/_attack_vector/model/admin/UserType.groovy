package org.n_is_1._attack_vector.model.admin

/**
 * types of users
 */
enum UserType {

    HACKER, // regular player
    GM,     // regular GM
    ADMIN   // developer / admin. Is both Hacker, GM and has access to admin functions
}