package org.n_is_1._attack_vector.model.admin.log

import org.joda.time.DateTime

/**
 * A record of an event that was logged
 */
class LogRecord {

    long timestamp
    String message
    LogAction action
    LogLevel level
    String userId

    // optional fields
    String siteId
    String generationId
    String scanId
    String runId
    String serviceId

    // for display on screen only
    transient String userName
    transient String displayTime
}
