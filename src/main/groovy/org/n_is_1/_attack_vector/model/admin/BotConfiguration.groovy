package org.n_is_1._attack_vector.model.admin

class BotConfiguration {

    String id
    String botToken
    String channelId = "-"
    String channelName = "Remove this config"

    @Deprecated
    String channel
    @Deprecated
    String description


    // there should only be one configuration with this flag set to true
    boolean active

    String pmUserId = "-"
    String pmUserName = "Remove this config"
}
