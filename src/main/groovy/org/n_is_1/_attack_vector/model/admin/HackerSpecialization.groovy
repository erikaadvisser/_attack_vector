package org.n_is_1._attack_vector.model.admin

/**
 * The IT skill specializations
 */
enum HackerSpecialization {
    NONE,
    ELITE,
    ARCHITECT

}
