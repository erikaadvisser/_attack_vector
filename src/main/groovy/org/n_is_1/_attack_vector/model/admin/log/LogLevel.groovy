package org.n_is_1._attack_vector.model.admin.log

/**
 * Levels of logging
 */
enum LogLevel {

    DEBUG,      // anything developer is interested in
    ERROR,      // errors that occur, for bug detecting and fixing
    ACTION,     // action log : every action a player takes. every command she types
    EVENT,      // event log  : actions and events with some significance (hack on service started, hack completed)
    HIGHLIGHT,  // highlight log : events interesting to the GM. Ex: player started hacking run, player received info from nowe, player got caught.
    GM          // gm audited actions. Ex: give script
}
