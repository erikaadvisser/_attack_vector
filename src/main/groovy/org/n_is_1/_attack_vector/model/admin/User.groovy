package org.n_is_1._attack_vector.model.admin

import org.n_is_1._attack_vector.web.model.UserModel

/**
 * Global user class, both for hackers and for GMs
 */
class User {

    /** unique id */
    String id

    /** Disabled hackers cannot login */
    boolean enabled

    /** Failed login counter */
    int failedLoginCount

    /** Blocked due to too many failed logins */
    boolean blocked

    /** Time when the block will be removed. */
    long blockedUntil


    /** Name used for login. For GM this is the GM name, for hackers this is their handle. */
    String loginName

    /** Player name, for SL only */
    String ocName

    String passcodeHash

    /** Must defeat those rainbow tables */
    String salt

    UserType type



    // --- the following fields are only used by hackers, for GMs there are null.

    /** Character name. Real name, not a hacker alias, for SL only */
    String icName

    /** Skill level, Optional, ranges from 0 - 10 */
    Integer skill_it_main

    /** IT Skill specialisation. */
    HackerSpecialization specialization


    User() {
        // empty constructor for persistence frameworks and alike
    }

    User(String id, UserModel model) {
        this.id = id
        update(model)
    }

    def update(UserModel model) {
        this.enabled = model.enabled
        this.blocked = model.blocked
        this.loginName = model.loginName
        this.ocName = model.ocName
        this.type = model.type
        this.icName = model.icName
        this.skill_it_main = model.skill_it_main
        this.specialization = model.specialization
    }
}
