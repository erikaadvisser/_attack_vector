package org.n_is_1._attack_vector.model.hacker

import groovy.transform.EqualsAndHashCode

/**
 * This class is a link between a hacker and a scan. Multiple hackers can have the same scan
 */
@EqualsAndHashCode
class HackerScanLink {

    String id
    String userId
    String scanId
    String siteId

    HackerScanLink() {
        // no arg constructor to allow backup/restore to work.
    }


}
