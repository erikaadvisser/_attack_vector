package org.n_is_1._attack_vector.model.run

/**
 * Enum for the different (visual) states of a node during a run.
 */
enum NodeStatus {
    UNDISCOVERED(0),             // scan, run: the existence of this node has not been discovered
    DISCOVERED(1),               // scan, run: existence is known, but the type of node is not known (the image is blank.)
    PROBED(2),                   // scan, run: additional info is know: image.
    // CONNECTIONS_SCANNED(3)    // non-mapped state: the connections of this node have been scanned.
    FREE(4),                     // scan, run: scanned: no ice blocking
    PROTECTED(4),                // scan, run: scanned: ice blocking
    HACKED(5)                    //       run: the blocking ice is hacked (resulting in a different image)

    /** reveal level, higher means more is revealed about it */
    int level

    NodeStatus(int level) {
        this.level = level
    }
}
