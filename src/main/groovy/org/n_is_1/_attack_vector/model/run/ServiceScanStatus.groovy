package org.n_is_1._attack_vector.model.run

/**
 * Determines what the hacker knows about this service
 */
enum ServiceScanStatus {

    UNEXPLORED,   // the hacker has not knowledge of the existence of this service
    EXISTENCE,    // the hacker knows the service exists but not its type (it knows the number of services in the node).
    TYPE_GENERAL, // the hacker knows if the service is Ice or not, no details
    TYPE,         // the hacker knows the type of the service
    STRENGTH,     // the hacker knows the strength of the service
    DATA          // the hacker knows inside data (the puzzle) of the service/ice.


}
