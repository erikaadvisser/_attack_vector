package org.n_is_1._attack_vector.model.run

import org.n_is_1._attack_vector.model.site.*

/**
 * Contains info about a single (current) run.
 */
class HackingRun {

    /** Unique id of the run. */
    String runId

    /** Site hacked */
    Site site

    /** The generation of the site that is hacked / the puzzle data */
    SiteGeneration generation

    /** The scan this run is based on */
    SiteScan scan

    /** timstamp of run start */
    long runStart

    /** End of run. This number can change during the run*/
    long traceEnd

    /** All nodes hacked */
    List<NodeService> hackedServices = []

    /** Location of hacker */
    SiteNode currentNode
    SiteNode previousNode

    /** The scan status of each node */
    Map<String, NodeStatus> nodeStatusById = [:]


    /** for SL */
    String hackerId

    /** Optional link to a mission */
    HackingMission mission

    /** Number of deflect scripts running */
    int deflectCount = 0

    /** There can only be one active run per hacker. If this is set to false, then a new run has been started by the hacker */
    boolean active = true

}
