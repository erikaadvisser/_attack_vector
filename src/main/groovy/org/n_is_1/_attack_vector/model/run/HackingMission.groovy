package org.n_is_1._attack_vector.model.run

import groovy.transform.AutoClone
import org.joda.time.DateTime
import org.joda.time.Duration
import org.n_is_1._attack_vector.model.site.SiteType
import org.n_is_1._attack_vector.services.ice.passwords.model.Faction

@AutoClone
class HackingMission {

    /** out of game id, unique and unchangeable */
    String id

    /** This is also the name of the mission */
    String targetSiteId
    List<String> traceSiteIds

    public long endTimeStamp


    /** trace site id -> List<hacker id>     for all traced hackers */
    Map<String, Set<String>> traces = new HashMap<>()

    boolean goalReached = false
    Set<String> hackersWithMissionInfo = new HashSet<>()
    Set<String> hackersTargetSite = new HashSet<>()
    Set<String> hackersTraceSites = new HashSet<>()

    /** service id -> hacker id. Every entry indicates by what hacker a resource service was hacked. */
    Map<String, String> resourcesHacked = new HashMap<>()

    /** archive purposes: mission parameters */
    String template
    SiteType difficulty
    String info
    int reward
    Faction faction

    String formattedTimeLeft() {
        def now = DateTime.now()
        def end = new DateTime(endTimeStamp)
        if (now.isAfter(endTimeStamp)) {
            return "finished"
        }
        def duration = new Duration(now, end)
        def seconds = duration.getStandardSeconds() % 60
        def minutes = duration.getStandardMinutes() % 60
        def hours = duration.getStandardHours()
        return String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

}
