/**
 This file contains the definitions of the various types of nodes
*/

var serviceDefs = function () {
    "use strict";

    /** This service defines the internals of the node. Each node has an OS service */
    var OS = {
        id: "OS",
        url: "-",
        tab: "OS",
        name: "Node OS",
        description: "",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Node id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'networkId',
                name: 'Network id ▣',
                description: 'Unique, used by the move command',
                value: '',
                length: 4,
            }, {
                key: 'name',
                name: 'Node name',
                description: 'The name is displayed when entering the node as an indication of its function. Optional.',
                value: '',
                length: 9,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ],
    };

    var SYSCON = {
        id: "SYSCON",
        url: null,
        tab: "Core",
        name: "System core",
        description: "",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'ice',
                name: 'Disable Ice',
                description : "'true' if hacking this core disables all Ice.",
                value: 'false',
                length: 4,
            }, {
                key: 'timer',
                name: 'Stop timer',
                description : "'true' if hacking this core stops the timer.",
                value: 'false',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var TEXT = {
        id: "TEXT",
        url: null,
        tab: "Text",
        name: "Text file",
        description: "content",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'scanName',
                name: 'Scan name',
                description: 'Name of service during scan',
                value: 'Data vault',
                length: 4,
            }, {
                key: 'runName',
                name: 'Run name',
                description: 'Name of service during hacking run',
                value: 'Data vault',
                length: 4,
            }, {
                key: 'text',
                name: 'Hacked text',
                description : 'Text displayed when this service is hacked.',
                value: '',
                length: 9,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var FILE = {
        id: "FILE",
        url: null,
        tab: "File",
        name: "File",
        description: "content",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'scanName',
                name: 'Scan name',
                description: 'Name of service during scan',
                value: 'Data vault',
                length: 4,
            }, {
                key: 'runName',
                name: 'Run name',
                description: 'Name of service during hacking run',
                value: 'Data vault',
                length: 4,
            }, {
                key: 'url',
                name: 'URL',
                description : 'The url where the file resides',
                value: '',
                length: 9,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var TIME = {
        id: "TIME",
        url: null,
        tab: "Time",
        name: "Time",
        description: "Extra time",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'time',
                name: 'Extra time',
                description : 'The amount of extra hack time received if this node is hacked. Format: minutes:seconds . Example: 0:30',
                value: '2:00',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var CODE = {
        id: "CODE",
        url: null,
        tab: "Code",
        name: "Passcode",
        description: "(password for ice)",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'scanName',
                name: 'Scan name',
                description: 'Name of service during scan',
                value: 'Passcode vault',
                length: 4,
            }, {
                key: 'runName',
                name: 'Run name',
                description: 'Name of service during hacking run',
                value: 'Passcode vault',
                length: 4,
            }, {
                key: 'targetId',
                name: 'Target id',
                description : 'The internal id of the ICE layer, copy paste the value of target service.',
                value: '',
                length: 4,
            }, {
                key: 'text',
                name: 'Hacked text',
                description: 'Explanation of origin of password (optional)',
                value: '',
                length: 9,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var RESOURCE = {
        id: "RESOURCE",
        url: null,
        tab: "-ṩ-",
        name: "Resource",
        description: "(valuable)",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'scanName',
                name: 'Scan name',
                description: 'Name of service during scan',
                value: 'Data bank',
                length: 4,
            }, {
                key: 'runName',
                name: 'Run name',
                description: 'Name of service during hacking run',
                value: 'Data bank',
                length: 4,
            }, {
                key: 'description',
                name: 'Description',
                description : 'Description of the files that you can steal.',
                value: '',
                length: 4,
            },
            {
                key: 'value',
                name: 'Value',
                description : 'Value in whole sonure.',
                value: '',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var SCAN_BLOCKER = {
        id: "SCAN_BLOCKER",
        url: null,
        tab: "s̶c̶a̶n̶",
        name: "Scan Blocker",
        description: "",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'scanName',
                name: 'Scan name',
                description: 'Name of service during scan',
                value: 'Scan blocker',
                length: 4,
            }, {
                key: 'runName',
                name: 'Run name',
                description: 'Name of service during hacking run',
                value: 'Scan blocker',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var REMOTE_TRACER = {
        id: "REMOTE_TRACER",
        url: null,
        tab: "Tracer",
        name: "Remote tracer",
        description: "Connects to remote trace site",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'scannable',
                name: 'Scan reveals site',
                description: "Will a scan reveal trace log site id?",
                value: 'true',
                editable: true,
                length: 4,
            }, {
                key: 'siteId',
                name: 'Trace site id',
                description: "id of trace site",
                value: '(will be auto filled at start of mission)',
                editable: false,
                length: 9,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var TRACE_LOGS = {
        id: "TRACE_LOGS",
        url: null,
        tab: "Chat",
        name: "Chat",
        description: "Chat service",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }

        ]
    };

    var CHAT = {
        id: "CHAT",
        url: null,
        tab: "Trace logs",
        name: "Trace logs",
        description: "Stores trace logs",
        ice: false,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'welcome',
                name: 'Welcome text',
                description: "Text to invite players to chat.",
                value: '',
                editable: true,
                length: 4,
            }, {
                key: 'authorName',
                name: 'Chatter name',
                description: "Display name of persons the hackers are chatting with.",
                value: 'Remote party',
                editable: true,
                length: 9,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };
    var ICE_WORD_SEARCH = {
        id: "ICE_WORD_SEARCH",
        tab: "",
        name: 'ICE - Pumer',
        description: '(word search)',
        url: "wordsearch",
        ice: true,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'strength',
                name: 'Strength',
                description: 'Indication for hackers how strong/difficult this ice is.',
                value: 'Average',
                length: 4,
            }, {
                key: 'x',
                name: 'Size-x',
                description: 'X size of the letter matrix (columns). Min 15',
                value: '20',
                length: 4,
            }, {
                key: 'y',
                name: 'Size-y',
                description: 'Y size of the letter matrix (rows). Min 15.',
                value: '20',
                length: 4,
            }, {
                key: 'wordcount',
                name: 'Words',
                description: 'Number of words to guess.',
                value: '8',
                length: 4,
            }, {
                key: 'use_words',
                name: 'Use words',
                description: 'Optional: these words will always be included in the puzzle. Count towards the number of words. Comma separated.',
                value: '',
                length: 4,
            }, {
                key: 'shortest',
                name: 'Shortest',
                description: 'Number of letters of the shortest word.',
                value: '3',
                length: 4,
            }, {
                key: 'longest',
                name: 'Longest',
                description: 'Number of letters of the longest word.',
                value: '8',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ],
        actions: ["save_os"]
    };

    var ICE_MAGIC_EYE = {
        id: "ICE_MAGIC_EYE",
        tab: "",
        name: 'ICE - Pryder',
        description: '(magic eye)',
        url: "magiceye",
        ice: true,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'strength',
                name: 'Strength',
                description : 'Valid options are: weak, average, strong.',
                value: 'Average',
                length: 4,
            }, {
                key: 'images',
                name: 'Images',
                description : 'Number of images',
                value: '2',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var ICE_MANUAL = {
        id: "ICE_MANUAL",
        url: "manual",
        tab: "",
        name: 'ICE - Mitra',
        description: '(manual puzzle)',
        ice: true,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'strength',
                name: 'Strength',
                description : 'Indication for hackers how strong/difficult this ice is.',
                value: 'Unknown',
                length: 4,
            }, {
                key: 'hint',
                name: 'Hint',
                description : 'What the hackers see to help them find the puzzle. If the puzzle is located elsewhere in the site, you can make a reference to it..',
                length: 4,
            }, {
                key: 'solution',
                name: 'Solution',
                description : 'Players find the solution from either the puzzle itself or the GM when they have finished the puzzle. When they enter' +
                ' the solution, the Ice is hacked.',
                value: '',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var ICE_PASSWORD = {
        id: "ICE_PASSWORD",
        tab: "",
        name: 'ICE - Kama',
        description: '(password)',
        url: "password",
        ice: true,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'strength',
                name: 'Strength',
                description : 'Valid options are: very_weak, weak, average, strong, very_strong, impenetrable.',
                value: 'weak',
                length: 4,
            }, {
                key: 'faction1',
                name: 'Faction-1',
                description : 'Faction of primary users. Valid options are: aquila, dugo, ekanesh, sona, pendzal',
                value: '',
                length: 4,
            }, {
                key: 'faction2',
                name: 'Faction-2',
                description : 'Optional/mandatory 2nd faction (depends on ice strength)',
                value: '',
                length: 4,
            }, {
                key: 'faction3',
                name: 'Faction-3',
                description : 'Optional/mandatory 3rd faction (depends on ice strength)',
                value: '',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var ICE_UNHACKABLE = {
        id: "ICE_UNHACKABLE",
        tab: "",
        name: 'ICE - Jagannatha',
        description: '(unhackable)',
        url: "unhackable",
        ice: true,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'strength',
                name: 'Strength',
                description : 'Indication for hackers how strong/difficult this ice is.',
                value: 'Extreme',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };

    var ICE_NETWALK = {
        id: "ICE_NETWALK",
        tab: "",
        name: 'ICE - Dahana',
        description: '(netwalk)',
        url: "netwalk",
        ice: true,
        fields: [
            {
                key: 'id',
                name: 'Service id',
                description: "Fixed internal id.",
                value: '',
                editable: false,
                length: 4,
            }, {
                key: 'strength',
                name: 'Strength',
                description : 'Valid options are: VERY_WEAK, WEAK, AVERAGE, STRONG, VERY_STRONG, IMPENETRABLE .',
                value: 'AVERAGE',
                length: 4,
            }, {
                key: 'sl-note',
                name: 'SL note',
                description: 'Note for SL, players will never read this.',
                value: '',
                length: 9,
            }
        ]
    };


    return {
        OS: OS,
        SYSCON: SYSCON,
        TEXT: TEXT,
        FILE: FILE,
        TIME: TIME,
        CODE: CODE,
        RESOURCE: RESOURCE,
        SCAN_BLOCKER: SCAN_BLOCKER,
        TRACE_LOGS: TRACE_LOGS,
        REMOTE_TRACER: REMOTE_TRACER,
        CHAT: CHAT,
        ICE_WORD_SEARCH: ICE_WORD_SEARCH,
        ICE_MAGIC_EYE: ICE_MAGIC_EYE,
        ICE_MANUAL: ICE_MANUAL,
        ICE_PASSWORD: ICE_PASSWORD,
        ICE_UNHACKABLE: ICE_UNHACKABLE,
        ICE_NETWALK: ICE_NETWALK,
    };


}();

var nodeDefs = function() {
    "use strict";
    var CONTENT = 'node';
    var ICE = 'ice';


    // --------------------------------------------------------------------------------------------------------------
    // Nodes
    // --------------------------------------------------------------------------------------------------------------

    var syscon = {
        id: 'SYSCON',
        type: CONTENT,
        name: 'Node - System Control',
        description: '(hack target)',
        nameSuggestion: 'syscon',
        services: [serviceDefs.OS, serviceDefs.SYSCON]
    };

    var info = {
        id: 'INFO',
        type: CONTENT,
        name: 'Node - Information',
        description: '(potential hack target)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.TEXT]
    };

    var password = {
        id: 'PASSWORD',
        type: CONTENT,
        name: 'Node - Password',
        description: '(potential hack target)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.CODE]
    };

    var resource = {
        id: 'RESOURCE',
        type: CONTENT,
        name: 'Node - Resource',
        description: '(potential hack target)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.RESOURCE]
    };

    var node_square = {
        id: 'NODE_SQUARE',
        type: CONTENT,
        name: 'Transit node',
        description: '(square)',
        nameSuggestion: '',
        services: [serviceDefs.OS]
    };

    var node_circle = {
        id: 'NODE_CIRCLE',
        type: CONTENT,
        name: 'Transit node',
        description: '(circle)',
        nameSuggestion: '',
        services: [serviceDefs.OS]
    };

    var node_diamond = {
        id: 'NODE_DIAMOND',
        type: CONTENT,
        name: 'Transit node',
        description: '(diamond)',
        nameSuggestion: '',
        services: [serviceDefs.OS]
    };

    var node_hexagon = {
        id: 'NODE_HEXAGON',
        type: CONTENT,
        name: 'Transit node',
        description: '(hexagon)',
        nameSuggestion: '',
        services: [serviceDefs.OS]
    };

    var fw_word_search = {
        id: 'FW_WORD_SEARCH',
        type: ICE,
        name: 'Icewall',
        description: '(Ice layers)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.ICE_WORD_SEARCH]
    };

    var fw_magic_eye = {
        //http://peeinears.github.io/MagicEye.js/
        id: 'FW_MAGIC_EYE',
        type: ICE,
        name: 'Icewall',
        description: '(Ice layers)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.ICE_MAGIC_EYE]
    };

    var fw_password = {
        id: 'FW_PASSWORD',
        type: ICE,
        name: 'Icewall',
        description: '(Ice layers)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.ICE_NETWALK]
    };

    var fw_unhackable = {
        id: 'FW_UNHACKABLE',
        type: ICE,
        name: 'Icewall',
        description: '(Ice layers)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.ICE_UNHACKABLE]
    };

    var fw_manual_1 = {
        id: 'FW_MANUAL_1',
        type: ICE,
        name: 'Icewall',
        description: '(Ice layers)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.ICE_MANUAL]
    };

    var fw_manual_2 = {
        id: 'FW_MANUAL_2',
        type: ICE,
        name: 'Icewall',
        description: '(Ice layers)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.ICE_MANUAL]
    };

    var fw_manual_3 = {
        id: 'FW_MANUAL_3',
        type: ICE,
        name: 'Icewall',
        description: '(Ice layers)',
        nameSuggestion: '',
        services: [serviceDefs.OS, serviceDefs.ICE_MANUAL]
    };


    return {
        SYSCON: syscon,
        INFO: info,
        PASSWORD: password,
        RESOURCE: resource,
        
        NODE_SQUARE: node_square,
        NODE_CIRCLE: node_circle,
        NODE_DIAMOND: node_diamond,
        NODE_HEXAGON: node_hexagon,

        FW_WORD_SEARCH: fw_word_search,
        FW_MAGIC_EYE: fw_magic_eye,
        FW_PASSWORD: fw_password,
        FW_UNHACKABLE: fw_unhackable,

        FW_MANUAL_1: fw_manual_1,
        FW_MANUAL_2: fw_manual_2,
        FW_MANUAL_3: fw_manual_3,
    }
}();

function getNodeType(typeString) {
    var type = nodeDefs[typeString];
    if (!type) {
        $.notify("node def not found for name '" + typeString + "'", {globalPosition: 'top', className: 'error'});
        return;
    }
    return type;
}
/*


 Andraste
 Vayu

 Aditaya Jagonnatha Iuno Derdriu

 Lamia Vayu Ares
 Ares Fedlimid Mot
 Athena Pryderi Damocles
 Pollux Anthea Antheia
 Kaveh Hermes Math
 Mercurius Þórr Oidipous

 Fergus Odin
 Xanthos Baltazar Kleio
 Polydeukes Gunnr
 Derdriu Kama Baldr



 Aditya  Iuno



 */
