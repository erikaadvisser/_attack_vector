///<reference path="../lib/jquery/jquery.d.ts" />
///<reference path="../lib/notify.js/notify.js.d.ts" />
/**
 * Thread is a utility for scheduling javascript events, backed by the javascript event loop.
 *
 * every 100ms the mainLoop is triggered by javascript interval. It checks if there are events on the queue,
 * and if so fires them. An event can set a wait, meaning that the next event has to wait that many times 100ms.
 */
var Thread = /** @class */ (function () {
    function Thread(term) {
        this.queue = [];
        /** Time in millis at which time the wait time is over and the next event can be started. */
        this.waitEnd = null;
        /** Optional indicator that the current event may take more than the wait time, and needs to complete first.
         The event function is responsible for setting and clearing the busy flag itself. */
        this.busy = false;
        this.anim = false;
        this.prompt = null;
        this.term = term;
        var that = this;
        this.intervalId = setInterval(function () {
            that.mainLoop();
        }, 100);
        this.queue = [];
        this.prompt = (term) ? term.get_prompt() : null;
    }
    Thread.prototype.echo = function (wait, text, formattedText) {
        text = (text == "") ? " " : text;
        var that = this;
        var fText = (formattedText) ? formattedText : text;
        this.schedule(function () {
            that.typed(that.term, text, 1, fText);
            that.setWait(wait);
            that.busy = true;
        });
    };
    Thread.prototype.display = function (wait, selector) {
        var that = this;
        this.schedule(function () {
            $(selector).removeClass("hidden_alpha");
            that.setWait(wait);
        });
    };
    Thread.prototype.hide = function (wait, selector) {
        var that = this;
        this.schedule(function () {
            $(selector).addClass("hidden_alpha");
            that.setWait(wait);
        });
    };
    Thread.prototype.run = function (wait, functionToRun) {
        var that = this;
        this.schedule(function () {
            functionToRun();
            that.setWait(wait);
        });
    };
    Thread.prototype.runWithThis = function (wait, functionToRun, withThis) {
        var that = this;
        this.schedule(function () {
            functionToRun.bind(withThis)();
            that.setWait(wait);
        });
    };
    Thread.prototype.stop = function () {
        this.queue = [];
        clearInterval(this.intervalId);
    };
    Thread.prototype.schedule = function (event) {
        this.queue.push(event);
    };
    Thread.prototype.wait = function (wait) {
        this.run(wait, function () { });
    };
    Thread.prototype.setWait = function (wait) {
        this.waitEnd = Date.now() + 100 * wait;
    };
    Thread.prototype.restorePrompt = function (wait) {
        var that = this;
        this.schedule(function () {
            that.term.set_prompt(that.prompt);
            that.setWait(wait);
        });
    };
    /** Main loop tick function. Triggers every 100ms. */
    Thread.prototype.mainLoop = function () {
        if (this.busy) {
            return;
        }
        if (this.waitEnd != null) {
            if (Date.now() < this.waitEnd) {
                return;
            }
            this.waitEnd = null;
        }
        if (this.queue.length > 0) {
            var event_1 = (this.queue.splice(0, 1))[0];
            event_1();
        }
    };
    Thread.prototype.typed = function (term, message, delay, formattedMessage) {
        this.anim = true;
        var that = this;
        var c = 0;
        if (message.length > 0) {
            term.set_prompt('');
            var interval = setInterval(function () {
                term.insert(message[c++]);
                if (c == message.length) {
                    clearInterval(interval);
                    // execute in next interval
                    setTimeout(function () {
                        term.set_command('');
                        term.echo(formattedMessage ? formattedMessage : message);
                        that.busy = false;
                    }, delay);
                }
            }, delay);
        }
    };
    ;
    return Thread;
}());
//# sourceMappingURL=thread.js.map