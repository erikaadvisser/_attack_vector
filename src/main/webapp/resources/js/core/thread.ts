///<reference path="../lib/jquery/jquery.d.ts" />
///<reference path="../lib/notify.js/notify.js.d.ts" />


/**
 * Thread is a utility for scheduling javascript events, backed by the javascript event loop.
 *
 * every 100ms the mainLoop is triggered by javascript interval. It checks if there are events on the queue,
 * and if so fires them. An event can set a wait, meaning that the next event has to wait that many times 100ms.
 */

class Thread {

    // out:JQuery;
    term;

    queue:(()=>void)[] = [];

    /** Time in millis at which time the wait time is over and the next event can be started. */
    waitEnd:number = null;

    /** Optional indicator that the current event may take more than the wait time, and needs to complete first.
     The event function is responsible for setting and clearing the busy flag itself. */
    busy:boolean = false;

    /** for cleaning up the setInterval for this thread. */
    intervalId;

    anim:boolean = false;

    prompt:string = null;

    constructor(term) {
        this.term = term;
        let that = this;
        this.intervalId = setInterval(
            function() {
                that.mainLoop();
            }, 100);
        this.queue = [];
        this.prompt = (term) ? term.get_prompt() : null;
    }




    echo(wait:number, text:string, formattedText?:string):void {
        text = (text == "") ? " " : text;

        let that = this;
        let fText = (formattedText) ? formattedText : text;
        this.schedule( function():void {
            that.typed(that.term, text, 1, fText);
            that.setWait(wait);
            that.busy = true;
        });
    }

    display(wait:number, selector:string):void {
        let that = this;
        this.schedule( function():void {
            $(selector).removeClass("hidden_alpha");
            that.setWait(wait);
        });
    }

    hide(wait:number, selector:string):void {
        let that = this;
        this.schedule( function():void {
            $(selector).addClass("hidden_alpha");
            that.setWait(wait);
        });
    }

    run(wait:number, functionToRun:any):void {
        let that = this;
        this.schedule( function():void {
            functionToRun();
            that.setWait(wait);
        });
    }

    runWithThis(wait:number, functionToRun:any, withThis:any):void {
        let that = this;
        this.schedule( function():void {
            functionToRun.bind(withThis)();
            that.setWait(wait);
        });
    }
    
    stop() {
        this.queue = [];
        clearInterval(this.intervalId);
    }

    schedule(event:()=>void):void {
        this.queue.push(event);
    }

    wait(wait:number) {
        this.run(wait, function(){});
    }

    setWait(wait:number):void {
        this.waitEnd = Date.now() + 100 * wait;
    }

    restorePrompt(wait:number):void {
        let that = this;
        this.schedule( function():void {
            that.term.set_prompt(that.prompt);
            that.setWait(wait);
        });
    }

    /** Main loop tick function. Triggers every 100ms. */
    mainLoop():void {
        if (this.busy) { return; }
        if (this.waitEnd != null) {
            if (Date.now() < this.waitEnd) {
                return;
            }
            this.waitEnd = null;
        }

        if (this.queue.length > 0) {
            let event:()=>void = (this.queue.splice(0,1))[0];
            event();
        }
    }


    typed(term, message, delay, formattedMessage?):void {
        this.anim = true;
        let that = this;
        var c = 0;
        if (message.length > 0) {
            term.set_prompt('');
            var interval = setInterval(function() {
                term.insert(message[c++]);
                if (c == message.length) {
                    clearInterval(interval);
                    // execute in next interval
                    setTimeout(function() {
                        term.set_command('');
                        term.echo(formattedMessage ? formattedMessage: message);
                        that.busy = false;
                    }, delay);
                }
            }, delay);
        }
    };

}