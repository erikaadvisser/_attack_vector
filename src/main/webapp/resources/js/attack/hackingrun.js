/**

 .----------------.  .----------------.  .----------------.  .-----------------. .----------------.  .-----------------. .----------------.
 | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
 | | _____  _____ | || |      __      | || |  _______     | || | ____  _____  | || |     _____    | || | ____  _____  | || |    ______    | |
 | ||_   _||_   _|| || |     /  \     | || | |_   __ \    | || ||_   \|_   _| | || |    |_   _|   | || ||_   \|_   _| | || |  .' ___  |   | |
 | |  | | /\ | |  | || |    / /\ \    | || |   | |__) |   | || |  |   \ | |   | || |      | |     | || |  |   \ | |   | || | / .'   \_|   | |
 | |  | |/  \| |  | || |   / ____ \   | || |   |  __ /    | || |  | |\ \| |   | || |      | |     | || |  | |\ \| |   | || | | |    ____  | |
 | |  |   /\   |  | || | _/ /    \ \_ | || |  _| |  \ \_  | || | _| |_\   |_  | || |     _| |_    | || | _| |_\   |_  | || | \ `.___]  _| | |
 | |  |__/  \__|  | || ||____|  |____|| || | |____| |___| | || ||_____|\____| | || |    |_____|   | || ||_____|\____| | || |  `._____.'   | |
 | |              | || |              | || |              | || |              | || |              | || |              | || |              | |
 | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'



 Het bekijken/aanpassen van de code is geen onderdeel van de IC hacking-game, het is OC Hacking.


 OC Hacking is tegen regels en geest van Frontier.


 N.B. buiten de larp ben je welkom om de broncode te bekijken: https://bitbucket.org/erikaadvisser/_attack_vector


 - Erik



 .----------------.  .----------------.  .----------------.  .-----------------. .----------------.  .-----------------. .----------------.
 | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
 | | _____  _____ | || |      __      | || |  _______     | || | ____  _____  | || |     _____    | || | ____  _____  | || |    ______    | |
 | ||_   _||_   _|| || |     /  \     | || | |_   __ \    | || ||_   \|_   _| | || |    |_   _|   | || ||_   \|_   _| | || |  .' ___  |   | |
 | |  | | /\ | |  | || |    / /\ \    | || |   | |__) |   | || |  |   \ | |   | || |      | |     | || |  |   \ | |   | || | / .'   \_|   | |
 | |  | |/  \| |  | || |   / ____ \   | || |   |  __ /    | || |  | |\ \| |   | || |      | |     | || |  | |\ \| |   | || | | |    ____  | |
 | |  |   /\   |  | || | _/ /    \ \_ | || |  _| |  \ \_  | || | _| |_\   |_  | || |     _| |_    | || | _| |_\   |_  | || | \ `.___]  _| | |
 | |  |__/  \__|  | || ||____|  |____|| || | |____| |___| | || ||_____|\____| | || |    |_____|   | || ||_____|\____| | || |  `._____.'   | |
 | |              | || |              | || |              | || |              | || |              | || |              | || |              | |
 | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'


 */
var canvas = new fabric.Canvas('canvas');
fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';

canvas.clear();
canvas.selection = false;
canvas.setBackgroundColor("#333333", canvas.renderAll.bind(canvas));

var nodesById = {};
var networkNodesById = {};
var hackerAvatar;
var currentNode;
var avatarMoveToNode;
var hackStartMillis;

// Function implemented by ice as a callback for when their window is closed;
var ice_fancybox_closed_function = null;

let delay_5 = (DEBUG) ? 1 : 5;
let delay_10 = (DEBUG) ? 1 : 10;
let delay_25 = (DEBUG) ? 1 : 25;


// ---------------------------------------- ----------------------------------------
// Code in this block is 'shared' with editor.js . At some moment, this code should reside
// in its own file, and we need a module framework (webpack or ecma 2016) to load the modules
// ---------------------------------------- ----------------------------------------

function addNode(props) {
    if (nodesById[props.id]) {
        alert("somehow an object is being added with an id that is already taken. id:" + props.id);
        return;
    }

    var opacity = nodeOpacityByStatus[props.statusForClient];
    var node = new fabric.Image(props.image, {
        left: props.x,
        top: props.y,
        height: props.size,
        width: props.size,
        lockRotation: true,
        lockScalingX: true,
        lockScalingy: true,
        lockMovementX: true,
        lockMovementY: true,
        selectable: false,
        opacity: 0
    });

    node.setControlsVisibility({
        mt: false,
        mb: false,
        ml: false,
        mr: false,
        mtr: false
    });

    node.origLeft = props.x;
    node.origTop = props.y;
    node.lineStart = [];
    node.lineEnd = [];
    node.id = props.id;
    node.nodeType = props.nodeType;
    node.services = props.services;
    node.programs = {};
    node.status = props.statusForClient;
    canvas.add(node);

    // FIXME: different from edit.js

    if ( node.id != undefined ) {
        nodesById[node.id] = node;
        var networkId = props.services[0].data['networkId'];
        networkNodesById[networkId] = node;

        node.connections = [];

        var nodeLabel = new fabric.Text(networkId, {
            fill: "#bbbbbb",
            evented: false,
            fontFamily: "courier",
            fontSize: 12,
            fontStyle: "normal", // "", "normal", "italic" or "oblique".
            // fontWeight: "0",
            hasControls: false,
            lockMovementX: true,
            lockMovementY: true,
            selectable: false,
            left: props.x - 20,
            top: props.y + 35,
            textAlign: "left", // "center", "right" or "justify".
            opacity: 0
        });
        canvas.add(nodeLabel);
        node.label = nodeLabel;

        if (printNodeLabelBackground) {
            var nodeLabelBackground = new fabric.Rect({
                evented: false,
                fontFamily: "courier",
                fontSize: "12",
                fontStyle: "normal", // "", "normal", "italic" or "oblique".
                fontWeight: "0",
                hasControls: false,
                lockMovementX: true,
                lockMovementY: true,
                selectable: false,
                width: 20,
                height: 20,
                fill: "333333",
                left: props.x - 20,
                top: props.y + 35,
                textAlign: "left", // "center", "right" or "justify".
                opacity: 0
            });
            canvas.add(nodeLabelBackground);
            node.labelBackground = nodeLabelBackground;
        }

        canvas.sendToBack(nodeLabelBackground);
        canvas.bringToFront(nodeLabel);

        updateNodeImage(node, false);
        if (node.status != "UNDISCOVERED") {
            animate(nodeLabel, "opacity", 1, 2000);
            if (printNodeLabelBackground) {
                animate(nodeLabelBackground, "opacity", 1, 2000);
            }
        }
    }
    // FIXME: different from edit.js

    return node;
}

function addLine(connectStart, connectEnd) {
    var stroke = "#aaaaaa";
    if (typeof edit_init != "undefined" && edit_init) {
        stroke = "#cccccc";
    }

    var line = new fabric.Line(
        [connectStart.left, connectStart.top, connectEnd.left, connectEnd.top], {
            stroke: stroke,
            strokeWidth: 2,
            strokeDashArray: [5, 5],
            selectable: false,
            opacity:0
        });
    line.startObject = connectStart;
    line.endObject = connectEnd;

    connectStart.lineStart.push(line);
    line.startObject = connectStart;

    connectEnd.lineEnd.push(line);
    line.endObject = connectEnd;
   
    canvas.add(line);
    canvas.sendToBack(line);

    function displayLine() {
        animate(line, "opacity", 1, 2000);
    }
    termThread.run(0, displayLine);
}


function loadSite(url, initFunction) {
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : url,
        data : null,
        dataType : 'json',
        timeout : 5000,
        success : function(data) {
            console.log("SUCCESS: ", data);
            initFunction(data);
            if (typeof edit_init != "undefined" && edit_init) {
                $.notify("Site loaded", {globalPosition: 'top center', className: "success"});
            }
        },
        error : function(e, reason) {
            console.log("ERROR: ", e);
            $.notify("Load failed, " + reason, {globalPosition: 'top', className: 'error'});
        }
    });
}

function createFromLoadGeneric(site) {
    site.nodes.forEach(function(nodeProps) {
        
        var image = $('[name="' + nodeProps.nodeType + "_" + nodeProps.statusForClient + '"]')[0];
        if (!nodeProps.nodeType || !image) {
            $.notify("Problem loading, failed to render node: " + nodeProps.id + ", nodeType id not found: " + nodeProps.nodeType, "warning");
        }
        else {
            nodeProps['image'] = image;
            nodeProps['nodeType'] = getNodeType(nodeProps.nodeType);

            nodeProps.services.forEach(function(serviceProps){
                var serviceTypeId = serviceProps.type;
                var serviceType = serviceDefs[serviceTypeId];
                serviceProps.type = serviceType;
            });

            var node = addNode(nodeProps);
        }
    });

    site.nodes.forEach(function(nodeProps) {
        nodeProps.connections.forEach(function(targetNodeId) {
            var node = nodesById[nodeProps.id];
            var targetNode = nodesById[targetNodeId];

            if (node && targetNode && node.status != "UNDISCOVERED" && targetNode.status != "UNDISCOVERED") {
                addLine(node, targetNode);

                // -------------------------------
                // FIXME: difference from edit.jsp

                node.connections.push(targetNode);
                targetNode.connections.push(node);

                // FIXME: difference from edit.jsp
                // -------------------------------


            }
        });

    });

    canvas.renderAll();
}

// End code 'shared' with editor.js
// ---------------------------------------- ----------------------------------------

function movePersona(payload) {
    var targetNodeId = payload.nodeId;
    var newNodeStatus = payload.newNodeStatus;

    avatarMoveToNode = nodesById[targetNodeId];

    termThread.run(2, function() {
        moveStep(currentNode, 0, 0, 200, hackerAvatar);
    });
    termThread.run(6, function() {
        moveStep(avatarMoveToNode, 0, 0, 800, hackerAvatar);
        currentNode = avatarMoveToNode;
    });
    termThread.run(2, function() {
        personaArrivesAtNode(avatarMoveToNode, newNodeStatus);
    });

    termThread.run(0, function() {
        moveStep(avatarMoveToNode, 20, 20, 200, hackerAvatar);
    });
}

function moveStep(node, leftDelta, topDelta, time, avatar) {
    animate(avatar, 'left', node.origLeft+leftDelta, time);
    animate(avatar, 'top', node.origTop+topDelta, time);
}

function personaArrivesAtNode(node, newNodeStatus)  {
    if (newNodeStatus != null) {
        termThread.run(delay_25, function() {
            animate(hackerAvatar, 'width', "30", 2500);
            animate(hackerAvatar, 'height', "30", 2500);
        });
        termThread.run(0, function() {
            node.status = newNodeStatus;
            updateNodeImage(node, true);
            animate(hackerAvatar, 'width', "40", 2500);
            animate(hackerAvatar, 'height', "40", 2500);
        });
    }

    // Object.keys(node.programs).forEach(function(key, index) {
    //     var program = node.programs[key].onPersonaArrive();
    // });

}

function hackedService(serviceId) {
    var hackSeconds = Math.floor((Date.now() - hackStartMillis) / 1000);
    
    $.get("/run/hacked/" + runId + "/" + serviceId + "?seconds=" + hackSeconds, function(response){
        if (response.allHacked) {
            termThread.setWait(10);
            termThread.run(0, function() {
                showNodeHacked(response.nodeId)
            });
        }
    });
}

function showNodeHacked(nodeId) {
    var node = nodesById[nodeId];
    node.status = "HACKED";

    updateNodeImage(node, true);
}

function updateNodeImage(node, fadeOut) {
    if (fadeOut) {
        animate(node, 'opacity', 0, 400);
        animate(node, 'left', "-=10", 400);

        termThread.wait(5);
    }

    var imageName = node.nodeType.id + '_' + node.status;
    var newImage = $('[name="' + imageName + '"]')[0];

    termThread.run(0,function(){
        node.setElement(newImage);
        canvas.renderAll();
        var newOpacity = nodeOpacityByStatus[node.status];
        animate(node, 'opacity', newOpacity, 400);
        if (fadeOut) {
            animate(node, 'left', "+=10", 400);
        }
    });

}

function terminalFunction(command, term) {
    if (term.get_prompt() == '') {
        // terminal currently not active.
        return;
    }
    
    if (command !== '') {
        try {
            $.post("/run/terminal/send/" + runId, { command: command }, function(data) {

                if (data.scriptId) {
                    $("#script_" + data.scriptId).addClass("strikethrough");
                }

                if (data.type === "NODES_DISCOVERED") {
                    termThread.run(25, function() {
                        animate(hackerAvatar, 'width', "80", 2500);
                        animate(hackerAvatar, 'height', "80", 2500);
                        animate(hackerAvatar, 'opacity', "0.6", 2500);
                    });

                    termThread.run(10, function() {
                        animate(hackerAvatar, 'width', "40", 2500);
                        animate(hackerAvatar, 'height', "40", 2500);
                        animate(hackerAvatar, 'opacity', "1", 2000);
                    });
                    termThread.run(10, function() {
                        term.echo();
                        term.echo(data.message);
                        nodesDiscovered(currentNode, data.payload.nodes, "DISCOVERED");
                        addConnections(data.payload.connections);
                    });
                    return;
                }

                term.echo();
                term.echo(data.message);

                if (data.type == "MOVE") {
                    movePersona(data.payload);
                }

                if (data.type === "ATTACK") {
                    $.fancybox.defaults.helpers.overlay = null;
                    $.fancybox.defaults.keys = {};
                    $.fancybox.defaults.afterClose = function() {
                        if (typeof ice_fancybox_closed_function === "function" ) {
                            ice_fancybox_closed_function();
                        }
                        else {
                            console.log("No close function found.");
                        }
                    };
                    hackStartMillis = Date.now();
                    $('#myiframe').fancybox({
                    });
                    var url = data.payload;
                    $.get( url, function(data) {
                        $.fancybox(data);
                    });
                }

                if (data.type === "TIME") {
                    traceEnd = data.payload;
                    secondsleft = Math.floor((traceEnd - currentMillis) / 1000);
                }

                if (data.type === "NODE_HACKED" || data.type === "FW_BYPASS") {
                    showNodeHacked(currentNode.id);
                }

                if (data.type === "BACKDOOR") {
                    var avatarMoveToNode = nodesById[data.payload.nodeId];
                    termThread.run(2, function() {
                        moveStep(currentNode, 0, 0, 200, hackerAvatar);
                    });
                    termThread.run(25, function() {
                        animate(hackerAvatar, 'width', "10", 2500);
                        animate(hackerAvatar, 'height', "10", 2500);
                        animate(hackerAvatar, 'opacity', "0", 2500);
                    });
                    termThread.run(25, function() {
                        currentNode = avatarMoveToNode;
                        moveStep(avatarMoveToNode, 0, 0, 0, hackerAvatar);
                        animate(hackerAvatar, 'width', "40", 2500);
                        animate(hackerAvatar, 'height', "40", 2500);
                        animate(hackerAvatar, 'opacity', "1", 2500);
                    });
                    termThread.run(2, function() {
                        moveStep(avatarMoveToNode, 20, 20, 200, hackerAvatar);
                    });
                }

                if (data.type === "SYSCON") {
                    if (data.payload.disableTimer) {
                        clearInterval(timeIntervalHandle);
                        $("#time").text("disabled");
                    }

                    data.payload.nodeStatus.forEach(function(nodeUpdate) {
                        var node = nodesById[nodeUpdate.id];
                        node.status = nodeUpdate.status;
                        updateNodeImage(node, true);
                    });

                    if (data.payload.discoveredNodes) {
                        data.payload.discoveredNodes.forEach(function(discovery) {
                            nodeRevealed(discovery.nodeId, discovery.connections, discovery.status);
                        });
                    }

                }

                if (data.type == "DISCONNECT") {
                    clearInterval(timeIntervalHandle);
                    term.pause();
                    $.fancybox.close();
                    term.echo("");
                    animate(hackerAvatar, 'opacity', "0", 2500);
                    termThread.setWait(20);
                    termThread.echo(20, "↼ Disconnected.", "[[i;#a9443b;]↼ Disconnected.]");

                }

                if (data.type == "RESET") {
                    clearInterval(timeIntervalHandle);
                    term.pause();
                    $.fancybox.close();
                    term.echo("");
                    animate(hackerAvatar, 'opacity', "0", 2500);
                    termThread.setWait(20);
                    termThread.echo(20, "↼ Connection lost.", "[[i;#a9443b;]↼ Connection lost.]");
                }


            });
        } catch(e) {
            term.error("" + e);
        }
    } else {
        term.echo('');
    }
}

function nodeRevealed(revealedNodeId, nodeIds, nodeStatus) {
    var revealedNode = nodesById[revealedNodeId];
    revealedNode.status = nodeStatus;
    updateNodeImage(revealedNode, false);
    animate(revealedNode.label, "opacity", 1, 1500);
    if (printNodeLabelBackground) {
        animate(revealedNode.labelBackground, "opacity", 1, 1500);
    }

    nodeIds.forEach( function(nodeId) {
        var targetNode = nodesById[nodeId];
        if (!revealedNode.connections.includes(targetNode) && !targetNode.connections.includes(revealedNode)) {
            addLine(revealedNode, targetNode);
            revealedNode.connections.push(targetNode);
            targetNode.connections.push(revealedNode);
        }
    });
}

function nodesDiscovered(fromNode, nodeIds, nodeStatus) {
    nodeIds.forEach( function(nodeId) {
        var targetNode = nodesById[nodeId];
        targetNode.status = nodeStatus;
        addLine(fromNode, targetNode);
        fromNode.connections.push(targetNode);
        targetNode.connections.push(fromNode);
        updateNodeImage(targetNode, false);

        function addLabel() {
            animate(targetNode.label, "opacity", 1, 1500);
        }

        termThread.run(0, addLabel);
    });
}

function addConnections(connections) {
    connections.forEach(function(connection){
        var fromNode = nodesById[connection.from];
        var toNode = nodesById[connection.to];
        addLine(fromNode, toNode);
    });
}



function animate(toAnimate, attribute, value, duration, callback) {
    animate(toAnimate, attribute, value, duration);
    setTimeout(callback, duration);
}


function animate(toAnimate, attribute, value, duration) {
    toAnimate.animate(attribute, value, {
        onChange: canvas.renderAll.bind(canvas),
        duration: duration,
        easing: fabric.util.ease.easeInOutSine
        // easing: fabric.util.ease.easeInOutSine
    });
}


function findSyscon() {
    var syscon = null;
    $.each(nodesById, function(id, node) {
        if (node.nodeType.id == "SYSCON") {
            syscon = node;
        }
    });
    return syscon;
}

function random(max) {
    return Math.floor(Math.random() * max);
}


// ------------

var patrollerIdCount = 0;

var Patroller = (function() {

    function Patroller(props) {
        this.id = patrollerIdCount ++;
        this.nextStepFunction = props.nextStepFunction;
        this.pauseDurationFunction = props.pauseDurationFunction;
        this.moveDuration = props.moveDuration;
        this.playerCaught = false;
        this.thread = new Thread(null);
        this.patrollerNode = props.startNode;
        this.lastNode = this.patrollerNode.connections[0];
        this.nextNode = null;
        this.arriveFunction = (props.arriveFunction) ? props.arriveFunction: noop;
        this.onPersonaArrive = (props.onPersonaArrive) ? props.onPersonaArrive: noop;

        var image = $('[name=' + props.name + ']')[0];
        // var image = $('[name="RED_GEMINI"]')[0];
        this.avatar = new fabric.Image(image, {
            id: this.id,
            left: this.patrollerNode.left +5,
            top: this.patrollerNode.top   +5,
            height: 40,
            width: 40,
            opacity: 0
        });
        canvas.add(this.avatar);
        canvas.bringToFront(this.avatar);
    }

    Patroller.prototype.run = function() {
        this.patrollerNode.programs[this.id] = this;
        animate(this.avatar, 'opacity', '0.4', 2000);
        // this.thread.runWithThis(20, function() {
        //     animate(this.avatar, 'opacity', '0.4', 2000);
        // }, this);
        this.thread.runWithThis(0, this.move, this);
    };

    Patroller.prototype.move = function() {
        if (this.playerCaught) {
            this.thread.stop();
            return;
        }

        var duration = this.pauseDurationFunction();
        this.thread.run(duration , function () {});

        this.nextNode = this.nextStepFunction();

        if (this.nextNode == null) {
            this.thread.stop();
            return;
        }


        duration = (DEBUG) ? 0 : this.moveDuration();
        this.thread.runWithThis(duration, function () {
            delete this.patrollerNode.programs[this.id];
            canvas.bringToFront(this.avatar);
            moveStep(this.nextNode, +5, 5, duration * 100, this.avatar);
        }, this);

        this.thread.runWithThis(0, this.arrive, this);
    };

    Patroller.prototype.arrive = function() {
        this.lastNode = this.patrollerNode;
        this.patrollerNode = this.nextNode;
        this.patrollerNode.programs[this.id] = this;
        this.nextNode= null;
        this.arriveFunction();
        this.thread.run(0, this.move.bind(this));
    };


    Patroller.prototype.guardscans = function() {
        // console.log("move this.id:"+ this.id);
        if (currentNode == this.patrollerNode) {
            this.onPersonaArrive();
        }
    };

    return Patroller;
}());

// --------------------------------- Persona arrive function ---------------------------------


function catchPersona() {
    this.playerCaught = true;
    this.thread.stop();
    detected();
    canvas.bringToFront(this.avatar);
    animate(this.avatar, 'height', 2000, 5000);
    animate(this.avatar, 'width', 2000, 5000);
    animate(this.avatar, 'opacity', '1', 5000);
    animate(this.avatar, 'left', canvas.width / 2, 5000);
    animate(this.avatar, 'top', canvas.height / 2, 5000);
}

function noop() {
}



// --------------------------------- Move function ---------------------------------

function wallFollowerNextNode() {
    var patroller = this;
    var connections = patroller.patrollerNode.connections;
    if (connections.length == 1) {
        return connections[0];
    }

    // create a clone that contains twice the connections.
    // this way if we have 1, 2, 3. we are in 2 and the previous node is 3,
    // then we parse 1, [2], (3), 1, 2, 3. And move to 1.

    var targets = connections.slice(0);
    var targetNodes = connections.concat(targets);

    var started = false;
    var target = null;
    targetNodes.forEach(function(node){
        if (target != null) {
            return "from loop";
        }
        if (started) {
            target = node;
            return "from loop";
        }
        if (node == patroller.lastNode) {
            started = true;
            return "from loop";
        }
    });
    if (target == null) {
        alert("failed to find next node :(" + targetNodes);
    }
    return target;
}

function moveOnce() {
    if (this.stopped != undefined) {
        this.thread.stop();
        return;
    }

    this.stopped = true;
    var nextNodeIndex = random(this.patrollerNode.connections.length);
    var nextNode = this.patrollerNode.connections[nextNodeIndex];
    return nextNode;
}

function moveToStart() {
    this.stopped = true;
    return startNode;
}



function randomNextNode() {
    var nextNodeIndex = random(this.patrollerNode.connections.length);
    return this.patrollerNode.connections[nextNodeIndex];
}

function moveAccordingToInstructions() {
    if (this.instructions.length == 0) {
        return null;
    }
    this.instruction = this.instructions.pop();
    var nextNode = nodesById[this.instruction.nodeId];
    this.pauseDurationFunction = (DEBUG) ? fixed(1) : fixed(this.instruction.waitDeciSeconds);
        
    return nextNode;
}

// --------------------------------- Atrive function ---------------------------------

function arriveActInstructions() {
    var action = this.instruction.action;
    if (action != "MOVE") {
        var that = this;

        if (action == "ANALYZE" || action == "SNOOP") {
            term.echo("Analyzing node " + that.patrollerNode.services[0].data["networkId"]);
            this.thread.run(9, function() {
                animate(that.avatar, 'opacity', "0.8", 1000);
                animate(that.avatar, 'left', "-=10", 1000);
            });
            this.thread.run(9, function() {
                animate(that.avatar, 'top', "-=10", 1000);
            });
            this.thread.run(9, function() {
                animate(that.avatar, 'left', "+=10", 1000);
            });
            this.thread.run(4, function() {
                animate(that.avatar, 'top', "+=10", 1000);
            });
            this.thread.run(5, function() {
                animate(that.avatar, 'opacity', "0", 1000);
            });
            this.thread.run(5, function() {
                $.post("/scan/probe/" + action + "/" + scanId, {serviceId: that.instruction.serviceId}, function (data) {
                    term.echo(data.message);
                });

            });
            this.thread.run(0, function() {
                canvas.remove(that.avatar);
            });
            return;
        }

        term.echo("Scanning node " + that.patrollerNode.services[0].data["networkId"]);
        if (action == "SCAN_CONNECTIONS") {
            this.thread.run(delay_25, function() {
                animate(that.avatar, 'width', "100", 2500);
                animate(that.avatar, 'height', "100", 2500);
                animate(that.avatar, 'opacity', "0.2", 2500);
            });
        }

        if (action == "SCAN_NODE") {
            this.thread.run(delay_25, function() {
                animate(that.avatar, 'width', "20", 2500);
                animate(that.avatar, 'height', "20", 2500);
                animate(that.avatar, 'opacity', "0.8", 2500);
            });
        }

        if (action == "SCAN_CONNECTIONS" || action == "SCAN_NODE") {
            this.thread.run(delay_10, function() {
                animate(that.avatar, 'width', "40", 2500);
                animate(that.avatar, 'height', "40", 2500);
                animate(that.avatar, 'opacity', "0.4", 2000);
            });


            this.thread.run(delay_10, function() {
                $.post("/scan/probe/" + action + "/" + scanId, {nodeId: that.patrollerNode.id}, function (data) {

                    if (data.type == "NODE_UPDATE") {
                        term.echo(data.message);
                        that.patrollerNode.status = data.status;
                        updateNodeImage(that.patrollerNode, true);
                        return;
                    }
                    if (data.type == "NODES_DISCOVERED") {
                        term.echo("New nodes discovered.");
                        nodesDiscovered(that.patrollerNode, data.payload.nodes, "DISCOVERED");
                        addConnections(data.payload.connections);
                        return;
                    }
                    if (data.type == "NO_NODES_DISCOVERED") {
                        term.echo("No new nodes found.");
                        return;
                    }
                    term.echo("Scan failed.");
                });
            });

            this.thread.run(delay_5, function() {
                animate(that.avatar, 'opacity', "0", 500);
            });
            this.thread.run(delay_5, function() {
                canvas.remove(that.avatar);
                terminalFunction("scan", term);
            });
        }
    }
}

function arriveAndDieInstructions() {
    var scanBlocker = new fabric.Image($('[name="RED_GEMINI"]')[0], {
        id: this.id,
        left: startNode.left +15,
        top: startNode.top   +3,
        height: 40,
        width: 40,
        opacity: 0
    });
    canvas.add(scanBlocker);
    canvas.bringToFront(scanBlocker);
    animate(scanBlocker, "opacity", 0.6, 1000);


    animate(scanBlocker, 'left', "-=25", 2000);
    // animate(this.avatar, 'width', "20", 1500);
    // animate(this.avatar, 'height', "20", 1500);
    animate(this.avatar, 'opacity', "0", 1500);
    setTimeout(function(){
        term.echo("Connection with scan probe lost.");
        animate(scanBlocker, "opacity", 0, 1000);
    }, 1000);
    this.thread.stop();

    setTimeout(function(){
        canvas.remove(scanBlocker);
        canvas.remove(this.avatar);
    }, 2000);

}

// --------------------------------- Duration function ---------------------------------

function dice(count, size, add) {
    var total = add;
    for (var i = 0; i < count; i++) {
        total += random(size);
    }
    return total;
}

function dice2d3_plus_1() {
    return dice(2,3,1) * 10;
}

function dice2d3() {
    return dice(2,3,0) * 10;
}

function fixed(value) {
    return f = function() {
        return value;
    };
    return f;
}

function startPatrols() {
    // for (var i = 0; i < 3; i++) {
    //     var startNode = networkNodesById["00"];
    //     new Patroller(startNode, "BLUE_" + (i + 5), moveOnce, fixed(0), fixed(10), 0);
    // }

    var p = new Patroller({
        startNode: startNode,
        name: "RED_GEMINI",
        nextStepFunction: moveAccordingToInstructions,
        pauseDurationFunction: fixed(0),
        moveDuration: fixed(0),
        arriveFunction: noop,
        instructions: []
    });

    p.run();

    // var patroller3 = new Patroller("RED_GEMINI", wallFollowerNextNode, fixed(0), fixed(10), true);
    // var patroller4 = new Patroller("RED_GEMINI", wallFollowerNextNode, fixed(0), fixed(10), true);
    // var patroller5 = new Patroller("RED_GEMINI", wallFollowerNextNode, fixed(0), fixed(10), true);
    // var patroller6 = new Patroller("RED_GEMINI", wallFollowerNextNode, fixed(0), fixed(10), true);
    // var wanderer = new Patroller("RED_VIRGIN", randomNextNode, dice2d3_plus_1, dice2d3, true);
    // var wanderer = new Patroller("RED_VIRGIN", randomNextNode, dice2d3_plus_1, dice2d3, true);
    // guard.initPatrol();
// patroller.initPatrol();
}

// loadSite("/run/load/" + runId, createFromLoadRun);






