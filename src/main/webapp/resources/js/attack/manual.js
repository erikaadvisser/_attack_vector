
var termActive = false;
var solution;

var iceTerm = $('#ice_term').terminal(
    function (cmd, term) {
        processSolution(cmd, term);
    }, {
        name: 'xxx',
        greetings: null,
        width: "100%",
        height: 150,
        prompt: "⇋ ",
        keypress : function (e) { return termActive; },
    });
var thread = new Thread(iceTerm);


function processSolution(cmd, term) {
    if (cmd == solutionPassword) {
        thread.echo(5, "⇁ token ok", "token. Status: [[b;#31708f;]success]");
        ice_bested();
    } else {
        thread.echo(0, "Invalid token .", "[[;#31708f;]Invalid token]");
        thread.echo(0, "Enter token:");
        thread.restorePrompt(0);
    }
}

function ice_bested() {
    thread.echo(2, "");
    thread.echo(5, "⇁ Authorization tokens accepted", "Authorization tokens [[b;#31708f;]accepted]");
    thread.echo(2, "");
    thread.echo(15, "↼ Access granted.", "[[i;#a9443b;]↼ Access granted.]");
    thread.run(5, function () { $("#word_search_container").attr("style", "background-color: white"); });
    thread.run(0, ice_bested_end);
}
function ice_bested_end() {
    $.fancybox.close();
    thread.stop();
    term.echo("Ice subverted.");
    term.focus();
    hackedService(serviceId);
}

$(function() {

    ice_fancybox_closed_function = function() {
        console.log("Stopped thread of manual");
        thread.stop();
        ice_fancybox_closed_function = null;
    };

    thread.echo(10, "↼ Connecting to ice, initiating attack.", "[[i;#a9443b;]↼ Connecting to ice, initiating attack.]");
    thread.echo(10, "↼ Previous generation ice detected.");
    thread.echo(15, "↼ Loading slingshot protocols.");
    thread.echo(12, "↼ Finding new attack vector.");
    thread.echo(10, "↼ ...............................................................................................................................", "Found.");
    thread.echo(8, "↺ Slingshot: find solution via alternate channel and enter solution");
    thread.echo(12, "↺ " + hint);
    thread.echo(5, " ");
    thread.echo(0, "Enter token:");
    thread.restorePrompt(0);
    thread.run(0, function() {
        termActive = true;
    })

});
