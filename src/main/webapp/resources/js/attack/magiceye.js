
var termActive = false;
var nextMagicEye = 0;
var solution;

var iceTerm = $('#ice_term').terminal(
    function (cmd, term) {
        processSolution(cmd, term);
    }, {
        name: 'xxx',
        greetings: null,
        width: "100%",
        height: 150,
        keypress : function (e) { return termActive; },
    });
var thread = new Thread(iceTerm);

function nextImage() {
    if (nextMagicEye == puzzleData.length) {
        ice_bested();
        return;
    }

    var magicEye = puzzleData[nextMagicEye];
    nextMagicEye += 1;
    solution = magicEye.solution;
    solution = solution.toLowerCase();
    solution = solution.replace(/\s+/g, '');
    thread.hide(20, "#wordsearch_main");
    thread.run(0, function() {
        var url = magicEye.location;
        var img = $("<img>", {src: url, "style": "width: 1500px; border-radius: 150px;"});
        $("#magicEye").html(img);
    });

    thread.display(5, "#wordsearch_main");

    thread.echo(3, "↺ Rendering token image]", "↺ [[;#3c763d;]Rendering token image]");
    thread.echo(0, "⇋ Enter token: ");
    thread.run(0, function() {
        termActive = true;
    })
}

function processSolution(cmd, term) {
    var attempt = cmd.toLowerCase();
    attempt = attempt.replace(/\s+/g, '');
    if ( attempt == solution) {
        thread.echo(5, "⇁ token ok", "token. Status: [[b;#31708f;]success]");
        nextImage();
    } else {
        thread.echo(0, "⇁ Invalid token .", "⇁ . [[;#31708f;]Invalid token] .");
        thread.echo(0, "⇋ Enter token: ");
    }
}

function ice_bested() {
    thread.echo(2, "");
    thread.echo(5, "⇁ Authorization tokens accepted", "Authorization tokens [[b;#31708f;]accepted]");
    thread.echo(2, "");
    thread.echo(15, "↼ Access granted.", "[[i;#a9443b;]↼ Access granted.]");
    thread.run(5, function () { $("#word_search_container").attr("style", "background-color: white"); });
    thread.run(0, ice_bested_end);
}
function ice_bested_end() {
    thread.stop();
    $.fancybox.close();
    term.echo("Ice subverted.");
    term.focus();
    hackedService(serviceId);
}

$(function() {

    ice_fancybox_closed_function = function() {
        thread.stop();
        console.log("Stopped thread of magic eye");
        ice_fancybox_closed_function = null;
    };

    "use strict";
    thread.echo(10, "↼ Connecting to ice, initiating attack.", "[[i;#a9443b;]↼ Connecting to ice, initiating attack.]");
    thread.echo(10, "↼ Power diagnostic active, starting probes.");
    thread.echo(3, "↼ Probe length  1.");
    thread.echo(0, "⇁ Invalid token .", "⇁ . [[;#31708f;]Invalid token] .");
    thread.echo(3, "↼ Probe length  2.");
    thread.echo(0, "⇁ Invalid token ..", "⇁ [[;#31708f;]Invalid token] ..");
    thread.echo(3, "↼ Probe length  4.");
    thread.echo(0, "⇁ .... Invalid token", "⇁ .... [[;#31708f;]Invalid token] ....");
    thread.echo(3, "↼ Probe length  8.");
    thread.echo(0, "⇁ Invalid token ........", "⇁  [[;#31708f;]Invalid token] ........");
    thread.echo(3, "↼ Probe length 16.");
    thread.echo(0, "⇁ Invalid token ................", "⇁ [[;#31708f;]Invalid token] ................");
    thread.echo(3, "↼ Probe length 32.");
    thread.echo(0, "⇁ Invalid token ................................", "⇁ [[;#31708f;]Invalid token] ................................");
    thread.echo(3, "↼ Probe length 64.");
    thread.echo(0, "⇁ Invalid token", "⇁ [[;#31708f;]Invalid token] ................................................................");
    thread.echo(30, "↺ Analysing power consumption");
    thread.echo(10, "↺ Converting to noise map");
    thread.echo(3, "↺ Analysis complete]", "↺ Analysis [[;#3c763d;]complete]");
    thread.echo(5, " ");

    nextImage();
});
