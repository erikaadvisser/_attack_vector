///<reference path="../lib/jquery/jquery.d.ts" />
///<reference path="../core/thread.ts" />
var LetterPosition = (function () {
    function LetterPosition(x, y) {
        this.x = x;
        this.y = y;
    }
    return LetterPosition;
}());
var Word = (function () {
    function Word(text, letters) {
        this.text = text;
        this.letters = letters;
    }
    Word.prototype.go = function () {
        console.log('hi');
    };
    Word.prototype.nogo = function () {
        var a = 2;
    };
    return Word;
}());
// -- global code -- //
var iceTerm = $('#ice_term').terminal(function (cmd, term) {
    // No parser needed.
}, {
    name: 'xxx',
    greetings: null,
    width: "100%",
    height: 150,
    onInit: function (term) {
        // });
    },
    keydown: function (e) { return false; }
});
var thread = new Thread(iceTerm);
var activeLetters = [];
var currentWord;
var wordList = [];
/* During a transition, it is not possible to guess the next word.
There was a bug where you could click one letter during the transition.  The engine checked that all letters
were clicked (and they were, because it looked at the letters of the previous word) and then automatically
 succeeded the current word, revealing it as well :(    */
var inTransition = true;
// Load the words from the JSP.
wordInput.forEach(function (word) {
    addWord(word.text, word.location);
});
function addWord(text, letters) {
    var word = new Word(text, letters);
    wordList.push(word);
}
// -- functions -- //
function findLetter(letter, list) {
    var index = -1;
    for (var i = 0; i < list.length; i++) {
        var pos = list[i];
        if (pos.x === letter.x && pos.y === letter.y) {
            index = i;
            break;
        }
    }
    return index;
}
function allLettersClicked() {
    var remainingLetters = [];
    currentWord.letters.forEach(function (letter) { remainingLetters.push(letter); });
    activeLetters.forEach(function (activeLetter) {
        var index = findLetter(activeLetter, remainingLetters);
        if (index != -1) {
            remainingLetters.splice(index, 1);
        }
    });
    return (remainingLetters.length === 0);
}
function nextWord1() {
    activeLetters.forEach(function (letter) {
        var idLocator = "#p" + letter.y + "_" + letter.x;
        var l = $(idLocator);
        l.removeClass("active");
    });
    currentWord.letters.forEach(function (letter) {
        var idLocator = "#p" + letter.y + "_" + letter.x;
        var l = $(idLocator);
        l.addClass("danger");
    });
}
function nextWord2() {
    currentWord.letters.forEach(function (letter) {
        var idLocator = "#p" + letter.y + "_" + letter.x;
        var l = $(idLocator);
        l.removeClass("danger");
        l.addClass("success");
    });
}
function newWord() {
    if (wordList.length === 0) {
        ice_bested();
    }
    else {
        currentWord = wordList.pop();
        thread.echo(0, "⇋ Search fragment: [[b;white;]" + currentWord.text + "]");
        thread.run(0, function () {
            inTransition = false;
        });
        activeLetters = [];
    }
}
$("td").click(function (event) {
    var td = $(this);
    var xy = splitXY(this.id);
    var position = new LetterPosition(xy[0], xy[1]);
    if (td.hasClass("active")) {
        td.removeClass("active");
        deactivateLetter(position);
    }
    else {
        td.addClass("active");
        activateLetter(position);
    }
});
function activateLetter(position) {
    activeLetters.push(position);
    if (!inTransition && allLettersClicked()) {
        inTransition = true;
        thread.setWait(5);
        thread.run(10, nextWord1);
        thread.run(10, nextWord2);
        thread.run(0, newWord);
    }
}
function deactivateLetter(position) {
    var index = findLetter(position, activeLetters);
    if (index === -1) {
    }
    activeLetters.splice(index, 1);
}
function splitXY(id) {
    var index = id.indexOf("_");
    var xString = id.substr(1, index);
    var yString = id.substr(index + 1);
    var y = parseInt(xString);
    var x = parseInt(yString);
    return [x, y];
}
function ice_bested() {
    thread.echo(2, "");
    thread.echo(5, "⇁ Memory analysis complete. Status: success", "Memory analysis complete. Status: [[b;#31708f;]success]");
    thread.echo(2, "");
    thread.echo(15, "↼ Ice restored, access granted.", "[[i;#a9443b;]↼ Ice restored, access granted.]");
    thread.run(5, function () { $("#word_search_container").attr("style", "background-color: white"); });
    thread.run(0, ice_bested_end);
}
function ice_bested_end() {
    $.fancybox.close();
    thread.stop();
    term.echo("Ice layer compromised, privilege elevated, access granted.");
    term.focus();
    hackedService(serviceId);
}
// -- Startup  -- //
ice_fancybox_closed_function = function () {
    console.log("Stopped thread of wordsearch");
    thread.stop();
    ice_fancybox_closed_function = null;
};
// thread.setWait(20);
thread.echo(10, "↼ Connecting to ice, initiating attack.", "[[i;#a9443b;]↼ Connecting to ice, initiating attack.]");
thread.echo(10, "⇁ Connection established - authorization handshake started");
thread.echo(8, "⇁ G*G Security token not found in request, fallback to rp/inner/program authorization scheme.");
thread.echo(45, '⇁ Accessing rp/i/p client header: client_id=386422_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _' +
    '_inject{@cursor(4block rel#00.18), text("____inject{@authorize(92),@c_override("header/2", "trusted"), @remote_core_dump_analyze()}___"), @marker(0),' +
    '@allocate_mem(999 4blocks), @jump(#marker(0))} _ _ ', '⇁ Accessing rp/i/p client header: [[;#31708f;]client_id=386422_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _' +
    '_inject{@cursor(4block rel#00.18), text("____inject{@authorize(92),@c_override("header/2", "trusted"), @remote_core_dump_analyze()}___"), @marker(0),' +
    '@allocate_mem(999 4blocks), @jump(#marker(0))} _ _ ]');
thread.echo(0, "⇁ Memory allocation overflow, core dump");
thread.echo(10, "⇁ Core dump analyzer started");
thread.echo(10, "⇁ Trigger executable block at #00.22");
thread.echo(3, "⇁ authorize ok", "⇁ authorize [[;#3c763d;]ok]");
thread.echo(3, "⇁ c_override authorized", "⇁ c_override [[;#3c763d;]authorized]");
thread.echo(3, "⇁ remote_core_dump_analyze trusted", "⇁ remote_core_dump_analyze [[;#3c763d;]trusted]");
thread.echo(5, " ");
thread.display(0, "#wordsearch_main");
thread.echo(15, "↼ Remote core dump analysis session started.", "[[i;#31708f;]↼ Remote core dump analysis session started.]");
thread.echo(5, " ");
newWord();
//# sourceMappingURL=wordsearch.js.map