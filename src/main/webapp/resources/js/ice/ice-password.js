
var termActive = false;
var solution;

var iceTerm = $('#ice_term').terminal(
    function (cmd, term) {
        processSolution(cmd, term);
    }, {
        name: 'xxx',
        greetings: null,
        width: "100%",
        height: 150,
        keypress : function (e) { return termActive; },
    });
var thread = new Thread(iceTerm);





/* ------------------------- ------------------------- -------------------------
   Display password guessing window (flashy thingy)

   The flashy password change window does the following:
   - display an actual possible password attempt for 2 clicks
   - display a random set of characters for 1 click
   (repeat)

   a click is 25 ms.

   The dictionaries are actual words that should fit with the category. The extra click of random characters is added so
   that it is very hard to actually make out the words, otherwise you would quickly see how few there are.
*/
var dic_dugo_name = ["Arvin", "Bagwis", "Bayani", "Benjie", "Crisanto", "Dakila", "Danilo", "Datu", "Efren", "Fermin", "Gener", "Homobono", "Isagani", "Isko", "Jejomar", "Kidlat", "Luzvimindo", "Magtanggol", "Makisig", "Melchor", "Nimuel", "Rizal", "Rodel", "Virgilio", "Bituin", "Chona", "Dalisay", "Divina", "Diwata", "Flordeliza", "Imee", "Imelda", "Jovelyn", "Laarni", "Lailani", "Liezel", "Ligaya", "Lilibeth", "Liwayway", "Liwliwa", "Lualhati", "Luningning", "Luzviminda", "Malaya", "Marilag", "Mayumi", "Mirasol", "Nenita", "Riza", "Rubylyn", "Rutchel", "Tala" ];
var dic_aquila_name = ["Aeliana", "Albia", "Antonia", "Aquilia", "Argentia", "Atticus", "Augusta", "Augustus", "Aurelia", "Aurelius", "Avita", "Caesar", "Camilla", "Cassia", "Cassius", "Cato", "Cecilia", "Cicero", "Claudia", "Claudius", "Clemensia", "Cornelius", "Crispus", "Cyprian", "Decima", "Decimus", "Drusilla", "Dulcia", "Fabia", "Faustina", "Felix", "Flavia", "Florentina", "Fortunata", "Gaia", "Galla", "Hilaria", "Horatia", "Julia", "Julius", "Junia", "Justus", "Laelia", "Laurentia", "Livia", "Lucius", "Lucretia", "Magnus", "Marcella", "Marcus", "Marilla", "Marius", "Martia", "Maxima", "Maximus", "Mila", "Nerilla", "Nero", "Octavia", "Octavius", "Philo", "Prima", "Priscilla", "Quintia", "Quintus", "Remus", "Romulus", "Rufina", "Rufus", "Sabina", "Seneca", "Septima", "Septimus", "Sergia", "Tanaquil", "Tatiana", "Tauria", "Tertia", "Tiberius", "Tullia", "Urban", "Urbana", "Valentina", "Varinia", "Vita"];
var dic_sona_name = ["Aaron", "Abdullah", "Aden", "Ahmad", "Aisha", "Aleah", "Ali", "Alma", "Amani", "Amare", "Amari", "Amarion", "Amina", "Amir", "Amira", "Antwan", "Caleb", "Callie", "Fatima", "Guadalupe", "Hamza", "Hana", "Hassan", "Ibrahim", "Imani", "Ismael", "Jaliyah", "Jamal", "Jamel", "Jazmin", "Jenn", "Kadyn", "Kale", "Kaleigh", "Kaley", "Kaliyah", "Kareem", "Kayden", "Kaylah", "Khalil", "Laila", "Lila", "Lina", "Lydia", "Lyla", "Makhi", "Mali", "Maritza", "Messiah", "Mina", "Muhammad", "Naima", "Nash", "Nasir", "Omar", "Rashad", "Salma", "Samir", "Samira", "Sanaa", "Sky", "Skye", "Tariq", "Yahir", "Yesenia", "Yusuf", "Zaire", "Zavier"];
var dic_ekanesh_name = ["Noushig", "Nouvart", "Nouver", "Nshan", "Ohanna", "Oskie", "Ovsanna", "Palasan", "Parounag", "Parounak", "Pavagan", "Payl", "Paylag", "Pergrouhi", "Perooz", "Perouze", "Pertag", "Pounig", "Pourasdan", "Pouregh", "Raffi", "Rafi", "Razmig", "Razmouhi", "Rehan", "Sahak", "Sebouh", "Seda", "Serpouhi", "Serpuhi", "Sevan", "Sevan", "Sevoug", "Shabouh", "Shahan", "Shake", "Shant", "Sharmagh", "Shnorhig", "Shnorhk", "Shogher", "Shoushan", "Siran", "Siranoush", "Siroun", "Sirpuhi", "Sirvart", "Sirvat", "Sosi", "Sossy", "Takouhi", "Takvor", "Talar", "Talin", "Taline", "Tangakin", "Taniel", "Teghtsanig", "Teghtsoun", "Teter", "Toukhtzam", "Tsakig", "Tsangali", "Tsdrig", "Tshoghig", "Vache", "Vahan", "Vanadour", "Vanagan", "Vanig", "Varoujan", "Vartan", "Vartanoush", "Vartavar", "Varteni", "Varteres", "Vartiter", "Vartouhi", "Vasag", "Vertchalous", "Vosdanig", "Vosgedzam", "Vosgetel", "Vosgi", "Voshkie", "Voskie", "Vrej", "Yeghnig", "Yervant", "Yeter", "Yeva", "Yranig", "Zabel", "Zadig", "Zanazan", "Zartar", "Zepour", "Zmroukhd", "Zoravar", "Zvart"];
var dic_pendzal_name = ["Anastasia", "Nikita", "Nastya", "Dima", "Dasha", "Alex", "Maria", "Sergey", "Anna", "Alexander", "Olga", "Andrey", "Alina", "Vlad", "Kate", "Sasha", "Katya", "Artem", "Julia", "Ivan", "Irina", "Anton", "marina", "Dmitry", "Liza", "Max", "Polina", "Kirill", "Tanya", "Maxim", "Alexandra", "Egor", "Daria", "Andrew", "Natasha", "Roman", "Ksenia", "Oleg", "Elena", "Pavel", "Ekaterina", "ilya", "Mary", "Denis", "Arina", "Ruslan", "Yana", "Alexandr", "Masha", "Igor", "Victoria", "Vladislav", "Ann", "Vladimir", "Kseniya", "alexey", "Diana", "Danil", "Svetlana", "Vadim", "Lera", "Kostya", "Kristina", "Gleb", "Sofia", "Daniel", "Lena", "zhenya", "Anastasiya", "Nick", "Helen", "Slava", "Natalia", "Daniil", "Tatiana", "Yaroslav", "Sasha", "Mark", "Darya", "Konstantin", "Sveta", "Pasha", "Oksana", "Andrei", "Yulia", "MIkhail", "Valeria", "Timur", "Ksusha", "Arthur", "Sonya", "misha", "Anya", "Kolya", "Alena", "Anatoly", "Veronika", "Bogdan", "Alyona", "Dmitriy", "Katerina", "Fedor", "Vika", "Mansur", "Angelina", "Roma", "Ira", "sergei", "Helena", "Danila", "Olesya", "matvey", "Olya", "Rafit", "Alice", "Boris", "NINA", "Dan", "Viktoria", "Leo", "Katherine", "Paul", "Lisa", "Artyom", "Galina", "Valentin", "Irene", "Zahar", "Vera", "George", "Valentina", "Aleksei", "Catherine", "Arseniy", "Lina", "Michael", "Tatyana", "Vitaliy", "Vlada", "serge", "Nastia", "Grisha", "Mariya", "Eugene", "Ulyana", "Vova", "Margarita", "Aleksandr", "Karina", "Leonid", "Natalya", "Stanislav", "Elizabeth", "Evgeny", "Ksenya", "Aymeric", "Kira", "Aleksey", "Christina", "Adam", "Alisa", "Renat", "Viktoriya", "Yegor", "Sophia", "David", "Vasilisa", "Nikolai", "Inna", "Yura", "Eva", "Nikolay", "Nataly", "Andru", "dina", "Artiom", "Marta", "Sonya", "Asya", "Bekhan", "Albina", "Gosha", "Veronica", "Arsenii", "Zhenya", "Petr", "Natalie", "Íèêèòà", "Lyuba", "German", "Juliya", "Maksim", "Polly", "Niko", "Madina", "Artur", "Evgeniya", "Vasiliy", "Jane", "Gregory", ];

var dic_dugo_culture = ["Cabatu", "Batongbayal", "Hideyoshi", "Hiroto", "Kaito", "Katsuro", "Shinobu", "Tarou", "Noburu", "Haruka", "Minoru", "Asul", "Salita", "Sibilyan", "Inabayan", "Dagita", "Agati", "Bugaoisa", "Damolao", "Gonong", "Catambay", "Karangalan", "Kapayapaan", "Sayaw", "Say", "Magaway", "Maga", "Lakas", "Tatak", "Pagtubos", "Datu", "Karapatan", "Pamana", "Maykapala"];
var dic_aquila_culture = ["Accipiter", "Viridis", "Noctua", "Alietum", "FeroxII", "Merula", "Sturnus", "Alcyon", "Tigris", "Ignis", "Ithaginis", "Fastus", "Legio", "Canatus", "Classis", "ADMP", "Militia", "Rifle", "Colonialis", "Prisci", "Civitas", "Militair", "Mulum", "Vindicatum", "Vexillium", "Contubernium", "Centurie", "Cohort", "Regiment", "Legio"];
var dic_sona_culture = ["Ghara", "Ahl", "Ohm", "Shahin", "Yaman", "Kardal", "Andhera", "Prakasa", "Qutb", "Rayyan", "Andhera", "Qutb", "Mu'azzaz", "Abhay", "sonuur", "mijn", "RPMU"];
var dic_ekanesh_culture = ["Mair", "Waker", "Wachter", "Priester", "Priesteres", "Godin", "Raad", "Siran", "Yeva", "Arushan", "Matriarch", "Dzar", "Tachar", "Virus", "Mkhitar", "Water", "Hogepriester", "Husik", "Huis", "Liefde", "Love", "Bron", "Heilig", "Natuur", "Jeknovorian", "Hacheli", "Yerkir", "Kanat", "Ts’ankapat", "Ker", "Poorter", "Hoeder"];
var dic_pendzal_culture = ["Tarir", "III", "Zhodu", "Yekaterina", "Dadomu", "Zvir", "Ciomna", "Vtotoroj", "Zorki", "Ziamlia", "Vady", "Zalinsky", "Lenya", "Opstand", "Rebel", "Vrij", "Vrijheid", "Strijd", "Nikifor", "Liudzi", "Prostoru", "Sonca", "Viera", "Vtotoroj", "Ciomna", "Wodka", "Drank", "Honar", "Ziamlia", "Mudrasc", "Nadzieja", "Liudzi", "Taniec", "Ruda", "Nadz", "Zyccio", "Jurg", "Sabas"];

var dic_number_Year = ["236", "235", "234", "234", "1", "2", "123", "666", "228", "217", "12345", "66", "0", "00", "000", "999", "I", "II", "III", "X", "XX"];
var dic_common_words = ["encrypt", "encryption", "enter", "exabyte", "faqfile", "firewall", "firmware", "flash", "folder", "font", "format", "frame", "freeware", "gigabyte", "graphics", "hack", "hacker", "hardware", "host", "icon", "inbox", "integer", "interface"];
var dic_all_words = ["allure", "baritone", "bluebonnet", "byzantine", "challenge", "colander", "cordage", "danubian", "dignity", "dug", "equine", "feebleminded", "forte", "glockenspiel", "harken", "hug", "infamous", "jeremiad", "leech", "maintain", "mildew", "naivety", "octet", "pants", "phonetics", "power", "put", "remedy", "sagamore", "sextant", "sob", "stone", "tablespoonful", "tiresome", "typed", "usury", "wart", "wrongdoer", "zoolog"];
var dic_human_random = [ "asdf", "adsg", "qwer", "vvvd", "ioas", "pppa"," jdjs", "slkl", "zzxc", "asaf", "aaaa", "bbbb", "qqqq", "zxcv", "mnbv", "lkjh", "poiu", "bcde", "cdef", "xxxx"];
var dic_true_random = ["jxpx", "hqdn", "bvwe", "spct", "uokn", "lkcv", "vsdy", "wqhf", "mqyz", "dbir", "wqmx", "kuzr", "gmzh", "jexk", "yjvm", "ftwh", "iecz", "bemz", "hmtn", "zhkl", "ygsn", "ulni", "inoc", "vtfk", "gyua", "jdwm", "alld", "xgus", "kxnc", "mbek"];

var dic_l33t = ["!", "3", "u", "#", "$", "?", "A", "B", "C", "D", "F", "G", "I", "J", "K", "L", "M", "N", "O", "P", "S", "T", "U", "V", "X", "Z" ];

var all_dics = {
    "Dugo name": dic_dugo_name,
    "Aquila name": dic_aquila_name,
    "Sona name": dic_sona_name,
    "Ekanesh name": dic_ekanesh_name,
    "Pendzal name": dic_pendzal_name,
    "Dugo culture": dic_dugo_culture,
    "Aquila culture": dic_aquila_culture,
    "Sona culture": dic_sona_culture,
    "Ekanesh culture": dic_ekanesh_culture,
    "Pendzal culture": dic_pendzal_culture,
    "Number/Year": dic_number_Year,
    "Common words": dic_common_words,
    "All words": dic_all_words,
    "4 Human random": dic_human_random,
    "4 True random": dic_true_random,
    "(l33t)": dic_l33t
};


var print = 0;

var passwordAttemptJQN = $("#password_attempt_holder");
var attempt = "";

function updateWord() {
    print++;
    if (print == 1) {
        attempt = "";
        currentDics.forEach(function (dic) {
            var list = all_dics[dic.name];
            var r = Math.floor(Math.random() * list.length);
            attempt += list[r];
        });
        if (attempt.length > 20) {
            attempt = attempt.substring(0, 20);
        }

        passwordAttemptJQN.text(attempt);
        return;
    }
    if (print == 3) {
        var randomChars = Math.random().toString(36).substring(2, attempt.length + 2);
        passwordAttemptJQN.text(randomChars);
    }
    if (print >= 3) {
        print = 0;
    }
}


/* ------------------------- ------------------------- -------------------------
   Adding dictionaries to the queue
*/


function clickDictionary(dictionary) {
    return function() {
        dictionaryClicked(dictionary);
    };
}

var nextDics = [];
var nextTotal = 0;


var l33tInNextDic = false;

function dictionaryClicked(dic) {

    if (dic.name == '(l33t)' && l33tInNextDic ) {
        return;
    }

    if (dic.points * nextTotal > 20) {
        $.notify("Guess time too high", {globalPosition: 'top center', className: 'warning'});
        return;
    }
    else {
        dictionaryClickedAndAdded(dic);
        disallowL33tOnly();
    }
}

var nextJQN = $("#next");

function dictionaryClickedAndAdded(dicToAdd) {

    nextDics.push(dicToAdd);

    nextDics.sort(function(a,b){ return a.prio - b.prio} );

    nextTotal = 1;
    nextJQN.empty();
    for (var i = 0; i < nextDics.length; i++ ) {
        var dic = nextDics[i];
        var html = '<\
span onclick=\'removeDicAt(' + i + ');\' class="label label-info password-dic"><span class="badge password-badge">' + dic.points + '</span> ' + dic.name + '</span> ';
        nextJQN.append(html);
        nextTotal = nextTotal * dic.points;
        if (i < nextDics.length-1) {
            nextJQN.append(" x ");
        }
    }

    $("#nextTime").text(nextTotal);

    if (nextDics.length >= 1) {
        $("#go_button").removeClass("hidden_alpha");
    }
    if (dicToAdd.name == '(l33t)') {
        l33tInNextDic = true;
    }
}

function removeDicAt(index) {
    nextDics.splice(index, 1);
    var currentDics = nextDics;
    $("#next").empty();
    nextDics = [];
    l33tInNextDic = false;
    currentDics.forEach(function(dic) {
        dictionaryClickedAndAdded(dic);
    });
    if (currentDics.length == 0 ) {
        $("#nextTime").text("0");
        nextTotal = 0;
        $("#go_button").addClass("hidden_alpha");
    }
    disallowL33tOnly();
}

var l33tJQN = $("#d_o_l");
function disallowL33tOnly() {
    if (nextIsOnlyL33t() ) {
        $("#go_button").addClass("hidden_alpha");
        $("#nextTime").text("0");
        l33tJQN.removeClass("label-info");
        l33tJQN.addClass("label-default");
        return;
    }
    if (l33tInNextDic) {
        l33tJQN.removeClass("label-info");
        l33tJQN.addClass("label-default");
    }
    else {
        l33tJQN.removeClass("label-default");
        l33tJQN.addClass("label-info");
    }
}

function nextIsOnlyL33t() {
    return (nextDics.length == 1 && l33tInNextDic );
}

$("#d_o_l").click(clickDictionary({ prio:  0, points:  3, name: "(l33t)"}));           // 0, "(l33t)"),
$("#d_o_h").click(clickDictionary({ prio:  1, points:  3, name: "4 Human random"}));   // 1, "4 Human random"),
$("#d_a_c").click(clickDictionary({ prio:  2, points:  3, name: "Aquila culture"}));   // 2, "Aquila culture"),
$("#d_a_n").click(clickDictionary({ prio:  3, points:  2, name: "Aquila name"}));      // 3, "Aquila name"),
$("#d_o_a").click(clickDictionary({ prio:  4, points: 16, name: "All words"}));        // 4, "All words"),
$("#d_o_c").click(clickDictionary({ prio:  5, points:  5, name: "Common words"}));     // 5, "Common words"),
$("#d_d_c").click(clickDictionary({ prio:  6, points:  3, name: "Dugo culture"}));     // 6, "Dugo culture"),
$("#d_d_n").click(clickDictionary({ prio:  7, points:  3, name: "Dugo name"}));        // 7, "Dugo name"),
$("#d_e_c").click(clickDictionary({ prio:  8, points:  2, name: "Ekanesh culture"}));  // 8, "Ekanesh culture"),
$("#d_e_n").click(clickDictionary({ prio:  9, points:  2, name: "Ekanesh name"}));     // 9, "Ekanesh name"),
$("#d_o_n").click(clickDictionary({ prio: 10, points:  2, name: "Number/Year"}));      //10, "Number/Year"),
$("#d_p_c").click(clickDictionary({ prio: 11, points:  4, name: "Pendzal culture"}));  //11, "Pendzal culture"),
$("#d_p_n").click(clickDictionary({ prio: 12, points:  3, name: "Pendzal name"}));     //12, "Pendzal name"),
$("#d_s_c").click(clickDictionary({ prio: 13, points:  2, name: "Sona culture"}));     //13, "Sona culture"),
$("#d_s_n").click(clickDictionary({ prio: 14, points:  4, name: "Sona name"}));        //14, "Sona name")




/* ------------------------- ------------------------- -------------------------
   Clicking the GO button (or having it auto clicked when the current attempt is finished
*/

var currentDics = [];
var currentJQN = $("#current");
var currentParentJQN = $("#current_parent");
var currentTimeJQN = $("#currentTime");
var password_guess_spinner_handle = null;
var searchName = "";
var successProgress = 999;

function processPasswords() {
    // Don't click on the hidden_alpha button, please!
    if (nextDics.length == 0 || nextIsOnlyL33t()) {
        return;
    }

    if (combinationAlreadyChecked() || won) {
        return;
    }

    currentDics = nextDics;

    // clear Next
    $("#next").empty();
    nextDics = [];
    $("#nextTime").text("0");
    $("#go_button").addClass("hidden_alpha");
    nextTotal = 0;
    l33tInNextDic = false;
    l33tJQN.removeClass("label-default");
    l33tJQN.addClass("label-info");

    // fill current
    currentJQN.empty();
    currentParentJQN.removeClass("hidden_alpha");

    var i;
    var total = 1;
    searchName = "";
    for (i = 0; i < currentDics.length; i++) {
        var dic = currentDics[i];
        var last = ( i == currentDics.length - 1);
        var html = '<\
span class="label label-primary unselectable"><span class="badge password-badge">' + dic.points + '</span> ' + dic.name + '</span> ';
        currentJQN.append(html);
        if (!last) {
            currentJQN.append(" X ");
        }
        total = total * dic.points;
        searchName += dic.name;
    }
    currentTimeJQN.text(total);


    // determine if this is going to be a success
    successProgress = determineSuccessProgress();


    // password guess showing
    if (password_guess_spinner_handle) {
        clearInterval(password_guess_spinner_handle);
    }
    password_guess_spinner_handle = setInterval(updateWord,25 );

    var interval = total * 5;
    // progress bar
    progress = 0;
    progressBarJQN.addClass("active");
    progressBarJQN.removeClass("progress-bar-success");
    if (progress_handle) {
        clearInterval(progress_handle);
    }
    progress_handle = setInterval(updateProgress, interval);
}

function combinationAlreadyChecked() {
    var combinationSearch = "";
    nextDics.forEach(function(dic) {
        combinationSearch += dic.name;
    });

    var exists = false;
    ourHistory.forEach(function (entry) {
        if (entry.search == combinationSearch) {
            exists= true;
        }
    });

    if (exists) {
        $.notify("Already tried that combination", {globalPosition: 'top center', className: 'warning'});
        return true;
    }
    return false;
}

function determineSuccessProgress() {
    var level = passwordsAvailable[searchName];
    if ( level == undefined) {
        return 999;
    }

    return 25 + Math.floor(Math.random() * 225);
}

var progress_handle = null;
var progress = 0;
var progressBarJQN = $("#progress_bar");


/* ------------------------- ------------------------- -------------------------
   Methods for handling the update of the progress bar, and possible successful searches
 */



function updateProgress() {
    progress += 1;
    var percentage = progress/2.5 + "%";
    progressBarJQN.css("width", percentage);

    if (progress >= 250) {
        handleFinishAttempt(false);
        return;
    }

    if (progress > successProgress ) {
        handleSuccess();
    }

}

var won = false;

function handleSuccess() {
    progressBarJQN.css("width", "100%");
    delete passwordsAvailable[searchName];
    var level = randomLevel();

    passwordsNeeded[level] --;
    passwordsFound[level] ++;

    $("#indicator_" + level).text(passwordsFound[level]);
    if (passwordsNeeded[level] <= 0) {
        $("#success_" + level).removeClass("hidden_alpha");
    }

    if (testHackComplete()) {
        var endTime = Date.now();
        var s = (endTime - startTime) / 1000;
        var m = s / 60;

        won = true;

        console.log("Defeating the password ice took " + m + " minutes (or " + s + ") seconds.");
        ice_bested();
    }

    handleFinishAttempt(true);
}

var levels = ["ENCRYPTED", "LATENCY", "VOLUME", "PRIORITY"];
function randomLevel() {
    for (var i = 0; i < 100; i++) {
        var r = Math.floor(Math.random() * 4);
        var level = levels[r];
        if (passwordsNeeded[level] > 0) {
            return level;
        }
    }
}

function testHackComplete() {
    for (var level in passwordsNeeded) {
        if (passwordsNeeded[level] > 0) {
            return false;
        }
    }
    return true;
}

/* ------------------------- ------------------------- -------------------------
   Attempt has finished
   - add current attempt to history
   - clear current attempt
   - see if a next one is ligned up, if so: start it
 */


function handleFinishAttempt(success) {
    progressBarJQN.removeClass("active");
    if (success) {
        progressBarJQN.addClass("progress-bar-success");
    }

    addHistory(currentDics, success);
    clearInterval(progress_handle);
    clearInterval(password_guess_spinner_handle);
    passwordAttemptJQN.html("&nbsp;");
    progress_handle = null;
    currentParentJQN.addClass("hidden_alpha");
    if (nextDics.length > 0) {
        processPasswords();
    }
}


var historyJQN = $("#history");
var ourHistory = [];

// toAdd = List<dictionaries>
function addHistory(toAdd, success) {
    var entry = { search: searchName, dics: toAdd, success: success };
    ourHistory.push(entry);
    ourHistory.sort(function(a,b){ return a.search.localeCompare(b.search); });

    historyJQN.empty();

    ourHistory.forEach(function (entry) {
        var labelType = (entry.success) ? "label-success" : "label-default";

        var html = "<div class='tight'>";
        entry.dics.forEach(function(dic) {
            html += "<span class='label " + labelType + "'>" + dic.name + "</span> ";
        });
        html += "</div>";

        historyJQN.append(html);
    });
}

// for measuring total time to complete puzzle
var startTime = Date.now();


$(function() {

    ice_fancybox_closed_function = function () {
        clearInterval(progress_handle);
        clearInterval(password_guess_spinner_handle);
        console.log("Stopped thread of password");
        ice_fancybox_closed_function = null;
    };

    thread.echo(10, "↼ Connecting to ice, initiating attack.", "[[i;#a9443b;]↼ Connecting to ice, initiating attack.]");
    thread.echo(5, "↼ User login accessed.");
    thread.echo(0, "");
    thread.echo(15, "⇁ (warn) Possible automated connection detected, Turing test initiated.", "⇁ [[i;#a9443b;](warn)] Possible automated connection detected, Turing test initiated.");
    thread.echo(10, "↺ Adjusting operation parameters.");
    thread.echo(5, "⇁ (success) Proof of human operator accepted.", "⇁ [[;#3c763d;](success)] Proof of human operator accepted.");
    thread.echo(0, "");
    thread.echo(10, "↼ Origin masked, attempt limitation bypassed.");
    thread.echo(10, "↺ Established network tunnel needs.");
    thread.display(10, "#needsBlock");
    thread.echo(10, "↼ Starting up password guessing interface.");
    thread.display(10, "#passwordGuessUi");
    thread.echo(0, "↼ Password guessing interface operational.", "↼ Password guessing interface [[;#31708f;]operational].");
    thread.run(0, function(){
        iceTerm.pause();
    });
});



/* ------------------------- ------------------------- -------------------------
  ICE defeated

 */

function ice_bested() {
    iceTerm.resume();
    thread.echo(0, "");
    thread.echo(10, "↼ Accounts with required attributes secures.");
    thread.echo(15, "↼ Establishing tunnel.");
    thread.echo(15, "↼ success.", "↼ [[;#3c763d;]success].");
    thread.echo(5, "↼ closing ui.");
    thread.run(0, ice_bested_end);
}

function ice_bested_end() {
    $.fancybox.close();
    thread.stop();
    term.echo("Ice layer compromised, privilege elevated, access granted.");
    term.focus();
    hackedService(serviceId);
}
