function netwalk(canvasId, puzzle, serviceId, state, IMAGE_SIZE) {

    const STATE_HACKING = "hack";
    const STATE_PRINT = "print";
    const STATE_PRINT_SOLUTION = "print-solution";

    if (state !== STATE_HACKING && state !== STATE_PRINT && state !== STATE_PRINT_SOLUTION) {
        console.log("Unknown state: " + state);
        return;
    }

    // set to true for quick testing (no simulated console messages)
    let netwalkDebug = false;

    let interactive = false;


    // Seed the random with the net string so that the randomization will be deterministic.
    let myRandom = new Math.seedrandom(puzzle.net);

    function random(max) {
        return Math.floor(myRandom() * max);
    }

    function getCell(net, x, y) {
        if (puzzle.wrapping) {
            let nextX = (x + puzzle.x ) % puzzle.x;
            let nextY = (y + puzzle.y ) % puzzle.y;
            return net[nextY][nextX];
        }
        else {
            if (net[y] != undefined) {
                return net[y][x]
            }
            return undefined;
        }
    }

    class Cell {
        constructor(imageName, endPoint, rotation, x, y, n, e, s, w,) {
            this.imageName = imageName;
            this.rotation = rotation;
            this.x = x;
            this.y = y;
            this.n = n;
            this.e = e;
            this.s = s;
            this.w = w;
            this.connected = false;
            this.perfect = false;
            this.image = null;
            this.centerImage = null;
            this.endPoint = endPoint;
            if (state !== STATE_PRINT_SOLUTION ) {
                let randomRotations = random(4);
                for (let i = 1; i < randomRotations; i++) {
                    this.rotate(true);
                }
            }
        }

        rotate(clockWise) {
            this.rotation += (clockWise) ? 90 : -90;

            if (clockWise) {
                let tmp = this.n;
                this.n = this.w;
                this.w = this.s;
                this.s = this.e;
                this.e = tmp;
            }
            else {
                let tmp = this.n;
                this.n = this.e;
                this.e = this.s;
                this.s = this.w;
                this.w = tmp;
            }

        }

        updateConnected(net) {
            if (this.connected) {
                return;
            }
            this.connected = true;
            this.perfect = true;
            let nCell = getCell(net, this.x, this.y - 1);
            let sCell = getCell(net, this.x, this.y + 1);
            let eCell = getCell(net, this.x + 1, this.y);
            let wCell = getCell(net, this.x - 1, this.y);

            let connectionMade = false;
            if (this.n) {
                if (nCell && nCell.s) {
                    if (!this.centerImage) {
                        nCell.updateConnected(net);
                        connectionMade = true;
                    }
                }
                else {
                    this.perfect = false;
                }
            }
            if (this.s) {
                if (this.s && sCell && sCell.n) {
                    if (!this.centerImage) {
                        sCell.updateConnected(net);
                        connectionMade = true;
                    }
                }
                else {
                    this.perfect = false;
                }
            }
            if (this.e) {
                if (eCell && eCell.w) {
                    if (!this.centerImage) {
                        eCell.updateConnected(net);
                        connectionMade = true;
                    }
                }
                else {
                    this.perfect = false;
                }
            }
            if (this.w) {
                if (wCell && wCell.e) {
                    if (!this.centerImage) {
                        wCell.updateConnected(net);
                        connectionMade = true;
                    }
                }
                else {
                    this.perfect = false;
                }
            }
            if (this.endPoint) {
                this.connected = connectionMade;
            }
            if (this.centerImage) {
                this.connected = false;
            }
        }
    }


    const fals = false;

    function createCell(from, x, y) {
        switch (from) {
            case "┳":
                return new Cell("3", fals, 180, x, y, fals, true, true, true);
            case "┫":
                return new Cell("3", fals, 270, x, y, true, fals, true, true);
            case "┻":
                return new Cell("3", fals, 0, x, y, true, true, fals, true);
            case "┣":
                return new Cell("3", fals, 90, x, y, true, true, true, fals);
            case "━":
                return new Cell("2s", fals, 90, x, y, fals, true, fals, true);
            case "┃":
                return new Cell("2s", fals, 0, x, y, true, fals, true, fals);
            case "╵":
                return new Cell("1", true, 180, x, y, true, fals, fals, fals);
            case "╶":
                return new Cell("1", true, 270, x, y, fals, true, fals, fals);
            case "╷":
                return new Cell("1", true, 0, x, y, fals, fals, true, fals);
            case "╴":
                return new Cell("1", true, 90, x, y, fals, fals, fals, true);
            case "┓":
                return new Cell("2", fals, 270, x, y, fals, fals, true, true);
            case "┛":
                return new Cell("2", fals, 0, x, y, true, fals, fals, true);
            case "┗":
                return new Cell("2", fals, 90, x, y, true, true, fals, fals);
            case "┏":
                return new Cell("2", fals, 180, x, y, fals, true, true, fals);
            case "╋":
                return new Cell("4", fals, 0, x, y, true, true, true, true);
            default:
                console.log("Invalid netwalk cell: " + from);
                return undefined;
        }
    }

    let TILE_SIZE = IMAGE_SIZE + 1;
    let HALF_SIZE = Math.floor(IMAGE_SIZE /2);
    let PADDING = 20;

    let CANVAS_X_SIZE = 2 * PADDING + puzzle.x * TILE_SIZE;
    let CANVAS_Y_SIZE = 2 * PADDING + puzzle.y * TILE_SIZE;

    let CENTER_X = Math.floor(CANVAS_X_SIZE / 2);
    let CENTER_Y = Math.floor(CANVAS_Y_SIZE / 2);

    let LINE_TOP_LEFT_X = PADDING;
    let LINE_TOP_LEFT_Y = PADDING;

    let xCenter = Math.floor(puzzle.x / 2);
    let yCenter = Math.floor(puzzle.x / 2);

    netwalkCanvas = new fabric.Canvas(canvasId);
    fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';
    netwalkCanvas.clear();
    netwalkCanvas.selection = false;

    const CANVAS_BACKGROUND_COLOR = (state === STATE_HACKING) ? "#333333" : "#f5f5f5";

    netwalkCanvas.setBackgroundColor(CANVAS_BACKGROUND_COLOR, netwalkCanvas.renderAll.bind(netwalkCanvas));
    let clearingCircle = new fabric.Circle({top: CENTER_Y, left: CENTER_X, stroke: '#444', fill: '#222', radius: 1});
    netwalkCanvas.add(clearingCircle);

    function animate(toAnimate, attribute, value) {
        toAnimate.animate(attribute, value, {
            onChange: netwalkCanvas.renderAll.bind(netwalkCanvas),
            duration: 150,
            easing: fabric.util.ease.easeInOutSine
        });
    }


    function clickObject(event) {
        let selectedObject = event.target;
        if (!selectedObject || !interactive) {
            return;
        }
        let cell = selectedObject.cell;

        cell.rotate(true);
        animate(cell.image, "angle", selectedObject.cell.rotation);
        if (cell.centerImage) {
            animate(cell.centerImage, "angle", selectedObject.cell.rotation);
        }
        updateVisualsAndCheckWin();
    }

    function updateVisualsAndCheckWin() {

        allCells(net, (cell) => {
            cell.connected = false;
        });

        allCells(net, (cell) => {
            if (cell.endPoint) {
                cell.updateConnected(net);
            }
        });

        allCells(net, (cell) => {
            if (cell.connected) {
                let image = $('#light_' + cell.imageName)[0];
                cell.image.setElement(image);
            }
            else {
                let image = $('#' + cell.imageName)[0];
                cell.image.setElement(image);
            }
        });

        let winCondition = true;
        allCells(net, (cell) => {
            if (!cell.perfect) {
                winCondition = false;
            }
        });
        if (winCondition && state == STATE_HACKING) {
            let centerCell = net[yCenter][xCenter];
            let image = $('#light_' + centerCell.imageName)[0];
            centerCell.image.setElement(image);

            win();

        }
        netwalkCanvas.renderAll();


    }

    function allCells(net, listener) {
        for (let y = 0; y < puzzle.y; y++) {
            for (let x = 0; x < puzzle.x; x++) {
                let cell = net[y][x];
                listener(cell);
            }
        }
    }


    let net = [];




    function displayMaze() {
        let i = 0;
        for (let y = 0; y < puzzle.y; y++) {
            let netRow = [];
            for (let x = 0; x < puzzle.x; x++) {
                let c = puzzle.net.charAt(i);
                i++;
                let cell = createCell(c, x, y);

                let image = $('#' + cell.imageName)[0];
                let cellImage = createImageObject(image, cell, x, y);
                netwalkCanvas.add(cellImage);
                cell.image = cellImage;

                if (x === xCenter && y === yCenter) {
                    let centerImage = $('#center')[0];
                    let centerCellImage = createImageObject(centerImage, cell, x, y);
                    netwalkCanvas.add(centerCellImage);
                    cell.centerImage = centerCellImage;

                }
                netRow.push(cell);
            }
            net.push(netRow);
        }
        drawGridLines();
        updateVisualsAndCheckWin();
        if (state === STATE_HACKING) {
            interactive = true;
        }
    }

    function makeLine(left, top, right, bottom, color, strokeWidth) {
        coords = [
            LINE_TOP_LEFT_X + left * TILE_SIZE,
            LINE_TOP_LEFT_Y + top * TILE_SIZE,
            LINE_TOP_LEFT_X + right * TILE_SIZE,
            LINE_TOP_LEFT_Y + bottom * TILE_SIZE
        ];
        return new fabric.Line(coords, {
            fill: color,
            stroke: color,
            strokeWidth: strokeWidth,
            selectable: false,
            evented: false,
        });
    }

    function drawGridLines() {
        for (let y = 0; y < puzzle.y + 1; y++) {
            let line = makeLine( 0, y, puzzle.x, y, 'gray', 0.3 );
            netwalkCanvas.add(line);
        }
        for (let x = 0; x < puzzle.x + 1; x++) {
            let line = makeLine( x, 0, x, puzzle.y, 'gray', 0.3);
            netwalkCanvas.add(line);
        }
        let color = puzzle.wrapping ? 'blue' : 'black';
        netwalkCanvas.add(makeLine(0,0,puzzle.x, 0, color, 2));
        netwalkCanvas.add(makeLine(0,puzzle.y,puzzle.x, puzzle.y, color, 2));
        netwalkCanvas.add(makeLine(0,0,0,puzzle.y, color, 2));
        netwalkCanvas.add(makeLine(puzzle.x,0,puzzle.x, puzzle.y, color, 2));
    }

    function createImageObject(image, cell, x, y) {
        let cellImage = new fabric.Image(image, {
            left: LINE_TOP_LEFT_X + HALF_SIZE + x * TILE_SIZE,
            top: LINE_TOP_LEFT_Y + HALF_SIZE + y * TILE_SIZE,
            height: IMAGE_SIZE,
            width: IMAGE_SIZE,
            lockRotation: true,
            lockScalingX: true,
            lockScalingY: true,
            lockMovementX: true,
            lockMovementY: true,
            selectable: false,
            cell: cell,
            angle: cell.rotation,
        });
        cellImage.setControlsVisibility({
            mt: false,
            mb: false,
            ml: false,
            mr: false,
            mtr: false
        });
        return cellImage;
    }

    let increaseCircleIntervalId = null;
    function win() {
        interactive = false;
        increaseCircleIntervalId = setInterval(increaseCircle, 20);
        ice_bested();
    }

    displayMaze();


    if (state === STATE_HACKING) {

        netwalkCanvas.on('mouse:down', clickObject);


        var iceTerm = $('#ice_term').terminal(
            function (cmd, term) {
                processSolution(cmd, term);
            }, {
                name: 'xxx',
                greetings: null,
                width: "100%",
                height: 150,
                keypress: function (e) {
                    return false;
                },
            });
        var thread = new Thread(iceTerm);

        if (!netwalkDebug) {
            thread.echo(10, "↼ Connecting to ice, initiating attack.", "[[i;#a9443b;]↼ Connecting to ice, initiating attack.]");
            thread.echo(12, "↼ Detected Dahana network obfuscation.", "↼ Detected [[;#31708f;]Dahana] network obfuscation.");
            thread.echo(3, "↼ Attempting automatic reconfiguration.");
            thread.echo(17, "↼ Reconfiguring.");
            thread.echo(25, "↺ Status check");
            thread.echo(10, "↼ Network integrity 17%", "↼ Network integrity [[;#a9443b;]17%]");
        }

        thread.display(0, "#" + canvasId);
        thread.echo(5, "↼ Manual reconfiguration started.");

        const start = performance.now();

        function ice_bested() {
            const end = performance.now();
            const time = Math.floor((end - start) / 1000);

            if (netwalkDebug) {
                alert("Time: " + (time) + "s");
            }
            console.log("Time: " + (time) + "s");
            thread.echo(10, "");
            thread.echo(5, "⇁ Configuration restored", "Configuration [[b;#31708f;]restored]");
            thread.echo(22, "");
            thread.echo(15, "↼ Access granted.", "[[i;#a9443b;]↼ Access granted.]");
            thread.run(5, () => {
                $("#" + canvasId).attr("style", "background-color: white");
            });
            thread.run(0, ice_bested_end);
        }

        function ice_bested_end() {
            thread.stop();
            netwalkCanvas.dispose();
            netwalkCanvas = null;
            let canvasElement = $('#' + canvasId);
            canvasElement.remove();
            $.fancybox.close();
            term.echo("Ice subverted.");
            term.focus();
            hackedService(serviceId);
        }

        function increaseCircle() {
            if (netwalkCanvas) {
                clearingCircle.radius += (Math.sqrt(clearingCircle.radius) / 8);
                netwalkCanvas.renderAll();
            }
            else {
                clearInterval(increaseCircleIntervalId);
            }
        }
    }


    let displayCount = 0;
    const delay = 500;
    let intervalHandle = setInterval(() => {
        if (displayCount * delay > 10 * 1000) {
            clearInterval(intervalHandle);
        }
        netwalkCanvas.renderAll();
        console.log('renderall');
        displayCount ++;
    }, delay);

};
