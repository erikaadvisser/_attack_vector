
function formatNumber2(input) {
    if (input < 10) {
        return "0" + input;
    }
    return input;
}

function formatNumber3(input) {
    if (input < 10) {
        return "00" + input;
    }
    if (input < 100) {
        return "0" + input;
    }
    return input;
}

var first = Math.floor(Math.random()*100);
var second = Math.floor(Math.random()*1000);
var third = Math.floor(Math.random()*100);

function nextNumber() {
    var formatted = formatNumber2(third) + "-" + formatNumber3(second) + "-" + formatNumber2(first);
    
    first ++;
    if (first >= 100) {
        first = 0;
        second ++;
    }
    if (second >= 1000) {
        second = 0;
        third ++
    }
    if (third >= 100) {
        third = 0;
    }
    
    return formatted;
}

var iceTerm = $('#ice_term').terminal(
    function (cmd, term) {
        processSolution(cmd, term);
    }, {
        name: 'xxx',
        greetings: null,
        width: "100%",
        height: 780,
        keypress : function (e) { return false; },
    });
var thread = new Thread(iceTerm);

function repeat() {
    var number = nextNumber();
    thread.echo(50, "↼ Attempting passcodes in block " + number, "↼ Attempting passcodes in block [[;#31708f;]" + number +"]");
    thread.run(0, function() {
        repeat();
    });
}

$(function() {

    ice_fancybox_closed_function = function() {
        console.log("Stopped thread of unhackable");
        thread.stop();
        ice_fancybox_closed_function = null;
    };

    thread.echo(10, "↼ Connecting to ice, initiating attack.", "[[i;#a9443b;]↼ Connecting to ice, initiating attack.]");
    thread.echo(15, "↼ No known strong attack vector found, reverting to passcode guessing.");
    thread.echo(10, "↼ Warning, weak attack vector", "↼ [[;#a9443b;]Warning], weak attack vector.");
    thread.run(0, function() {
        repeat();
    });
});
