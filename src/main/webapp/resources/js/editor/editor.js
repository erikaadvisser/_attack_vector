var canvas = new fabric.Canvas('canvas');
fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';

var connectStart;
var placing;
var nodesById;
var allIdsUsed;
var networkNodesById;
var tooltipsActive;
var activeService;

function resetCanvas() {
    if (typeof edit_init != "undefined" && edit_init) {
        canvas.clear();
        canvas.selection = false;
        canvas.setBackgroundColor("#333333", canvas.renderAll.bind(canvas));

        connectStart = null;
        placing = null;
        nodesById = {};
        allIdsUsed = [];
        networkNodesById = {};
        tooltipsActive = false;
        activeService = null;
    }
}

resetCanvas();




// ---------------------------------------- ----------------------------------------
// Code in this block is 'shared' with hackingrun.js . At some moment, this code should reside
// in its own file, and we need a module framework (webpack or ecma 2016) to load the modules
// ---------------------------------------- ----------------------------------------

function addNode(props) {
    if (nodesById[props.id]) {
        alert("somehow an object is being added with an id that is already taken. id:" + props.id);
        return;
    }

    var node = new fabric.Image(props.image, {
        left: props.x,
        top: props.y,
        height: props.size,
        width: props.size,
        lockRotation: true,
        lockScalingX: true,
        lockScalingy: true,
        lockMovementX: !edit_init,
        lockMovementY: !edit_init,
    });

    node.setControlsVisibility({
        mt: false,
        mb: false,
        ml: false,
        mr: false,
        mtr: false
    });

    node.lineStart = [];
    node.lineEnd = [];
    node.id = props.id;
    node.nodeType = props.nodeType;
    node.services = props.services;
    canvas.add(node);
    nodesById[node.id] = node;
    var networkId = props.services[0].data['networkId'];
    networkNodesById[networkId] = '1';


    var nodeIdLabel = new fabric.Text(networkId, {
        evented: false,
        fontFamily: "courier",
        fontSize: "12",
        fontStyle: "normal", // "", "normal", "italic" or "oblique".
        fontWeight: "0",
        fill: "#bbbbbb",
        hasControls: false,
        lockMovementX: true,
        lockMovementY: true,
        selectable: false,
        left: props.x - 20,
        top: props.y + 35,
        textAlign: "left", // "center", "right" or "justify".
    });
    canvas.add(nodeIdLabel);

    node.label = nodeIdLabel;

    return node;
}


function addLine(connectStart, connectEnd) {
    var stroke = "#aaaaaa";
    if (typeof edit_init !== "undefined" && edit_init) {
        stroke = "#cccccc";
    }

    var line = new fabric.Line(
        [connectStart.left, connectStart.top, connectEnd.left, connectEnd.top], {
            stroke: stroke,
            strokeWidth: 4,
            strokeDashArray: [5, 5],
            selectable: false
        });
    line.startObject = connectStart;
    line.endObject = connectEnd;

    connectStart.lineStart.push(line);
    line.startObject = connectStart;

    connectEnd.lineEnd.push(line);
    line.endObject = connectEnd;


    canvas.add(line);
    canvas.sendToBack(line);
}

function loadSite(url, initFunction) {
    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : url,
        data : null,
        dataType : 'json',
        timeout : 5000,
        success : function(data) {
            console.log("SUCCESS: ", data);
            initFunction(data);
            if (typeof edit_init != "undefined" && edit_init) {
                $.notify("Site loaded", {globalPosition: 'top center', className: "success"});
            }
        },
        error : function(e, reason) {
            console.log("ERROR: ", e);
            $.notify("Load failed, " + reason, {globalPosition: 'top', className: 'error'});
        }
    });
}

function createFromLoadGeneric(site, statusMap) {
    site.nodes.forEach(function(nodeProps) {
        
        var suffix = "";
        if ( statusMap[nodeProps.id] != undefined) {
            suffix = "_" + statusMap[nodeProps.id];
        }
        
        var image = $('[name="' + nodeProps.nodeType + suffix + '"]')[0];
        if (!nodeProps.nodeType || !image) {
            $.notify("Problem loading, failed to render node: " + nodeProps.id + ", nodeType id not found: " + nodeProps.nodeType, "warning");
        }
        else {
            nodeProps['image'] = image;
            nodeProps['nodeType'] = getNodeType(nodeProps.nodeType);

            nodeProps.services.forEach(function(serviceProps){
                var serviceTypeId = serviceProps.type;
                var serviceType = serviceDefs[serviceTypeId];
                serviceProps.type = serviceType;
            });

            var node = addNode(nodeProps);
        }
    });

    site.nodes.forEach(function(nodeProps) {
        nodeProps.connections.forEach(function(targetNodeId) {
            var node = nodesById[nodeProps.id];
            var targetNode = nodesById[targetNodeId];

            if (node && targetNode) {
                addLine(node, targetNode);
            }
        });

    });

    canvas.renderAll();
}

// End code 'shared' with hackingrun.js
// ---------------------------------------- ----------------------------------------








function drag(event) {
    placing = {
        object : event.originalEvent.target
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function createNetworkId() {
    "use strict";

    var i, s, possibleId;

    for (i = 0; i < 10; i++) {
        s = ("0" + i);
        possibleId = s.substr(s.length-2);
        if (!networkNodesById[possibleId]) {
            return possibleId
        }
    }

    for (i = 10; i < 100; i++) {
        s = ("" + i);
        possibleId = s.substr(s.length-2);
        if (!networkNodesById[possibleId]) {
            return possibleId
        }
    }
    return createId();
}

/** Approximation of uuid. Seconds 1-jan since 2016. */
function createId() {
    var base =  Math.floor(Date.now() / 1000) - 1450656000;
    for (var i = 0; i < 1000; i++) {
        var id = i + base;
        if (!allIdsUsed.includes("" + id)) {
            var newId = "" + id;
            allIdsUsed.push(newId);
            return newId;
        }
    }
    alert("Failed to create ID, data corrupt. load and try again.");
}

function drop_image_and_create_node(event) {
    var image = placing.object;
    var pointer = canvas.getPointer(event.e);
    var type = getNodeType(image.name);
    var networkId = createNetworkId();


    var services = [];

    var serviceCount = 0;
    type.services.forEach( function (serviceDef) {
        var service = {};
        service.type = serviceDef;
        service.data = {};
        serviceDef.fields.forEach( function(field) {
            service.data[field.key] = field.value;
        });
        service.data['id'] = createId();
        services.push(service);

        serviceCount += 1;
    });
    var nodeId = services[0].data['id'];
    services[0].data['networkId'] = networkId;
    services[0].data['name'] = type.nameSuggestion;

    addNode({
        id: nodeId,
        x: pointer.x,
        y: pointer.y,
        nodeType: type,
        image: image,
        size: image.height,
        content: "",
        services: services
    });

    canvas.deactivateAll();
    canvas.renderAll();
}

function delete_object() {
    var activeObject = canvas.getActiveObject();
    if (activeObject) {
        deleteLines(activeObject);
        canvas.remove(activeObject);
        canvas.remove(activeObject.label);
        nodesById[activeObject.id] = undefined;
        var networkId = activeObject.services[0].data['networkId'];
        networkNodesById[networkId] = undefined;
    }
}
function delete_lines() {
    var activeObject = canvas.getActiveObject();
    if (activeObject) {
        deleteLines(activeObject);
    }
}

function deleteLines(activeObject) {
    activeObject.lineStart.forEach( function(line) {
        var index = line.endObject.lineEnd.indexOf(line);
        line.endObject.lineEnd.splice(index, 1);
        canvas.remove(line);
    });

    activeObject.lineEnd.forEach( function(line) {
        var index = line.startObject.lineStart.indexOf(line);
        line.startObject.lineStart.splice(index, 1);
        canvas.remove(line);
    });

    activeObject.lineStart = [];
    activeObject.lineEnd = [];

}

function snap() {
    "use strict";
    canvas.getObjects().forEach(function(object) {
        if (object.type === "image") {
            object.set({
                left : 20 * Math.floor((object.left+10) / 20),
                top : 20 * Math.floor((object.top+10) / 20)
            });
            objectMove(object);
        }
    });
    canvas.renderAll();
}

function object_selected(event) {
    "use strict";

    var selectedObject  =  event.target;
    if (connectStart && event.e && event.e.ctrlKey) {
        addLine(connectStart, selectedObject);
        connectStart = selectedObject;
        canvas.renderAll();
    }
    else {
        connectStart = selectedObject;
    }
    initNodeDataEditor(selectedObject);
    activateTabsAndTab(selectedObject, selectedObject.services.length-1);
}

function initNodeDataEditor(selectedObject) {
    $("#node_type_name").text(selectedObject.nodeType.name);
    $("#node_type_description").text(selectedObject.nodeType.description);
    
        $("#node-services-tab-list").empty();
    $("#node-services-tab-content").empty();


    var serviceCount;
    var tabCount = selectedObject.services.length;
    for (serviceCount = 0; serviceCount < tabCount; serviceCount++) {
        var service = selectedObject.services[serviceCount];

        createServiceDataTab(serviceCount, service.type, tabCount, true);

        $('#data_tab_' + serviceCount).empty();
        var customFieldCount = (service.type.fields) ? service.type.fields.length : 0;

        var i;
        for (i = 0; i < customFieldCount; i++) {
            var field = service.type.fields[i];
            var fieldKey = field.key;
            var value = service.data[fieldKey];
            appendInput(serviceCount, i, field, value);
        }
    }
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // activeServiceTab = e.target;
        var targetSelector = e.currentTarget.hash;
        var numberText = targetSelector.substr("#svc_data_tab_".length)
        activeService = parseInt(numberText)
    });

    // activeServiceTab = null;
    activeService = 0;
}

function createServiceDataTab(tabNumber, type, tabCount, showHeader) {
    var tabName = (type.tab) ? type.tab : tabNumber;

    var active = (tabNumber === 0) ? 'active' : '';
    var tabContentId = "svc_data_tab_" + tabNumber;
    var tabHeadHtml = '<\
li class="' + active + '"><a href="#' + tabContentId + '" aria-controls="home" role="tab" data-toggle="tab">' + tabName + '</a></li>';

    var tabContentHtml = '<\
div class="tab-pane ' + active + '" id="' + tabContentId + '">\</div>';


    $("#node-services-tab-list").append(tabHeadHtml);
    $("#node-services-tab-content").append(tabContentHtml);
    if (showHeader) {
        appendHeader(tabNumber, type, tabCount);
    }
}

function appendHeader(tabNumber, type, tabCount) {
    var linkUp = (tabNumber < 2) ? '◀' : '<a class="character_image" onclick="btn_move_layer_up();return false;">◀</a>';
    var linkDown = (tabNumber == 0 || tabNumber>= tabCount -1) ? '▶' : '<a class="character_image" onclick="btn_move_layer_down();return false;">▶</a>';
    var linkDelete = (tabNumber == 0) ? "" : '<a class="character_image pull-right" onclick="btn_delete_layer();return false;">[x]</a>';

    var html = '<\
div class="form-group service_layer_form_group">\
    <label class="col-lg-3 control-label text-muted">Service</label>\
    <div class="col-lg-8">\
        <div class="text-muted" style="margin-top: 7px;margin-bottom: 0;">\
        <strong><span id="node_type_name" style="color: #8a6d3b;">' + type.name + '</span> </strong>\
        <span>' + type.description + '</span>' + linkDelete + '\
        </div>\
    </div>\
</div>\
<div class="form-group" >\
    <label class="col-lg-3 control-label text-muted text_not_bold">Layer</label>\
    <div class="col-lg-8">\
        <div class="text-muted" style="margin-top: 7px;margin-bottom: 0;">\
        ' + tabNumber + ' ' + linkUp + ' ' + linkDown + '\
        </div>\
    </div>\
</div>\
';
    var tabContentId = "svc_data_tab_" + tabNumber;
    $('#' + tabContentId).append(html);

}

function appendInput(tabNumber, index, field, value) {
    var tabContentId = "svc_data_tab_" + tabNumber;
    var itemId = "service_data_" + tabNumber + "_" + index;
    var readonly = (field.editable === false) ? "readonly" : ""; // value not always set
    var html = '<\
div class="form-group form-group-sm local-form-group-small">\
<label for="' + itemId + '" class="col-lg-3 control-label text-muted">' + field.name + '</label>\
<div class="col-lg-' + field.length + '">\
<input id="' + itemId + '" type="text" class="form-control has_help_tooltip" data-placement="bottom" title="' + field.description + '" \
oninput="changeServiceField(this.value, ' + tabNumber + ', ' + index + ');" ' + readonly + '>\
</div>\
</div>\
';
    $('#' + tabContentId).append(html);
    $('#' + itemId).val(value)

    showTooltips(false);
}

function showTooltips(toggle) {
    if (toggle) {
        tooltipsActive = !tooltipsActive;
    }
    if (tooltipsActive) {
        $(".has_help_tooltip").tooltip({
            'trigger': "hover",
        });
        $("#btn_toggle_tooltips").addClass("tooltip_help_active").removeClass("tooltip_help_inactive");
    }
    else {
        $(".has_help_tooltip").tooltip("destroy");
        $("#btn_toggle_tooltips").addClass("tooltip_help_inactive").removeClass("tooltip_help_active");
    }
}

function selectionCleared() {
    "use strict";

    $("#node_type_name").text("");
    $("#node_type_description").text("(select node)");

    $("#node-services-tab-list").empty();
    $("#node-services-tab-content").empty();

    var type = { tab: "-"};
    createServiceDataTab(0, type, false);
    activeService = null;

}

function changeServiceField(value, tab, fieldNumber) {
    var activeObject = canvas.getActiveObject();
    if (!activeObject) { return; }

    var serviceType = activeObject.services[tab].type;
    var key = serviceType.fields[fieldNumber].key;
    var oldValue = activeObject.services[tab].data[key];

    activeObject.services[tab].data[key] = value;
    if (tab == 0 && fieldNumber == 1) {
        activeObject.label.setText(value);
        networkNodesById[oldValue] = undefined;
        networkNodesById[value] = "1";

        canvas.renderAll();
    }
}

function objectMove(object) {
    object.lineStart.forEach(function(line) {
        line.set({
            x1: object.left,
            y1: object.top});
    });

    object.lineEnd.forEach(function(line) {
        line.set({
            x2: object.left,
            y2: object.top});
    });

    object.label.set({
        left: object.left - 20,
        top: object.top + 35,
    })


}


function object_moving_or_scaling(event) {
    var object = event.target;
    objectMove(object);

    canvas.renderAll();
}


function save() {
    let templateUses = $("#templateUses").val().trim();
    console.log("=" + templateUses + "=");
    if (templateUses !== "" && !(String(Number(templateUses)) === templateUses)) {
        $.notify("The 'template use count' must be a number.", {globalPosition: 'top', className: 'error'});
        return;
    }

    var systemMap = {
        id: siteId,
        nodes : [],
        name: $("#site_name").val(),
        description: $("#site_description").val(),
        faction: $("#faction").val(),
        gmName: $("#gm_name").val(),
        startNodeNetworkId: $("#start_node").val(),
        hackTime: $("#hack_time").val(),
        hackable: $("#site_hackable").prop('checked'),
        scannable: $("#site_scannable").prop('checked'),
        templateUses: $("#templateUses").val(),
        type: $("#site_type").val(),
    };
    
    canvas.getObjects().forEach(function (object) {
        if (object.type === "image") {

            var serviceData = [];
            var layerCount = 0;
            object.services.forEach(function (service){
                var nodeService = { type: service.type.id,
                                    data: service.data,
                                    id: service.data['id'],
                                    layer: layerCount};

                serviceData.push(nodeService);
                layerCount += 1;
            });

            var node = {
                id: object.id,
                nodeType: object.nodeType.id,
                x: object.left,
                y: object.top,
                content: object.content,
                size: object.height,
                connections: [],
                services: serviceData,
            };

            object.lineStart.forEach(function(line) {
                node.connections.push(line.endObject.id);
            });
            systemMap.nodes.push(node);
        }
    });

    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : "/gm/editor/save",
        data : JSON.stringify(systemMap),
        dataType : 'json',
        timeout : 5000,
        success : function(data) {
            console.log("SUCCESS: ", data);
            if (data.success) {
                $.notify(data.text, { globalPosition: 'top center', className: data.type});
            }
            else {
                $.notify(data.text, { globalPosition: 'top center', className: data.type, autoHideDelay: 10000});
            }
        },
        error : function(e, reason) {
            console.log("ERROR: ", e);
            $.notify("Save failed, " + reason, {globalPosition: 'top', className: 'error'});
        }
    });
}

function load() {
    loadSite("/gm/editor/load/" + siteId + "/", createFromLoadInEditor);
}

function createFromLoadInEditor(site) {
    resetCanvas();
    createFromLoadGeneric(site, {});

    site.nodes.forEach(function(node) {
        node.services.forEach(function(service){
            allIdsUsed.push(service.data['id']);
        });
    });
  


    $("#site_name").val(site.name);
    $("#site_description").val(site.description);
    $("#faction").val(site.faction);
    $("#gm_name").val(site.gmName);
    $("#start_node").val(site.startNodeNetworkId);
    $("#hack_time").val(site.hackTime);
    $("#site_hackable").prop('checked', site.hackable);
    $("#site_type").val(site.type);
    $("#templateUses").val(site.templateUses);



    if (typeof edit_init != "undefined" && edit_init) {
        updatePageTitle();
    }
}

// -------------------------------------------------
// layers 
// -------------------------------------------------


function btn_add_layer_text() {
    addLayer(serviceDefs.TEXT);
}

function btn_add_layer_file() {
    addLayer(serviceDefs.FILE);
}

function btn_add_layer_code() {
    addLayer(serviceDefs.CODE);
}

function btn_add_layer_time() {
    addLayer(serviceDefs.TIME);
}

function btn_add_layer_resources() {
    addLayer(serviceDefs.RESOURCE);
}

function btn_add_layer_remote_tracer() {
    addLayer(serviceDefs.REMOTE_TRACER);
}

function btn_add_layer_trace_logs() {
    addLayer(serviceDefs.TRACE_LOGS);
}

function btn_add_layer_syscon() {
    addLayer(serviceDefs.SYSCON);
}

function btn_add_layer_fw_me() {
    addLayer(serviceDefs.ICE_MAGIC_EYE);
}

function btn_add_layer_fw_pw() {
    addLayer(serviceDefs.ICE_PASSWORD);
}

function btn_add_layer_fw_nw() {
    addLayer(serviceDefs.ICE_NETWALK);
}

function btn_add_layer_fw_ws() {
    addLayer(serviceDefs.ICE_WORD_SEARCH);
}

function btn_add_layer_fw_xx() {
    addLayer(serviceDefs.ICE_UNHACKABLE);
}

function btn_add_layer_fw_gm() {
    addLayer(serviceDefs.ICE_MANUAL);
}

function btn_add_layer_scan_blocker() {
    addLayer(serviceDefs.SCAN_BLOCKER);
}

function btn_add_layer_chat() {
    addLayer(serviceDefs.CHAT);
}


function addLayer(serviceDef) {
    var activeObject = canvas.getActiveObject();
    if (!activeObject) {
        return;
    }
    var service = {};
    service.type = serviceDef;
    service.data = {};
    serviceDef.fields.forEach( function(field) {
        service.data[field.key] = field.value;
    });
    service.data['id'] = createId();
    var layer = activeObject.services.length;
    activeObject.services.push(service);
    activateTabsAndTab(activeObject, layer);
}



function btn_delete_layer() {
    var activeObject = canvas.getActiveObject();
    if (!activeObject || activeService == null || activeService == 0) {
        return;
    }
    var services = activeObject.services;
    services.splice(activeService,1);
    activateTabsAndTab(activeObject, activeObject.services.length-1);
}

function btn_move_layer_up() {
    var activeObject = canvas.getActiveObject();
    if (!activeObject || activeService == null || activeService < 2) {
        return;
    }
    var serviceToMove = activeObject.services.splice(activeService,1)[0];
    activeObject.services.splice(activeService-1,0, serviceToMove);
    activateTabsAndTab(activeObject, activeService-1);
}

function btn_move_layer_down() {
    var activeObject = canvas.getActiveObject();
    if (!activeObject || activeService == null || activeService == 0) {
        return;
    }
    var serviceToMove = activeObject.services.splice(activeService,1)[0];
    activeObject.services.splice(activeService+1,0, serviceToMove);
    activateTabsAndTab(activeObject, activeService+1);
}


function activateTabsAndTab(activeObject, tabToActivate) {
    initNodeDataEditor(activeObject);
    $("#node-services-tab-list a")[tabToActivate].click();
}



function updatePageTitle() {
    var site_name = $("#site_name").val();
    $("head title").text("_ " + site_name);
}

if (typeof edit_init != "undefined" && edit_init) {
    "use strict";


    $("#node-library img").attr("draggable", "true");
    $("#node-library img").on("dragstart", drag);

    $(".btn_tooltip").tooltip();

    $("#btn_snap").click(snap);
    $("#btn_delete").click(delete_object);
    $("#btn_delete_lines").click(delete_lines);

    canvas.on('object:selected', object_selected);
    canvas.on('selection:cleared', selectionCleared);
    canvas.on('object:scaling', object_moving_or_scaling);
    canvas.on('object:moving', object_moving_or_scaling);


    $("#btn_save").click(save);
    $("#btn_load").click(load);

    $("#btn_add_layer_text").click(btn_add_layer_text);
    $("#btn_add_layer_file").click(btn_add_layer_file);
    $("#btn_add_layer_code").click(btn_add_layer_code);
    $("#btn_add_layer_time").click(btn_add_layer_time);
    $("#btn_add_layer_resources").click(btn_add_layer_resources);
    $("#btn_add_layer_scan_blocker").click(btn_add_layer_scan_blocker);
    $("#btn_add_layer_chat").click(btn_add_layer_chat);
    $("#btn_add_layer_remote_tracer").click(btn_add_layer_remote_tracer);
    $("#btn_add_layer_trace_logs").click(btn_add_layer_trace_logs);
    $("#btn_add_layer_syscon").click(btn_add_layer_syscon);

    $("#btn_add_layer_fw_ws").click(btn_add_layer_fw_ws);
    $("#btn_add_layer_fw_me").click(btn_add_layer_fw_me);
    $("#btn_add_layer_fw_nw").click(btn_add_layer_fw_nw);
    $("#btn_add_layer_fw_xx").click(btn_add_layer_fw_xx);
    $("#btn_add_layer_fw_gm").click(btn_add_layer_fw_gm);
    $("#btn_add_layer_fw_pw").click(btn_add_layer_fw_pw);

    $("#btn_delete_layer").click(btn_delete_layer);

    $("#site_name").change(updatePageTitle);
}

