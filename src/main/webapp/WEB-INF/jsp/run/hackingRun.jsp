<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%@ include file="../fragments/hacking_warning.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="../../../resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="../../../resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="../../../resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="../../../resources/jslib/bootstrap.min.js"></script>
    <script src="../../../resources/jslib/jquery.terminal-min.js"></script>
    <link href="../../../resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="../../../resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../../../resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../../../resources/jslib/jquery.fancybox.js"></script>
    <script src="/resources/jslib/fabric.js"></script>
    <script src="../../../resources/jslib/notify.min.js"></script>
    <script src="../../../resources/js/core/thread.js"></script>
    <%@ include file="../fragments/keepAlive.jsp" %>
</head>
<body class style="background-color: #222222">

<span id="time" style="
    pointer-events:none;
    position:absolute;
    font-size: 12px;
    z-index: 8080;
    color: #ccc;
    background-color: #333;
    border-radius: 5px;
    line-height: 16px;
    padding-left: 3px;
    padding-right: 3px;
    font-family: monospace;"></span>

<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Detection timer: <span id="time-location">&lt;not running&gt;</span></span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Site: ${run.site.name}</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2 text text-dark">
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <c:forEach var="script" items="${scripts}">
                <c:set var="usedClass" value="${script.used ? 'strikethrough' : ''}"/>
                <span id="script_${script.id}" class="${usedClass}">${script.command()}</span><br>
            </c:forEach>
        </div>
        <div class="col-lg-5" id="main_term">
        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <canvas id="canvas" width="607" height="815" style="border-radius: 3px 3px 3px 3px"></canvas>
        </div>
    </div>
</div>

<%@ include file="../fragments/menu_hacker.jsp" %>


<div style="display: none">

<c:set var="nodeTypes" value="<%=org.n_is_1._attack_vector.model.site.NodeType.values()%>"/>
<c:forEach items="${nodeTypes}" var="type">
    <span><img src="/resources/images/icons/black-white-pearls/template.png"               height="80" width="80" name="${type.name()}_UNDISCOVERED"></span>
    <span><img src="/resources/images/icons/black-white-pearls/template.png"               height="80" width="80" name="${type.name()}_DISCOVERED"></span>
    <span><img src="/resources/images/icons/black-white-pearls/${type.imageName}"          height="80" width="80" name="${type.name()}_PROBED"></span>
    <span><img src="/resources/images/icons/matte-blue-and-white-square/${type.imageName}" height="80" width="80" name="${type.name()}_FREE"></span>
    <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/${type.imageName}" height="80" width="80" name="${type.name()}_PROTECTED"></span>
    <span><img src="/resources/images/icons/matte-white-square/${type.imageName}"          height="80" width="80" name="${type.name()}_HACKED"></span>
</c:forEach>

    <span><img src="/resources/images/icons/yellow-road-sign/culture/astrology1-scorpion-sc37.png"      height="80" width="80" name="HACKER_AVATAR"></span>

    <span><img src="/resources/images/icons/red-road-sign/culture/astrology2-virgin.png"                height="80" width="80" name="RED_VIRGIN"></span>
    <span><img src="/resources/images/icons/red-road-sign/culture/astrology2-archer.png"                height="80" width="80" name="RED_ARCHER"></span>
    <span><img src="/resources/images/icons/red-road-sign/culture/astrology2-gemini.png"                height="80" width="80" name="RED_GEMINI"></span>
    <span><img src="/resources/images/icons/red-road-sign/culture/astrology2-scale.png"                 height="80" width="80" name="RED_SCALE"></span>
    <span><img src="/resources/images/icons/red-road-sign/culture/astrology2-water-carrier.png"         height="80" width="80" name="RED_WATER_CARRIER"></span>

    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number1-sc17.png"            height="80" width="80" name="BLUE_1"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number2-sc17.png"            height="80" width="80" name="BLUE_2"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number3-sc17.png"            height="80" width="80" name="BLUE_3"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number4-sc17.png"            height="80" width="80" name="BLUE_4"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number5-sc17.png"            height="80" width="80" name="BLUE_5"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number6-sc17.png"            height="80" width="80" name="BLUE_6"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number7-sc17.png"            height="80" width="80" name="BLUE_7"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number8-sc17.png"            height="80" width="80" name="BLUE_8"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number9-sc17.png"            height="80" width="80" name="BLUE_9"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number10-sc17.png"           height="80" width="80" name="BLUE_10"></span>
</div>
</body>
<script>

    var nodeOpacityByStatus = {
        UNDISCOVERED: 0,
        DISCOVERED : 1,
        PROBED: 1,
        FREE: 1,
        PROTECTED: 1,
        HACKED: 1
    };

    var runId = "${run.runId}";
    var hackerStartNodeId = "${run.currentNode.id}";
    var siteStartNodeId = "${run.site.startNodeId}";
    //    var nodeOpacity = 0.2;
    var nodeOpacity = 1;
    var printNodeLabelBackground = true;
    let DEBUG = ${debug};

</script>
<script src="../../../resources/js/core/node_definitions.js"></script>
<script src="../../../resources/js/attack/hackingrun.js"></script>
<%@ include file="../fragments/hacking_warning.jsp" %>
<script>
    function toDisplayTime(sec_num) {
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    }

    var timeIntervalHandle;

    function clockStarted() {
        if (traceEnd == -42) {
            $("#time").text("disabled");
            return;
        }
        var position = $("#time-location").offset();
        position.top --;
        $("#time").css(position);
        $("#time-location").text("");


        timeIntervalHandle = setInterval(function () {
            var adjustedCurrentMillis = Date.now() - serverMillisAhead;
            var secondsleft = Math.floor((traceEnd -adjustedCurrentMillis) / 1000);
            if (secondsleft <= 0) {
                secondsleft = 0;
            }


            if (secondsleft == 0) {
                detected();
            }

            var display = toDisplayTime(secondsleft);
            $("#time").text(display);

            if (secondsleft <= 300) {
                var fontSize = Math.floor(60 - (secondsleft/6.25));
                $("#time").css("font-size", fontSize);

                var lineHeight = 17 + Math.floor((300-secondsleft) / 10);
                $("#time").css("line-height", lineHeight + "px");

                var r = Math.floor(204 + 51 - (secondsleft/(300/51)));
                var gb = Math.floor(secondsleft / (300/204));
                var rgb = "rgb("+r+","+gb+","+gb+")";
                $("#time").css("color",rgb);
            }
        }, 500);
    }

    var term = $('#main_term').terminal(terminalFunction, {
        greetings: '',
        name: 'term',
        height: 780,
        prompt: '⇋ ',
        outputLimit: -1,
        exit: false
    });
    var termThread = new Thread(term);

    function detected() {
        clearInterval(timeIntervalHandle);
        term.pause();
        $.fancybox.close();
        term.echo("");
        termThread.setWait(20);
        termThread.echo(20, "↼ Intrusion detected. Persona disabled and trace initiated.", "[[i;#a9443b;]↼ Intrusion detected. Persona disabled and trace initiated.]");
        termThread.echo(30, "Intrusion logged.");
        $.get("/run/detected_timeout/${run.runId}");
    }

    function displayHackerAndLine(hackerLeft, hackerTop, hackerOpacity) {
        var siteStartNode = nodesById[siteStartNodeId];

        var image = $('[name="HACKER_AVATAR"]')[0];
        hackerAvatar = new fabric.Image(image, {
            left: hackerLeft,
            top: hackerTop,
            height: 40,
            width: 40,
            opacity: hackerOpacity
        });

        avatarNode = hackerAvatar;
        // The patroller requires a connection to be able to find some kind of (random) previous node.
        avatarNode.connections = [avatarNode];
        avatarNode.programs = {};
        canvas.add(hackerAvatar);
        canvas.bringToFront(hackerAvatar);
        var line = new fabric.Line(
                [siteStartNode.left, 810, siteStartNode.left, siteStartNode.top], {
                    stroke: "#222",
                    strokeWidth: 3,
                    strokeDashArray: [5, 10],
                    selectable: false,
                    opacity:0
                });

        canvas.add(line);
        canvas.sendToBack(line);
        canvas.renderAll();
        animate(line, "opacity", 1, 1000);
    }

    function createFromLoadRunning(site) {
        createFromLoadGeneric(site);
        var hackerStartNode = nodesById[hackerStartNodeId];
        currentNode = hackerStartNode;

        displayHackerAndLine(currentNode.left, currentNode.top, 0);

        termThread.echo(0, "🜁 Verdant OS. 🜃", "[[b;;]🜁 Verdant OS. 🜃]");
        termThread.echo(0, "");
        termThread.echo(0, "Restoring Persona v2.3");
        termThread.run(20, function() {
            animate(hackerAvatar, "opacity", 1, 2000);
        });
        termThread.echo(0, "Done");
        termThread.run(0, function() {
            moveStep(currentNode, 20, 20, 1000, hackerAvatar);
            termThread.restorePrompt(0);
        });
    }


    function createFromLoadEntering(site) {
        createFromLoadGeneric(site);
        var hackerStartNode = nodesById[hackerStartNodeId];
        displayHackerAndLine(hackerStartNode.left, 810, 1);

        var hackerThread = new Thread(term);

        let debug = false;

        if (!debug) {

            hackerThread.echo(10, "🜁 Verdant OS. 🜃", "[[b;;]🜁 Verdant OS. 🜃]");
            hackerThread.echo(10, "");

            hackerThread.run(0, function() {

                hackerAvatar.animate('top', hackerStartNode.origTop, {
                    onChange: canvas.renderAll.bind(canvas),
                    duration: 10000,
                    easing: fabric.util.ease.easeInSine
                });
            });

            var personaId = "" + random(10)+random(10)+random(10)+random(10)+random(10)+random(10)+ '-' +
                random(10)+random(10)+random(10)+random(10)+ '/'+random(10);

            hackerThread.echo(10, "");
            hackerThread.echo(10, "Persona v2.3 booting");
            hackerThread.echo(5, "- unique ID: " + personaId, "- unique ID: [[;#31708f;]" + personaId + "]");
            hackerThread.run(0, function(){
                hackerAvatar.animate('opacity', "0.3", {
                    onChange: canvas.renderAll.bind(canvas),
                    duration: 5000,
                    easing: fabric.util.ease.easeOutSine
                });
            });
            hackerThread.echo(5, "- Matching fingerprint with OS deamon");
            hackerThread.echo(5, "  - Suppressing persona signature..................................................", "  - [[;#3c763d;]ok] Suppressing persona signature");
            hackerThread.echo(5, "  - Connection bandwidth adjusted", "  - [[;#3c763d;]ok] Connection bandwidth adjusted");
            hackerThread.echo(5, "  - Content masked", "  - [[;#3c763d;]ok] Content masked [[;#3c763d;]97%]");
            hackerThread.echo(15, "  - Operating speed reduced to mimic OS deamon", "  - [[;#3c763d;]ok] Operating speed reduced to mimic OS deamon");
            hackerThread.echo(15, "  - Network origin obfuscated", "  - [[;#3c763d;]ok] Network origin obfuscated ");
            hackerThread.run(0, function() {
                animate(hackerAvatar, "opacity", 1, 5000);
            });
            hackerThread.echo(10, "- Persona creation complete", "- Persona creation [[;#3c763d;]complete]");
            hackerThread.echo(0, "");
            hackerThread.echo(40, "Entering node");

        }
        hackerThread.run(0, function() {
            $.get("/run/startClock/" + runId, function(response) {
                traceEnd = response;
                clockStarted()
            });
        });
        hackerThread.echo(0, "Persona accepted by node OS");
        hackerThread.run(0, function() {
            currentNode = hackerStartNode;
            moveStep(currentNode, 20, 20, 600, hackerAvatar);

            if (currentNode.status == "DISCOVERED" || currentNode.status == "PROBED") {
                $.get("/run/nodeStatus/" + runId + "/" + currentNode.id, function(response) {
                    personaArrivesAtNode(currentNode, response.status);
                    termThread.run(0, function() {
                        hackerThread.restorePrompt(0);
                    });
                });
            }
            else {
                hackerThread.restorePrompt(0);
            }
        });
    }

    var traceEnd = ${run.traceEnd};
    var currentMillisServer = ${currentMillis};
    var currentMillis = Date.now();
    var serverMillisAhead = currentMillisServer - currentMillis;
    if (traceEnd == 0 ) {
        loadSite("/run/load/" + runId, createFromLoadEntering);
    }
    else {
        clockStarted();
        loadSite("/run/load/" + runId, createFromLoadRunning);
    }


</script>
</html>