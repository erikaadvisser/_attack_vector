<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Beacon _Attack Vector iframe</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="/resources/beacon/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/beacon/css/adm.css"/>
    <link rel="stylesheet" type="text/css" href="/resources/beacon/css/alert-default.css"/>
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
</head>
<body>
<div class="container adm-container" style="padding: 0 0 0 0; border: none">
    <div class="block">
        <div id="main" class="col-xs-12" style="padding: 0 0 0 0;">
            <form method="post">
                <div id="admPORTAL" class="col-xs-12" style="padding: 0 0 0 0;">
                    <h2><i class="fa fa-terminal"></i>&nbsp;Hacking missions</h2>
                    <hr/>
                    <div class="col-xs-12 col-md-6">
                        <button class="btn btn-ui btn-ui-holo-alt" onclick="window.location.href='/gm/missions/'; return false;"><i class="fa fa-play"></i>&nbsp;Show missions
                        </button>
                    </div>
                    <br>
                    <hr/>

                    <%--<div class="col-xs-12">--%>
                    <%--<button class="btn btn-outline-success btn-lg btn-block" onclick="sendBroadCast(bcreset);">--%>
                    <%--<i class="fa fa-check"></i>&nbsp;CLEAR BROADCASTS--%>
                    <%--</button>--%>
                    <%--<br/><br/>--%>
                    <%--</div>--%>


                    <div class="col-xs-12">
                        <p>Site id:</p>
                        <div class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <%--<div class="input-group-addon btn-ui btn-ui-holo"><i class="fa fa-magic"></i></div>--%>
                                    <input type="text" class="form-control" placeholder="ex: hr.iccn.prakasha" name="siteId" id="siteId"
                                           maxlength="80" value="${parameters.siteId}" />
                                </div>
                            </div>
                            <%--<button class="btn btn-ui btn-ui-holo input-group-addon" style="width: auto;"><i class="fa fa-magic"></i></button>--%>
                        </div>


                    </div>

                    <div class="col-xs-12 col-md-6">
                        <p>Difficulty
                            <select class="form-control" name="difficulty" id="difficulty">
                                <option value="TEMPLATE_SINGLE">Single puzzle (encrypted folder, locked door)</option>
                                <option value="TEMPLATE_EASY">Easy (online forum, small corporate)</option>
                                <option value="TEMPLATE_MEDIUM" selected>Medium (large Corporate, police)</option>
                                <option value="TEMPLATE_HARD">Hard (military, government, icc)</option>
                                <option value="TEMPLATE_UNHACKABLE">Impossible (because we say so)</option>
                            </select>
                        </p>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <p>Info to find:
                            <input type="text" class="form-control" name="info" id="info"
                                   placeholder="ex: tell GM codeword Penguin" value="${parameters.info}" />
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <p>&nbsp;Faction used in puzzles

                            <%--<div class="col-xs-12" style="margin-left: 11px;">--%>
                            <select class="form-control" name="faction" id="faction">
                                <option value="AQUILA">Aquila</option>
                                <option value="DUGO">Dugo</option>
                                <option value="EKANESH">Ekanesh</option>
                                <option value="PENDZAL">Pendzal</option>
                                <option value="SONA">Sona</option>
                                <option value="OTHER" selected>Random</option>
                            </select>
                            <%--</div>--%>
                        </p>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <p>Additional reward
                            <select class="form-control" name="reward" id="reward">
                                <option value="0">0 Sonure</option>
                                <option value="50">50 Sonure</option>
                                <option value="100">100 Sonure</option>
                                <option value="150">150 Sonure</option>
                                <option value="200">200 Sonure</option>
                                <option value="250">250 Sonure</option>
                            </select>
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <p>Mission time
                            <select class="form-control" name="time" id="time">
                                <option value="30">30 minutes</option>
                                <option value="60" >60 minutes</option>
                                <option value="90">90 minutes</option>
                                <option value="120">2 hours</option>
                                <option value="180">3 hours</option>
                                <option value="240">4 hours</option>
                                <option value="480">8 hours</option>
                                <option value="1440">24 hours</option>
                                <option value="59999999" >Infinite</option>
                                <option value="5">5 minutes</option>
                            </select>
                        </p>
                    </div>


                    <div class="col-xs-12 col-md-6">
                        <button class="btn btn-ui btn-ui-holo-alt"><i class="fa fa-play"></i>&nbsp;Create & start
                            mission
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script>
    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top left', className: 'error'});
    }
    $("#difficulty").val("${parameters.difficulty}");
    $("#reward").val("${parameters.reward}");
    $("#time").val("${parameters.time}");
    $("#faction").val("${parameters.faction}");


</script>
</html>