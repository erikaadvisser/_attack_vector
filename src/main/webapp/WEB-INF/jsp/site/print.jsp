<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setDateHeader("Expires", -1);
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>S: ${site.name}</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet"/>
    <%--<link rel="stylesheet" type="text/css" media="print" href="bootstrap.min.css">--%>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/jslib/fabric.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
    <script src="/resources/js/core/thread.js"></script>

    <script>
        var print_solution = ${printSolution};
        var print_puzzles = ${gm};
        var analyzed = [];
        <c:forEach var="serviceId" items="${analyzed}">
        analyzed.push('${serviceId}');</c:forEach>
    </script>
    <%@ include file="../fragments/keepAlive.jsp" %>

</head>
<body style="padding-top: 10px;">



<div class="container">
    <div class="row">
        <div class="col-lg-5">
            ${site.name}
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5" id="canvas-col">
            <div id="canvas-container">
                <canvas id="canvas" width="607" height="815" style="
                border-radius: 3px 3px 3px 3px;
                border-width: 1px;
                border-color: #999999;
                border-style: dashed;"></canvas>
            </div>
        </div>
    </div>
    <div style="display: none">
    <c:set var="nodeTypes" value="<%=org.n_is_1._attack_vector.model.site.NodeType.values()%>"/>
    <c:forEach items="${nodeTypes}" var="type">
        <span><img src="/resources/images/icons/black-white-pearls/template.png"               height="80" width="80" name="${type.name()}_UNDISCOVERED"></span>
        <span><img src="/resources/images/icons/black-white-pearls/template.png"               height="80" width="80" name="${type.name()}_DISCOVERED"></span>
        <span><img src="/resources/images/icons/black-white-pearls/${type.imageName}"          height="80" width="80" name="${type.name()}_PROBED"></span>
        <span><img src="/resources/images/icons/blue-white-pearls/${type.imageName}"           height="80" width="80" name="${type.name()}_FREE"></span>
        <span><img src="/resources/images/icons/red-white-pearls/${type.imageName}"            height="80" width="80" name="${type.name()}_PROTECTED"></span>
    </c:forEach>

        <div style="display: none">
            <span><img src="/resources/images/ice/netwalk/print/1.png" height="40" width="40" id="1"    ></span>
            <span><img src="/resources/images/ice/netwalk/print/2.png" height="40" width="40" id="2"    ></span>
            <span><img src="/resources/images/ice/netwalk/print/2s.png" height="40" width="40" id="2s"  ></span>
            <span><img src="/resources/images/ice/netwalk/print/3.png" height="40" width="40" id="3"    ></span>
            <span><img src="/resources/images/ice/netwalk/print/4.png" height="40" width="40" id="4"    ></span>
            <span><img src="/resources/images/ice/netwalk/print/1.png" height="40" width="40" id="light_1" ></span>
            <span><img src="/resources/images/ice/netwalk/print/2.png" height="40" width="40" id="light_2" ></span>
            <span><img src="/resources/images/ice/netwalk/print/2s.png" height="40" width="40" id="light_2s" ></span>
            <span><img src="/resources/images/ice/netwalk/print/3.png" height="40" width="40" id="light_3" ></span>
            <span><img src="/resources/images/ice/netwalk/print/4.png" height="40" width="40" id="light_4" ></span>
            <span><img src="/resources/images/ice/netwalk/print/c.png" height="40" width="40" id="center" ></span>
        </div>

    </div>
    <br>
    <br>
<c:forEach items="${site.nodes}" var="node" varStatus="nodeStatus">
    <!-- ${node.id}-->

    <c:if test="${scan.nodeStatusById.get(node.id).toString() == 'DISCOVERED'}">
    <div class="row">
        <div class="col-lg-7">
            <table class="table" style="margin-bottom: 0px;  margin-top: 20px; border-top: none;">
                <tr>
                    <td width="50" class="text-right"><span class="text-muted">Node</span></td>
                    <td width="300" class="">▣ <strong>${node.services[0].data['networkId']}</strong> - ${node.nodeType.text}</td>
                    <td width="300" class="">( unknown )</td>
                </tr>
            </table>
        </div>
    </div>
    </c:if>

    <c:if test="${scan.nodeStatusById.get(node.id).toString() == 'PROBED'}">
    <div class="row">
        <div class="col-lg-7">
            <table class="table" style="margin-bottom: 0px;  margin-top: 20px; border-top: none;">
                <tr>
                    <td width="50" class="text-right"><span class="text-muted">Node</span></td>
                    <td width="300" class="">▣ <strong>${node.services[0].data['networkId']}</strong> - ${node.nodeType.text}</td>
                    <td width="300" class="">${node.services[0].data['name']}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7">
            <table class="table" style="margin-bottom: 0px; border-top: none;">
                <tr>
                    <td width="50" class="tr_print text-right"><span class="text-muted">Services</span></td>
                    <td width="400" class="tr_print">${node.services.size()} detected</td>
                </tr>
            </table>
        </div>
    </div>
    </c:if>


    <c:if test="${scan.nodeStatusById.get(node.id).level >= 3}">
        <%--&lt;%&ndash;<hr style="border-top: 1px solid #bbb;">&ndash;%&gt;--%>
        <div class="row">
            <div class="col-lg-7">
                <table class="table" style="margin-bottom: 0px;  margin-top: 20px; border-top: none;">
                    <tr>
                        <td width="50" class="text-right"><span class="text-muted">Node</span></td>
                        <td width="300" class="">▣ <strong>${node.services[0].data['networkId']}</strong> - ${node.nodeType.text}</td>
                        <td width="300" class="">${node.services[0].data['name']}</td>
                    </tr>
                </table>
            </div>
        </div>
        <c:forEach items="${node.services}" var="service" varStatus="status">
            <c:set var="serviceId" value="${(gm)? service.id: status.index}"/>
            <div class="row">
                <div class="col-lg-7">
                    <table class="table" style="margin-bottom: 0px; border-top: none;">
                        <tr>
                            <td width="50" class="tr_print text-right"><span class="text-muted">Service</span></td>
                            <td width="400" class="tr_print"><strong>${serviceId}</strong> - ${service.scanName()}</td>
                        </tr>
                        <c:if test="${service.hasScanInfo()}">
                            <tr>
                                <td width="50" class="tr_print">&nbsp;</td>
                                <td width="400" class="tr_print">Scanned: ${service.scanInfo()}</td>
                            </tr>
                        </c:if>
                        <c:if test="${scan.analyzedServiceIds.contains(service.id) && !service.type.isIce()}">
                            <tr>
                                <td width="50" class="tr_print">&nbsp;</td>
                                <td width="400" class="tr_print">Snooped: ${service.snoopInfo(site)}</td>
                            </tr>
                        </c:if>
                        <c:if test="${gm}">
                        <table class="table" style="margin-top: 0px; margin-bottom: 0px; border-top: none;">
                            <c:forEach items="${service.type.gmPrintData}" var="key">
                                <tr>
                                    <td width="128" class="tr_print text-right"><span class="text-muted">${key}</span></td>
                                    <td width="400" class="tr_print">${service.data[key]}</td>
                                </tr>
                            </c:forEach>
                            <c:if test="${service.type.isIce() && printSolution}">
                                <tr>
                                    <td width="128" class="tr_print text-right"><span class="text-muted">passcode</span></td>
                                    <td width="400" class="tr_print">${generation.iceGenerationsById[service.id].passcode}</td>
                                </tr>
                            </c:if>
                        </table>
                        </c:if>
                    </table>
                </div>
            </div>
        <c:if test="${service.type.isIce()}">
            <div class="row">
                <div class="col-lg-7">
                    <div id="service_${service.id}"></div>
                </div>
            </div>
            <%--<c:if test="${gm}">--%>
                <script>
                    $.get( "/print/ice/${generation.id}/${service.id}", function(data) {
                        $("#service_${service.id}").html(data);
                    });
                </script>
            <%--</c:if>--%>
        </c:if>

        </c:forEach>
    </c:if>


</c:forEach>



</body>
<script>
    var edit_init = false;
    var siteId = "${site.id}";
    var nodesById = {};
    var networkNodesById = {};
    var x;
    var statusMap = {};
    var printNodeLabelBackground = false;
    var nodeOpacityByStatus = {
        UNDISCOVERED: 0,
        DISCOVERED : 1,
        PROBED: 1,
        FREE: 1,
        PROTECTED: 1,
        HACKED: 1,
        AVATAR: 0
    };
    var termThread = new Thread(null);

<c:forEach items="${site.nodes}" var="node">
    statusMap[${node.id}] = "${scan.nodeStatusById.get(node.id)}";</c:forEach>
    let DEBUG = false;
</script>
</body>
<script src="/resources/js/core/node_definitions.js"></script>
<script src="../../../resources/js/attack/hackingrun.js"></script>
<script>
    function createFromLoadPrint(site) {
        site.nodes.forEach(function(node) {
            node.status = statusMap[node.id];
            node.statusForClient = statusMap[node.id];

        });
        createFromLoadGeneric(site);
    }

    $(function() {
        var url = "/resources/images/backgrounds/purple_circle_sphere_rings_crop.jpg";
        setTimeout(function() {
            fabric.Image.fromURL(url, function(img) {
                img.set({width: canvas.width, height: canvas.height, originX: 'left', originY: 'top'});
                canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
            })
        }, 100);

        loadSite("/print/load/" + siteId + "/", createFromLoadPrint);

        canvas.selection = false;
        canvas.forEachObject(function (o) {
            o.selectable = false;
        });

    });
</script>

</html>