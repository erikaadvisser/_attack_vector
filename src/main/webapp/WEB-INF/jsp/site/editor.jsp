<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_av Site editor</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/jslib/fabric.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
    <%@ include file="../fragments/keepAlive.jsp" %>

</head>
<body style="padding-top: 10px">

<div class="container">

    <div class="row">
        <div class="col-lg-offset-1 col-lg-10 dark_well">
            <br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row form-horizontal" >
                        <div class="form-group">
                            <label for="site_id" class="col-lg-3 control-label text-muted">Site id</label>
                            <div class="col-lg-8">
                                <input id="site_id" type="text" class="form-control" readonly value="${siteId}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="site_name" class="col-lg-3 control-label text-muted">Name</label>
                            <div class="col-lg-8">
                                <input id="site_name" type="text" class="form-control" placeholder="Display name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="site_description" class="col-lg-3 control-label text-muted">Description</label>
                            <div class="col-lg-8">
                                <textarea id="site_description" rows="1" class="form-control" placeholder="For SL only"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="site_name" class="col-lg-3 control-label text-muted">Site type</label>
                            <div class="col-lg-4">
                                <select id="site_type" class="form-control">
                                    <option value="REGULAR">Site exists outside of missions</option>
                                    <option value="TEMPLATE_TRACER" selected>(Template) Tracer</option>
                                    <option value="TEMPLATE_SINGLE">(Template) Single</option>
                                    <option value="TEMPLATE_EASY">(Template) Easy</option>
                                    <option value="TEMPLATE_MEDIUM">(Template) Medium</option>
                                    <option value="TEMPLATE_HARD">(Template) Hard</option>
                                    <option value="TEMPLATE_UNHACKABLE">(Template) Unhackable</option>
                                    <option value="MISSION">Site part of a mission</option>
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row form-horizontal" >
                        <div class="form-group">
                            <label for="gm_name" class="col-lg-3 control-label text-muted">SL Name</label>
                            <div class="col-lg-2">
                                <input id="gm_name" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hack_time" class="col-lg-3 control-label text-muted">Detect time</label>
                            <div class="col-lg-2">
                                <input id="hack_time" type="text" class="form-control" placeholder="Time for hack (mm:ss)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="start_node" class="col-lg-3 control-label text-muted">Start node</label>
                            <div class="col-lg-2">
                                <input id="start_node" type="text" class="form-control" placeholder="Node Id">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="site_hackable" class="col-lg-3 control-label text-muted">Site hackable</label>
                            <div class="col-lg-1 text-left">
                                <input type="checkbox" id="site_hackable" class="form-control"/>
                            </div>
                                <label for="templateUses" class="col-lg-3 control-label text-muted">Template use count</label>
                                <div class="col-lg-3">
                                    <input type="text" id="templateUses" class="form-control" placeholder="(templates only)"/>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>


    <div class="row">
        <div class="col-lg-3 text-muted">
            Nodes, drag to editor to add
        </div>
        <div class="col-lg-5 text-muted">
            &nbsp;&nbsp;&nbsp;&nbsp;Site editor
        </div>
        <div class="col-lg-4 text-muted">
            Actions
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 dark_well" id="node-library">
            <br>
            <p class="text-muted">Content nodes</p>

            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/cube.png" height="80" width="80" name="SYSCON" title="System Control"></span>
            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/tile4-sc36.png" height="80" width="80" name="INFO" Title="information"></span>
            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/puzzle3-ps.png" height="80" width="80" name="PASSWORD" Title="password"></span>
            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/tile2.png" height="80" width="80" name="RESOURCE" Title="Resource"></span>
            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shape-square-frame.png" height="80" width="80" name="NODE_SQUARE" Title="Node square"></span>
            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shapes-circle-frame.png" height="80" width="80" name="NODE_CIRCLE" Title="Node circle"></span>
            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shapes-diamond-frame.png" height="80" width="80" name="NODE_DIAMOND" Title="Node diamond"></span>
            <span><img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shapes-hexagon-frame.png" height="80" width="80" name="NODE_HEXAGON" Title="Node hexagon"></span>
            <!--<span><hr></span>-->
            <p class="text-muted">Automated mini game ice nodes</p>
            <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/symbols-shapes/spinner2-sc30.png" height="80" width="80" name="FW_WORD_SEARCH" Title="Ice Word search"></span>
            <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/symbols-shapes/spinner3-sc36.png" height="80" width="80" name="FW_MAGIC_EYE" Title="Magic-eye"></span>
            <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/symbols-shapes/spinner8-sc36.png" height="80" width="80" name="FW_PASSWORD"  Title="Netwalk"></span>
            <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/snowflake4-sc37.png" height="80" width="80" name="FW_UNHACKABLE"  Title="Unhackable"></span>
            <!--<span><hr></span>-->
            <p class="text-muted">Manual mini game ice nodes</p>
            <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/flower3.png" height="80" width="80" name="FW_MANUAL_1" Title="Manual Puzzle 1" ></span>
            <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/flower13-sc36.png" height="80" width="80" name="FW_MANUAL_2"  Title="Manual Puzzle 2"></span>
            <span><img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/flower-cauliflower.png" height="80" width="80" name="FW_MANUAL_3" Title="Manual Puzzle 3"></span>

            <br>
            <br>

        </div>
        <div class="col-lg-5" id="canvas-col">
            <div id="canvas-container" ondragover="allowDrop(event)" ondrop="drop_image_and_create_node(event)">
                <canvas id="canvas" width="607" height="815" style="border-radius: 3px 3px 3px 3px"></canvas>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-12 dark_well">
                    <br>
                    <div>
                        <div id="btn_save" class="btn btn-info">Save</div>
                        <div id="btn_load" class="btn btn-info">Load</div>
                        <div id="btn_delete_lines" class="btn btn-info">Delete Lines</div>
                        <div id="btn_delete" class="btn btn-info btn-">Delete</div>
                        <div id="btn_snap" class="btn btn-info">Snap</div>
                        <div id="btn_connect" class="btn btn-info disabled btn_tooltip" data-placement="bottom" title="Control-click new node to connect with current selected node.">Connect</div>
                    </div>
                    <br>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12 text-muted">
                    Add service to node
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 dark_well">
                    <br>
                    <div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_text" data-placement="bottom" title="Text data">Text</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_file" data-placement="bottom" title="File data">File</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_code" data-placement="bottom" title="Ice passcode">Code</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_time" data-placement="bottom" title="extra hack time">Time</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_resources" data-placement="bottom" title="Resources worth money">-ṩ-</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_scan_blocker" data-placement="bottom" title="scan blocker">s̶c̶a̶n̶</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_remote_tracer" data-placement="bottom" title="Remote tracer">Tracer</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_trace_logs" data-placement="bottom" title="Trace logs">t-logs</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_syscon" data-placement="bottom" title="System control">Syscon</div>

                    </div>
                    <div style="height: 8px;">&nbsp;</div>
                    <div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_fw_ws" data-placement="bottom" title="Ice: word search">[ws]</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_fw_me" data-placement="bottom" title="Ice: magic eye">[me]</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_fw_nw" data-placement="bottom" title="Ice: netwalk">[nw]</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_fw_xx" data-placement="bottom" title="Ice: unhackable">[xx]</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_fw_gm" data-placement="bottom" title="Ice: GM ruled">[gm]</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_fw_pw" data-placement="bottom" title="Ice: password">[pw]</div>
                        <div class="btn btn-sm btn-info btn_tooltip" id="btn_add_layer_chat" data-placement="bottom" title="Chat">Chat</div>
                    </div>
                    <br>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12 text-muted">
                    Node details
                </div>
            </div>
            <%--<div class="row form-horizontal dark_well" >--%>
                <%--<div class="form-group">--%>
                    <%--<div class="col-lg-offset-3 col-sm-8">--%>
                        <%--<button type="submit" id="node_apply" class="btn btn-info" disabled>Apply</button>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <div class="row form-horizontal dark_well" >
                <div class="form-group">
                    <label for="node_type" class="col-lg-1 control-label text-muted">&nbsp;</label>
                    <div class="col-lg-9">
                        <div id="node_type" class="text-muted" style="margin-top: 7px;margin-bottom: 0;">
                            <strong><span id="node_type_name" style="color: #8a6d3b;"></span></strong>
                            <span id="node_type_description" >(select node)</span>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="btn btn-default btn-xs tooltip_help tooltip_help_inactive" id="btn_toggle_tooltips" onclick="showTooltips(true); return false;">tooltips</div>
                    </div>
                </div>
                <div class="col-lg-12" style="height: 509px;">
                    <ul class="nav nav-tabs" role="tablist" id="node-services-tab-list">
                        <li role="presentation" class="active"><a href="#svc_data_tab_0" aria-controls="home" role="tab" data-toggle="tab">OS</a></li>
                    </ul>
                    <br>
                    <div class="tab-content" id="node-services-tab-content">
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<script>
    var edit_init = true;
    var siteId = "${siteId}";
    $(function(){
        if (${loadOnStartup}) {
            load();
        }
    });
</script>
<script src="/resources/js/core/node_definitions.js"></script>
<script src="/resources/js/editor/editor.js"></script>
</html>