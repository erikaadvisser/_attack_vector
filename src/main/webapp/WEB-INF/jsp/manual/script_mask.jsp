<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Mask</h3>
<hr class="dark_red_hr">
This script is used during a hacking run. It masks the presence of the hacker. It then takes longer for
the site to detect the hacker.<br>
<br>
A single mask script give the hacker approximately one minute of extra hacking time. Multiple mask scripts can be used during the same run.<br>
<br>
No additional parameters have to be supplied when running this script<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run mask 3eba-4b78-be5d</div>
    </div>
</div>

<%@ include file="_footer.jsp" %>