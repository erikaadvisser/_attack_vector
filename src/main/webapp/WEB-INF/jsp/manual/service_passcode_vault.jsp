<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: Passcode vault</h3>
                <hr class="dark_red_hr">
                A Passcode vault stores one passcode that allow access through one specific Ice layer.<br>
                <br>
                <br>
                <span class="text-strong">Hacking</span><br>
                <br>
                When you hack a Passcode vault, you get the passcode. This passcode looks something like this:<br>
                <br>
                truth abstraction analyst navigable
                <br>
                <br>
                A passcode can be used when you are at the correct Ice layer to disable it without hacking it.
                The <a href="command_passcode">passcode</a> command is used for this. A correct passcode will always
                disable the Ice layer, regardless of how strong it is, and it is one of the few ways to bypass a
                <a href="service_ice_jagannatha.html">Jagannatha</a> Ice layer.
                <br>
                <br>
                A hacker can use a <a href="script_snoop.html">Snoop script </a> to determine the Ice layer that this vault contains the passcode for.
<%@ include file="_footer.jsp" %>