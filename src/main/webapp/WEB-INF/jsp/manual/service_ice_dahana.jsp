<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Ice Service: Dahana</h3>
<hr class="dark_red_hr">
Dahana is Ice that protects service layers below it, blocking the hacker.
<br>
<br>
<span class="text-strong">Attack Vector</span><br>
<br>
Dahana Ice can be hacked by reconfiguring the network. A typical weak level scrambled network configuration looks like this:<br>
<br>
<img src="/resources/images/manual/dahana_scrambled.png" height="290" width="290"/>
<br>
<br>
Click the nodes to rotate them until they form the connected network like this:<br>
<br>
<img src="/resources/images/manual/dahana_solved.png"  height="290" width="290" />
<br>
<br>
<br>
A hacker can use an <a href="script_analyze.html">Analyze script </a> to gain the scambled network configuration in advance. This allows for this Ice to
be hacked 'offline'.<br>
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>