<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Nodes</h3>
                <hr class="dark_red_hr">
                A site is made up of nodes. There are many different types of nodes that each look differently.
                A node type is an indication of the <a href="services.html">service layers</a> that a node contains.<br>
                <br>
                There are <a href="nodes_regular.html">regular</a> and <a href="nodes_ice.html">ice</a> nodes.<br>
                <br>
                In theory all node types can hold all kinds of services. But in practice, in most sites you can quickly tell how
                interesting a node is by looking a the node type.<br>
                <br>
                When a node contains an Ice layer, it will be displayed in grey metallic.<br>
                <br>
                <br>
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shape-square-frame.png" height="80" width="80"/>
                            </td>
                            <td>Node without any Ice layers</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/symbols-shapes/shape-square-frame.png" height="80" width="80"/>
                            </td>
                            <td>Same node with one or more Ice layers</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-white-square/symbols-shapes/shape-square-frame.png" height="80" width="80"/>
                            </td>
                            <td>If a node with Ice layers has been neutralized (all Ice layers hacked) then this is what it looks like.</td>
                        </tr>
                    </tbody>
                </table>
<%@ include file="_footer.jsp" %>