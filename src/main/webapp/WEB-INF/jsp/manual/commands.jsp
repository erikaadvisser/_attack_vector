<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Hacking commands</h3>
<hr class="dark_red_hr">
You interact with the site by typing commands. You can always get a list of all possible commands by typing <a href="command_help">help</a>.<br>
<br>
<span class="text-strong">List of commands</span><br>
<br>
&nbsp; &nbsp;&nbsp; <a href="command_move">move</a> to another node<br>
&nbsp; &nbsp;&nbsp; <a href="command_view">view</a> the service layers of this node<br>
&nbsp; &nbsp;&nbsp; <a href="command_hack">hack</a> a service layer (both Ice and non-Ice)<br>
&nbsp; &nbsp;&nbsp;&nbsp; <a href="command_run">run</a> a script<br>
&nbsp;<a href="command_passcode">passcode</a> - provide a passcode to an Ice layer<br>
&nbsp; <a href="command_message">message</a> - send a message on a chat client<br>
&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <a href="command_dc">dc</a> - disconnect<br>
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>