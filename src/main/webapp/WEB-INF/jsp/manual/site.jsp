<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Site map</h3>
                <hr class="dark_red_hr">
                The computer systems that you can hack are called sites.<br>
                <br>
                When you are hacking a site, you try to remain undetected. But all sites are always scanning for intruders, so you will have a limited time
                to complete your hack, before you are detected. How much time you have, depends on how good the site is protected.<br>
                <br>
                A site consists of one or more nodes, and your goal as a hacker is to progress through the nodes to those nodes that hold valuables.<br>
                A site usually has defenses in the form of Ice layers that will try to block your progress.<br>
                <br>
                A site is represented by the site map that shows its nodes, for example:<br>
                <br>
                <img src="/resources/images/manual/site.png" /><br>
                <br>
                In this example there are four different nodes, see <a href="nodes.html">Nodes</a> for an explanation of all nodes.<br>
                <br>
                When hacking a site you always start at the bottom and try to make your way into the site. Grey metallic nodes contain Ice and block you.<br>
                <br>
                Moving about the site, you use the node's network address to navigate. These are the black numbers below the nodes. So you could move from
                '00' to '01'. Note that the network addresses are not numbers, so you are not moving from '0' to '1'.
<%@ include file="_footer.jsp" %>