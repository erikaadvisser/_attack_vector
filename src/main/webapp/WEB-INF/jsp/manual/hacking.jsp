<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Hacking</h3>
<hr class="dark_red_hr">
There is a <a href="https://www.youtube.com/watch?v=709DZcS1WTw" target="_blank">Youtube video</a> explaining how hacking works (Dutch).<br>
<br>
You start the hack from the scanning screen by typing 'attack'. You are take to the hacking screen and your hack begins.<br>
<br>
When the hack starts, you have a limited time to complete the hack. The site is looking for intruders and will eventually find you. How long it
takes, depends on the security of the site.<br>
<br>
<img src="/resources/images/manual/detection-timer.png"/><br>
<br>
When the timer reaches zero, the site detects you. You want to prevent this, as the site can try to trace you once they detect you.
You can stop a hack by navigating away from the hack page using the menu on the bottom of the page, or just by closing your browser window.<br>
<br>
The hacking map<br>
<br>
<img src="/resources/images/manual/hacking.png"><br>
<br>
You are represented on the map as a yellow icon. You start at the bottom and your goal is to get to <a href="nodes">nodes</a> with things of interest. In this example
those are nodes 02 and 03 at the top. Between you and your targets is a metallic node that contains an <a href="nodes_ice">Ice layer</a> that will block your progress.
Hacking is about finding a way through or around those Ice nodes to get to the nodes that contain things of value.
<br>
<br>
You interact with the site by typing <a href="commands">commands</a>.
<%@ include file="_footer.jsp" %>