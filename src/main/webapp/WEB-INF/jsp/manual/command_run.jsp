<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - run</h3>
<hr class="dark_red_hr">
This command executes a <a href="scripts">hacking script</a> for the hacker. Hacking scripts become available to hackers with a high enough
skill level. Some hacking scripts are used during scanning, others are used during hacking.<br>
<br>
For convenience, hacking scripts are shown the left hand side of the screen during scanning and hacking. Here you can see three
scripts, one of which has already been used. (<del class="text-dark">run analyze e77f-4aeb-beee</del>)<br>
<br>
<span class="text-strong">Example</span><br>
<div style="position: absolute; left: -200px; z-index: 1000" class="text-dark">
    <br>
    run mask 3eba-4b78-be5d<br>
    <del>run analyze e77f-4aeb-beee</del><br>
    run snoop 02b0-4f36-b315<br>
</div>

<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run mask 3eba-4b78-be5d</div>
    </div>
    <div>
        ⊿00 ↠ Script reports success. Detection delayed by: 1:02
    </div>
</div>
<br>
<br>
Depending on the script, you need to supply additional information after the script-id. For scanning scripts,
you need to indicate which node and which service you are targeting. For scripts used in a hacking run,
you sometimes need to indicate what service layer you are targetting.<br>
<br>
Just try the script, and the error message will tell you what to do.

<%@ include file="_footer.jsp" %>