<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Scan report</h3>
<hr class="dark_red_hr">
There is a <a href="https://www.youtube.com/watch?v=AtjZ8e0kyiY" target="_blank">Youtube video</a> explaining how scanning works (Dutch).<br>
<br>
The scan report gives you all information gained during scanning, and includes extra information,
such as Ice strength and any information gained using scan scripts.<br>
<br>
<img src="/resources/images/manual/scan-report.png" width="512" class="round_border_15"><br>
<br>
Scan reports are meant to be printed and used to plan an attack on a site.
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>