<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: (other)</h3>
                <hr class="dark_red_hr">
                There are many services for many functions, ranging from a service that manages the home entertainment
                system to a service that controls the mixing of chemicals in a production plant.<br>
                <br>
                <br>
                <span class="text-strong">Hacking</span><br>
                <br>
                The results of hacking this service depend on what the service is. You might learn something, or you might gain control
                over what the service manges. In the above example: hacking a home entertainment system might allow the hacker
                to choose what media is played, or possibly delete the locally stored media collection. Hacking a controller that
                mixes chemicals could allow the hacker to change the mixture of chemicals, leading to a whole new realm of possibilities.<br>
                <br>
                <br>
                <span class="text-strong">Note:</span> The respose to hacking this kind of service is usually descriptive rather than literal. For instance the response
                to hacking a financial ledger might be:
                "There is no evidence of corruption by Mr Long". This should be treated as an OC notice from the GM.<br>
                The reason for this is that the text is entered by the GMs possibly during the event itself
                and it might be much more efficient to just describe what you find, rather than creating the actual set of documents your hacker is downloading.
<%@ include file="_footer.jsp" %>