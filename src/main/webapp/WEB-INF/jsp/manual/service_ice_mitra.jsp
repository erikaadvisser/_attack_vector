<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Ice Service: Mitra</h3>
<hr class="dark_red_hr">
Mitra is Ice that protects service layers below it, blocking the hacker.
<br>
<br>
<span class="text-strong">Attack Vector</span><br>
<br>
Mita Ice is a special kind of Ice that allows the GMs to insert a custom puzzle into a hacking game, without the software
supporting this game yet. If you encounter Mitra Ice, then the GM needs to provide the puzzle to you.<br><br>
For instance, this puzzle could be a Sudoku puzzle, the GM will provide you with the piece of paper containing the Sudoku.<br>
<br>
The puzzle will have a clear solution which is a word, phrase or number. when you solve the puzzle, you enter the solution in
the hacking interface, and this will hack the ice.<br>
<br>
The effect of the <a href="script_analyze.html">Analyze script </a> depends on the puzzle, it is up to the GM to provide you
with the puzzle or the information or hints that you gain.
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>