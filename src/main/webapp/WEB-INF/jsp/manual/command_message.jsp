<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - message</h3>
<hr class="dark_red_hr">
This command sends a message to a message client. The text after 'message' will be sent.<br>
To view messages, use the 'hack' command to hack the chat service.
<br>
This command only works if there is a chat client service in the current node, that is not protected by ICE.
<br><br>
Note: you don't need to enter the service-layer for this command, see example.
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ message Hello there!</div>
    </div>
    <div>
        ⊿00 ↠ Message posted
    </div>
</div>

<%@ include file="_footer.jsp" %>