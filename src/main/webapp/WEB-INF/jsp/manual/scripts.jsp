<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Scripts</h3>
<hr class="dark_red_hr">
When you have a high enough skill level, you will get access to hacking scripts. These scripts are run-once programs that
help you to hack a site. They either help in scanning a site or during the actual hacking run.<br>
<br>
Around 06:00 PCT all your scripts become obsolete. At this time the security systems patch the holes that the scripts exploit.
However, using your network of fellow hackers you can acquire new scripts to for this day. In the scripts tab at the bottom of
the screen, you can manage your scripts.<br>
<br>
You can also share your scripts to other hackers, just tell them the code of the script. This can be useful to hack through
well protected sites.<br>
<br>
<br>
<span class="text-strong">List of scan scripts</span><br>
<br>
&nbsp; <a href="script_snoop">snoop</a> Gain information about: Local core, central core or passcode vault<br>
<a href="script_analyze">analyze</a> Gain information about an Ice layer<br>
<br>
<br>
<span class="text-strong">List of hacking run scripts</span><br>
<br>
<a href="script_mask">mask</a> Delay detection<br>
<a href="script_elevate">elevate</a> Disable a single Ice layer in a node containing other Ice layers<br>
<a href="script_bypass">bypass</a> Disable all Ice layers in a node<br>
<a href="script_deflect">deflect</a> Protect against traces <br>
<a href="script_storm">storm</a> Mask detection <br>
<a href="script_backdoor">backdoor</a> Move to a node that contains the central core<br>

<br>
&nbsp;
<%@ include file="_footer.jsp" %>