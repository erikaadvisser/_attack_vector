<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Bypass</h3>
<hr class="dark_red_hr">
This script is used during a hacking run. It disables all Ice layers in the current node.<br>
<br>
No additional parameters have to be supplied when running this script<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run bypass be3f-44b3-a633</div>
    </div>
</div>
<br>

<%@ include file="_footer.jsp" %>