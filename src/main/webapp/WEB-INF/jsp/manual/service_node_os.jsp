<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: Node OS</h3>
                <hr class="dark_red_hr">
                The Node OS is present in every node and makes that the node works at all. Part of the Node OS is
                managing the connection to other nodes.<br>
                <br>
                When the Node OS is blocked by Ice, you cannot move through a node to new nodes. You can always move
                back to where you came from.<br>
                <br>
                <span class="text-strong">Hacking</span><br>
                <br>
                When you hack the Node OS, you will scan for any nodes that are connected to this node. If you did not
                make a <a href="scan.html">scan</a> of the site before the attack, this is how you uncover the site map.<br>
                <br>
                If you already have the site map, there is nothing to be gained by hacking the Node OS.

<%@ include file="_footer.jsp" %>