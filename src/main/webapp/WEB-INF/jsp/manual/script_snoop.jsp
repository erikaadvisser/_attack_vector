<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Snoop</h3>
<hr class="dark_red_hr">
This script is used during scanning. It targets a single non-Ice service layer and reveals what can be gained
by hacking that service.<br>
<br>
When you run this script, you need to designate a target using the node network-id and service layer.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run snoop 3eba-4b78-be5d 04 1</div>
    </div>
</div>
This targets Ice layer 1 in node 04.<br>
<br>
<br>
Only these services can be targeted:<br>
<br>
<span class="text-strong"><a href="service_central_core">Central core</a></span><br>
What happens when you hack a central core, depends on how well it is secured. With the snoop script you can
learn this in advance.<br>
<br>
<span class="text-strong"><a href="service_local_core">Local core</a></span><br>
By using snoop on this service, you learn how much extra time you will gain when you hack this service.<br>
<br>

<%@ include file="_footer.jsp" %>