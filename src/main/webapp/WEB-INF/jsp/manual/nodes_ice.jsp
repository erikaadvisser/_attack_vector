<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Ice Nodes</h3>
                <hr class="dark_red_hr">
                These nodes usually hold ice layers.
                <br><br>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <td>Node</td>
                            <td>Name</td>
                            <td>Expected Services</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/symbols-shapes/spinner8-sc36.png" height="80" width="80"/>
                            </td>
                            <td>Kama</td>
                            <td>
                                <a href="service_ice_kama">Ice: Kama</a> (password guessing)<br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/symbols-shapes/spinner2-sc30.png" height="80" width="80"/>
                            </td>
                            <td>Pumer</td>
                            <td>
                                <a href="service_ice_pumer">Ice: Pumer</a> (word search)<br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/symbols-shapes/spinner3-sc36.png" height="80" width="80"/>
                            </td>
                            <td>Pryder</td>
                            <td>
                                <a href="service_ice_pryder">Ice: Pryder</a> (magic-eye)<br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/snowflake4-sc37.png" height="80" width="80"/>
                            </td>
                            <td>Jagannatha</td>
                            <td>
                                <a href="service_ice_jagannatha">Ice: Jagannatha</a> (-)<br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/flower3.png" height="80" width="80"/>
                            </td>
                            <td>Mitra</td>
                            <td><a href="service_ice_mitra">Ice: Mitra</a> (varies)</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/flower13-sc36.png" height="80" width="80"/>
                            </td>
                            <td>Mitra</td>
                            <td><a href="service_ice_mitra">Ice: Mitra</a> (varies)</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/ultra-glossy-silver-buttons/natural-wonders/flower-cauliflower.png" height="80" width="80"/>
                            </td>
                            <td>Mitra</td>
                            <td><a href="service_ice_mitra">Ice: Mitra</a> (varies)</td>
                        </tr>
                    </tbody>
                </table>

                <br>
                <br>





<%@ include file="_footer.jsp" %>