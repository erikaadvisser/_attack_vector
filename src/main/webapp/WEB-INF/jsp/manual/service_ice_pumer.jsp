<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Ice Service: Pumer</h3>
<hr class="dark_red_hr">
Pumer is Ice that protects service layers below it, blocking the hacker.
There is a <a href="https://www.youtube.com/watch?v=UMbFm0qLJqI" target="_blank">Youtube video</a> explaining how to deal with this ice (Dutch).<br>
<br>
<br>
<span class="text-strong">Attack Vector</span><br>
<br>
Pumer Ice has a weakness that allows hackers to trigger an error and then help the Ice to recover from this
error but at the same time gain access.<br>
<br>
This process is very similar to the game known as word search.<br>
<br>
The hacking interface shows you a grid of letters and asks you where a word is located. Click on the letters
to indicate where the word is located.<br>
<br>
<img src="/resources/images/manual/pumer.png" width="615">

<br>
<br>
<br>
A hacker can use an <a href="script_analyze.html">Analyze script </a> to gain the word search puzzle in advance. This allows for this Ice to
be hacked 'offline'.<br>
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>