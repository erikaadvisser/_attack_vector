<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: Data bank</h3>
                <hr class="dark_red_hr">
                A Data bank stores files that may be worth something on the black market. These are secrets so selling them will cause harm to businesses or people.<br>
                <br>
                <br>
                <span class="text-strong">Hacking</span><br>
                <br>
                When you hack a Data bank, you download the contents and get an initial indication of what the files are and what they may be worth.<br>
                <br>
                <br>
                <span class="text-strong">Note:</span> No actual files are downloaded and there is no support in this mini-game to represent or sell these files (yet).
                You will have to contact the GM about selling the files, and they will tell you how this plays out.<br><br>
                The GM can look up what you downloaded in the game logs, so you don't have to worry about proving what you downloaded. The price that is quoted when you
                download the files is only a rough indication, so the actual value can be higher or lower.
<%@ include file="_footer.jsp" %>