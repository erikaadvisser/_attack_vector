<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: Central core</h3>
                <hr class="dark_red_hr">
                The Central core is the heart of a site. It manages the site security. Traditionally, the node
                that houses the Central core is called Syscon (System Control).<br>
                <br>
                <br>
                <span class="text-strong">Hacking</span><br>
                <br>
                When you hack the Central core, you always reveal the entire site map.<br>
                <br>
                Depending on the level of security, two additional things can be accomplished:<br>
                <br>
                - Disable intrusion detection<br>
                This will stop the detection timer. You will have infinite time to hack the rest of the site.<br>
                <br>
                - Disable all Ice<br>
                This will have the effect that all remaining Ice is instantly hacked, allowing you free movement through the entire site.<br>
                <br>
                <br>
                How vulnerable a Central core is, depends on how much money the site's owners spent on it. A hacker can use a
                <a href="script_snoop.html">Snoop script </a> to determine the vulnerability of a Central core prior to hacking a site.
<%@ include file="_footer.jsp" %>