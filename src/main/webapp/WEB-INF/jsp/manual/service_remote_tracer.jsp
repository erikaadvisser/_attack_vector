<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: Remote tracer</h3>
<hr class="dark_red_hr">
A remote tracer provides details about your connection to a remote site that will perform a trace.<br>
<br>
<br>
<span class="text-strong">Hacking</span><br>
<br>
When you enter a node that contains a remote tracer service, the service will cause the remote site to initiate a trace against you.
The remote site will do the trace, and store the trace data. So the regular solution is to visit the site and delete the <a href="service_trace_logs">trace logs</a>.<br>
<br>
However, an elite can also employ a <a href="script_storm">storm script</a> to effectively remove traces from the remote sites.<br>
<br>
When you scan a remote trace service, you sometimes can already determine the location of the remote trace site.<br>
<br>
When you hack a remote trace service, you will always retrieve the location of the remote trace site.<br>
<br>
<%@ include file="_footer.jsp" %>