<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - view</h3>
<hr class="dark_red_hr">
This command shows the service layers of the current node, and which of those layers are protected by Ice.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ view</div>
    </div>
    <div>
        ⊿00 ↠ Node service layers<br>
        *layer 0 - Node OS 775/2<br>
        layer 1 - Ice:Pumer 883/3
    </div>
</div>
<br>
Here we see two layers. Pumer Ice at layer 1, and the Node OS at layer 0 is protected by the Ice, as indicated by the *.<br>
<br>
See <a href="services">service</a> for details on how service layers work.

<%@ include file="_footer.jsp" %>