<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service layers</h3>
                <hr class="dark_red_hr">
                Each node contains one or more services, they determine what is actually present in a node.<br>
                <br>
                Go here for <a href="service_list.html">a list of all services</a>.<br>
                <br>
                Services are layered, and therefore they are known as service layers. At the bottom (layer 0) there is
                always the Node OS service. If you <a href="command_view.html">view</a> the layers of a node with only a Node OS, you see the following:<br>
                <br>
                <br>
                &nbsp;&nbsp;⊿00 ↠ Node service layers<br>
                &nbsp;&nbsp;layer 0 - Node OS <span class="text-very-dark">775/2</span><br>
                <br>
                <br>
                The Node OS service is the basis for the site and manages connections to other nodes.
                The Node OS allows anyone (including the hacker) to move between the nodes.<br>
                <br>
                On top of the Node OS there can be other service layers, these would be in layers 1, 2, etc. An example of another service is a <a href="service_data_vault.html">data vault</a>. This service contains information
                that might be interesting to the hacker.<br>
                <br>
                <br>
                &nbsp;&nbsp;⊿00 ↠ Node service layers<br>
                &nbsp;&nbsp;layer 0 - Node OS <span class="text-very-dark">775/2</span><br>
                &nbsp;&nbsp;layer 1 - Data vault <span class="text-very-dark">873/1</span><br>
                <br>
                <br>
                Ice service layers (or Ice layers for short) are special, they protect all layers that lie below them. So an Ice service in layer 2 protects layers 1 and 0.
                A hacker cannot interact with any services that protected by Ice. This also means that a hacker cannot move through a node with Ice.<br>
                <br>
                <br>
                &nbsp;&nbsp;⊿00 ↠ Node service layers<br>
                &nbsp;&nbsp;*layer 0 - Node OS <span class="text-very-dark">775/2</span><br>
                &nbsp;&nbsp;*layer 1 - Data vault <span class="text-very-dark">873/1</span><br>
                &nbsp;&nbsp;layer 2 - Ice:Kama <span class="text-very-dark">880/4</span><br>
                <br>
                <br>
                The layers 0 and 1 are shown with a * to mark them as protected.<br>
                <br>
                <br>
                The main way a hacker interacts with a service layer is via the <a href="command_hack.html">hack</a> command. As you can guess, you cannot hack a service layer
                that is protected by Ice. Other commands that interact with service layers are <a href="command_passcode">passcode</a> and <a href="command_run">run</a>.<br>
                <br>
                <br>
                In the examples the service layers have a number shown after them,for example: <span class="text-very-dark"><strong>775/2</strong></span>.<br>
                This number is the version number of the software. The higher the version number, the harder it is to hack.





<%@ include file="_footer.jsp" %>