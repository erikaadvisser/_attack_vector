<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - help</h3>
<hr class="dark_red_hr">
The help command give you a list of all commands.<br>
<br>
Interestingly it does not list the <a href="command_run">run</a> command, as it is not available to all hackers. You need
a minimum 'informatica' skill level to be able to use this command.
<br>
&nbsp;
<%@ include file="_footer.jsp" %>