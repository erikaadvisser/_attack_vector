<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Scanning</h3>
<hr class="dark_red_hr">
There is a <a href="https://www.youtube.com/watch?v=AtjZ8e0kyiY" target="_blank">Youtube video</a> explaining how scanning works (Dutch).<br>
<br>
When you want to hack a site, the first thing you do is scan the site. When you enter the site name, you are taken
to the scanning interface. Here you type "scan" to start scanning.<br>
<br>
Scanning will progress automatically until all nodes are fully scanned. This will give you a site map, as well as an
indication of what each node contains.<br>
<br>
<table>
    <tr>
        <td><img src="/resources/images/manual/scan.PNG"/></td>
        <td>&nbsp;</td>
        <td>The nodes look slightly different during scanning.
            Nodes with Ice are shown in red and other nodes are shown in blue.
            If it is not yet known if there is Ice, then the nodes are shown in black.
    </tr>
</table>

<br>

At any time you can stop scanning by going back to the home page. Here all your scans are shown on the right side of the page.
Aand by clicking on a scan-Id in your list, you can return to it to resume scanning or attacking the site.<br>
<br>
<img src="/resources/images/manual/scan-list.png"><br>
<br>
You can share scans-ids with other hackers. If you tell them the scan-id and they enter it in the text box on the home screen.<br>
<br>
The moon-icons under the 'action' section allows you to view the <a href="scan_report">scan report</a>.<br>
<br>
🌒 - Scan is not yet complete<br>
🌕 - Scan is complete<br>
<br>
On the scan screen you can run two scripts: <a href="script_analyze.html">Analyze</a> and <a href="script_snoop.html">Snoop</a>.
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>