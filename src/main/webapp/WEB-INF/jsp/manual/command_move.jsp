<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - move</h3>
<hr class="dark_red_hr">
This command moves your hacker persona (your representation in the site) to a new node.<br>
<br>
You enter the node's network id to tell where you want to go. Note that the network id is not a number per se, you
need to enter the full text that is shown below the node. '01' is not the same as '1'.<br>
<br>
You cannot move past a node that contains an active Ice layer, but you can always move back to where you came from.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ move 01</div>
    </div>
    <div>
        ⊿00 ↠ Establishing persona in ▣ 01
    </div>
</div>

<%@ include file="_footer.jsp" %>