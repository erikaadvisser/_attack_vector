<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Elevate</h3>
<hr class="dark_red_hr">
This script is used during a hacking run. It disables a single Ice layer. It cannot disable the last (active) Ice layer
of a node.<br>
<br>
When running this script you have to indicate what layer you are targetting.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run elevate f6a3-4676-b34f 2</div>
    </div>
</div>
<br>
This targets the Ice at layer 2.

<%@ include file="_footer.jsp" %>