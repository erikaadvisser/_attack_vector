<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - hack</h3>
<hr class="dark_red_hr">
This command hacks a specific service layer. This can be an Ice layer that you are trying to bypass, or another layer such as
a data vault from which you want to steal data.<br>
<br>
You need to indicate which layer you want to hack.<br>
<br>
<span class="text-strong">Example</span><br>
<br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ hack 1</div>
    </div>
    <div>
        ⊿00 ↠ Hacking Passcode vault (v876/3)...<br>
        Success.<br>
        Recovered passcode: truth abstraction analyst navigable
    </div>
</div>
<br>
In this case, hacking revealed a passcode that can be used to bypass some Ice layer in this site.

<%@ include file="_footer.jsp" %>