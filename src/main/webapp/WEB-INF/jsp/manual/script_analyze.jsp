<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Analyze</h3>
<hr class="dark_red_hr">
This script is used during scanning. It targets a single Ice service layer and reveals details about the puzzle
you need to solve to hack it.<br>
<br>
When you run this script, you need to designate a target using the node network-id and service layer.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run analyze 3eba-4b78-be5d 04 1</div>
    </div>
</div>
This targets Ice layer 1 in node 04.<br>
<br>
<br>
The effect of the script depends on the type of Ice:<br>
<br>
<span class="text-strong"><a href="service_ice_pumer">Pumer</a></span><br>
The script reveals the entire puzzle.<br>
<br>
<span class="text-strong"><a href="service_ice_pryder">Pryder</a></span><br>
The script reveals the entire puzzle.<br>
<br>
<span class="text-strong"><a href="service_ice_kama">Kama</a></span><br>
The script reveals the factions that are using the site, which helps in hacking the Ice.<br>
<br>
<span class="text-strong"><a href="service_ice_jagannatha">Jagganatha</a></span><br>
The script does not work against this type of Ice.<br>
<br>
<span class="text-strong"><a href="service_ice_mitra">Mitra</a></span><br>
The script does not work against this type of Ice.<br>

<%@ include file="_footer.jsp" %>