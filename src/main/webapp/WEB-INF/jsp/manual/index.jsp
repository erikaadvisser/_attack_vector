<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<br><br>
Attack Vector - the manual<br>
<br>
<br>
Overview of interesting topics:<br>
<br>
<span class="text-strong">Site</span>: <a href="site.html">Site</a> - <a href="nodes.html">Nodes</a> - <a href="services.html">Services layers</a> (<a href="service_list.html">list</a>)<br>
<br>
<span class="text-strong">Hack</span>: <a href="scan.html">Scan</a> - <a href="scan_report.html">Report</a> - <a href="hacking.html">Hacking</a> - <a href="commands.html">Commands</a> - <a href="scripts.html">Scripts</a><br>
<br>
<%@ include file="_footer.jsp" %>