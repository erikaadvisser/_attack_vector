<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Backdoor</h3>
<hr class="dark_red_hr">
This script is used during a hacking run. It moves your persona directly to the node that contains the central core (syscon node), bypassing any defences.<br>
<br>
You must have scanned this service to be able to use the script. You supply the node id and service layer as parameters to the script.<br>

<br>
<span class="text-strong">Example: transport to node 99 service layer 1</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run backdoor d489-48db-902d 99 1</div>
    </div>
</div>

<%@ include file="_footer.jsp" %>