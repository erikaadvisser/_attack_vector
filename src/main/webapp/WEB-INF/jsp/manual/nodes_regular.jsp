<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Regular Nodes</h3>
                <hr class="dark_red_hr">
                These nodes usually hold services that are not Ice layers.
                <br>
                <br>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <td>Node</td>
                            <td>Name</td>
                            <td>Expected Services</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/cube.png" height="80" width="80"/>
                            </td>
                            <td>Syscon</td>
                            <td>
                                <a href="service_central_core">System core</a><br>
                                <a href="service_local_core">Local core</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/tile4-sc36.png" height="80" width="80"/>
                            </td>
                            <td>Information</td>
                            <td>
                                <a href="service_data_bank">Data vault</a><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/puzzle3-ps.png" height="80" width="80"/>
                            </td>
                            <td>Passcode</td>
                            <td>
                                <a href="service_passcode_vault">Passcode vault</a><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/tile2.png" height="80" width="80"/>
                            </td>
                            <td>Data bank</td>
                            <td>
                                <a href="service_data_bank">Data bank</a><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shape-square-frame.png" height="80" width="80"/>
                            </td>
                            <td>Transit</td>
                            <td>None. This node connects other nodes</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shapes-circle-frame.png" height="80" width="80"/>
                            </td>
                            <td>Transit</td>
                            <td>None. This node connects other nodes</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shapes-diamond-frame.png" height="80" width="80"/>
                            </td>
                            <td>Transit</td>
                            <td>None. This node connects other nodes</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/resources/images/icons/matte-blue-and-white-square/symbols-shapes/shapes-hexagon-frame.png" height="80" width="80"/>
                            </td>
                            <td>Transit</td>
                            <td>None. This node connects other nodes</td>
                        </tr>
                    </tbody>
                </table>

                <br>
                <br>





<%@ include file="_footer.jsp" %>