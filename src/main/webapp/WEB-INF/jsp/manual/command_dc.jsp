<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - dc</h3>
<hr class="dark_red_hr">
This command disconnects your console from the site.<br>
Alternatively: when you switch to scan mode (or go to any other page), you automatically dc.<br>
<br>
By disconnecting, you prevent the site from being able to trace you using the current connection. DC before the detection timer runs out.<br>
<br>
Note that disconnecting does not make a hacker invulnerable to tracing. All the logs that the site built up during your hack are still there
if you did not delete them. However, disconnecting does prevent automatic detection by the site's primary security systems.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ dc</div>
    </div>
    <div>
        ⊿00 ↠ Disconnecting
    </div>
    <div style="color:#a9443b;">
        ↼ Disconnected.
    </div>
</div>

<%@ include file="_footer.jsp" %>