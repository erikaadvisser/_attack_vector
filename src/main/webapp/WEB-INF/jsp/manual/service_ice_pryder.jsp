<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Ice Service: Pryder</h3>
<hr class="dark_red_hr">
Pryder is Ice that protects service layers below it, blocking the hacker.
There is a <a href="https://www.youtube.com/watch?v=em1wxDr370Y" target="_blank">Youtube video</a> explaining how to deal with this ice (Dutch).<br>
<br>
<br>
<span class="text-strong">Attack Vector</span><br>
<br>
Pryder Ice can be hacked by viewing magic-eye images, like this one.<br>
<br>
<table>
    <tr>
        <td><a href="/manual/service_ice_pryder_example"><img src="/resources/images/ice/magic-eye-obsolete/word/simple/hello.png" width="315" class="round_border_15"></a></td>
        <td>&nbsp;</td>
        <td>Click on the image for a full size version. This image contains the word: HELLO
    </tr>
</table>

<br>
<br>
Each image contains one or more words or numbers. Depending on the strength of the Ice, a single
layer of Pryder Ice will contain one or more images. You 'solve' the image by entering the word(s) or number(s) that they contain.<br>
<br>
A hacker can use an <a href="script_analyze.html">Analyze script </a> to gain the magic eye images in advance. This allows for this Ice to
be hacked 'offline'.<br>
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>