<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: Trace logs</h3>
<hr class="dark_red_hr">
When a <a href="service_remote_tracer">remote tracer</a> has traced notified a tracer site to perform a trace, this site will store the trace logs in the trace logs service.<br>
<br>
<span class="text-strong">Hacking</span><br>
<br>
By hacking this service, all recent traces will be removed.
<br>
Hacking this service will not prevent future traces.<br>
<br>
<%@ include file="_footer.jsp" %>