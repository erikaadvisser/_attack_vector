<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Ice Service: Kama</h3>
                <hr class="dark_red_hr">
                Kama is Ice that protects service layers below it, blocking the hacker.
                There is a <a href="https://www.youtube.com/watch?v=md_a4b8SL_4" target="_blank">Youtube video</a> explaining how to deal with this ice (Dutch).<br>
                <br>
                <br>
                <span class="text-strong">Attack Vector</span><br>
                <br>
                Kama Ice has a weakness that it allows users to create accounts with weak passwords.
                It also allows attackers to try different passwords in quick succession. The attacker can therefore try a
                brute force attack to find passwords of users. If enough passwords are found, the hacker can establish a
                tunnel through the Ice layer, bypassing it.<br>
                <br>
                <table>
                    <tr>
                        <td><img src="/resources/images/manual/kama_needs.png"/></td>
                        <td>&nbsp;</td>
                        <td>There are 4 categories of passwords, and this box shows you how many passwords you need in each category.<br><br>
                            This is just flavor, you need 5 passwords in total (2+0+1+2). Any password will do.
                    </tr>
                </table>
                <br>
                When hacking the ice, you choose which set of common passwords you want to use. Such a set is called a dictionary, and there are many different
                dictionaries. A dictionary is selected with a button that looks like this:<br>
                <h4><span class="label label-info button_like term"><span class="badge password-badge">3</span> Dugo name</span></h4>
                <br>
                This dictionary contains all common Dugo names. So by using it, you will try all common Dugo names as passwords.
                The (3) indicates that it will take 3 seconds to try all these names.<br>
                <br>
                You can also try combinations of dictionaries. For instance if you click both "Dugo names" and "Number/Year" you will try all passwords that consist
                of a Dugo name in combination with a number or a year. This is displayed as:<br>
                <br>
                <h4>
                    <span class="label label-info button_like term"><span class="badge password-badge">3</span> Dugo name</span>
                    X
                    <span class="label label-info button_like term"><span class="badge password-badge">2</span> Number/Year</span>
                    = 6 seconds
                </h4>
                <br>
                You can try each combination only once (no use in trying the same combinations again), and you can only try combinations that take at most 20 seconds
                to calculate. Trying combinations that take longer is not efficient enough.<br>
                <br>
                A special case is the (l33t) prefix. Choosing this option explores the password variants where some letters are changed by numbers. For instance 'password'
                could be written as 'passw0rd'.<br>
                <br>
                <span class="text-strong">Tip</span> Trying the dictionaries with names and words from all cultures takes a long time. Usually a site will attract users from only
                one or two factions. Knowing those factions will help you choose the best dictionaries to try.

                <br>
                <br>
                A hacker can use an <a href="script_analyze.html">Analyze script </a> to gain some information about the kind of users that have accounts
                in this node. This information is useful when trying to decide what type of passwords to use. However, it is not possible to figure out
                exactly how to hack this ice in advance.<br>
                <br>
                <br>
                &nbsp;
<%@ include file="_footer.jsp" %>