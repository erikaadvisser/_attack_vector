<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Storm</h3>
<hr class="dark_red_hr">
This script is used during a hacking run. Run the script in a node that contains one or more remote trace services. The
script will overload the remote tracers and this will mask any trace of you at those remote sites.<br>
<br>
Effectively it will erase any recent traces that those sites have of you. But it does not protect you against future traces.
So do not move back into this node.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run storm d489-48db-902d</div>
    </div>
</div>

<%@ include file="_footer.jsp" %>