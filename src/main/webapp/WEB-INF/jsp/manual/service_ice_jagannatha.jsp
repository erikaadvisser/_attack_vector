<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Ice Service: Jagannatha</h3>
<hr class="dark_red_hr">
Jagannatha is Ice that protects service layers below it, blocking the hacker.
There is a <a href="https://www.youtube.com/watch?v=uBAu2a1sQMk" target="_blank">Youtube video</a> explaining how to deal with this ice (Dutch).<br>
<br>
<br>
<span class="text-strong">Attack Vector</span><br>
<br>
This is the toughest, most expensive Ice available. There is no easy way to hack Jagannatha Ice.<br>
<br>
If you try to use the <a href="/manual/command_hack">hack</a> command then your hacking software will attempt to guess the passcode. However, this
will take months.
<br>
<br>
So in effect you will need to find another way to circumvent this ice. One option is to obtain the passcode from somewhere, be it from within the system,
or by social engineering.<br>
<br>
The <a href="script_analyze.html">Analyze script </a> does not yield any useful information about this kind of Ice.
<br>
<br>
&nbsp;
<%@ include file="_footer.jsp" %>