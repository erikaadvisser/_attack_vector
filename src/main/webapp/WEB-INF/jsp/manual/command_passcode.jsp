<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Command - passcode</h3>
<hr class="dark_red_hr">
This command sends a passcode to an Ice layer. If it is a correct passcode, you can bypass the Ice layer without
any further difficulty.<br>
<br>
Passcodes look like this: truth abstraction analyst navigable<br>
<br>
Passcodes can be found in a <a href="service_passcode_vault">Passcode vault</a> or possibly via social engineering.<br>
<br>
When you use this command, you have to indicate which layer you are targeting.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ passcode 1 truth abstraction analyst navigable</div>
    </div>
    <div>
        ⊿00 ⇥ Authentication accepted, Ice allows access.
    </div>
</div>
<br>
<br>

<%@ include file="_footer.jsp" %>