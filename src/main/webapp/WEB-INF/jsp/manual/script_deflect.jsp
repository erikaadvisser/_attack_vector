<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
<h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Script: Deflect</h3>
<hr class="dark_red_hr">
This script is used during a hacking run. When the script is run, it will provide protection against the next remote trace targeted at you. The next trace
will automatically fail.<br>
<br>
This script can be run multiple times in parallel, each remote trace that is cancelled will also remove one instance of the script.<br>
<br>
The script deactivates when the hack ends, even if not used it cannot be re-used in the next hack.<br>
<br>
<span class="text-strong">Example</span><br>
<div class="terminal">
    <div class="terminal-output">
        <div class="command">⇋ run deflect d489-48db-902d</div>
    </div>
</div>

<%@ include file="_footer.jsp" %>