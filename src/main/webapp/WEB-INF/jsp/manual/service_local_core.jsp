<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Service: Local core</h3>
                <hr class="dark_red_hr">
                A Local core manages the detection of intruders.<br>
                <br>
                <br>
                <span class="text-strong">Hacking</span><br>
                <br>
                When you hack a Local core, you delay the time it takes the system to detect your presence. In effect, you gain extra time to hack the site.<br>
                <br>
                 A hacker can use a <a href="script_snoop.html">Snoop script </a> to determine how much extra time would be gained when the Central Core is hacked.
<%@ include file="_footer.jsp" %>