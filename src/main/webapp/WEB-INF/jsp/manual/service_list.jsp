<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="_header.jsp" %>
                <h3><a href="javascript:history.back()" class="btn btn-info">⮜</a> Services list</h3>
                <hr class="dark_red_hr">
                These are the regular services:<br>
                <br>
                <ul>
                    <li><a href="service_node_os.html">Node OS</a></li>
                    <li><a href="service_central_core.html">Central core</a></li>
                    <li><a href="service_local_core.html">Local core</a></li>
                    <li><a href="service_data_bank.html">Data bank</a></li>
                    <li><a href="service_passcode_vault.html">Passcode vault</a></li>
                    <li><a href="service_remote_tracer">Remote tracer</a></li>
                    <li><a href="service_trace_logs">Trace logs</a></li>
                    <li><a href="service_data_vault.html">(Other)</a></li>
                </ul>
                The following are the Ice services:<br>
                <br>
                <ul>
                    <li><a href="service_ice_dahana.html">Dahana</a> &nbsp; &nbsp; (netwalk)</li>
                    <li><a href="service_ice_pumer.html">Pumer</a> &nbsp; &nbsp; &nbsp;(word search)</li>
                    <li><a href="service_ice_pryder.html">Pryder</a> &nbsp; &nbsp; (magic-eye)</li>
                    <li><a href="service_ice_jagannatha.html">Jagannatha</a> (-)</li>
                    <li><a href="service_ice_mitra.html">Mitra</a> &nbsp; &nbsp; &nbsp;(varies)</li>
                    <li><a href="service_ice_kama.html">Kama</a> &nbsp; &nbsp; &nbsp;&nbsp;(password)</li>
                </ul>

<%@ include file="_footer.jsp" %>