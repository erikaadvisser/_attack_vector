<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/js/core/thread.js"></script>
    <link rel="icon" href="/resources/images/Vector-icon-64.png" />

</head>
<body class style="background-color: #222222">


<div class="container">
    <%--<span id="logoContainer" style="transition: opacity 8000ms; opacity: 0.01">--%>
        <img id="logo" src="/resources/images/logo.png" style="position: absolute;
        z-index: 100;
        opacity: 0;
        top: 680px;
        left: 345px;
        transition: opacity 28000ms, top 4000ms, left 8000ms;
        transition-timing-function: ease;
        ;
        /*transition: ;*/
        /*transition: left 80000ms;*/
        "/>
    <%--</span>--%>

    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Cluster 0|0 &nbsp; Mem 0|0 &nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Open source libraries and other sources used</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div id="main_term">
            </div>
        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div id="opensource_term">
            </div>
        </div>
    </div>
</div>

<%@ include file="fragments/menu_logged_out.jsp" %>
</body>

<script>

    var term = $('#main_term').terminal(function(term) {}, {
        greetings: '',
        name: 'term',
        height: 620,
        keypress : function (e) { return false; },
        outputLimit: -1
    });
    var thread = new Thread(term);


    var osTerm = $('#opensource_term').terminal(function(term) {}, {
        greetings: '',
        name: 'opensource_term',
        height: 620,
        keypress : function (e) { return false; },
        outputLimit: -1
    });
    $('#opensource_term').css("background-color", "#333");
    $('#opensource_term').css("border-radius", "3px 3px 3px 3px");
    $('#opensource_term .cmd').css("background-color", "#333");

    var osThread = new Thread(osTerm);



    thread.wait(10);

    thread.echo(10, "About ↼ Attack Vector ⇁");
    thread.run(0, function(){
        $("#logo").css("z-index", "100");
        $("#logo").css("opacity", "0.5");
    });
    thread.echo(10, "");
    thread.echo(10, "This is the hacking game for the Frontier Larp");
    thread.echo(10, "Organised by Stichting Eos Dynamic (http://www.eosfrontier.space/)");
    thread.echo(10, "");
    //    thread.echo(10, "Tutorials can be found here:...");
    //    thread.echo(10, "");
    //    thread.echo(10, "");
    thread.echo(10, "The game was created by Erik Aad Visser, starting in 2016.");
    thread.echo(10, "The code is open source (MIT License) available at:");
    thread.echo(10, "https://bitbucket.org/erikaadvisser/_attack_vector");
    thread.echo(10, "");
    thread.echo(10, "Special thanks go to:");
    thread.echo(10, "- Julian de Vries for his enthousiasm, feedback and support");
    thread.echo(10, "- All amazing open source software that made this project possible!");
    thread.echo(10, "");
    thread.run(0, function(){
        term.pause();
        thread.stop();
    });

    osThread.echo(0, "(please wait)");
    osThread.echo(130, "");
    osThread.echo(0, "");
    osThread.echo(0, "This software was written using the following open sources:");
    osThread.echo(0, "");
    osThread.echo(0, "> Images and icons");
    osThread.echo(0, "");
    osThread.echo(0, "iconsetc          (http://iconsetc.com)                                 [*free]");
    osThread.echo(0, "Design Contest    (http://icons.webtoolhub.com/icon-n52180-detail.aspx) [CC BY]");
    osThread.echo(0, "");
    osThread.echo(0, "> Javascript");
    osThread.echo(0, "");
    osThread.echo(0, "Bootstrap         (http://getbootstrap.com)                   [MIT]");
    osThread.echo(0, "Fabric.js         (http://fabricjs.com)                       [MIT?]");
    osThread.echo(0, "Jquery            (https://jquery.com)                        [Apache-2]");
    osThread.echo(0, "Jquery Terminal   (http://terminal.jcubic.pl)                 [BSD-3]");
    osThread.echo(0, "Jquery Fancybox   (http://fancyapps.com/fancybox)             [CC BY-NC]");
    osThread.echo(0, "Jquery Datatables (http://www.datatables.net)                 [MIT]");
    osThread.echo(0, "Notify JS         (http://notifyjs.com)                       [MIT]");
    osThread.echo(0, "MagicEye.js       (https://github.com/peeinears/MagicEye.js)  [MIT]");
    osThread.echo(0, "Seedrandom.js     (https://github.com/davidbau/seedrandom)    [MIT]");
    osThread.echo(0, "");
    osThread.echo(0, "> Java");
    osThread.echo(0, "");
    osThread.echo(0, "Apache Groovy     (http://groovy-lang.org)                    [Apache2]");
    osThread.echo(0, "Spring - various  (https://spring.io)                         [Apache2]");
    osThread.echo(0, "Joda-Time         (http://www.joda.org/joda-time)             [Apache2]");
    osThread.echo(0, "Jetty             (http://www.eclipse.org/jetty)              [Apache2]");
    osThread.echo(0, "Tomcat Jasper     (http://tomcat.apache.org)                  [Apache2]");
    osThread.echo(0, "SLF4J             (http://www.slf4j.org)                      [MIT]");
    osThread.echo(0, "Logback           (http://logback.qos.ch/logback-classic)     [LGPL]");
    osThread.echo(0, "Jackson           (http://github.com/FasterXML/jackson)       [Apache2]");
    osThread.echo(0, "MongoDB           (http://www.mongodb.org)                    [Apache2]");
    osThread.echo(0, "");
    osThread.echo(0, "And various others.");
    osThread.echo(0, "See software sources for details.");
    osThread.run(0, function(){
        osterm.pause();
        osThread.stop();
    });



</script>
</html>