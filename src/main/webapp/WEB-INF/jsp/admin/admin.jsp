<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>_Backup & Restore</title>

    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/jslib/notify.min.js"></script>
    <link rel="icon" href="/resources/images/Vector-icon-64.png" />

</head>
<body class style="background-color: #222222">

<div class="container text">
    <br><br>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <p>
                Download backup zip file <a class="btn btn-info" href="backup">Download</a>
            </p>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-lg-12">
            <form method="POST" enctype="multipart/form-data" action="upload">
                <table>
                    <tr>
                        <td>Restore database from backup:&nbsp;</td>
                        <td><input class="btn btn-info" type="file" name="file" /></td>
                        <td>&nbsp;&nbsp;<input class="btn btn-info" type="submit" value="Restore" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            Purge all data: <a class="btn btn-info" onclick="purge(); return false;">purge</a>.
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <p>
                Remove scans: <a class="btn btn-info" href="removeScans">Remove</a>
            </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            Remove missions and mission sites: <a class="btn btn-info" onclick="deleteScansAndMissions(); return false;">Remove</a>.
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            Delete logs: <a class="btn btn-info" onclick="deleteLogs(); return false;">Remove</a>.
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            Discord: <a href="/discord/" >Manage Discord bot config</a>.
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            Version: ${commitIdAbbrev} - ${commitMessageFull}
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-lg-12">
            System info:<br><br>
            maxMemory: <c:out value="${maxMemory}" /> MB<br>
            allocatedMemory: <c:out value="${allocatedMemory}" /> MB<br>
            freeMemory: <c:out value="${freeMemory}" /> MB<br>
        </div>
    </div>



</div>
<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">↼ Attack Vector ⇁</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/gm/sites">Sites</a></li>
                <li><a href="/gm/missions/">Missions</a></li>
                <li><a href="/gm/user/">Users</a></li>
                <li><a href="/gm/logs/">Logs</a></li>
                <li class="active"><a href="#">Admin</a></li>
            </ul>
            <%@ include file="../fragments/menuLogout.jsp" %>
        </div>
    </div>
</div>

</body>
<script>
    var message="${flashMessage}";
    var type="${flashType}";
    if (message){
        if (!type) {
            type = "warning"
        }
        $.notify(message, {globalPosition: 'top center', className: type});
    }

    function purge() {
        var confirmed = confirm("Are you sure you want to delete all data?");
        if (confirmed === true) {
            window.location.href="/admin/purge";
        }
    }
    function deleteScansAndMissions() {
        var confirmed = confirm("Are you sure you want to delete all missions and mission sited (even active ones?)");
        if (confirmed === true) {
            window.location.href="/admin/deleteScansAndMissions";
        }
    }

    function deleteLogs() {
        var confirmed = confirm("Are you sure you want to delete all logs");
        if (confirmed === true) {
            window.location.href="/admin/deleteLogs";
        }
    }
</script>
</html>