<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="../../../resources/jslib/notify.min.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
    <link rel="icon" href="/resources/images/Vector-icon-64.png" />

    <%-- Data tables --%>
    <link rel="stylesheet" type="text/css" href="/resources/jslib/DataTables-1.10.13/css/dataTables.bootstrap.css"/>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/dataTables.bootstrap.js"></script>
</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2" style="background-color: #333333">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Discord channels</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5 text">
            <div>
                <br/>
                <br/>
                Send test message to active channel<br/><br/>
                <form class="text" method="post" action="/discord/message">
                    <div class="form-inline">
                        <div class="form-group">
                            <input type="text" class="form-control" style="width: 450px;" name="value" value="Bot is ready to rock and roll" autofocus>
                        </div>
                        <button type="submit" class="btn btn-success">Send</button>
                    </div>
                </form>
            </div>
            <div>
                <br/>
                <br/>
                <br/>
                A Discord  configuration looks like this:<br/>
                <br/>
                {channel};{bot-token};{description}<br/>
                <br/>
                For example:<br/>
                1234123;super-secret-bot-token-123;Demo channel<br/>
                <br/>
                <br/>
            </div>
            <div id="actions">
                <form class="text" method="post" action="/discord/add">
                    <div class="form-inline">
                        <div class="form-group">
                            <input type="text" class="form-control" style="width: 450px;" name="value" placeholder="New configuration" autofocus>
                        </div>
                        <button type="submit" class="btn btn-info">Add</button>
                    </div>
                </form>
            </div>
            <div>
                <br/><br/><br/><br/>
                Back to admin page: <a href="/admin/">go</a>.
            </div>

            <hr />

        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div>&nbsp;</div>
                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text-muted text" id="scanTable">
                        <thead>
                        <tr>
                            <td class="text-strong">Active</td>
                            <td class="text-strong">Channel</td>
                            <td class="text-strong">User</td>
                            <td class="text-strong">Actions</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${configurations}" var="config">
                            <c:set var="active" value="${config.active ? 'Y': '-'}"/>
                            <tr>
                                <td class="table-very-condensed">${active}</td>
                                <td class="table-very-condensed"><span title="${config.channelId}">${config.channelName}</span></td>
                                <td class="table-very-condensed"><span title="${config.pmUserId}">${config.pmUserName}</span></td>
                                <td class="table-very-condensed">
                                    <a class="aimage" href="/discord/activate/${config.id}" >Activate</a>
                                    &nbsp;&nbsp;
                                    <a class="aimage" href="/discord/remove/${config.id}" >Remove</a>
                            </tr>
                        </c:forEach>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="../fragments/menu_hacker.jsp" %>
</body>

<script>


    var message="${flashMessage}";
    var type="${flashType}";
    if (message){
        if (!type) {
            type = "warning"
        }
        $.notify(message, {globalPosition: 'top center', className: type});
    }

</script>
</html>