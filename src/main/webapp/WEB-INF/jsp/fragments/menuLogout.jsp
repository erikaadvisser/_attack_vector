<%@ page contentType="text/html; charset=UTF-8" %>
<ul class="nav navbar-nav navbar-right">
    <li >
        <a href="/manual" target="_blank">Manual</a>
    </li>
    <li>
        <a href="/me/">ꕺ ${myUser.loginName} </a>
    </li>
    <li >
        <a href="/logout">ꕻ Logout</a>
    </li>
</ul>
