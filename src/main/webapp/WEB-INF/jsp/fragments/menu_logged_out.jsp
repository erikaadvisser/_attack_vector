<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse"></button>
            <a class="navbar-brand" href="#">🜁 Verdant OS 🜃</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <c:if test="${page == 'login'}">
                    <li class="active"><a href="#">Login</a></li>
                    <%--<li><a href="/signUp">Sign up</a></li>--%>
                    <li><a href="/about">About</a></li>
                </c:if>
                <c:if test="${page == 'signUp'}">
                    <li><a href="/login">Login</a></li>
                    <li class="active"><a href="#">Sign up</a></li>
                    <li><a href="/about">About</a></li>
                </c:if>
                <c:if test="${page == 'about'}">
                    <li><a href="/login">Login</a></li>
                    <%--<li><a href="/signUp">Sign up</a></li>--%>
                    <li class="active"><a href="#">About</a></li>
                </c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li >
                    <a href="/manual" target="_blank">Manual</a>
                </li>
            </ul>
        </div>
    </div>
</div>

