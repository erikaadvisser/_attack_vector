<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse"></button>
            <a class="navbar-brand" href="#">🜁 Verdant OS 🜃</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/scripts">Scripts</a></li>
                <c:if test="${page == 'home'}">
                    <li class="active"><a href="#">Home</a></li>
                </c:if>
                <c:if test="${page == 'scan'}">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="#">Scan</a></li>
                </c:if>
                <c:if test="${page == 'run'}">
                    <li><a href="/">Home</a></li>
                    <li><a href="/scan/${run.scan.id}">Scan</a></li>
                    <li class="active"><a href="#">Run</a></li>
                </c:if>
            </ul>
            <%@ include file="menuLogout.jsp" %>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
