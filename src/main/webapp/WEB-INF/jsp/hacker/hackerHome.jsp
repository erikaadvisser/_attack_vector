<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="../../../resources/jslib/notify.min.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
    <link rel="icon" href="/resources/images/Vector-icon-64.png" />

    <%-- Data tables --%>
    <link rel="stylesheet" type="text/css" href="/resources/jslib/DataTables-1.10.13/css/dataTables.bootstrap.css"/>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/dataTables.bootstrap.js"></script>
</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Cluster 1|20 &nbsp; Mem 1|10</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Scans</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div id="main_term">

            </div>
            <div id="actions" class="hidden_alpha transition-opacity-medium">
                <form class="text" onsubmit="scanSite(); return false;">
                    <div class="form-inline">
                        <div class="form-group">
                            <input type="text" class="form-control" id="siteName" placeholder="Site ID or scan ID" autofocus>
                        </div>
                        <button type="button" class="btn btn-info" id="btn_scan">Scan</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div>&nbsp;</div>
                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text-muted text" id="scanTable">
                        <thead>
                        <tr>
                            <td class="text-strong">Site ID</td>
                            <td class="text-strong">Scan ID</td>
                            <td class="text-strong">Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${scans}" var="scan">
                            <c:set var="complete" value="${scan.scan.scanComplete ? 'scan': 'scan'}"/>
                            <c:set var="complete_title" value="${scan.scan.scanComplete ? 'Scan complete - print results' : 'Scan incomplete - print results'}"/>
                            <tr>
                                <td class="table-very-condensed">${scan.siteId}</td>
                                <td class="table-very-condensed"><a href="/scan/${scan.scan.id}">${scan.scan.id}</a></td>
                                <td class="table-very-condensed">
                                    <a class="aimage" href="/print/${scan.scan.id}" title="${complete_title}" target="_blank">${complete}</a>
                                    <a class="aimage" href="#" onclick="delete_scan('${scan.scan.id}'); return false;" title="Delete">🞮</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="../fragments/menu_hacker.jsp" %>
</body>

<script>

    function createDataTable(selector, pageLength) {
        $(selector).DataTable({
            "ordering": false,
            "pageLength": pageLength,
            "pagingType": "numbers",
            "lengthChange": false,
            language: {
                searchPlaceholder: "Search"
            },
        });
    }


    $(document).ready(function() {
        createDataTable("#scanTable", 15);
    });


    var term = $('#main_term').terminal(function(term) {}, {
        greetings: '',
        name: 'term',
        height: 220,
        keypress : function (e) { false },
        outputLimit: -1
    });
    var thread = new Thread(term);


    function btn_scan() {
        var site = $("#siteName").val();
        if (site) {
            window.location.href="/scan/lookup/" + site + "/";
        }
    }

    $("#btn_scan").click(scanSite);

    function scanSite() {
        var site = $("#siteName").val();
        if (site) {
            window.location.href="/scan/lookup/" + site + "/";
        }
    }


    function delete_scan(scanId) {
        var confirmed = confirm("Delete this scan?");
        if (confirmed == true) {
            window.location.href="/scan/delete/" + scanId;
        }
    }


    thread.echo(0, "🜁 Verdant OS 🜃", "[[b;;]🜁 Verdant OS. 🜃]");
    thread.echo(0, "");
    thread.display(0, "#actions");
    thread.echo(0, "Choose site to investigate or attack");
    thread.run(0, function() {term.pause()});

    var message="${flashMessage}";
    var type="${flashType}";
    if (message){
        if (!type) {
            type = "warning"
        }
        $.notify(message, {globalPosition: 'top center', className: type});
    }

</script>
</html>