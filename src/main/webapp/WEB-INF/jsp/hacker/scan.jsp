<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<%@ include file="../fragments/hacking_warning.jsp" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="../../../resources/jslib/notify.min.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
    <%@ include file="../fragments/keepAlive.jsp" %>
</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Cluster 7|20 &nbsp; Mem 2|10 &nbsp; Detection timer <span id="time">&lt;not running&gt;</span></span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Site: ${site.id}</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2 text text-dark">
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <c:forEach var="script" items="${scripts}">
                <c:set var="usedClass" value="${script.used ? 'strikethrough' : ''}"/>
                <span id="script_${script.id}" class="${usedClass}">${script.command()}</span><br>
            </c:forEach>
        </div>
        <div class="col-lg-5">
            <div id="main_term">
            </div>
        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <canvas id="canvas" width="607" height="815" style="border-radius: 3px 3px 3px 3px"></canvas>
        </div>
    </div>
</div>

<%@ include file="../fragments/menu_hacker.jsp" %>

<div style="display: none">

<c:set var="nodeTypes" value="<%=org.n_is_1._attack_vector.model.site.NodeType.values()%>"/>
<c:forEach items="${nodeTypes}" var="type">
    <span><img src="/resources/images/icons/black-white-pearls/template.png"               height="80" width="80" name="${type.name()}_UNDISCOVERED"></span>
    <span><img src="/resources/images/icons/black-white-pearls/template.png"               height="80" width="80" name="${type.name()}_DISCOVERED"></span>
    <span><img src="/resources/images/icons/black-white-pearls/${type.imageName}"          height="80" width="80" name="${type.name()}_PROBED"></span>
    <span><img src="/resources/images/icons/blue-white-pearls/${type.imageName}"           height="80" width="80" name="${type.name()}_FREE"></span>
    <span><img src="/resources/images/icons/red-white-pearls/${type.imageName}"            height="80" width="80" name="${type.name()}_PROTECTED"></span>
</c:forEach>

    <span><img src="/resources/images/icons/yellow-road-sign/culture/astrology1-scorpion-sc37.png"      height="80" width="80" name="HACKER_AVATAR"></span>

    <%--<span><img src="/resources/images/icons/red-road-sign/culture/astrology2-virgin.png"                height="80" width="80" name="RED_VIRGIN"></span>--%>
    <%--<span><img src="/resources/images/icons/red-road-sign/culture/astrology2-archer.png"                height="80" width="80" name="RED_ARCHER"></span>--%>
    <span><img src="/resources/images/icons/red-road-sign/culture/astrology2-gemini.png"                height="80" width="80" name="RED_GEMINI"></span>
    <%--<span><img src="/resources/images/icons/red-road-sign/culture/astrology2-scale.png"                 height="80" width="80" name="RED_SCALE"></span>--%>
    <%--<span><img src="/resources/images/icons/red-road-sign/culture/astrology2-water-carrier.png"         height="80" width="80" name="RED_WATER_CARRIER"></span>--%>

    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number1-sc17.png"            height="80" width="80" name="BLUE_1"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number2-sc17.png"            height="80" width="80" name="BLUE_2"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number3-sc17.png"            height="80" width="80" name="BLUE_3"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number4-sc17.png"            height="80" width="80" name="BLUE_4"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number5-sc17.png"            height="80" width="80" name="BLUE_5"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number6-sc17.png"            height="80" width="80" name="BLUE_6"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number7-sc17.png"            height="80" width="80" name="BLUE_7"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number8-sc17.png"            height="80" width="80" name="BLUE_8"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number9-sc17.png"            height="80" width="80" name="BLUE_9"></span>
    <span><img src="/resources/images/icons/blue-road-sign/culture/chinese-number10-sc17.png"           height="80" width="80" name="BLUE_10"></span>
</div>

</body>
<script>
    let DEBUG = ${debug};
</script>
<script src="/resources/jslib/fabric.js"></script>
<script src="../../../resources/js/core/node_definitions.js"></script>
<script src="../../../resources/js/attack/hackingrun.js"></script>
<%@ include file="../fragments/hacking_warning.jsp" %>

<script>

    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    var scanId = "${scan.id}";
    var printNodeLabelBackground = true;

    var nodeOpacityByStatus = {
        UNDISCOVERED: 0,
        DISCOVERED : 0.4,
        PROBED: 0.4,
        FREE: 0.4,
        PROTECTED: 0.4,
        HACKED: 0.4,
        AVATAR: 1
    };

    var avatarNode;

    function terminalFunction(command, term) {
        if (command !== '') {
            try {
                $.post("/scan/command/${scan.id}", { command: command }, function(data) {
                    term.echo();
                    term.echo(data.message);

                    if (data.scriptId) {
                        $("#script_" + data.scriptId).addClass("strikethrough");
                    }

                    if (data.type === "ATTACK") {
                        var runId = data.payload;
                        location.href = "/run/" + runId;
                    }

                    if (data.type === "SCAN") {
                        var probe = new Patroller({
                            startNode: avatarNode,
                            name: "BLUE_" + (random(9) + 1),
                            nextStepFunction: (${site.scannable}) ? moveAccordingToInstructions : moveToStart,
                            pauseDurationFunction: fixed(0),
                            moveDuration: fixed(10),
                            arriveFunction: (${site.scannable}) ? arriveActInstructions : arriveAndDieInstructions

                        });
                        probe.instructions = data.payload;
                        probe.run();
                    }

                });
            } catch(e) {
                term.error("" + e);
            }
        } else {
            term.echo('');
        }

    }


    var term = $('#main_term').terminal(terminalFunction, {
        greetings: "[[b;;]🜁 Verdant OS. 🜃]\n\nRemote connection established to ${site.name}\n\n'scan' or 'attack' to proceed.",
        name: 'term',
        height: 780,
        prompt: '⇋ ',
        outputLimit: -1
    });
    var termThread = new Thread(term);


//    thread.echo(5, "Power stabilized, system waking up.");
//    thread.echo(5, "Loading" +
//            "..............................................................................................." +
//            "..............................................................................................." +
//            "..............................................................................................." +
//            "..............................................................................................." +
//            "...............................................................................................", "Loaded");
//    thread.echo(0, "");
//    thread.echo(0, "🜁 Verdant OS 🜃", "[[b;;]🜁 Verdant OS. 🜃]");
//    thread.echo(3, "");
//    thread.echo(10, "Cluster holding at @16|20", "Cluster holding at [[;#3c763d;]@16|20]");
//    thread.echo(10, "Cryptoprocessor ok", "Cryptoprocessor [[;#3c763d;]ok]");
//    thread.echo(10, "Laren network stable");
//    thread.echo(0, "");
    <%--termThread.echo(3, "Remote connection established to ${site.name}");--%>
//    termThread.echo(0, "");
//    termThread.echo(0, "Choose to 'scan' or 'attack'");
//    termThread.echo(0, "");


    var siteId = "${site.id}";
    var scanId = "${scan.id}";

    var startNode;

    function createFromLoadForScan(site) {
        createFromLoadGeneric(site);

        var startNodeId = "${site.startNodeId}";
        startNode = nodesById[startNodeId];

        var image = $('[name="HACKER_AVATAR"]')[0];
        hackerAvatar = new fabric.Image(image, {
            left: startNode.left,
            top: 810,
            height: 40,
            width: 40,
            opacity: 1
        });

        avatarNode = hackerAvatar;
        // The patroller requires a connection to be able to find some kind of (random) previous node.
        avatarNode.connections = [avatarNode];
        avatarNode.programs = {};


        canvas.add(hackerAvatar);
        canvas.bringToFront(hackerAvatar);
        var line = new fabric.Line(
                [hackerAvatar.left, hackerAvatar.top, startNode.left, startNode.top], {
                    stroke: "#222",
                    strokeWidth: 3,
                    strokeDashArray: [5, 10],
                    selectable: false,
                    opacity:0
                });

        canvas.add(line);
        canvas.sendToBack(line);
        canvas.renderAll();
        animate(line, "opacity", 1, 1000);

    }

    loadSite("/scan/load/${scan.id}", createFromLoadForScan);

</script>
</html>