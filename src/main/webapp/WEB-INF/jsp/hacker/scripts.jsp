<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_Hackers</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>

    <%-- Data tables --%>
    <link rel="stylesheet" type="text/css" href="/resources/jslib/DataTables-1.10.13/css/dataTables.bootstrap.css"/>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/dataTables.bootstrap.js"></script>

</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Sripts</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div>&nbsp;</div>
            <div class="text">
                The scripts you own are shown on the right.<br><br><br>
                Scripts exploit security flaws, but those flaws will be patched automatically by systems, therefore
                a script can only be used on the day it is created. The daily patching cycle runs at 06:00 primary colony time (PCT).<br><br>
                Scripts like <del>run mask 78ca-4bf6-aaa2</del> have been used already.<br><br>
                New scripts can be created once per day.<br>This also expires old (obsolete) scripts.<br><br>
                <button type="button" class="btn btn-info btn-sm" id="btn_prune_create" onclick="prune_create();">Prune and create</button><br><br><br>
            </div>

        </div>

        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div>&nbsp;</div>
                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text-muted text" id="scriptTable">
                        <thead>
                        <tr>
                            <td>Created</td>
                            <td>Owner</td>
                            <td>Script</td>
                            <td>Id</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${scripts}" var="script">
                            <tr>
                                <td class="table-very-condensed">${script.displayCreate()}</td>
                                <td class="table-very-condensed">${script.originalOwnerName}</td>
                                <td class="table-very-condensed">
                                    <c:if test="${!script.usable()}"><del></c:if>
                                        ${script.type.command}
                                    <c:if test="${!script.usable()}"></del></c:if>
                                </td>
                                <td class="table-very-condensed">
                                    <c:if test="${!script.usable()}"><del></c:if>
                                        ${script.id}
                                    <c:if test="${!script.usable()}"></del></c:if>
                                </td>
                                <td class="table-very-condensed">
                                    <c:if test="${deleted && !script.expired}"><a class="aimage" href="#" onclick="undelete_script('${script.id}'); return false;" title="Undelete">💫</a></c:if>
                                    <c:if test="${!deleted}"><a class="aimage" href="#" onclick="delete_script('${script.id}'); return false;" title="Delete">🞮</a></c:if>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${!deleted}">
                        <span class="text">Show <a href="/scripts/deleted">deleted</a> scripts</span>
                    </c:if>
                    <c:if test="${deleted}">
                        <span class="text">Show <a href="/scripts">active</a> scripts</span>
                    </c:if>

                </div>

            </div>
        </div>

    </div>

    <div class="navbar navbar-inverse navbar-fixed-bottom" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse"></button>
                <a class="navbar-brand" href="#">🜁 Verdant OS 🜃</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Scripts</a></li>
                    <li><a href="/">Home</a></li>
                </ul>
                <%@ include file="../fragments/menuLogout.jsp" %>
            </div>
        </div>
    </div>



</div>
</body>
<script>
    $("#form_type").val("${user.type}");
    $("#form_specialization").val("${user.specialization}");


    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    function delete_script(scriptId) {
        window.location.href = "/scripts/delete/" + scriptId;
    }

    function undelete_script(scriptId) {
        window.location.href = "/scripts/undelete/" + scriptId;
    }

    function prune_create() {
        window.location.href = "/scripts/prune_create";
    }

    $("#scriptTable").DataTable({
        "ordering": true,
        "pageLength": 24,
        "pagingType": "numbers",
        "lengthChange": false,
        language: {
            searchPlaceholder: "Search"
        },
    });


</script>

</html>