<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_Hackers</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Logged in as: ${myUser.loginName}</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Site map</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <button class="btn btn-info" onclick="window.location.href='/gm/user/';">⬖ Back</button>
            <div>&nbsp;</div>
            <div class="text">
                Give a script to the hacker:<br><br>
                <table class="table text" style="width: 80%">
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_trace">Add</button></td>
                        <td>Lvl&nbsp;3</td>
                        <td>Mask</td>
                        <td>+1 minute hack time</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_virus">Add</button></td>
                        <td>Lvl&nbsp;3</td>
                        <td>Elevate</td>
                        <td>hack 1 Ice Layer</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_snoop">Add</button></td>
                        <td>Lvl&nbsp;4</td>
                        <td>Snoop</td>
                        <td>Reveals gains of Syscon, password vault or local core.</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_analyze">Add</button></td>
                        <td>Lvl&nbsp;4</td>
                        <td>Analyze</td>
                        <td>Reveals a single Ice puzzle</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_deflect">Add</button></td>
                        <td>lvl&nbsp;7</td>
                        <td>Deflect</td>
                        <td>Deflect a single trace</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_storm">Add</button></td>
                        <td>lvl&nbsp;8</td>
                        <td>Storm</td>
                        <td>Remove all traces from one node</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_softdos">Add</button></td>
                        <td>lvl&nbsp;8</td>
                        <td>Softdos</td>
                        <td>decrease detection by 30-50%</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_bypass">Add</button></td>
                        <td>lvl&nbsp;9</td>
                        <td>Bypass</td>
                        <td>hack all Ice in node</td>
                    </tr>
                    <tr>
                        <td><button type="button" class="btn btn-info btn-xs" id="btn_add_backdoor">Add</button></td>
                        <td>lvl&nbsp;10</td>
                        <td>Backdoor</td>
                        <td>Move to Syscon (Central Core)</td>
                    </tr>
                </table>
                <div>
                    <br>
                    Use the field and buttons below to manipulate existing scripts of the hacker.
                    <br><br>
                </div>


               <form class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" id="script_id" placeholder="Version">
                    </div>
                    <button type="button" class="btn btn-info" id="btn_used">Set used</button>
                    <button type="button" class="btn btn-info" id="btn_unused">Set unused</button>
                    <button type="button" class="btn btn-info" id="btn_delete" data-placement="right"
                            title="Deleting a script removes it from the view, the hacker can still see the deleted script and undelete it.">(Un)delete</button>
               </form>
            </div>


            <div class="text">
                <br>
                Scripts:<br><br>
                <c:forEach items="${scripts}" var="script">
                    <span class="text-muted" style="-webkit-user-select: none;">${script.displayCreate()}
                    </span><c:if test="${!script.used}"><a href="#" onclick="$('#script_id').val('${script.id}');return false;">${script.command()}</a></c:if>
                    <c:if test="${script.used}">
                        <del><a href="#" onclick="$('#script_id').val('${script.id}');return false;">${script.command()}</a></del><c:out value="${script.type.padding}" escapeXml="false"/>
                        <span class="text-muted" style="-webkit-user-select: none;">(used: ${script.displayUsed()})</span>
                    </c:if>
                    <c:if test="${script.deleted}"><span <span class="text-muted">(deleted)</span></c:if>

                    <br>
                </c:forEach>
            </div>
        </div>

        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <%--<button class="btn btn-info" onclick="window.location.href='/gm/user/';">⬖ Back</button>--%>
                <div>&nbsp;</div>
                <form class="form-horizontal" method="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">User</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control md" placeholder="Also: hacker handle" value="${user.loginName}" name="loginName">
                        </div>
                    </div>
                    <input type="hidden" name="id" value="${user.id}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">New passcode</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control md" placeholder="&#x1F539; Passcode" value="${user.passcode}"  name="passcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">OC name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control md" placeholder="OC name" value="${user.ocName}" name="ocName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">Type</label>
                        <div class="col-sm-9">
                            <select class="form-control md" name="type" id="form_type">
                                <option value="HACKER">Hacker</option>
                                <option value="GM">GM</option>
                                <option value="ADMIN">Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label text-muted">Character name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control md" placeholder="IC name (for hackers)" value="${user.icName}" name="icName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label text-muted">Skill IT</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control md" placeholder="IT skill (0-10)" value="${user.skill_it_main}" name="skill_it_main">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label text-muted">Specialization</label>
                        <div class="col-sm-6">
                            <select class="form-control md" name="specialization" id="form_specialization">
                                <option value="NONE">None</option>
                                <option value="ELITE">Elite</option>
                                <option value="ARCHITECT">Architect</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">Enabled</label>
                        <div class="col-sm-9">
                            <div class="checkbox">
                                <label>
                                    <c:if test="${user.enabled}">
                                        <input type="checkbox" name="enabled" checked><span class="text-muted">User can log in</span>
                                    </c:if>
                                    <c:if test="${!user.enabled}">
                                        <input type="checkbox" name="enabled"><span class="text-muted">User can log in</span>
                                    </c:if>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </div>
                </form>

                <c:if test="${user.blocked}">
                <div class="text text-center">
                    <br><br><br>
                    User is currently blocked - too many failed logins ( ${user.failedLoginCount} ).<br><br>
                    User automatically unblocks at ${user.blockedUntil.toString("HH:mm:ss 'on' YYYY-MM-DD")}.<br><br>
                    <button class="btn btn-info" onclick="window.location.href='/gm/user/unblock/${user.id}';">Unblock user</button>


                </div>
                </c:if>

            </div>
        </div>
    </p>
</div>

<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">↼ Attack Vector ⇁</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/gm/sites">Sites</a></li>
                <li><a href="/gm/missions/">Missions</a></li>
                <li class="active"><a href="/gm/user/">Users</a></li>
                <li><a href="/gm/logs/">Logs</a></li>
            </ul>
            <%@ include file="../../fragments/menuLogout.jsp" %>
        </div>
    </div>
</div>

</body>
<script>
    $("#form_type").val("${user.type}");
    $("#form_specialization").val("${user.specialization}");

    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    function send(action, requiresScriptId) {
        var script_id = $("#script_id").val();
        if (requiresScriptId && !script_id) { return; }
        var command = {
            action: action,
            script_id: script_id,
            hacker_id: "${user.id}"
        };

        $.ajax({
            type : "POST",
            contentType : "application/json",
            url : "script",
            data : JSON.stringify(command),
            dataType : 'json',
            timeout : 5000,
            success : function(run) {
                location.reload()
            },
            error : function(e, reason) {
                console.log("ERROR: ", e);
                $.notify("action failed: " + reason, {globalPosition: 'top', className: 'error'});
            }
        });
    }


    $("#btn_add_trace").click(btn_add_trace);
    $("#btn_add_virus").click(btn_add_virus);
    $("#btn_add_analyze").click(btn_add_analyze);
    $("#btn_add_snoop").click(btn_add_snoop);
    $("#btn_add_deflect").click(btn_add_deflect);
    $("#btn_add_storm").click(btn_add_storm);
    $("#btn_add_softdos").click(btn_add_softdos);
    $("#btn_add_backdoor").click(btn_add_backdoor);
    $("#btn_add_bypass").click(btn_add_bypass);
    $("#btn_used").click(btn_used);
    $("#btn_unused").click(btn_unused);
    $("#btn_delete").click(btn_delete);


    function btn_add_trace() { send("add_mask", false); }
    function btn_add_virus() { send("add_elevate", false); }
    function btn_add_snoop() { send("add_snoop", false); }
    function btn_add_analyze() { send("add_analyze", false); }
    function btn_add_bypass() { send("add_bypass", false)}
    function btn_add_deflect() { send("add_deflect", false)}
    function btn_add_storm() { send("add_storm", false)}
    function btn_add_softdos() { send("add_softdos", false)}
    function btn_add_backdoor() { send("add_backdoor", false)}
    function btn_used() { send("set_used", true); }
    function btn_unused() { send("set_unused", true); }
    function btn_delete() { send("delete", true); }

</script>

</html>