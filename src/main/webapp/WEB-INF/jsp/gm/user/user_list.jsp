<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_Hackers</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>



    <%-- Data tables --%>
    <link rel="stylesheet" type="text/css" href="/resources/jslib/DataTables-1.10.13/css/dataTables.bootstrap.css"/>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/dataTables.bootstrap.js"></script>

</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Cluster 16|20 &nbsp; Mem 1|10 &nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Users</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div class="text">
                <br><br>
                Manage the users of the site.<br>
                Users are located in the pane to the right.<br>
                Click on a row to edit a user.
                <br><br>
                <br><br>
            </div>
            <div id="actions" >
                <p class="text">
                <form class="form-inline">
                    <button type="button" class="btn btn-info" onclick="window.location.href='/gm/user/new';">New user</button>
                </form>
                </p>
            </div>
        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div>&nbsp;</div>
                <h4 class="text-center text-muted">Hackers</h4>
                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text-muted text" id="userTable_hackers">
                        <thead>
                        <tr>
                            <td class="text-strong">Login</td>
                            <td class="text-strong">IC name</td>
                            <td class="text-strong">Skill</td>
                            <td class="text-strong">OC name</td>
                            <td class="text-strong">Enabled</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${hackers}" var="user">
                            <tr>
                                <td class="table-very-condensed"><a href="/gm/user/${user.id}">${user.loginName}</a></td>
                                <td class="table-very-condensed">${user.icName}</td>
                                <td class="table-very-condensed">${user.skill_it_main}</td>
                                <td class="table-very-condensed">${user.ocName}</td>
                                <td class="table-very-condensed">${user.enabled}</td>
                            </tr>
                        </c:forEach>
                        </tbody>

                    </table>
                </div>

                <h4 class="text-center text-muted">Game Masters & Admin</h4>
                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text" id="userTable_gm_admin">
                        <thead>
                        <tr>
                            <td><strong>Login</strong></td>
                            <td><strong>OC name</strong></td>
                            <td><strong>Role</strong></td>
                            <td><strong>Enabled</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${gmsAdmins}" var="user">
                            <tr>
                                <td class="table-very-condensed"><a href="/gm/user/${user.id}">${user.loginName}</a></td>
                                <td class="table-very-condensed">${user.ocName}</td>
                                <td class="table-very-condensed">${user.type.toString().toLowerCase()}</td>
                                <td class="table-very-condensed">${user.enabled}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">↼ Attack Vector ⇁</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/gm/sites">Sites</a></li>
                <li><a href="/gm/missions/">Missions</a></li>
                <li class="active"><a href="#">Users</a></li>
                <li><a href="/gm/logs/">Logs</a></li>
            </ul>
            <%@ include file="../../fragments/menuLogout.jsp" %>
        </div>
    </div>
</div>
</body>
<script>
    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }


    function createDataTable(selector, pageLength) {
        $(selector).DataTable({
            "ordering": false,
            "pageLength": pageLength,
            "pagingType": "numbers",
            "lengthChange": false,
            language: {
                searchPlaceholder: "Search"
            },
        });
    }


    $(document).ready(function() {
        createDataTable("#userTable_hackers", 15);
        createDataTable("#userTable_gm_admin", 8);

    });



</script>
</html>