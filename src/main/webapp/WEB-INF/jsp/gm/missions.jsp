<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>

    <%-- Data tables --%>
    <link rel="stylesheet" type="text/css" href="/resources/jslib/DataTables-1.10.13/css/dataTables.bootstrap.css"/>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/dataTables.bootstrap.js"></script>

</head>
<body class style="background-color: #222222">


<div class="container">

    <div class="row no-padding">
        <%--<div class="col-lg-1">--%>
        <%--</div>--%>
        <div class="col-lg-6 no-padding" >
            <div style="height: 815px; width: 100%;" class="dark_well">
                <br>

                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text-muted beaconText" id="sitesTable">
                        <thead>
                        <tr>
                            <td class="text-strong">Target Site</td>
                            <td class="text-strong">Time left</td>
                            <td class="text-strong">Success</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${missions}" var="mission">
                            <tr>
                                <td class="table-very-condensed"><a href="/gm/mission/${mission.id}/">${mission.targetSiteId}</a></td>
                                <td class="table-very-condensed">${mission.formattedTimeLeft()}</td>
                                <td class="table-very-condensed">${mission.goalReached}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                </div>

                <hr />
                <div id="actions" >
                    <p class="beaconText">&nbsp;
                        <a href="/beacon/" class="btn beaconBtn beaconText">Start new mission</a>
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">↼ Attack Vector ⇁</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/gm/sites">Sites</a></li>
                <li class="active"><a href="#">Missions</a></li>
                <li><a href="/gm/user/">Users</a></li>
                <li><a href="/gm/logs/">Logs</a></li>
            </ul>
            <%@ include file="../fragments/menuLogout.jsp" %>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>

</body>

<script>

    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    function createDataTable(selector, pageLength) {
        $(selector).DataTable({
            "ordering": false,
            "pageLength": pageLength,
            "pagingType": "numbers",
            "lengthChange": false,
            language: {
                searchPlaceholder: "Search"
            },
        });
    }


    $(document).ready(function() {
        createDataTable("#sitesTable", 32);
    });


</script>
</html>