<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>

    <%-- Data tables --%>
    <link rel="stylesheet" type="text/css" href="/resources/jslib/DataTables-1.10.13/css/dataTables.bootstrap.css"/>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/dataTables.bootstrap.js"></script>

</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-1">&nbsp;
        </div>
    </div>

    <div class="row">
        <div class="col-lg-1">
        </div>
        <div class="col-lg-10">
            <div style="height: 815px; width: 100%;" class="dark_well">
                <div>&nbsp;</div>
                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text-muted text" id="sitesTable">
                        <thead>
                        <tr>
                            <td class="text-strong">Hackable</td>
                            <td class="text-strong">Site ID</td>
                            <td class="text-strong">Type</td>
                            <td class="text-strong">Action</td>
                            <td class="text-strong">Mission</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${sites}" var="site">
                            <tr>
                                <c:if test="${site.active}">
                                    <td class="table-very-condensed">${site.hackable}</td>
                                    <td class="table-very-condensed"><a target="_blank" href="/gm/editor/${site.id}/">${site.id}</a></td>
                                    <%--<td class="table-very-condensed">${site.name}</td>--%>
                                    <td class="table-very-condensed">${site.type}
                                        <c:if test='${site.type.toString().startsWith("TEMPLATE")}'>
                                            <span class="text-dark">(uses: ${site.templateUses})</span>
                                        </c:if>
                                    </td>
                                    <td class="table-very-condensed">
                                        <a class="aimage" target="_blank" href="/gm/print/${site.id}/" title="Print">🌐</a><a class="aimage" target="_blank" href="/gm/print-solution/${site.id}/" title="Print solution">💠</a><a class="aimage" href="#" onclick="deactivate('${site.id}'); return false;">🞮</a>
                                    </td>
                                    <td class="table-very-condensed">${site.missionId}</td>
                                </c:if>
                                <c:if test="${!site.active}">
                                <td class="table-very-condensed text-dark"><s>${site.id}</s></td>
                                <%--<td class="table-very-condensed text-dark"><s>${site.name}</s></td>--%>
                                <td class="table-very-condensed"></td>
                                <td class="table-very-condensed">
                                    <a href="/gm/sites/activate/${site.id}/">💫</a>
                                </td>
                                <td class="table-very-condensed">${site.missionId}</td>
                                </c:if>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${active}">
                        <span class="text">Show <a href="/gm/sites/inactive"> ${inactiveCount} inactive</a> sites</span>
                    </c:if>
                    <c:if test="${!active}">
                        <span class="text">Show <a href="/gm/sites">active</a> sites</span>
                    </c:if>
                    <hr />
                    <div id="actions" >
                        <p class="text">
                        <div class="form-inline">
                            <button type="button" class="btn btn-info" id="btn_edit">Create new site</button>
                            <div class="form-group">
                                <input type="text" class="form-control" id="siteName" placeholder="Site id">
                            </div>
                        </div>
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">↼ Attack Vector ⇁</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Sites</a></li>
                <li><a href="/gm/missions/">Missions</a></li>
                <li><a href="/gm/user/">Users</a></li>
                <li><a href="/gm/logs/">Logs</a></li>
            </ul>
            <%@ include file="../fragments/menuLogout.jsp" %>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>

</body>

<script>

    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    function createDataTable(selector, pageLength) {
        $(selector).DataTable({
            "ordering": false,
            "pageLength": pageLength,
            "pagingType": "numbers",
            "lengthChange": false,
            language: {
                searchPlaceholder: "Search"
            },
        });
    }


    $(document).ready(function() {
        createDataTable("#sitesTable", 32);
    });



    function btn_edit() {
        var site = $("#siteName").val();
        window.open("editor/" + site + "/");
    }

    function deactivate(site) {
        window.location.href="/gm/sites/deactivate/" + site + "/";
    }

    $("#btn_edit").click(btn_edit);

    $("a").tooltip({'trigger': "hover"});

</script>
</html>