<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>

    <%-- Data tables --%>
    <link rel="stylesheet" type="text/css" href="/resources/jslib/DataTables-1.10.13/css/dataTables.bootstrap.css"/>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/resources/jslib/DataTables-1.10.13/js/dataTables.bootstrap.js"></script>

</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-10" style="background-color: #111111">
            <span class="text">Logs</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-10" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 1214px;" class="dark_well">
                <div class="form-inline">
                    <div class="form-group" style="display: block; position: relative; top: 30px; left: 15px;">
                        <select id="levelSelect" class="form-control" onchange="levelChanged(event)">
                            <option value="ACTION">1.. actions</option>
                            <option value="EVENT">2.. Events</option>
                            <option value="HIGHLIGHT">3.. Highlights</option>
                            <option value="GM">-- GM Actions</option>
                            <option value="ERROR">-- Errors (experimental)</option>
                        </select>
                    </div>
                </div>
                <div style="padding: 0px 10px 0px 10px;">
                    <table class="table table-condensed text-muted text" id="sitesTable" style="display: none" width="100%">
                        <thead>
                        <tr>
                            <td class="text-strong">Timestamp</td>
                            <td class="text-strong">Level</td>
                            <td class="text-strong">Action</td>
                            <td class="text-strong">User</td>
                            <td class="text-strong">Message</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${logs}" var="record">
                            <tr>
                                <td class="table-very-condensed" width="130">${record.displayTime}</td>
                                <td class="table-very-condensed" width="50">${record.level}</td>
                                <td class="table-very-condensed">${record.action}</td>
                                <td class="table-very-condensed">${record.userName}</td>
                                <td class="table-very-condensed">${record.message}</td>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">↼ Attack Vector ⇁</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/gm/sites/">Sites</a></li>
                <li><a href="/gm/missions/">Missions</a></li>
                <li><a href="/gm/user/">Users</a></li>
                <li class="active"><a href="#">Logs</a></li>
            </ul>
            <%@ include file="../fragments/menuLogout.jsp" %>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>

</body>

<script>

    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    $("#levelSelect option[value='${level}']").prop('selected', true);

    function levelChanged(event) {
        window.location.href="/gm/logs/" + event.currentTarget.value;
    }

    function createDataTable(selector, pageLength) {
        $(selector).DataTable({
            "order": [[ 0, "desc" ]],
            "pageLength": pageLength,
            "pagingType": "numbers",
            "lengthChange": false,
            language: {
                searchPlaceholder: "Search"
            },
            "dom": '<"top"pf>rt<"bottom"l><"clear">'
        });
    }


    $(document).ready(function() {
        createDataTable("#sitesTable", 32);
        $("#sitesTable").show();
    });


    var term = $('#main_term').terminal(function(term) {}, {
        greetings: '',
        name: 'term',
        height: 220,
        outputLimit: -1
    });
    var thread = new Thread(term);

    function print(site) {
        window.open("print/" + site + "/")
    }

    function print_solution(site) {
        window.open("print-solution/" + site + "/")
    }

    function btn_edit() {
        var site = $("#siteName").val();
        window.open("editor/" + site + "/");
    }

    function deactivate(site) {
        var confirmed = confirm("Deactivating a site renders it invisible and unavailable. Are you sure you want to deactivate this site?");
        if (confirmed == true) {
            window.location.href="/gm/sites/deactivate/" + site + "/";
        }
    }

    function activate(site) {
        var confirmed = confirm("Deactivating a site renders it invisible and unavailable. Are you sure you want to deactivate this site?");
        if (confirmed == true) {
            window.location.href="/gm/sites/deactivate/" + site + "/";
        }
    }


    $("#btn_edit").click(btn_edit);

    thread.echo(0, "🜁ttack 🜃ector", "[[b;;]🜁ttack 🜃ector ]");
    thread.echo(0, "");
    thread.echo(0, "Frontier Hacking SL Interface");
    thread.echo(0, "");
    thread.echo(0, "Enter a site name and click one of the buttons.");
    thread.echo(0, "The site does not have to exist yet.");
    thread.echo(0, "");
    thread.echo(0, "Actions");
    thread.echo(0, "print site scan               - 🌐");
    thread.echo(0, "print SL version site scan    - 💠");
    thread.echo(0, "deactivate site               - 🞮", "deactivate site               - [[;#337ab7;]🞮]");
    thread.echo(0, "reactivate site               - 💫");
    thread.run(0, function() {term.pause()});

    $("a").tooltip({'trigger': "hover"});

</script>
</html>