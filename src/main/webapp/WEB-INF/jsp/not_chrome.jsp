<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/js/core/thread.js"></script>
    <link rel="icon" href="/resources/images/Vector-icon-64.png" />

</head>
<body class style="background-color: #222222">


<div class="container">
    <%--<span id="logoContainer" style="transition: opacity 8000ms; opacity: 0.01">--%>
    <img id="logo" src="/resources/images/logo.png" style="position: absolute;
        z-index: 100;
        opacity: 0;
        top: 680px;
        left: 345px;
        transition: opacity 28000ms, top 4000ms, left 8000ms;
        transition-timing-function: ease;
        ;
        /*transition: ;*/
        /*transition: left 80000ms;*/
        "/>
    <%--</span>--%>

    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Cluster 0|0 &nbsp; Mem 0|0 &nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">&nbsp;</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div class="text">
                <br><br><br><br>
                You seem to be using a browser that is not Chrome.<br><br>

                The game relies heavily on javascript.
                Browsers have small differences between them and this lead to bugs that could break the game, possibly
                giving players unfair advantages.<br><br>
                Chrome should be available on all platforms, so it seems reasonable to restrict the game to Chrome only.
                If there is a legitimate reason why Chrome can't be used, please let us know.
            </div>
        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div  class="text">
                    <h2><br>&nbsp;This game only runs on Chrome.</h2><br><br>
                </div>
            </div>
        </div>
    </div>

    <div class="navbar navbar-inverse navbar-fixed-bottom" >
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                </button>
                <a class="navbar-brand" href="#">🜁 Verdant OS 🜃</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/login">Login</a></li>
                    <li><a href="/about">About</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>

</body>

<script>
    var thread = new Thread(null);
    thread.wait(100);
    thread.run(0, function(){
        $("#logo").css("z-index", "100");
        $("#logo").css("opacity", "0.5");
    });

</script>
</html>