<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>

<div class="container transition-color-slow" id="word_search_container" style="padding-top: 0px; background-color: #222222">
    <div>Sector 393, thread fd90, worker dd9</div>
    <div class="row">
        <div class="col-lg-8">
            <div class="text-left">
                <h4 class="text-success term"><strong>
                    Ice: <span class="text-info">${iceType.text} ${iceType.version}</span><br>
                    Attack vector: <span class="text-danger">rp/i/p injection + remote core dump modification</span><br>
                    Attack software: <span class="text-muted">Baseline</span><br></strong>
                </h4>
                <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 5px;"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div id="ice_term" ></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 15px;"/>
        </div>
    </div>
    <div id="wordsearch_main" class="transition-opacity-slow hidden_alpha">
        <div class="row">
            <div class="col-lg-11">
                <table class="table table-bordered">
                    <c:forEach items="${puzzle.rows}" var="row" varStatus="rowStatus"><tr>
                        <c:forEach items="${row}" var="letter" varStatus="letterStatus">
                            <s:eval expression="T(org.n_is_1._attack_vector.services.ice.wordsearch.WsUtil).convert(letter)" var="letterClass" />
                            <td class="text-center transition-color-fast" id="p${rowStatus.index}_${letterStatus.index}"><span class="text-info bold ${letterClass}">&nbsp;</span></td></c:forEach>
                    </tr></c:forEach>
                </table>
            </div>
        </div>
    </div>

</div>
<script>

    var wordInput = [
    <c:forEach items="${puzzle.words}" var="word">
        {text: "${word.text}", location: [<c:forEach items="${word.locations}" var="location">{x:${location.x}, y:${location.y}},</c:forEach>]},</c:forEach>
    ];
    var serviceId = ${serviceId};
</script>
<script src="../../../resources/js/attack/wordsearch.js"></script>
