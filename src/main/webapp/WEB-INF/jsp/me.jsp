<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_Hackers</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Cluster 1|20 &nbsp; Mem 1|10</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Your profile</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
        </div>

        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <%--<button class="btn btn-info" onclick="window.location.href='/gm/user/';">⬖ Back</button>--%>
                <div>&nbsp;</div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-muted">Login</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control md" placeholder="Also: hacker handle" value="${user.loginName}" name="loginName" disabled>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="${user.id}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-muted">OC name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control md" placeholder="OC name" value="${user.ocName}" name="ocName" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-muted">Type</label>
                        <div class="col-sm-10">
                            <select class="form-control md" name="type" id="form_type" disabled>
                                <option value="HACKER">Hacker</option>
                                <option value="GM">GM</option>
                                <option value="ADMIN">Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label text-muted">Character name</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control md" placeholder="IC name (for hackers)" value="${user.icName}" name="icName" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label text-muted">Skill IT</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control md" placeholder="IT skill (0-10)" value="${user.skill_it_main}" name="skill_it_main" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label text-muted">Specialization</label>
                        <div class="col-sm-7">
                            <select class="form-control md" name="specialization" id="form_specialization" disabled>
                                <option value="NONE">None</option>
                                <option value="ELITE">Elite</option>
                                <option value="ARCHITECT">Architect</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="text text-center">
                    <br>
                    <br>
                    <br>
                    <br>
                    Change your passcode. Please use a passcode (or passphrase) of 8+ characters.
                    <br>
                    <br>
                </div>
                <form class="form-horizontal" action="/me/passcode" method="post">
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-muted">Passcode</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control md" placeholder="&#x1F539; Passcode" name="passcode1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label text-muted">Again</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control md" placeholder="&#x1F539; Please repeat" name="passcode2" id="passcode2">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info">Update passcode</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </p>
</div>

<div class="navbar navbar-inverse navbar-fixed-bottom" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">🜁 Verdant OS 🜃</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="active">
                    <a href="/me/">ꕺ ${myUser.loginName} </a>
                </li>
                <li >
                    <a href="/logout">ꕻ Logout</a>
                </li>
            </ul>
        </div>
    </div>
</div>

</body>
<script>
    $("#form_type").val("${user.type}");
    $("#form_specialization").val("${user.specialization}");

    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    $("#passcode2")[0].onpaste = function(e) {
        e.preventDefault();
    }




</script>

</html>