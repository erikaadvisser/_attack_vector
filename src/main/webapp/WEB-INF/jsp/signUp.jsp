<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_Hackers</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
</head>
<body class style="background-color: #222222">


<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">Details</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div class="row">
                <div class="col-lg-12 text">
                    <h3>Sign up</h3>
                    <p>
                        Create an account to try out the hacking mini-game of Frontier.<br>
                        <br>
                        This is available to everyone, but most relevant to players of Frontier who play hackers or are
                        interested in doing so.<br>
                        <br>
                        There are tutorial missions available: <a href="https://docs.google.com/document/d/1pY-D2HHTuIE1nzv-zeqE4SWiBlgvv73cAif9gDxvVEw/edit?usp=sharing">here</a>.<br>
                        <br>
                        Note that your account here and the tutorial missions are not part of the game world of Frontier. Nothing you gain or learn here are relevant for the next events.<br>
                        <br>
                        <br>
                        <br>
                        Finally: this is still a work in progress and by no means perfect. Feedback is welcome, please contact erik.aad.visser@gmail.com
                    </p>
                </div>
            </div>
        </div>

        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <div style="height: 815px; width: 607px;" class="dark_well">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <form class="form-horizontal" method="post" onsubmit="return testPasswordMatch();">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">Login</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control md" placeholder="Also: hacker handle" value="${user.loginName}" name="loginName">
                        </div>
                    </div>
                    <input type="hidden" name="id" value="${user.id}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">Passcode</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control md" placeholder="&#x1F539; Passcode" name="passcode" id="passcode1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">Again</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control md" placeholder="&#x1F539; Please repeat" name="passcode2" id="passcode2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-muted">OC name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control md" placeholder="OC name" value="${user.ocName}" name="ocName">
                        </div>
                    </div>
                        <input type="hidden" class="form-control md" value="no-character" name="icName">
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </p>
</div>

<%@ include file="fragments/menu_logged_out.jsp" %>

</body>
<script>
    $("#form_type").val("${user.type}");
    $("#form_specialization").val("${user.specialization}");

    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: '${flashType}'});
    }

    function testPasswordMatch() {
        var code1 = $("#passcode1").val();
        var code2 = $("#passcode2").val();
        if (code1 != code2) {
            $.notify("Passcodes do not match.", {globalPosition: 'top center', className: '${flashType}'});
            return false;
        }
        return true;
    }

</script>

</html>