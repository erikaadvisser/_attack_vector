<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_attack_vector</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <script src="/resources/jslib/jquery.terminal-min.js"></script>
    <link href="/resources/css/jquery.terminal.css" rel="stylesheet"/>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/resources/css/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/resources/jslib/jquery.fancybox.js"></script>
    <script src="../../resources/jslib/notify.min.js"></script>
    <script src="/resources/js/core/thread.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
    <link rel="icon" href="/resources/images/Vector-icon-64.png" />

</head>
<body class style="background-color: #222222">
<script>
    if (!window.chrome) {
        window.location.href = "/notChrome"
    }
</script>


<div class="container">
    <%--<span id="logoContainer" style="transition: opacity 8000ms; opacity: 0.01">--%>
        <img id="logo" src="/resources/images/logo.png" style="position: absolute;
        z-index: 100;
        opacity: 0;
        top: 680px;
        left: 345px;
        transition: opacity 28000ms, top 4000ms, left 8000ms;
        transition-timing-function: ease;
        ;
        /*transition: ;*/
        /*transition: left 80000ms;*/
        "/>
    <%--</span>--%>

    <div class="row">
        <div class="col-lg-2">
            <span class="text">&nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #333333">
            <span class="text">Cluster 0|0 &nbsp; Mem 0|0 &nbsp;</span>
        </div>
        <div class="col-lg-5" style="background-color: #111111">
            <span class="text">&nbsp;</span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-5">
            <div id="main_term">

            </div>
            <div id="actions">
                <p class="text">
                    <form class="form-inline" action="/loginSubmit" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" style="width: 150px;" name="loginName" placeholder="Handle" autofocus>
                        </div>
                        <div class="form-group">
                                <input type="password" style="width: 120px;" class="form-control" name="passcode" placeholder="&#x1F539; Passcode">
                        </div>
                        <span style="color:white">&gt;</span>
                        <button type="submit" class="btn btn-info" id="btn_login">Connect</button>
                    </form>
                </p>
                <%--<div>--%>
                <%--</div>--%>
            </div>
        </div>
        <div class="col-lg-5" style="background-color: #111111;padding-top: 10px;padding-bottom: 10px;">
            <svg height="815" width="607" class="dark_well">
            </svg>
        </div>
    </div>
</div>

<%@ include file="fragments/menu_logged_out.jsp" %>
</body>

<script>


    var flashMessage = "${flashMessage}";
    if (flashMessage) {
        $.notify(flashMessage, {globalPosition: 'top center', className: 'error'});
    }


    var term = $('#main_term').terminal(function(term) {}, {
        greetings: '',
        name: 'term',
        height: 220,
        outputLimit: -1
    });
    var thread = new Thread(term);


//    $.get("/loginSubmit");


    thread.echo(10, "Network console bootstrap complete");
    thread.echo(0, "");
    thread.echo(0, "Please enter credentials");
    thread.run(55, function() {term.pause()});
    thread.run(0, function(){
        $("#logo").css("z-index", "100");
        $("#logo").css("opacity", "0.5");
//        $("#logo").css("top", "35px");
//        $("#logo").css("left", "15px");
        thread.stop();
    });


</script>
</html>