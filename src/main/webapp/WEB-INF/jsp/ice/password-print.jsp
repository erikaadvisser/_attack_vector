<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<div class="row">
    <div class="col-lg-offset-1 col-lg-4 text-center">
        <h4><span class="label label-default">Ice analysis</span></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-offset-0 col-lg-12">
        <table class="table" style="margin-bottom: 10px;">
            <tr>
                <td width="200" class="text-right tr_print"><span class="text-muted">Attack vector</span></td>
                <td class="tr_print">Dictionary based password guessing (Passwords)</td>
            </tr>
            <tr>
                <td class="text-right tr_print"><span class="text-muted">Ice strength</span></td>
                <td class="tr_print">${puzzle.strength}</td>
            </tr>
        </table>
    </div>
</div>
<div id="print_${serviceId}" style="display: none;">
    <c:forEach items="${puzzle.needed.keySet()}" var="key">
    <div class="row">
        <div class="col-lg-offset-2 col-lg-2 text-right">${key}</div>
        <div class="col-lg-2">${puzzle.needed[key]}</div>
    </div>
    </c:forEach>
    <br>
    <div class="row">
        <div class="col-lg-offset-2 col-lg-12">Site users are members of these factions:<strong>
            <c:forEach items="${puzzle.factions}" var="faction">${faction.capitalized()} </c:forEach></strong>
        </div>
    </div>


    <div class="row" id="solution_${status.index}" style="display: none;">
        <div class="col-lg-offset-2 col-lg-10">
            <br><br>Solutions:<br><br><span class="term">
            <c:forEach items="${puzzle.available.keySet()}" var="key">
                ${key}<br>
            </c:forEach>
            </span>
        </div>
    </div>

    <script>
        if (print_solution) {
            $("#solution_${status.index}").css("display", "");
        }
    </script>
</div>
<script>
    if (print_puzzles || analyzed.includes('${serviceId}')) {
        $("#print_${serviceId}").css("display", "");
    }
</script>

