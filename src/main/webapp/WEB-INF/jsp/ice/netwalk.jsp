<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<script>
    let netwalkCanvas = false;
    function renderAll() {
        if (netwalkCanvas && netwalkCanvas.renderAll) {
            netwalkCanvas.renderAll();
        }
    }
</script>
<div class="container transition-color-slow" id="word_search_container" style="padding-top: 0px; background-color: #222222">
    <div>Sector 004, thread 0s34, worker g03</div>
    <div class="row">
        <div class="col-lg-8">
            <div class="text-left">
                <h4 class="text-success term"><strong>
                    Ice: <span class="text-info">Dahana 881/2</span><br>
                    Attack vector: <span class="text-danger">Network reconfiguration</span><br>
                    Attack software: <span class="text-muted">Baseline</span><br></strong>
                </h4>
                <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 5px;"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div id="ice_term"></div>
        </div>
        <div class="col-lg-2 hidden_alpha transition-opacity-medium" id="needsBlock">
            <div class="well dark_well" style="height: 150px; border-color: #999;">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 15px;"/>
        </div>
    </div>

    <div id="NetwalkUi">
        <div class="row">
            <div class="col-lg-12">
                <div>
                    <canvas id="netwalkCanvas" width="${40 + x * 81}" height="${40 + y * 81}" class="hidden_alpha transition-opacity-slow" style="border-radius: 3px 3px 3px 3px"
                            oncontextmenu="return false;"></canvas>

                </div>
                <div style="display: none">
                    <!-- Load the images so they can be used in Fabric -->
                    <span><img src="/resources/images/ice/netwalk/hack/1.png" height="80" width="80" id="1" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack/2.png" height="80" width="80" id="2" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack/2s.png" height="80" width="80" id="2s" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack/3.png" height="80" width="80" id="3" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack/4.png" height="80" width="80" id="4" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack-light/1.gif" height="80" width="80" id="light_1" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack-light/2.gif" height="80" width="80" id="light_2" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack-light/2s.gif" height="80" width="80" id="light_2s" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack-light/3.gif" height="80" width="80" id="light_3" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack-light/4.gif" height="80" width="80" id="light_4" onload="renderAll()"></span>
                    <span><img src="/resources/images/ice/netwalk/hack-light/c.gif" height="80" width="80" id="center" onload="renderAll()"></span>
                </div>
                <br/>
                <br/>
                <br/>
            </div>
        </div>

    </div>
</div>
<script src="/resources/js/lib/seedrandom/seedrandom.js"></script>
<script src="/resources/js/ice/netwalk.js"></script>

<script>

    let puzzle = JSON.parse('${puzzleDef}');
    let serviceId = ${serviceId};

    console.log("before");
    netwalk("netwalkCanvas", puzzle, serviceId, "hack", 80);

</script>
