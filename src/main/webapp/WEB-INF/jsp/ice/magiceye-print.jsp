<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<div class="row">
    <div class="col-lg-offset-1 col-lg-4 text-center">
        <h4><span class="label label-default">Ice analysis</span></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-offset-0 col-lg-12">
        <table class="table" style="margin-bottom: 10px;">
            <tr>
                <td width="200" class="text-right tr_print"><span class="text-muted">Attack vector</span></td>
                <td class="tr_print">Unshielded cryptoprocessor power analysis (Magic Eye)</td>
            </tr>
            <tr>
                <td class="text-right tr_print"><span class="text-muted">Ice strength</span></td>
                <td class="tr_print">${puzzle.strength}</td>
            </tr>
        </table>
    </div>
</div>
<div id="print_${serviceId}" style="display: none;">
    <c:forEach items="${puzzle.images}" var="image" varStatus="status">
    <div class="row">
        <div class="col-lg-12">
            <img src="${image.location}" class="round_border_60" width="1500">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-center">
            <span id="solution_${serviceId}_${status.index}"></span>
            <br>
            <br>
        </div>
    </div>

    <script>
        if (print_solution) {
            $("#solution_${serviceId}_${status.index}").html("Solution: <strong>${image.solution}</strong>");
        }
    </script>
</c:forEach>
</div>
<script>
    if (print_puzzles || analyzed.includes('${serviceId}')) {
        $("#print_${serviceId}").css("display", "");
    }
</script>

