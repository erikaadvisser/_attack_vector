<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>


<div class="row">
    <div class="col-lg-offset-1 col-lg-4 text-center">
        <h4><span class="label label-default">Ice analysis</span></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-offset-0 col-lg-12">
        <table class="table" style="margin-bottom: 10px;">
            <tr>
                <td width="200" class="text-right tr_print"><span class="text-muted">Attack vector</span></td>
                <td class="tr_print">Slingshot (Puzzle located elsewhere)</td>
            </tr>
            <tr id="print_${serviceId}" style="display: none;">
                <td class="text-right tr_print"><span class="text-muted">Information</span></td>
                <c:set var="hint" value="${(puzzle.hint != null && puzzle.hint.trim().length() > 0 ) ? puzzle.hint : '((Ask the GM for the puzzle))'}"></c:set>
                <td class="tr_print">${hint}</td>
            </tr>
            <tr id="print_solution_${serviceId}" style="display: none;">
                <td class="text-right tr_print"><span class="text-muted">Solution</span></td>
                <td class="tr_print">${puzzle.solution}</td>
            </tr>
        </table>
    </div>
</div>
<script>
    if (print_solution) {
        $("#print_solution_${serviceId}").css("display", "");
    }
    if (print_puzzles || analyzed.includes('${serviceId}')) {
        $("#print_${serviceId}").css("display", "");
    }
</script>
<%--<div class="row">--%>
    <%--<div class="col-lg-12">--%>
        <%--<span id="description_${status.index}"></span>--%>
        <%--<br>--%>
        <%--<br>--%>
    <%--</div>--%>
<%--</div>--%>
<%--<div class="row">--%>
    <%--<div class="col-lg-12">--%>
        <%--<span id="solution_${statuatts.index}"></span>--%>
        <%--<br>--%>
        <%--<br>--%>
    <%--</div>--%>
<%--</div>--%>
<%--<script>--%>
    <%--if (print_solution) {--%>
        <%--$("#description_${status.index}").text("Description: ${puzzle.description}");--%>
        <%--$("#solution_${status.index}").text("Passcode: ${puzzle.passcode}");--%>
    <%--}--%>
<%--</script>--%>
