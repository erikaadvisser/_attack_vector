<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<div class="container transition-color-slow" id="word_search_container" style="padding-top: 0px; background-color: #222222">
    <div>Sector 004, thread 0s34, worker g03</div>
    <div class="row">
        <div class="col-lg-8">
            <div class="text-left">
                <h4 class="text-success term"><strong>
                    Ice: <span class="text-info">${iceType.text} ${iceType.version}</span><br>
                    Attack vector: <span class="text-danger">Dictionary based password guessing</span><br>
                    Attack software: <span class="text-muted">Baseline</span><br></strong>
                </h4>
                <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 5px;"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div id="ice_term"></div>
        </div>
        <div class="col-lg-2 hidden_alpha transition-opacity-medium" id="needsBlock">
            <div class="well dark_well" style="height: 150px; border-color: #999;">
                <div class="text">Require accounts that allow<br>network traffic:</div>
                <div class="text">&nbsp;</div>
                <div class="text">Encrypted&nbsp;&nbsp; : <span id="indicator_ENCRYPTED">0</span>/${puzzle.needed['ENCRYPTED']} <span id="success_ENCRYPTED" class="text-success hidden_alpha"><strong>(ok)</strong></span></div>
                <div class="text">Lo latency&nbsp; : <span id="indicator_LATENCY">0</span>/${puzzle.needed['LATENCY']} <span id="success_LATENCY" class="text-success hidden_alpha"><strong>(ok)</strong></span></div>
                <div class="text">Hi volume&nbsp;&nbsp; : <span id="indicator_VOLUME">0</span>/${puzzle.needed['VOLUME']} <span id="success_VOLUME" class="text-success hidden_alpha"><strong>(ok)</strong></span></div>
                <div class="text">Hi priority : <span id="indicator_PRIORITY">0</span>/${puzzle.needed['PRIORITY']} <span id="success_PRIORITY" class="text-success hidden_alpha"><strong>(ok)</strong></span></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 15px;"/>
        </div>
    </div>

    <div id="passwordGuessUi" class="hidden_alpha transition-opacity-medium">
        <div class="row">
            <div class="col-lg-3">&nbsp;
                <div class="well well-lg password-guess" id="password_attempt_holder">&nbsp;</div>
            </div>
            <div class="col-lg-9">
                <h2 class="text-muted hidden_alpha  term" id="current_parent">
                    <span id="current"></span>
                    = <span id="currentTime">0</span>s
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="progress" style="margin-bottom: 0;background-color: #666;">
                    <div id="progress_bar" class="progress-bar progress-bar-striped active" role="progressbar" style="width: 0%; transition: width 0s;">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-9">
                <h3 class="text-muted term">
                    Next: <span id="next"></span> = <span id="nextTime">0</span>s
                </h3>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-11">
                        <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 15px;"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 scrollbar" style="height:320px;">
                        <h4 class="text-muted term">History:<span id="history"></span>
                        </h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-11">
                        <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 15px;"/>
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12 term">
                        <h3>
                            <span class="label label-danger hidden_alpha button_like" id="go_button" onclick="processPasswords();">GO</span>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text">
                        <h4>Password dictionaries</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 text-right">
                        <h3>
                            <div class="tight">
                                <span id="d_e_n" class="label label-info button_like term"><span class="badge password-badge">2</span> Ekanesh name</span>
                            </div>
                            <div class="tight">
                                <span id="d_a_n" class="label label-info button_like term"><span class="badge password-badge">2</span> Aquila name</span>
                            </div>
                            <div class="tight">
                                <span id="d_d_n" class="label label-info button_like term"><span class="badge password-badge">3</span> Dugo name</span>
                            </div>
                            <div class="tight">
                                <span id="d_s_n" class="label label-info button_like term"><span class="badge password-badge">4</span> Sona name</span>
                            </div>
                            <div class="tight">
                                <span id="d_p_n" class="label label-info button_like term"><span class="badge password-badge">3</span> Pendzal name</span>
                            </div>
                        </h3>
                    </div>
                    <div class="col-lg-4 text-center">
                        <h3>
                            <div class="tight">
                                <span id="d_o_c" class="label label-info button_like term"><span class="badge password-badge">5</span> Common words</span>
                            </div>
                            <div class="tight">
                                <span id="d_o_a" class="label label-info button_like term"><span class="badge password-badge">16</span> All words</span>
                            </div>
                            <div class="tight">&nbsp;
                            </div>
                            <div class="tight">
                                <span id="d_o_n" class="label label-info button_like term"><span class="badge password-badge">2</span> Number/Year</span>
                            </div>
                            <div class="tight">
                                <span id="d_o_h" class="label label-info button_like term"><span class="badge password-badge">3</span> 4 Human random</span>
                            </div>
                        </h3>
                    </div>
                    <div class="col-lg-4">
                        <h3>
                            <div class="tight">
                                <span id="d_e_c" class="label label-info button_like term"><span class="badge password-badge">2</span> Ekanesh culture</span>
                            </div>
                            <div class="tight">
                                <span id="d_a_c" class="label label-info button_like term"><span class="badge password-badge">3</span> Aquila culture</span>
                            </div>
                            <div class="tight">
                                <span id="d_d_c" class="label label-info button_like term"><span class="badge password-badge">3</span> Dugo culture</span>
                            </div>
                            <div class="tight">
                                <span id="d_s_c" class="label label-info button_like term"><span class="badge password-badge">2</span> Sona culture</span>
                            </div>
                            <div class="tight">
                                <span id="d_p_c" class="label label-info button_like term"><span class="badge password-badge">4</span> Pendzal culture</span>
                            </div>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 text-right text">
                        <h5><br>Prefix only:</h5>
                    </div>
                    <div class="col-lg-4 text-center">
                        <h3>
                            <span id="d_o_l" class="label label-info button_like term"><span class="badge password-badge">3</span> (l33t)</span>
                            <br><br><br>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var passwordsFound = {
        "ENCRYPTED": 0,
        "LATENCY": 0,
        "VOLUME": 0,
        "PRIORITY": 0
    };

    var passwordsNeeded = {
        "ENCRYPTED": ${puzzle.needed['ENCRYPTED']},
        "LATENCY": ${puzzle.needed['LATENCY']},
        "VOLUME": ${puzzle.needed['VOLUME']},
        "PRIORITY": ${puzzle.needed['PRIORITY']}
    };

    if (passwordsNeeded['LATENCY'] == 0) {
        $("#success_LATENCY").removeClass("hidden_alpha");
    }
    if (passwordsNeeded['VOLUME'] == 0) {
        $("#success_VOLUME").removeClass("hidden_alpha");
    }
    if (passwordsNeeded['PRIORITY'] == 0) {
        $("#success_PRIORITY").removeClass("hidden_alpha");
    }

    var passwordsAvailable = {
        <c:forEach var="key" items="${puzzle.available.keySet()}">
        "${key}": "${puzzle.available[key]}",</c:forEach>
    };
    var serviceId = ${serviceId};
</script>
<script src="../../../resources/js/ice/ice-password.js"></script>
<%--


    "Dugo name": dic_dugo_name,
    "Aquila name": dic_aquila_name,
    "Sona name": dic_sona_name,
    "Ekanesh name": dic_ekanesh_name,
    "Pendzal name": dic_pendzal_name,
    "Dugo culture": dic_dugo_culture,
    "Aquila culture": dic_aquila_culture,
    "Sona culture": dic_sona_culture,
    "Ekanesh culture": dic_ekanesh_culture,+---
    "Pendzal culture": dic_pendzal_culture,
    "Number/Year": dic_number_Year,
    "Common words": dic_common_words,
    "All words": dic_all_words,
    "4 Human random": dic_human_random,
    "4 True random": dic_true_random

Rationale for the wordsets:

A 4 random character password (26 characters) has 456,976 options.

This is taken (arbitrarily) to come down to a factor of 8 points.
Which means that each point is worth 57k options.

Estimate for the English language is 1000k words ~ 17.5 points -> round to 16 points
Estimate for English names: 650k ~ 11.3 points -> round to 12 points.


The dictionaries only use the 'common' ones, so the names for each culter are divided by 4.



And yes, we are cheating with the multiplication of multiple options, multiplying points does not really work that way.



Options:

People name combinations
 3s (name)
 6s (name) x (number)
 9s (name) x (name)
 9s (name) x (hrandom)
15s (name) x (common word)
 9s (l33t) x (name)
18s (l33t) x (name) x (number)

People culture combinations
 3s (culture)
 6s (culture) x (number)
 9s (culture) x (culture)
 9s (culture) x (hrandom)
15s (culture) x (common word)
 9s (l33t) x (culture)
18s (l33t) x (culture) x (number)

Combi name culter
 9s (people name) x (people culture)

-----------------------------------
147 s




Commons
 2s (number)
 4s (number) x (number)
 5s (common word)
10s (common word) x (number)
15s (l33t) x (common word)
15s (common word) x (hrandom)
16s (all words)
 3s (hrandom)
 9s (hrandom) x (hrandom)
------------------------------------
79s

Obscure
 8s (number) x (number) x (number)
16s (number) x (number) x (number) x (number)
20s (common word) x (number) x (number)
 6s (hrandom) x (number)
18s (hrandom) x (hrandom) x (number)
 9s (l33t) x (hrandom)
18s (l33t) x (hrandom) x (number)
------------------------------------
95s


CrossFaction
 9s (name1) x (name2)
 9s (name1) x (culture2)
 9s (name2) x (culture1)
 9s (culture1) x (culture2)



--%>