<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<div class="container transition-color-slow" id="word_search_container" style="padding-top: 0px; background-color: #222222">
    <div>Sector 004, thread 0s34, worker g03</div>
    <div class="row">
        <div class="col-lg-8">
            <div class="text-left">
                <h4 class="text-success term"><strong>
                    Ice: <span class="text-info">${iceType.text} ${iceType.version}</span><br>
                    Attack vector: <span class="text-danger">Unshielded cryptoprocessor power analysis</span><br>
                    Attack software: <span class="text-muted">Baseline</span><br></strong>
                </h4>
                <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 5px;"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div id="ice_term"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <hr style="border-top-color: #300; margin-top: 5px; margin-bottom: 15px;"/>
        </div>
    </div>
    <div id="wordsearch_main" class="transition-opacity-medium hidden_alpha">
        <div class="row">
            <div class="col-lg-11">
                <span id="magicEye">
                    <img src="/resources/images/ice/magiceye/blank/blank_1.png"/>
                </span>
            </div>
        </div>
    </div>

</div>
<script>
    var puzzleData = [
        <c:forEach items="${puzzle.images}" var="magiceye">
        {solution: "${magiceye.solution}", location: "${magiceye.location}" },</c:forEach>
    ];
    var serviceId = ${serviceId};

</script>
<script src="../../../resources/js/attack/magiceye.js"></script>
