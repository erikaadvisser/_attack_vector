<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>

<div class="row">
    <div class="col-lg-offset-1 col-lg-4 text-center">
        <h4><span class="label label-default">Ice analysis</span></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <table class="table" style="margin-bottom: 10px;">
            <tr>
                <td width="200" class="text-right tr_print"><span class="text-muted">Attack vector</span></td>
                <td class="tr_print">rp/i/p injection + remote core dump modification (Word search)</td>
            </tr>
            <tr>
                <td class="text-right tr_print"><span class="text-muted">Ice strength</span></td>
                <td class="tr_print">${puzzle.strength}</td>
            </tr>
        </table>
    </div>
</div>
<div id="print_${serviceId}" style="display: none;">
    <div class="row">
        <div class="col-lg-offset-1 col-lg-11">
            <table class="table table-bordered" border="1">
                <c:forEach items="${puzzle.rows}" var="row" varStatus="rowStatus"><tr>
                    <c:forEach items="${row}" var="letter" varStatus="letterStatus">
                        <s:eval expression="T(org.n_is_1._attack_vector.services.ice.wordsearch.WsUtil).convert(letter)" var="letterClass" />
                        <td class="text-center transition-color-fast" id="p${serviceId}_${rowStatus.index}_${letterStatus.index}"><span class="text-info bold ${letterClass}">&nbsp;</span></td></c:forEach>
                </tr></c:forEach>
            </table>
            <br>
            <span id="solution_${serviceId}"></span>
        </div>
    </div>
</div>


<script>
    var wordInput = [
        <c:forEach items="${puzzle.words}" var="word">
        {text: "${word.text}", location: [<c:forEach items="${word.locations}" var="location">{x:${location.x}, y:${location.y}},</c:forEach>]},</c:forEach>
    ];
    var serviceId = ${serviceId};

    if (print_puzzles || analyzed.includes('${serviceId}')) {
        $("#print_${serviceId}").css("display", "");
        console.log("wordinput: " + wordInput);
        var solutionText = "";
        wordInput.forEach(function(word) {
            if (print_solution) {
                word.location.forEach(function(location) {
                    var letter = $("#p${serviceId}_" + location.y + "_" + location.x);
                    letter.prop("style", "background-color: #d0b940; text-decoration: underline;")
                });
            }
            solutionText = solutionText + " " + word.text;
        });
        $("#solution_${serviceId}").text("Words: " + solutionText);

    }
</script>
