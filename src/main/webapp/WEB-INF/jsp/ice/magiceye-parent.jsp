<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Word Search</title>
    <link href="../../../resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="../../../resources/css/core.css" rel="stylesheet" media="screen"/>
    <script src="../../../resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="../../../resources/jslib/bootstrap.min.js"></script>
    <link href="../../../resources/css/local.css" rel="stylesheet"/>
    <link href="../../../resources/css/jquery.terminal.css" rel="stylesheet"/>
    <script src="../../../resources/jslib/jquery.terminal-min.js"></script>
    <script src="../../../resources/js/core/thread.js"></script>

</head>
<body style="padding-top: 0px; background-color: #444444">
<%@ include file="magiceye.jsp" %>
</body>
</html>