<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>_av Site editor</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <link href="/resources/css/core.css" rel="stylesheet" media="screen"  />
    <script src="/resources/jslib/jquery-1.11.3.min.js"></script>
    <script src="/resources/jslib/bootstrap.min.js"></script>
    <link href="/resources/css/local.css" rel="stylesheet"/>
    <script src="/resources/jslib/fabric.js"></script>
    <script src="/resources/jslib/notify.min.js"></script>
    <script src="/resources/jslib/magiceye.js"></script>
    <script src="/resources/jslib/TextDepthMapper.js"></script>

    <style>
        #magiceye {
            max-width: 100%;
            width: 1000px;
            height: 600px;
            margin: 0 auto;
            border-radius: 60px;
        }
    </style>
</head>
<body style="padding-top: 10px">

<div class="container">
    <div class="row">
        <div class="col-lg-11">
            <img id="magiceye" src/>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-9">
            <div class="form-group">
                <label for="text-input" class="col-lg-1 control-label text-muted text-right">Contents</label>
                <div class="col-lg-8">
                    <input type="text" id="text-input" rows="3" class="form-control" placeholder="Text for image"></input>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-lg-offset-3 col-sm-8">
                    <button type="submit" id="render" class="btn btn-info">Render</button>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
<script>
    (function() {

        $('#render').click( function (e) {
            e.preventDefault();
            var text = $('#text-input').val();
            renderMagicEye(text);
        });

        function renderMagicEye(text) {
            MagicEye.render({
                el: 'magiceye',
                colors: generatePalette(10),
                depthMapper: new MagicEye.TextDepthMapper(text, { textAlign: 'center' })
            });
        }

        function randomRGBa() {
//            var a = Math.floor(Math.random() * 2) *256;
//            return [a,a,a,255];
            var r = Math.floor(Math.random() * 256) + 0;
            var g = Math.floor(Math.random() * 256) + 0;
            var b = Math.floor(Math.random() * 256) + 0;

            return [r, g, b, 255];
        }

        function generatePalette(numColors) {
            var palette = [];
            for (var i = 0; i < numColors; i++) {
                palette.push(randomRGBa());
            }
            return palette;
        }

        renderMagicEye('enter-text');

    })();
</script>

</html>
