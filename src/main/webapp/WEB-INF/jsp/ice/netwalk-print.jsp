<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>


<div class="row">
    <div class="col-lg-offset-1 col-lg-4 text-center">
        <h4><span class="label label-default">Ice analysis</span></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-offset-0 col-lg-12">
        <table class="table" style="margin-bottom: 10px;">
            <tr>
                <td width="200" class="text-right tr_print"><span class="text-muted">Attack vector</span></td>
                <td class="tr_print">Network reconfiguration (Netwalk)</td>
            </tr>
            <tr>
                <td class="text-right tr_print"><span class="text-muted">Ice strength</span></td>
                <td class="tr_print">${strength}</td>
            </tr>
        </table>
    </div>
</div>
<div id="print_${serviceId}" style="display: none">
    <div class="row">
        <div class="col-lg-12" >
            <canvas id="netwalkCanvas_${serviceId}" width="${40 + x * 41}" height="${40 + y * 41}" style="border-radius: 3px 3px 3px 3px"></canvas>
        </div>
    </div>
    <script>
        <%--if (print_solution) {--%>
        <%--$("#solution_${serviceId}_${status.index}").html("Solution: <strong>${image.solution}</strong>");--%>
        <%--}--%>
    </script>
</div>

<script src="/resources/js/lib/seedrandom/seedrandom.js"></script>
<script src="/resources/js/ice/netwalk.js"></script>
<script>
    if (print_puzzles || analyzed.includes('${serviceId}')) {
        let puzzle = JSON.parse('${puzzleDef}');
        let serviceId = ${serviceId};

        $("#print_${serviceId}").css("display", "");
        let action = (print_solution) ? "print-solution" : "print";

        netwalk("netwalkCanvas_${serviceId}", puzzle, serviceId, action, 40);
    }
</script>
