<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<div class="row">
    <div class="col-lg-offset-1 col-lg-4 text-center">
        <h4><span class="label label-default">Ice analysis</span></h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-offset-0 col-lg-12">
        <table class="table" style="margin-bottom: 10px;">
            <tr id="print_solution_${serviceId}" style="display: none;">
                <td class="text-right tr_print"><span class="text-muted">Puzzle type</span></td>
                <td class="tr_print">Unhackable</td>
            </tr>
            <tr>
                <td width="200" class="text-right tr_print"><span class="text-muted">Attack vector</span></td>
                <td class="tr_print">Weak: passcode guessing</td>
            </tr>
            <tr>
                <td class="text-right tr_print"><span class="text-muted">Ice strength</span></td>
                <td class="tr_print">Impenetrable</td>
            </tr>
            <tr id="print_${serviceId}" style="display: none;">
                <td class="text-right tr_print"><span class="text-muted">Conclusion</span></td>
                <td class="tr_print">No vulnerabilities known, chance of hacking ice < 0.1%</td>
            </tr>
        </table>
    </div>
</div>

    <script>
    if (print_solution) {
        $("#print_solution_${serviceId}").css("display", "");
    }
    if (analyzed.includes('${serviceId}')) {
        $("#print_${serviceId}").css("display", "");

    }
</script>

