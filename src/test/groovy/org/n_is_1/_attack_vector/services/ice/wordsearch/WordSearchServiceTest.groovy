package org.n_is_1._attack_vector.services.ice.wordsearch

import org.junit.Before
import org.junit.Test
import org.n_is_1._attack_vector.services.ice.wordsearch.model.LetterMap
import org.n_is_1._attack_vector.services.ice.wordsearch.model.WordSearchConfig
import org.n_is_1._attack_vector.services.ice.wordsearch.model.WordSearchPuzzle
import org.n_is_1._attack_vector.services.util.RandomService

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class WordSearchServiceTest {

    WordSearchService service

    @Before
    void setup() {
        // fix random source
        def random = new RandomService()
        random.random = new Random(1)

        def dictionaryService = new DictionaryService(random: random)
        dictionaryService.afterPropertiesSet()

        service = new WordSearchService(dictionaryService: dictionaryService, random: random)
    }

    @Test(expected = RuntimeException)
    void createPuzzleWithAccidentalDoubleSolution() {
        def config = new WordSearchConfig()
        config.x = 10
        config.y = 10
        config.shortest = 3
        config.longest = 3
        config.word_count = 1
        config.use_words = ["DYN"]

// Expected map:
//        [D, Y, N, K, }, N, Y, D, E, B]
//        [E, H, A, D, X, Y, Y, K, _, _]
//        [_, _, i, n, j, e, c, t, {, @]
//        [a, u, t, h, o, r, i, z, e, (]
//        [9, 2, ), ,,  , @, c, _, o, v]
//        [e, r, r, i, d, e, (, ", h, e]
//        [a, d, e, r, ", ,,  , ", t, r]
//        [u, s, t, e, d, /, 1, ", ), ,]
//        [ , @, r, e, m, o, t, e, _, c]
//        [o, r, e, _, d, u, m, p, _, a]

        def puzzle = service.createPuzzle(config, false)
        service.countMatches("DYN", puzzle.map)

        int rows = puzzle.map.sizeY
        puzzle.initForRendering()

        for (int i = 0; i < rows; i += 1) {
            println "${puzzle.rows[i]}"
        }


    }

    @Test
    void testMatchCharsHorizontal() {
        def rows = new ArrayList<String>()

        rows.add("ABC")
        rows.add("---")
        rows.add("BA-")
        rows.add("---")

        def map = new LetterMap(rows)
        int matches = service.countMatches("AB", map)

        def puzzle = new WordSearchPuzzle(map: map)
        puzzle.initForRendering()
        for (int i = 0; i < rows.size(); i += 1) {
            println "${puzzle.rows[i]}"
        }

        assertThat(matches, equalTo(2))
    }

    @Test
    void testMatchCharsVertical() {
        def rows = new ArrayList<String>()

        rows.add("A--")
        rows.add("B--")
        rows.add("--B")
        rows.add("--A")

        def map = new LetterMap(rows)
        int matches = service.countMatches("AB", map)

        def puzzle = new WordSearchPuzzle(map: map)
        puzzle.initForRendering()
        for (int i = 0; i < rows.size(); i += 1) {
            println "${puzzle.rows[i]}"
        }

        assertThat(matches, equalTo(2))

    }

    @Test
    void testMatchCharsBoth() {
        def rows = new ArrayList<String>()

        rows.add("A--")
        rows.add("BA-")
        rows.add("-B-")

        def map = new LetterMap(rows)
        int matches = service.countMatches("AB", map)

        def puzzle = new WordSearchPuzzle(map: map)
        puzzle.initForRendering()
        for (int i = 0; i < rows.size(); i += 1) {
            println "${puzzle.rows[i]}"
        }

        assertThat(matches, equalTo(3))
    }


    @Test
    void testMatchOne() {
        def rows = new ArrayList<String>()

        rows.add("AB-")
        rows.add("---")
        rows.add("A-B")

        def map = new LetterMap(rows)
        int matches = service.countMatches("AB", map)

        def puzzle = new WordSearchPuzzle(map: map)
        puzzle.initForRendering()
        for (int i = 0; i < rows.size(); i += 1) {
            println "${puzzle.rows[i]}"
        }

        assertThat(matches, equalTo(1))
    }

}