
           _|_|      _|      _|                          _|            _|      _|                        _|
         _|    _|  _|_|_|_|_|_|_|_|    _|_|_|    _|_|_|  _|  _|        _|      _|    _|_|      _|_|_|  _|_|_|_|    _|_|    _|  _|_|
         _|_|_|_|    _|      _|      _|    _|  _|        _|_|          _|      _|  _|_|_|_|  _|          _|      _|    _|  _|_|
         _|    _|    _|      _|      _|    _|  _|        _|  _|          _|  _|    _|        _|          _|      _|    _|  _|
         _|    _|      _|_|    _|_|    _|_|_|    _|_|_|  _|    _|          _|        _|_|_|    _|_|_|      _|_|    _|_|    _|
 _|_|_|

A hacking simulation intended for use in Live Action RolePlaying games.


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Some description about this game is provided here:

http://adventureforum.nl/ld50/attack_vector.html


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Instructions for installing for windows are located in the file "install-windows" in this directory.



- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Once it is running, you can log in with the default GM account or Admin account

username: gm
password: 22gg88

username: admin
password: 77aa44


The GM is used to create new sites and new users.
Admins can do the same, but are intended to do out-of-game maintenance as well,
such as exporting and importing data.




- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Getting started in Intellij
- import project
- add the src/main/groovy folder to sources (via project properties)
- add typescript compilation


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Running:

The application is a Spring boot standalone java app, the main method class is: org.n_is_1._attack_vector.Application

Add environment variables to the application

MONGODB_URI     The connect URL for Mongo DB. For example: mongodb://av2:*******@192.168.99.100/admin?authMechanism=SCRAM-SHA-1
ENVIRONMENT     The name of the server environment. For example: dev-ervi or prod-eosfrontier.space-new
MONGODB_NAME    The name of the database. Optional, if not set, then the database from the MONGODB_URI is used. In this example: av2

(or: MONGODB_URI=mongodb://av2:av2@localhost/admin?authMechanism=SCRAM-SHA-1;ENVIRONMENT=dev-ervi)


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Deploy to Herouku:

> optional, if you want to do heroku stuff (does not work in windows git-bash)
heroku login

> upload, build & deploy:
git push heroku master


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Configuring the Discord channel:

! This step is optional, you don't need Discord integration to run Attack Vector. Discord integration allows AV to report the results of hacks
to a channel on Discord via a bot, or to allow chatting with a GM via a hacked site (work in progress).


First you need to create a Discord Bot. See the document /docs/Discord-integration.txt for some notes on this topic.

Once the bot is set up, and has rights to connect to your discord server, you need to configure Attack-Vector for it.

Log in on as Admin, and click the link to the Discord configuration. There you need to input a bot configuration string.
This string contains the bot token, which should be kept secret!

An example of a bot configuration String is:

222222222222222222;xxxxxxxxxxxxxxxxxxxxxxxx.yyyyyy.zzzzzzzzzzzzzzzzzzzzzzzzzzz;Example bot config;111111111111111111;GM-username
<ChannelId>;<bot-token>;<description>;<user-id>;<user-name>

This should then show up in the UI and it can be activated, and a test message can be sent to see if it works.

