Discord dev page:

https://discordapp.com/developers/docs/intro


JDA library docs:

https://github.com/DV8FromTheWorld/JDA


Bot specific stuff:
https://discordapp.com/developers/docs/topics/oauth2#bots







Add bot to server:
--

An admin of the server needs to go to:

https://discordapp.com/oauth2/authorize?&client_id=411868151190388737&scope=bot&permissions=3072

and then approve the bot and confirm the permissions.

This will create invite the Attack_Vector bot to the server, and create an Attack_Vector role that is specific to this bot.
The role can then be managed to allow access to specific channels.



Permissions: 3072 = 1024+2048
VIEW_CHANNEL	0x00000400 (=1024)
SEND_MESSAGES	0x00000800 (=2048)

See: https://discordapp.com/developers/docs/topics/permissions#permissions-bitwise-permission-flags
