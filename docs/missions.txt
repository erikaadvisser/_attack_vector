
When a hacker starts a hacking run, the system checks if the site is part of a mission.
If so, then the hacker becomes part of the mission.


When a hacker enters a node with a 'tracer' service, then the mission records that that hacker is traced by this trace site.
When a hacker deletes a trace log service in a trace site, then the mission records that it is deleted,


> Tracer service
> Tracelog service



Generation a mission from templates:

1. Pick a template
 a. select all templates that match the criteria (difficulty) that have the least amount of uses
 b. pick a random template from these

2. Create remote trace sites
 a. check how many remote trace services exist in the template
 b. pick a unique trace template for each one. Do this based on how many times a template has been used before
  1. copy the template into a new site.
   a. The site id will be the target site id, prefixed with trace-xxxxxx where xxxxx is random.
   b. The site missionId field is set with the mission ID

3. Clone mission template into a site
 a. copy template site into a new site with the mission site id
 b. Update the content of the service with text "MISSION"
  1. replace data["text"] with target text
  2. add data["mission"] with value "true"
 c. for each remote trace service
  1. fill in the site id of a trace site.

4. Create mission records
 a. with all data from steps 1-3.

5. Update uses
 a. increase the uses count for main template
 b. increase the uses count for all trace sites


6. Start mission timer
 a. Somehow arrange that the end of the mission will trigger the end-mission handling



>

Start run

IF (mission)
 IF (mission end time has passed)
  - Deny mission
 ELSE:

 - set run.mission field
 - adjust run max time to mission end time

 IF (main target site)
  - add hacker to mission hacker lists for main target

 IF (tracer site)
  - add hacker to mission hacker lists for main target



Move

IF (node contains remote trace service)
  IF (run.mission != null)
   - Add hacker to mission.traces
   - Inform hacker that (s)he is traced (new trace log added) and show trace site id
  ELSE
   - inform hacker that tracing failed due to incorrect setup

Hack (remote tracer)
IF (run.mission)
 - Display site id of trace site




Hack (trace logs)

IF (run.mission)
 - Display the trace logs that were removed (mission.traces[site id])
 - Empty the trace log list for this site
ELSE
 - Inform hacker that tracing is not correctly configured and logs are empty


Hack (data vault)

IF (service['mission'] = "true")
 - Display 'info' text
 - Add hacker to hackersWithMissionInfo
 - set goalReached = true

